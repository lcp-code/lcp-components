<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_button_green',
  'type' => 'builder',
  'group' => 'Кнопки',
  'inner' => array(
    'description' => 'Зеленая кнопка, основанная на компоненте lcp_button_default',
    'css' => array(
      '0' => '/components/builder/buttons/lcp-button-green/lcp-button-green.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'href',
    '3' => 'target',
    '4' => 'onclick',
    '5' => 'onmouseover',
    '6' => 'onmouseout'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => 'Новая кнопка',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор',
    '1' => 'Название кнопки',
    '2' => 'Ссылка для кнопки',
    '3' => '<div>Открытие ссылки:</div><div>_self - в текущем окне (по умолч.),</div><div>_blank - в новом окне,</div><div>_parent - во фрейм-родитель,</div><div>_top - в новом окне, вместо фреймов</div>',
    '4' => 'Событие, возникающее при нажатии левой кнопкой мыши',
    '5' => 'Событие, возникающее при наведении указателя мыши на объект',
    '6' => 'Событие, возникающее, когда указатель мыши уходит с объекта'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
$css_install_arr[]='/components/builder/buttons/lcp-button-green/lcp-button-green.css';
// $js_install_arr[]='/components/builder/buttons/lcp-button-green/lcp-button-green.js';
?>



<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_button_green extends lcp_button_default implements lcp_components_build{
    function __build_component(){
      $html='';
      $this->class='lcp_button_green '.$this->class;
      $attr_str=$this->__get_attributes_html();
      $html.='<a '.$attr_str.'>'.$this->value.'</a>';
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_button_green_init($param=array()){
    $lcp_obj=new lcp_button_green($param);
    return $lcp_obj->html();
  }
}
?>
