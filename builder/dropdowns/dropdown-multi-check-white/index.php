<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'dropdown_multi_check_white',
  'type' => 'builder',
  'group' => 'Выпадающий список',
  'inner' => array(
    'description' => '
    <p>Белый выпадающий список. Создан на основе тега div (в браузерах и мобильных устройствах отображается одинаково). Основан на компоненте "lcp_dropdown_multi_check".</p>
    <p>Для получения значения через js, используйте функцию: get_dropdown_multi_check_opt_value("id_dropdown");</p>
    <p>Чтобы установить значение через js, используйте функцию: set_dropdown_multi_check_opt_value("id_dropdown","values_arr");</p>
    ',
    'css' => array(
      '0' => '/components/builder/dropdowns/dropdown-multi-check-white/dropdown-multi-check-white.css'
    ),
    'js' => array(
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'name',
    '2' => 'value',
    '3' => 'text',
    '4' => 'class',
    '5' => 'onclick',
    '6' => 'onmousedown',
    '7' => 'onmouseup',
    '8' => 'height_child',
    '9' => 'width',
    '10' => 'html_img_down_arrow',
    '11' => 'id_child_arr',
    '12' => 'title_child_arr',
    '13' => 'name_child_arr',
    '14' => 'value_child_arr',
    '15' => 'class_child_arr',
    '16' => 'onclick_child_arr',
    '17' => 'alt_title',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '<i class="fa fa-sort-desc" aria-hidden="true"></i>',
    '11' => '',
    '12' => '',
    '13' => '',
    '14' => '',
    '15' => '',
    '16' => '',
    '17' => '-- Не выбрано --',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Имя выпадающего списка (тег name)',
    '2' => 'Благодаря свойству value, отображается (и выбирается) необходимое значение списка',
    '3' => 'Текст, отображаемый по-умолчанию при загрузке компонента (если ни один пункт не выбран)',
    '4' => 'Класс для изменения стилей (тег class)',
    '5' => 'Событие, возникающее при клике левой кнопкой мыши (тег onclick)',
    '6' => 'Событие, возникающее при нажатии левой кнопкой мыши (тег onmousedown)',
    '7' => 'Событие, возникающее при отпускании левой кнопкой мыши (тег onmouseup)',
    '8' => 'Максимальная высота выпадающего списка',
    '9' => 'Ширина компонента',
    '10' => 'Изображение, html-код или тег из fontawesome для отображения стрелки вниз выпадающего списка (крайняя правая кнопка)',
    '11' => 'Массив идентификаторов дочерних (выпадающих) пунктов списка (тег id)',
    '12' => 'Массив отображаемого текста для дочерних (выпадающих) пунктов списка',
    '13' => 'Массив имен для дочерних (выпадающих) пунктов списка (тег name)',
    '14' => 'Массив значений (предназначенных для выбора), указанных для дочерних (выпадающих) пунктов списка (тег value)',
    '15' => 'Массив классов для изменения стилей, указанных для дочерних (выпадающих) пунктов списка (тег class)',
    '16' => 'Массив событий, возникающих при клике левой кнопкой мыши, для дочерних (выпадающих) пунктов списка (тег onclick)',
    '17' => 'Если значение не указано или ничего не выбрано - отобразить альтернативный текст',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
  ),
  'set_value_js' => array(
  ),
  'set_description_js' => array(
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}
$css_install_arr[]='/components/builder/dropdowns/dropdown-multi-check-white/dropdown-multi-check-white.css';
// $js_install_arr[]='/components/builder/dropdowns/dropdown-multi-check-white/dropdown-multi-check-white.js';
?>



<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class dropdown_multi_check_white extends lcp_dropdown_multi_check implements lcp_components_build{
    function __init_component(){
      $this->class='dropdown_multi_check_white '.$this->class;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function dropdown_multi_check_white_init($param=array()){
    $lcp_obj=new dropdown_multi_check_white($param);
    return $lcp_obj->html();
  }
}
?>
