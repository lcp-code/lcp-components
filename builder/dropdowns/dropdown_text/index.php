<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'dropdown_text',
  'type' => 'builder',
  'group' => 'Выпадающий список',
  'inner' => array(
    'description' => 'Выпадающий список с возможностью ввода и изменения текста. Основано на двух компонентах: lcp_text_default и dropdown_white',
    'css' => array(
      '0' => '/components/builder/dropdowns/dropdown_text/dropdown_text.css'
    ),
    'js' => array(
      '0' => '/components/builder/dropdowns/dropdown_text/dropdown_text.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'text_id',
    '2' => 'name',
    '3' => 'value',
    '4' => 'text',
    '5' => 'class',
    '6' => 'onclick',
    '7' => 'text_onchange',
    '8' => 'onmousedown',
    '9' => 'onmouseup',
    '10' => 'width',
    '11' => 'html_img_down_arrow',
    '12' => 'id_child_arr',
    '13' => 'title_child_arr',
    '14' => 'name_child_arr',
    '15' => 'value_child_arr',
    '16' => 'class_child_arr',
    '17' => 'onclick_child_arr'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '',
    '11' => '<i class="fa fa-sort-desc" aria-hidden="true"></i>',
    '12' => '',
    '13' => '',
    '14' => '',
    '15' => '',
    '16' => '',
    '17' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id) выпадающего списка',
    '1' => 'Идентификатор (тег id) текстового поля',
    '2' => 'Имя выпадающего списка (тег name)',
    '3' => 'Текст внутри текстового поля (ничем не отличается от text, но имеет приоритет)',
    '4' => 'Текст внутри текстового поля',
    '5' => 'Класс для изменения стилей выпадающего списка (тег class)',
    '6' => 'Событие, возникающее при клике левой кнопкой мыши на выпадающем списке (тег onclick)',
    '7' => 'Событие, возникающее при изменении текста в текстовом поле (тег onchange)',
    '8' => 'Событие, возникающее при нажатии левой кнопкой мыши на выпадающем списке (тег onmousedown)',
    '9' => 'Событие, возникающее при отпускании левой кнопкой мыши на выпадающем списке (тег onmouseup)',
    '10' => 'Ширина выпадающего списка',
    '11' => 'Изображение, html-код или тег из fontawesome для отображения стрелки вниз выпадающего списка (крайняя правая кнопка)',
    '12' => 'Массив идентификаторов дочерних (выпадающих) пунктов списка (тег id)',
    '13' => 'Массив отображаемого текста для дочерних (выпадающих) пунктов списка',
    '14' => 'Массив имен для дочерних (выпадающих) пунктов списка (тег name)',
    '15' => 'Массив значений (предназначенных для выбора), указанных для дочерних (выпадающих) пунктов списка (тег value)',
    '16' => 'Массив классов для изменения стилей, указанных для дочерних (выпадающих) пунктов списка (тег class)',
    '17' => 'Массив событий, возникающих при клике левой кнопкой мыши, для дочерних (выпадающих) пунктов списка (тег onclick)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function dropdown_text($param=''){
  $text_param=$param;
  $text_param['width'] = '100%';
  $text_param['id'] = $param['text_id'];
  $text_param['onchange'] = $param['text_onchange'];
  $text_param['onkeydown'] = ' dropdown_keyboard(event,$(this).parent(\'.dropdown_title2\').parent(\'.dropdown_block2\').parent(\'.dropdown_blocks\').parent(\'.dropdown_default2\').children(\'.dropdown_default2_list\')); ';
  $text_param['value'] = (!empty($param['value']))?$param['value']:$param['text'];
  $text_param['name'] = '';
  $text_param['class'] = '';
  $text_param['onclick'] = '';
  $text_param['onmousedown'] = '';
  $text_param['onmouseup'] = '';
  $dropdown_param=$param;
  $dropdown_param['text'] = components('lcp_text_default',$text_param);
  $dropdown_param['value'] = '';
  $dropdown_param['class'] .= ' text_dropdown ';
  $html = components('dropdown_white',$dropdown_param);
  $html = str_replace('onclick="dropdown_default2_list_start(this);"','',$html);
  $html = str_replace('class="dropdown_down_img"','onclick="text_dropdown_start(this);" class="dropdown_down_img"',$html);
  if (!empty($param['value'])){
    $html = str_replace('data-value="" class="dropdown_title2"','data-value="'.$param['value'].'" class="dropdown_title2"',$html);
  }
  // $html = str_replace('class="dropdown_down_img"','onclick="text_dropdown_start_sel(this);" class="dropdown_down_img"',$html); // пока не понятно зачем нужна
  $html = str_replace('onclick="dropdown_default2_opt_click(this);','onclick="text_dropdown_opt_click(this);',$html);
  return $html;
}
?>
