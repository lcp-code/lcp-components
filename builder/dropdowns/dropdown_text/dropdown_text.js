// function text_dropdown(params=[]){
// получение выпадающего списка через js
//   var text_params=[];
//   var dropdown_params=[];
//   for (var key in params) {
//     text_params[key] = params[key];
//   }
//   for (var key in params) {
//     dropdown_params[key] = params[key];
//   }
//   text_params['width'] = '100%';
//   text_params['id'] = params['text_id'];
//   text_params['onchange'] = params['text_onchange'];
//   text_params['onkeydown'] = ' dropdown_keyboard(event,$(this).parent(\'.dropdown_title2\').parent(\'.dropdown_block2\').parent(\'.dropdown_blocks\').parent(\'.dropdown_default2\').children(\'.dropdown_default2_list\')); ';
//   dropdown_params['text'] = lcp_text_default(text_params);
//   dropdown_params['value'] = '';
//   dropdown_params['class'] += ' text_dropdown ';
//   var html='';
//   html = dropdown_start(dropdown_params);
//   html = html.replace(/onclick="dropdown_start_sel\(this\);"/g,'');
//   if (typeof params['value']!='undefined'){
//     html = html.replace(/data-value="" class="dropdown_title2"/g,'data-value="'+params['value']+'" class="dropdown_title2"');
//   }
//   html = html.replace(/class="dropdown_down_img"/g,'onclick="text_dropdown_start_sel(this);" class="dropdown_down_img"');
//   html = html.replace(/onclick="dropdown_opt_click\(this\);/g,'onclick="text_dropdown_opt_click(this);');
//   return html;
// }
//
// function set_text_dropdown_child_arr(param){
// функция для отображения нового списка (обычно не используется)
//   var id_dropdown=param['id'];
//   var html=get_dropdown_child_arr(param);
//   html = html.replace(/onclick="dropdown_opt_click\(this\);/g,'onclick="text_dropdown_opt_click(this);');
//   $('#'+id_dropdown).children('.dropdown_default2_list').html(html);
// }
//
// function show_set_text_dropdown_child_arr(param){
// функция для отображения только списка
//   var id_dropdown=param['id'];
//   var text=param['text'];
//   set_text_dropdown_child_arr(param);
//   if (text.length == 0){
//     $('#'+id_dropdown).children('.dropdown_default2_list').addClass('dropdown_default2_list_hide');
//   } else {
//     $('#'+id_dropdown).children('.dropdown_default2_list').removeClass('dropdown_default2_list_hide');
//   }
// }

function text_dropdown_start(obj){
  // Функция для открытия/скрытия списка. Вызывается при нажатии на компонент (обычно не используется)
  var obj_html=$(obj).parent('.dropdown_block2').parent('.dropdown_blocks').parent('.dropdown_default2').children('.dropdown_default2_list');
  if ($(obj_html).hasClass('dropdown_default2_list_show')===true){
    blur_remove=1;
    $(obj_html).removeClass('dropdown_default2_list_show');
    $(obj_html).addClass('dropdown_default2_list_hide');
  } else {
    $(obj_html).removeClass('dropdown_default2_list_hide');
    $(obj_html).addClass('dropdown_default2_list_show');
    $(obj_html).focus();
  }
}

function text_dropdown_opt_click(obj){
  // функция при выборе пункта из списка (обычно не используется)
  var obj_title=$(obj).html();
  var obj_val=$(obj).attr('data-value');
  var obj_list=$(obj).parent('.dropdown_default2_list_ul').parent('.dropdown_default2_list');
  $(obj).parent('.dropdown_default2_list_ul').parent('.dropdown_default2_list').parent('.dropdown_default2').children('.dropdown_blocks').children('.dropdown_block2').children('.dropdown_title2').children('.text_default').val(obj_title);
  $(obj).parent('.dropdown_default2_list_ul').parent('.dropdown_default2_list').parent('.dropdown_default2').children('.dropdown_blocks').children('.dropdown_block2').children('.dropdown_title2').attr('data-value',obj_val);
  dropdown_default2_list_hide(obj_list);
  $(obj).parent('.dropdown_default2_list_ul').parent('.dropdown_default2_list').parent('.dropdown_default2').children('.dropdown_blocks').children('.dropdown_block2').children('.dropdown_title2').children('.text_default').focus();
}
