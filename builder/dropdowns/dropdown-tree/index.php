<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'dropdown_tree',
  'type' => 'builder',
  'group' => 'Выпадающий список',
  'inner' => array(
    'description' => 'Белый выпадающий список с деревом категорий. Создан на основе тега div (в браузерах и мобильных устройствах отображается одинаково). Основан на компоненте "dropdown_white". Все свойства и события можно найти там.',
    'css' => array(
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'sql',
    '1' => 'id_parent_start',
    '2' => 'html_left',
    '3' => 'field_html_left',
    '4' => 'except_arr'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '&nbsp;&nbsp;&nbsp;',
    '3' => '',
    '4' => ''
  ),
  'set_description_php' => array(
    '0' => 'SQL запрос для отображения первой родительской категории. Для изменения родительского id (чтобы отображились дочерние пункты), на месте значения укажите [id_parent]. Например: SELECT title FROM table WHERE id_parent="[id_parent]"',
    '1' => 'Начальное значение [id_parent]',
    '2' => 'Добавить html для сдвига слева',
    '3' => 'Указать поле, для которого стоит добавлять сдвиг влево и отображение текста в выпадающем списке (обязательное поле)',
    '4' => 'Массив id для исключения из отображения'
  ),
  'get_php' => array(
    'text'
  ),
  'get_description_php' => array(
    'Функция возвращает html текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>


<?php
function dropdown_tree($param=''){
  global $fin_arr,$dropdown_tree_k;
  $fin_arr=array();
  $dropdown_tree_k=0;
  if (function_exists('dropdown_cikl')===false){
  function dropdown_cikl($params){
    global $fin_arr,$dropdown_tree_k;
    $id_parent_start=$params['id_parent_start'];
    $level=$params['level'];
    $sql_arr=$params['sql_arr'];
    $sql=$params['sql'];
    $sql_field=$params['sql_field'];
    $html_left=$params['html_left'];
    $field_html_left=$params['field_html_left'];
    $except_arr=$params['except_arr'];
    $level++;

    for ($i=0; $i < $level; $i++) {
			$str_indent .= $html_left;
		}

    for ($i=0; $i < count($sql_arr[$sql_field[0]]); $i++) {
      $id_parent_start=$sql_arr[$sql_field[0]][$i];
      $sql_new=$sql;
      $sql_new=str_replace('[id_parent]',$id_parent_start,$sql_new);
      if (array_search($id_parent_start,$except_arr)===false){
      $sql_arr2=sqldb($sql_new);
      for ($i2=0; $i2 < count($sql_arr2[$sql_field[0]]); $i2++) {
        for ($i3=0; $i3 < count($sql_field); $i3++) {
          $str_indent_s='';
          if ($sql_field[$i3]==$field_html_left){
            $str_indent_s=$str_indent;
          }
          if (!(($sql_field[$i3]=='id')&&(array_search($sql_arr2[$sql_field[$i3]][$i2],$except_arr)!==false))){
            $fin_arr[$sql_field[$i3]][$dropdown_tree_k]=$str_indent_s.$sql_arr2[$sql_field[$i3]][$i2];
          } else {
            break;
          }
        }
        $dropdown_tree_k++;
      }
      dropdown_cikl(array(
        'sql'=>$sql,
        'id_parent_start'=>$id_parent_start,
        'level'=>$level,
        'sql_arr'=>$sql_arr2,
        'sql_field'=>$sql_field,
        'html_left'=>$html_left,
        'field_html_left'=>$field_html_left,
        'except_arr'=>$except_arr
      ));
    }
    }
  }
}



  $sql=$param['sql'];
  $id_parent_start=$param['id_parent_start'];
  $html_left=$param['html_left'];
  $field_html_left=$param['field_html_left'];
  $except_arr=$param['except_arr'];
  $default_sql='';
  $default_id_parent_start='';
  $default_html_left='&nbsp;&nbsp;&nbsp;';
  $default_field_html_left='';
  $default_except_arr='';
  $sql=(!empty($sql))?$sql:$default_sql;
  $id_parent_start=(!empty($id_parent_start))?$id_parent_start:$default_id_parent_start;
  $html_left=(!empty($html_left))?$html_left:$default_html_left;
  $field_html_left=(!empty($field_html_left))?$field_html_left:$default_field_html_left;
  $except_arr=(!empty($except_arr))?$except_arr:$default_except_arr;

  if (array_search($id_parent_start,$except_arr)===false){
  $sql_new=$sql;
  $sql_new=str_replace('[id_parent]',$id_parent_start,$sql_new);
  $sql_arr=sqldb($sql_new);
  if (count($sql_arr)>0){
    $sql_field=array_keys($sql_arr)or die (exit(var_dump($sql_arr)));
    for ($i=0; $i < count($sql_arr[$sql_field[0]]); $i++) {
      for ($i2=0; $i2 < count($sql_field); $i2++) {
        if (!(($sql_field[$i2]=='id')&&(array_search($sql_arr[$sql_field[$i2]][$i],$except_arr)!==false))){
          $fin_arr[$sql_field[$i2]][$dropdown_tree_k]=$sql_arr[$sql_field[$i2]][$i];
        } else {
          break;
        }
      }
      $dropdown_tree_k++;
    }
  }
  if (count($fin_arr)>0){
    if ((count(sqldb(str_replace('childr=\'[id_parent]\' AND','childr<>\''.$id_parent_start.'\' AND',$sql))))>0){
      dropdown_cikl(array(
        'sql'=>$sql,
        'id_parent_start'=>$id_parent_start,
        'level'=>'0',
        'sql_arr'=>$sql_arr,
        'sql_field'=>$sql_field,
        'html_left'=>$html_left,
        'field_html_left'=>$field_html_left,
        'except_arr'=>$except_arr
      ));
    }
  }
}

  for ($i=0; $i < count($sql_field); $i++) {
    if ($sql_field[$i]!=$field_html_left){
      $sql_field_id=$sql_field[$i];
      break;
    }
  }

if (count($fin_arr[$field_html_left])>0){
  $param['title_child_arr']=array_merge($param['title_child_arr'],$fin_arr[$field_html_left]);
}
if (count($fin_arr[$sql_field_id])>0){
  $param['value_child_arr']=array_merge($param['value_child_arr'],$fin_arr[$sql_field_id]);
}
  // $param['value_child_arr']=$fin_arr[$sql_field_id];
  $html = components('dropdown_white',$param);
  return $html;
}
?>
