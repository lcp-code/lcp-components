<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'dropdown_white',
  'type' => 'builder',
  'group' => 'Выпадающий список',
  'inner' => array(
    'description' => '
    <p>Белый выпадающий список. Создан на основе тега div (в браузерах и мобильных устройствах отображается одинаково). Основан на компоненте "lcp_dropdown_default2".</p>
    <p>Для получения значения через js, используйте функцию: get_dropdown_default2_opt_value("id_dropdown");</p>
    <p>Чтобы установить значение через js, используйте функцию: set_dropdown_opt_value("id_dropdown","values");</p>
    ',
    'css' => array(
      '0' => '/components/builder/dropdowns/dropdown-white/dropdown-white.css'
    ),
    'js' => array(
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'name',
    '2' => 'value',
    '3' => 'text',
    '4' => 'class',
    '5' => 'onclick',
    '6' => 'onmousedown',
    '7' => 'onmouseup',
    '8' => 'height_child',
    '9' => 'width',
    '10' => 'html_img_down_arrow',
    '11' => 'id_child_arr',
    '12' => 'title_child_arr',
    '13' => 'name_child_arr',
    '14' => 'value_child_arr',
    '15' => 'class_child_arr',
    '16' => 'onclick_child_arr'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '<i class="fa fa-sort-desc" aria-hidden="true"></i>',
    '11' => '',
    '12' => '',
    '13' => '',
    '14' => '',
    '15' => '',
    '16' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Имя выпадающего списка (тег name)',
    '2' => 'Благодаря свойству value, отображается (и выбирается) необходимое значение списка',
    '3' => 'Текст, отображаемый по-умолчанию при загрузке компонента (если ни один пункт не выбран)',
    '4' => 'Класс для изменения стилей (тег class)',
    '5' => 'Событие, возникающее при клике левой кнопкой мыши (тег onclick)',
    '6' => 'Событие, возникающее при нажатии левой кнопкой мыши (тег onmousedown)',
    '7' => 'Событие, возникающее при отпускании левой кнопкой мыши (тег onmouseup)',
    '8' => 'Максимальная высота выпадающего списка',
    '9' => 'Ширина компонента',
    '10' => 'Изображение, html-код или тег из fontawesome для отображения стрелки вниз выпадающего списка (крайняя правая кнопка)',
    '11' => 'Массив идентификаторов дочерних (выпадающих) пунктов списка (тег id)',
    '12' => 'Массив отображаемого текста для дочерних (выпадающих) пунктов списка',
    '13' => 'Массив имен для дочерних (выпадающих) пунктов списка (тег name)',
    '14' => 'Массив значений (предназначенных для выбора), указанных для дочерних (выпадающих) пунктов списка (тег value)',
    '15' => 'Массив классов для изменения стилей, указанных для дочерних (выпадающих) пунктов списка (тег class)',
    '16' => 'Массив событий, возникающих при клике левой кнопкой мыши, для дочерних (выпадающих) пунктов списка (тег onclick)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
    '0'=>'set_dropdown_opt_value',
  ),
  'set_value_js' => array(
    '0'=>'id_dropdown,values'
  ),
  'set_description_js' => array(
    '0'=>'Установить значение для выпадающего списка через js. Например: set_dropdown_opt_value(id_dropdown,"val2"), где id_dropdown - id выпадающего списка, val2 - значение выпадающего списка заданное в value_child_arr',
  ),
  'get_js' => array(
    '0'=>'get_dropdown_default2_opt_value',
  ),
  'get_description_js' => array(
    '0' => 'Получить значение выпадающего списка. Например: get_dropdown_default2_opt_value(id_dropdown), где id_dropdown - id выпадающего списка',
  )
);
}
$css_install_arr[]='/components/builder/dropdowns/dropdown-white/dropdown-white.css';
// $js_install_arr[]='/components/builder/dropdowns/dropdown-white/dropdown-white.js';
?>



<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class dropdown_white extends lcp_dropdown_default2 implements lcp_components_build{
    function __build_component(){
      $html='';
      $this->class='dropdown_white dropdown_default2 '.$this->class;
      $this->style='width:'.$this->width.';'.$this->style;
      $attr_str=$this->__get_attributes_html();

      if ((!empty($this->value))||($this->value=='0')){
        if (!empty($this->value_child_arr)){
          for ($i=0; $i < count($this->value_child_arr); $i++) {
            if ($this->value===$this->value_child_arr[$i]){
              $titl=$this->title_child_arr[$i];
              break;
            }
          }
        }
      }
      if ((empty($titl))&&(empty($this->text))){
        $titl=$this->title_child_arr[0];
      } else {
        $titl=(!empty($titl))?$titl:$this->text;
      }
      if (!empty($this->height_child)){
        $height_child_html.='';
        $height_child_html.='overflow-y: auto;';
        $height_child_html.='max-height: '.$this->height_child.';';
        $height_child_style=' style="'.$height_child_html.'" ';
      }

      $html='';
      $html.='<div '.$attr_str.'>';
      $html.='<div class="dropdown_blocks" onclick="dropdown_default2_list_start(this);">';
      $html.='<div class="dropdown_block2">';
      $html.='<div class="dropdown_title2" data-value="'.$this->value.'">';
      $html.=$titl;
      $html.='</div>';
      $html.='<div class="dropdown_down_img">';

      $html.=$this->html_img_down_arrow;
      $html.='</div>';
      $html.='</div>';
      $html.='</div>';


      $html.='<div tabindex="'.mt_rand().'" class="dropdown_default2_list dropdown_default2_list_hide" onblur="dropdown_default2_blur(this);" onkeydown="dropdown_keyboard(event,this);">';
      $html.='<div class="dropdown_default2_list_ul" '.$height_child_style.'>';
      for ($i=0; $i < count($this->title_child_arr); $i++) {
        $id_child_arr_str=(!empty($this->id_child_arr[$i]))?' id="'.$this->id_child_arr[$i].'" ':'';
        $html.='<div class="dropdown_default2_option '.$this->class_child_arr[$i].'" '.$id_child_arr_str.$this->name_child_arr[$i].' onclick="'.$this->onclick_child_arr[$i].'dropdown_default2_opt_click(this);" data-value="'.$this->value_child_arr[$i].'">';
        $html.=$this->title_child_arr[$i];
        $html.='</div>';
      }
      $html.='</div>';
      $html.='</div>';
      $html.='</div>';
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function dropdown_white_init($param=array()){
    $lcp_obj=new dropdown_white($param);
    return $lcp_obj->html();
  }
}
?>
