<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_html_navigation',
  'type' => 'builder',
  'group' => 'html',
  'inner' => array(
    'description' => 'Навигация (образец: Главная > Каталог > Пример страницы). Высчитывает от id в datas',
    'css' => array(
      '0' => '/components/builder/html/html-navigation/html-navigation.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id_datas',
    '1' => 'href',
    '2' => 'onclick',
    '3' => 'id_except',
    '4' => 'sql_except',
    '5' => 'id_home_arr'
  ),
  'set_value_php' => array(
    '0' => '1',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '1'
  ),
  'set_description_php' => array(
    '0' => 'ID текущей записи в таблице datas',
    '1' => 'Ссылка для перехода на категорию (тег href). Для указания значения поля БД, используйте: [add_pole_column="column_db"]. Например, для id: [add_pole_column="id"].',
    '2' => 'Действие при клике мыши на категорию (тег onclick). Для указания значения поля БД, используйте: [add_pole_column="column_db"]. Например, для id: [add_pole_column="id"].',
    '3' => 'Исключить ID (только одно значение)',
    '4' => 'Исключить значения из БД, используя SQL (н-р, " AND status<>8")',
    '5' => 'Массив ID, которые могут крепиться к домашней странице'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_html_navigation($param=''){

  // Функция для преобразования строки [add_pole_column="column"] в соответствующее значение колонки БД
  if (function_exists('str_replace_db')===false){
    function str_replace_db($param){
      $str=$param['str'];
      $id_datas=(int)$param['id_datas'];

      $kkk=0;
      while ((strpos($str,'[add_pole_column=',0))!==false){
        $pos_start=strpos($str,'[add_pole_column=',0)+18;
        $pos_end=strpos($str,']',$pos_start);
        if ($pos_end===false){
          break;
        } else {
          $pos_end--;
        }
        $pos_end=($pos_end===false)?strlen($str):$pos_end;
        $res_column=substr($str,$pos_start,$pos_end-$pos_start);
        if ($res_column[0]=='"'){
          $res_column[0]='';
          $res_column=trim($res_column);
        }
        if ($res_column[strlen($res_column)-1]=='\\'){
          $res_column[strlen($res_column)-1]='';
          $res_column=trim($res_column);
        }
        if (((int)$id_datas)=='1'){
          $str=str_replace('[add_pole_column=\''.$res_column.'\']','1',$str);
          $str=str_replace('[add_pole_column="'.$res_column.'"]','1',$str);
          $str=str_replace('[add_pole_column=\\\''.$res_column.'\\\']','1',$str);
          $str=str_replace('[add_pole_column=\"'.$res_column.'\"]','1',$str);
        } else {
          $str=str_replace('[add_pole_column=\''.$res_column.'\']',getcelldb($res_column,$res_column,'datas','id=\''.((int)$id_datas).'\''),$str);
          $str=str_replace('[add_pole_column="'.$res_column.'"]',getcelldb($res_column,$res_column,'datas','id=\''.((int)$id_datas).'\''),$str);
          $str=str_replace('[add_pole_column=\\\''.$res_column.'\\\']',getcelldb($res_column,$res_column,'datas','id=\''.((int)$id_datas).'\''),$str);
          $str=str_replace('[add_pole_column=\"'.$res_column.'\"]',getcelldb($res_column,$res_column,'datas','id=\''.((int)$id_datas).'\''),$str);
        }
        $kkk++;
      }
      return $str;
    }
  }



  $id_datas=$param['id_datas'];
  $href=$param['href'];
  $onclick=$param['onclick'];
  $id_except=$param['id_except'];
  $sql_except=$param['sql_except'];
  $id_home_arr=$param['id_home_arr'];
  $default_id_datas='1';
  $default_href='';
  $default_onclick='';
  $default_id_except='';
  $default_sql_except='';
  $default_id_home_arr='1';
  $id_datas=(!empty($id_datas))?$id_datas:$default_id_datas;
  $href=(!empty($href))?$href:$default_href;
  $onclick=(!empty($onclick))?$onclick:$default_onclick;
  $id_except=(!empty($id_except))?$id_except:$default_id_except;
  $sql_except=(!empty($sql_except))?$sql_except:$default_sql_except;
  $href=(!empty($href))?' href="'.$href.'" ':'';
  $onclick=(!empty($onclick))?' onclick="'.$onclick.'" ':'';
  $id_except=(!empty($id_except))?' AND id<>\''.$id_except.'\' ':'';
  $sql_except=(!empty($sql_except))?' '.$sql_except.' ':'';


  if (empty($id_home_arr)){
    $id_home_arr[1]=$default_id_home_arr;
  }
  $level='0';
  $datas_arr[0]=getarrayvaluedb(array('id','childr','title_short'),'id,childr,title_short','datas','id=\''.$id_datas.'\' ','','1');
  // if (empty($id_home_arr[$datas_arr[$level]['id'][0]])){
  //   echo $datas_arr[$level]['title_short'][0];
  //   exit();
  while(empty($id_home_arr[$datas_arr[$level]['id'][0]])){
    $id_datas=$datas_arr[$level]['childr'][0];
    if ($id_datas=='1'){
      break;
    }
    $level++;
    $datas_prov=getcelldb('childr','childr','datas','id=\''.$id_datas.'\'','','1');
    $datas_arr[$level]=getarrayvaluedb(array('id','childr','title_short'),'id,childr,title_short','datas','id=\''.$id_datas.'\' '.$id_except.$sql_except,'','1');
    if (empty($datas_prov)){
      break;
    }
    if (!empty($id_home_arr[$id_datas])){
      break;
    }
    // if ($level=='2'){
    //   echo $id_datas.'zdes'.$datas_arr[$level]['title_short'][0].$datas_prov;
    //   exit();
    // }
    while (empty($datas_arr[$level]['childr'][0])){
      $id_datas=$datas_prov;
      $datas_arr[$level]=getarrayvaluedb(array('id','childr','title_short'),'id,childr,title_short','datas','id=\''.$id_datas.'\' '.$id_except.$sql_except,'','1');
      $datas_prov=getcelldb('childr','childr','datas','id=\''.$id_datas.'\'','','1');
      // if ($level=='1'){
      //   echo 'tyt'.$datas_arr[$level]['title_short'][0];
      //   exit();
      // }
      if (!empty($id_home_arr[$id_datas])){
        break;
      }
      if (empty($datas_prov)){
        break;
      }
    }
    if (!empty($id_home_arr[$id_datas])){
      break;
    }
    if (empty($datas_prov)){
      break;
    }
    if ($level>100){
      echo 'Возникла ошибка в навигации!!!';
      break;
    }
  }
// }
  $level++;
  $datas_arr[$level]['id']['0']='1';
  $datas_arr[$level]['childr']['0']='0';
  $datas_arr[$level]['title_short']['0']='Главная';


  $html='';
  for ($i=(count($datas_arr)-1); 0<=$i; $i--) {
    if ($i<=0){
      $html.='<span class="lcp_html_navigation_span">'.$datas_arr[$i]['title_short'][0].'</span>';
    } else {
      // echo (int)$datas_arr[$i]['id']['0'];
      // exit();
      $href_new=str_replace_db(array(
        'str' => $href,
        'id_datas' => (int)$datas_arr[$i]['id']['0']
      ));
      $onclick_new=str_replace_db(array(
        'str' => $onclick,
        'id_datas' => (int)$datas_arr[$i]['id']['0']
      ));
      $html.='<a class="lcp_html_navigation_span_a" '.$href_new.$onclick_new.'>'.$datas_arr[$i]['title_short'][0].'</a><span class="lcp_html_navigation_span"> > </span>';
    }
  }
  return $html;
}
?>
