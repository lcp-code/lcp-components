<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_html_navigation',
  'type' => 'builder',
  'group' => 'html',
  'inner' => array(
    'description' => 'Навигация (образец: Главная > Каталог > Пример страницы). Высчитывает от текущей записи myrow по таблице datas_parent_arr без рекурсии',
    'css' => array(
      '0' => '/components/builder/html/html-navigation/html-navigation.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'myrow',
    '1' => 'href',
    '2' => 'onclick',
    '3' => 'sql_except',
    '4' => 'type_data'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => 'html'
  ),
  'set_description_php' => array(
    '0' => 'Переменная $myrow текущей записи в таблице datas',
    '1' => 'Ссылка для перехода на категорию (тег href). Для указания значения поля БД, используйте: [add_pole_column="column_db"]. Например, для id: [add_pole_column="id"].',
    '2' => '(Временно не работает!). Действие при клике мыши на категорию (тег onclick). Для указания значения поля БД, используйте: [add_pole_column="column_db"]. Например, для id: [add_pole_column="id"].',
    '3' => 'Исключить значения из БД, используя SQL (н-р, " AND status<>8")',
    '4' => 'Получить в виде, где html - в виде html, arr - в виде массива данных без html'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_html_navigation($param=''){
  global $id_shop;
  $myrow=$param['myrow'];
  $href=$param['href'];
  // $onclick=$param['onclick'];
  $sql_except=$param['sql_except'];
  $type_data=$param['type_data'];

  $default_href='';
  $default_onclick='';
  $default_sql_except='';
  $default_type_data='html';
  $href=(!empty($href))?$href:$default_href;
  $onclick=(!empty($onclick))?$onclick:$default_onclick;
  $sql_except=(!empty($sql_except))?$sql_except:$default_sql_except;
  $href=(!empty($href))?' href="'.$href.'" ':'';
  $onclick=(!empty($onclick))?' onclick="'.$onclick.'" ':'';
  $sql_except=(!empty($sql_except))?' '.$sql_except.' ':'';
  $type_data=(!empty($type_data))?$type_data:$default_type_data;
  $id_shop=(!empty($id_shop))?$id_shop:'1';


  $navigation_arr=getarrayvaluedb(array('id','title_short','cat'),'datas.id,datas_dynamic.title_short,datas_dynamic.cat','datas_parent_arr,datas,datas_dynamic,datas_dynamic_shop','
  datas_parent_arr.id_datas=\''.$myrow['id'].'\' AND
  datas.id=datas_parent_arr.id_parent AND
  datas_dynamic.id_datas=datas.id AND
  datas_dynamic.active=\'1\' AND
  datas_dynamic_shop.id_datas_dynamic=datas_dynamic.id AND
  datas_dynamic_shop.id_shop=\''.$id_shop.'\'
  '.$sql_except,'BY datas_parent_arr.numm');
  unset($navigation_arr['getarrayvaluedb_sql']);
  $navigation_arr['id'][]=$myrow['id'];
  $navigation_arr['title_short'][]=$myrow['title_short'];
  $navigation_arr['cat'][]=$myrow['cat'];
  array_unshift($navigation_arr['id'],'-1');
  array_unshift($navigation_arr['title_short'],'Главная');
  array_unshift($navigation_arr['cat'],'/');

  $html=($type_data=='arr')?array():'';
  if ($type_data=='html'){
    if (count($navigation_arr['id'])>0){
      $html.='<div class="titledirect" itemscope="" itemtype="https://schema.org/BreadcrumbList">';
      for ($i=0; $i < count($navigation_arr['id']); $i++) {
        if ($i==(count($navigation_arr['id'])-1)){
          $html.='<span class="lcp_html_navigation_span">'.$navigation_arr['title_short'][$i].'</span>';
        } else {
          $html.='<span itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem"><a class="lcp_html_navigation_span_a" title="'.$navigation_arr['title_short'][$i].'" itemprop="item" href="'.getcuts($navigation_arr['cat'][$i]).'"><span itemprop="name">'.$navigation_arr['title_short'][$i].'</span></a><meta itemprop="position" content="'.($i+1).'"></span>';
          $html.='<span class="lcp_html_navigation_span"> > </span>';
        }
      }
      $html.='</div>';
    }
  } else {
    $html=$navigation_arr;
  }

  return $html;
}
?>
