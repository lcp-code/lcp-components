function lcp_load_products_more(param){
  var ids=param['id'];
  var get=param['get'];
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='products_more_field';
  }
  if (get=='get_load_products_more'){
    return get_load_products_more(ids);
  }
  if (get=='get_for_save_products_more'){
    return get_for_save_products_more(ids);
  }
}

function sortable_pl_more(ids){
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='products_more_field';
  }
  $('#'+ids).sortable();
  $('#'+ids).disableSelection();
}

function del_products(obj){
  $(obj).parent('.lcp_product_more_block_pl_div').parent('.pl_field_items').remove();
}

function get_load_products_more(ids){
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='products_more_field';
  }
  var obj=$('#'+ids).children('.pl_field_items');
  var files={};
  files['id']={};
  files['numm']={};
  files['pl_field']={};
  files['index']={};
  for (var i = 0; i < obj.length; i++) {
    var obj_item=$(obj).eq(i);
    files['id'][i]=$(obj_item).attr('data-id');
    files['numm'][i]=i;
    files['pl_field'][i]=ids;
    files['index'][$(obj_item).attr('data-id')]=i;
  }
  files['length']=obj.length;
  return files;
}

function get_for_save_products_more(ids){
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='products_more_field';
  }
  var files=get_load_products_more(ids);
  files['deistv']={};
  var files_del={};

  if (typeof first_id_arr[ids]==='undefined'){
    alert('Ошибка! Возможно, id в get_for_save_products_more введен не верно!');
    return;
  }

  // Нахождение удаленных фото
  var bb='false';
  var k=-1;
  for (var i1 = 0; i1 < first_id_arr[ids]['length']; i1++) {
    bb='false'
    for (var i2 = 0; i2 < files['length']; i2++) {
      if (first_id_arr[ids][i1]==files['id'][i2]){
        bb='true';
        break;
      }
    }
    if (bb=='false'){
      k++;
      files_del[k]=first_id_arr[ids][i1];
      files_del['length']=(k+1);
    }
  }

  // Нахождение добавленных и обновленных фото
  var bb='false';
  var k=-1;
  for (var i1 = 0; i1 < files['length']; i1++) {
    if (files['id'][i1]==''){
      files['deistv'][i1]='INSERT';
    } else {
      bb='false'
      for (var i2 = 0; i2 < first_id_arr[ids]['length']; i2++) {
        if (files['id'][i1]==first_id_arr[ids][i2]){
          bb='true';
          files['deistv'][i1]='UPDATE';
          break;
        }
      }
      if (bb=='false'){
        files['deistv'][i1]='DELETE';
      }
    }
  }

  // Добавление к массиву удаленных фото
  k=files['length'];
  for (var i = 0; i < files_del['length']; i++) {
    files['id'][k]=files_del[i];
    files['deistv'][k]='DELETE';
    k++;
  }

  // добавляем к существующим id аттрибуты
  files['length']=k;
  files['numm']={};
  files['pl_field']={};

  files_get=get_load_products_more(ids);
  files_get_new=get_load_products_more(ids);
  for (var i = 0; i < files['length']; i++) {
    var files_get_index=files_get['index'][files['id'][i]];
    files['numm'][i]=files_get['numm'][files_get_index];
  }

  // добавляем к новым (insert) фото аттрибуты
  k=0;
  for (var i = 0; i < files_get['length']; i++) {
    if (files_get['id'][i]==''){
      files_get_new['numm'][k]=files_get['numm'][i];
      k++;
    }
  }

  k=0;
  numm=0;
  for (var i = 0; i < files['length']; i++) {
    if (files['deistv'][i]=='INSERT'){
      k++;
    }
    if (files['deistv'][i]!='DELETE'){
      files['numm'][i]=numm;
      numm++;
    }
    files['pl_field'][i]=ids;
  }

  return files;
}

$(function(){
    sortable_pl_more();
  });
