<?php
// ini_set('error_reporting', E_ALL & ~E_NOTICE);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_html_parser_one',
  'type' => 'builder',
  'group' => 'html',
  'inner' => array(
    'description' => 'Блок для отображения статистики парсера',
    'css' => array(),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'parser_api',
    '2' => 'title',
  ),
  'set_value_php' => array(
    '0' => 'chart_area',
    '1' => '',
    '2' => '',
  ),
  'set_description_php' => array(
    '0' => 'id диаграммы. Если создано несколько диаграмм на одной странице - то поле обязательно',
    '1' => 'URL адрес парсера для разных функций, например, get_status, start, reset и пр. (https://parser.ru/api.php)',
    '2' => 'Отображаемое название парсера',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>



<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_html_parser_one extends lcp_components implements lcp_components_build{
    public $id='chart_area';
    public $parser_api;
    public $title;

    function __construct($param=array()){
      $this->id=isset($param['id'])?$param['id']:$this->id;
      $this->parser_api=isset($param['parser_api'])?$param['parser_api']:$this->parser_api;
      $this->title=isset($param['title'])?$param['title']:$this->title;
      parent::__construct($param);
    }

    function __build_component(){
      $html='';

      $parser_arr=file_get_contents($this->parser_api.'?action=get_status&format=json');
      if ($parser_arr===false){
        $parser_arr['status']='Ошибка получения данных!';
        $parser_arr['dater_last_work']='-';
        $parser_arr['dater_start']='-';
        $parser_arr['dater_end']='-';
        $parser_arr['parser_count']='-';
        $parser_arr['works_count']='-';
        $parser_arr['works_completed_count']='-';
        $parser_arr['works_success_count']='-';
        $parser_arr['works_failed_count']='-';
        $parser_arr['works_left_count']='-';
      } else {
        $parser_arr=json_decode(htmlspecialchars_decode($parser_arr), true);
        $parser_arr=$parser_arr['ymaps'];
      }


      $html_left='';
      $html_left.='<div style="font-size: 21px;font-weight: bold;color: #333333;padding-bottom: 15px;">'.$this->title.'</div>';
      $html_left.=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>'<b>Статус парсера:</b> '.$parser_arr['status'],
      ));
      $html_left.=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>'<b>Последнее сканирование:</b> '.$parser_arr['dater_last_work'],
      ));
      $html_left.=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>'<b>Дата начала:</b> '.$parser_arr['dater_start'],
      ));
      $html_left.=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>'<b>Дата завершения:</b> '.$parser_arr['dater_end'],
      ));
      $html_left.=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>'<b>Количество сканируемых за один проход:</b> '.$parser_arr['parser_count'],
      ));
      $html_left.=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>'<b>Всего:</b> '.$parser_arr['works_count'],
      ));
      $html_left.=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>'<b>Выполненных:</b> '.$parser_arr['works_completed_count'],
      ));
      $html_left.=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>'<b>Выполненных успешно:</b> '.$parser_arr['works_success_count'],
      ));
      $html_left.=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>'<b>Выполненных неуспешно:</b> '.$parser_arr['works_failed_count'],
      ));
      $html_left.=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>'<b>Осталось:</b> '.$parser_arr['works_left_count'],
      ));
      if ($parser_arr['status']=='Не работает'){
        $parser_error_texts=file_get_contents($this->parser_api.'?action=start&format=json');
        $html_left.=components('lcp_html_block_default2',array(
          'title'=>' ',
          'html'=>'<b>Ошибка:</b><div><pre>'.$parser_error_texts.'</pre></div>',
        ));
      }



      if ($parser_arr['status']=='Ошибка получения данных!'){
        $diagramm_title[0]='Ошибка получения данных!';
        $diagramm_data[0]='500';
        $diagramm_background[0]='rgb(255, 0, 0)';
      } else
      if ($parser_arr['works_success_count']=='-'){
        $diagramm_title[0]='Выполнено';
        $diagramm_title[1]='Осталось';
        $diagramm_data[0]=$parser_arr['works_completed_count'];
        $diagramm_data[1]=$parser_arr['works_left_count'];
        $diagramm_background[0]='rgb(0, 134, 23)';
        $diagramm_background[1]='rgb(200, 200, 200)';
      } else {
        $diagramm_title[0]='Выполнено успешно';
        $diagramm_title[1]='Выполнено неуспешно';
        $diagramm_title[2]='Осталось';
        $diagramm_data[0]=$parser_arr['works_success_count'];
        $diagramm_data[1]=$parser_arr['works_failed_count'];
        $diagramm_data[2]=$parser_arr['works_left_count'];
        $diagramm_background[0]='rgb(0, 134, 23)';
        $diagramm_background[1]='rgb(255, 0, 0)';
        $diagramm_background[2]='rgb(200, 200, 200)';
      }


      $html_diagramm='';
      $html_diagramm.='<div id="canvas-holder_'.$this->id.'" style="width:100%">';
      $html_diagramm.='<canvas id="'.$this->id.'"></canvas>';
      $html_diagramm.='</div>';
      $html_diagramm.='<script type="text/javascript">';
      $html_diagramm.='var config_'.$this->id.' = {';
      $html_diagramm.='type: \'pie\',';
      $html_diagramm.='data: {';
      $html_diagramm.='datasets: [{';
      $html_diagramm.='data: [\'';
      $html_diagramm.=implode('\',\'',$diagramm_data);
      $html_diagramm.='\'],';
      $html_diagramm.='backgroundColor: [\'';
      $html_diagramm.=implode('\',\'',$diagramm_background);
      $html_diagramm.='\'],';
      $html_diagramm.='label: \'Dataset 1\'';
      $html_diagramm.='}],';
      $html_diagramm.='labels: [\'';
      $html_diagramm.=implode('\',\'',$diagramm_title);
      $html_diagramm.='\']';
      $html_diagramm.='},';
      $html_diagramm.='options: {';
      $html_diagramm.='responsive: true';
      $html_diagramm.='}';
      $html_diagramm.='};';
      $html_diagramm.='';
      $html_diagramm.='$(function(){';
      $html_diagramm.='var ctx = document.getElementById(\''.$this->id.'\').getContext(\'2d\');';
      $html_diagramm.='window.myPie = new Chart(ctx, config_'.$this->id.');';
      $html_diagramm.='});';
      $html_diagramm.='</script>';

      $html_right_title=components('dropdown_white',array(
        'text'=>'-- Выберите действие --',
        'title_child_arr'=>array('Включить','Выключить','Начать с начала','Очистить всё'),
        'onclick_child_arr'=>array('parser_management_'.$this->id.'({\'action\':\'start_parser\'});return;','parser_management_'.$this->id.'({\'action\':\'stop_parser\'});return;','parser_management_'.$this->id.'({\'action\':\'reset_parser\'});return;','parser_management_'.$this->id.'({\'action\':\'clear_parser\'});return;'),
      ));

      $html_right_body=components('lcp_html_block_default2',array(
        'title'=>' ',
        'html'=>$html_diagramm,
      ));
      $html_right='';
      $html_right.=components('lcp_html_block_default2',array(
        'title'=>$html_right_title,
        'html'=>$html_right_body,
      ));


      $html.=components('lcp_block_one_line2',array(
        'tbody'=>array($html_left,$html_right),
        'width_columns'=>array('100%','400px'),
        'text_align_col_tbody'=>array('left','left'),
        'text_valign_col_tbody'=>array('top','top'),
        'padding_arr'=>array('0px','0px')
      ));

      $html.=components('lcp_ajax',array(
        'function_name'=>'parser_management_'.$this->id,
        'begin_html'=>'
        var action=param[\'action\'];
        var action_str=\'запустить\';
        if (action==\'start_parser\'){
          action_str=\'запустить\';
        } else
        if (action==\'stop_parser\'){
          action_str=\'остановить\';
        } else
        if (action==\'reset_parser\'){
          action_str=\'перезапустить\';
        } else
        if (action==\'clear_parser\'){
          action_str=\'очистить\';
        }
        if (confirm("Вы действительно хотите "+action_str+" парсер?")===false){
          return;
        }
        lcp_loading_default({loading_show:\'true\'});',
        'url'=>'/components/builder/html/html-parser-one/ajax-html-parser-one.php',
        'data_js'=>'{
          action:action,
          parser_api:\''.$this->parser_api.'\',
        }',
        'lcp_success_true'=>'
        location.reload();
        lcp_loading_default({loading_show:\'false\'});
        ',
        'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});',
        'error'=>'lcp_loading_default({loading_show:\'false\'});'
      ));
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_html_parser_one_init($param=array()){
    $lcp_obj=new lcp_html_parser_one($param);
    return $lcp_obj->html();
  }
}
?>
