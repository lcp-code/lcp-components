<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_product_more',
  'type' => 'builder',
  'group' => 'html',
  'inner' => array(
    'description' => 'Компонент для отображения дополнительных блоков товаров, таких как "Вам может быть интересно", "Читайте похожие статьи", "Рекомендуемые товары" и пр.',
    'css' => array(
      '0' => '/components/builder/html/product-more/style.css'
    ),
    'js' => array(
      '0' => '/components/builder/html/product-more/script.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'id_datas',
    '2' => 'title'
  ),
  'set_value_php' => array(
    '0' => 'lcp_html_parent_products_more',
    '1' => '1',
    '2' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор блока (тег id)',
    '1' => 'ID текущей записи в таблице datas',
    '2' => 'Название блока',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_product_more($param=''){
  $ids=$param['id'];
  $id_datas=$param['id_datas'];
  $product_id_arr=$param['product_id_arr'];
  $default_ids='lcp_html_parent_products_more';
  $default_id_datas='1';
  $ids=(!empty($ids))?$ids:$default_ids;
  $id_datas=(!empty($id_datas))?$id_datas:$default_id_datas;

  $html='';
  $html.='<div id="'.$ids.'" class="lcp_html_parent_article" data-id="'.$id_datas.'">';
  $html.='<div class="lcp_html_parent_article_button">';
  $html.=components('lcp_button_default',array(
    'value' => 'Добавить статью',
    'onclick'=>'show_tree_products_more_'.$ids.'({id_catalog:$(\'#'.$ids.'\').attr(\'data-id\')});popup_tree_products_more_'.$ids.'();',
    'class'=>'lcp_button_products_navigation'
  ));
  $html.='</div>';

  $html.='<div class="lcp_product_more_all">';
  $html.='<div class="lcp_product_more_block">';
  $html.='<div class="lcp_product_more_block_img_div"><div class="lcp_product_more_block_img" style="background:url(http://fortran-new.ru/polz/2019/07/https.jpg);"></div></div>';
  $html.='<div class="lcp_product_more_block_title">Заголовок статьи</div>';
  $html.='</div>';
  $html.='</div>';

  $html.='</div>';









  $popup_content='';
  $popup_content.='<div id="content_tree_products_'.$ids.'"></div>';
  $html.=components('lcp_popup_default',array(
    'id_popup' => 'popup_tree_products_more_'.$ids,
    'title' => 'Выберите статью:',
    'content_html' => $popup_content,
    'popup_show' => 'popup_tree_products_more_'.$ids,
    'popup_close' => 'popup_tree_products_more_close_'.$ids,
    'height' => '400px',
    'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));




  $html.=components('lcp_ajax',array(
    'function_name'=>'show_tree_products_more_'.$ids,
    'begin_html'=>'
    var id_catalog=param[\'id_catalog\'];
    if (typeof id_catalog===\'undefined\'){
      var id_catalog=\''.$id_datas.'\';
      }
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/components/builder/html/product-more/ajax-product-more.php',
    'data_js'=>'{
      action:\'show_tree_products\',
      id_catalog:id_catalog,
      id_obj:\''.$ids.'\',
      id_except:\''.$id_except.'\'
    }',
    'lcp_success_true'=>'$(\'#content_tree_products_'.$ids.'\').html(data.string[\'html\']);lcp_loading_default({loading_show:\'false\'});',
    'lcp_success_false'=>'lcp_message(data.string,\'false\');lcp_loading_default({loading_show:\'false\'});',
    'error'=>'lcp_message(\'Ошибка AJAX!\',\'false\');lcp_loading_default({loading_show:\'false\'});',
    'show_error'=>'false'
  ));


  $html.=components('lcp_ajax',array(
    'function_name'=>'get_tree_more_'.$ids,
    'begin_html'=>'
    var id_catalog=param[\'id_catalog\'];
    if (typeof id_catalog===\'undefined\'){
      var id_catalog=\''.$id_datas.'\';
    }
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/components/builder/html/product-more/ajax-product-more.php',
    'data_js'=>'{
      action:\'get_tree_more\',
      id_catalog:id_catalog,
      id_obj:\''.$ids.'\'
    }',
    'lcp_success_true'=>'$(\'#'.$ids.'>.lcp_html_parent_article_block\').html(data.string[\'html\']);popup_tree_products_more_close_'.$ids.'();lcp_loading_default({loading_show:\'false\'});',
    'lcp_success_false'=>'lcp_message(data.string,\'false\');lcp_loading_default({loading_show:\'false\'});',
    'error'=>'lcp_message(\'Ошибка AJAX!\',\'false\');lcp_loading_default({loading_show:\'false\'});',
    'show_error'=>'false'
  ));


  // $html.='<div class="dropdown_default2 '.$class.'" '.$id_dropdown.$name.$onclick.$onmousedown.$onmouseup.$width.'>';
  return $html;
}
?>
