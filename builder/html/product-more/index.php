<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_product_more',
  'type' => 'builder',
  'group' => 'html',
  'inner' => array(
    'description' => 'Компонент для отображения дополнительных блоков товаров, таких как "Вам может быть интересно", "Читайте похожие статьи", "Рекомендуемые товары" и пр.',
    'css' => array(
      '0' => '/components/builder/html/product-more/style.css'
    ),
    'js' => array(
      '0' => '/components/builder/html/product-more/script.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'id_except',
    '2' => 'catalog_id',
    '3' => 'popup_title',
    '4' => 'button_title',
    '5' => 'status_arr',
    '6' => 'status_check_arr',
    '7' => 'files_arr',
  ),
  'set_value_php' => array(
    '0' => 'products_more_field',
    '1' => '',
    '2' => '',
    '3' => 'Выберите статью:',
    '4' => 'Выбрать статью',
    '5' => '',
    '6' => '',
    '7' => '',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Исключить ID (только одно значение)',
    '2' => 'ID категории, которая должна всегда открываться',
    '3' => 'Заголовок всплывающего окна для выбора объектов',
    '4' => 'Заголовок кнопки для отображения окна выбора объектов',
    '5' => 'Массив со статусами отображаемых страниц',
    '6' => 'Массив со статусами страниц, которые можно выбрать',
    '7' => 'Массив с объектами (только id).',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
    '0' => 'id',
    '1' => 'get'
  ),
  'set_value_js' => array(
    '0' => 'products_more_field',
    '1' => ''
  ),
  'set_description_js' => array(
    '0' => 'Идентификатор (тег id), по которому следует искать элемент',
    '1' => '<div>Получение данных об объектах (по заданному id), где:</div><div>get_load_products_more - получить готовые объекты и аттрибуты (рекомендуется для сохранения в БД в виде массива)</div><div>get_for_save_products_more - получить действия для удаления, обновления и добавления прошлых и новых объектов со всеми аттрибутами (рекомендуется для сохранения в БД в виде отдельной таблицы с соответствующими полями)</div>'
  ),
  'get_js' => array(
    '0' => 'array'
  ),
  'get_description_js' => array(
    '0' => 'Возвращает массив с аттрибутами объектов'
  )
);
}
?>




<?php
function lcp_product_more($param=''){
  $puth_php='components/builder/html/product-more/index.php';
  $puth_php=str_replace('index.php','',$puth_php);

  $id_datas='1';
  $ids=$param['id'];
  $id_except=$param['id_except'];
  $files_arr=$param['files_arr'];
  $status_arr=$param['status_arr'];
  $status_check_arr=$param['status_check_arr'];
  $popup_title=$param['popup_title'];
  $button_title=$param['button_title'];
  $catalog_id=$param['catalog_id'];
  $default_ids='products_more_field';
  $default_popup_title='Выберите статью:';
  $default_button_title='Выбрать статью';
  $ids=(!empty($ids))?$ids:$default_ids;
  $popup_title=(!empty($popup_title))?$popup_title:$default_popup_title;
  $button_title=(!empty($button_title))?$button_title:$default_button_title;
  $status_str=implode(',',$status_arr);
  $status_check_str=implode(',',$status_check_arr);


  $html='';
  $html.='<div class="lcp_html_parent_article_more_button">';
  $html.=components('lcp_button_default',array(
    'value' => $button_title,
    'onclick'=>'show_tree_products_more_'.$ids.'({id_catalog:'.((!empty($catalog_id))?'\''.$catalog_id.'\'':'$(\'#'.$ids.'\').attr(\'data-id\')').'});popup_tree_products_more_'.$ids.'();',
    'class'=>'lcp_button_add_art'
  ));
  $html.='</div>';




  $popup_content='';
  $popup_content.='<div id="content_tree_products_more_'.$ids.'"></div>';
  $html.=components('lcp_popup_default',array(
    'id_popup' => 'popup_tree_products_more_'.$ids,
    'title' => $popup_title,
    'content_html' => $popup_content,
    'popup_show' => 'popup_tree_products_more_'.$ids,
    'popup_close' => 'popup_tree_products_more_close_'.$ids,
    'height' => '400px',
    'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));


  $html.=components('lcp_ajax',array(
    'function_name'=>'show_tree_products_more_'.$ids,
    'begin_html'=>'
    var id_catalog=param[\'id_catalog\'];
    if (typeof id_catalog===\'undefined\'){
      var id_catalog=\''.$id_datas.'\';
      }
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/'.$puth_php.'ajax-product-more.php',
    'data_js'=>'{
      action:\'show_tree_products\',
      id_catalog:id_catalog,
      id_obj:\''.$ids.'\',
      id_except:\''.$id_except.'\',
      status_str:\''.$status_str.'\',
      status_check_str:\''.$status_check_str.'\'
    }',
    'lcp_success_true'=>'$(\'#content_tree_products_more_'.$ids.'\').html(data.string[\'html\']);lcp_loading_default({loading_show:\'false\'});',
    'lcp_success_false'=>'lcp_message(data.string,\'false\');lcp_loading_default({loading_show:\'false\'});',
    'error'=>'lcp_message(\'Ошибка AJAX!\',\'false\');lcp_loading_default({loading_show:\'false\'});',
    'show_error'=>'false'
  ));


  $html.=components('lcp_ajax',array(
    'function_name'=>'get_tree_more_'.$ids,
    'begin_html'=>'
    var id_catalog=param[\'id_catalog\'];
    if (typeof id_catalog===\'undefined\'){
      var id_catalog=\''.$id_datas.'\';
      }
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/'.$puth_php.'ajax-product-more.php',
    'data_js'=>'{
      action:\'load_datas\',
      ids:id_catalog
    }',
    'lcp_success_true'=>'
    data.string[\'id\']=\''.$ids.'\';
    view_pl_more_'.$ids.'(data.string);
    popup_tree_products_more_close_'.$ids.'();
    lcp_loading_default({loading_show:\'false\'});
    ',
    'lcp_success_false'=>'lcp_message(data.string,\'false\');lcp_loading_default({loading_show:\'false\'});',
    'error'=>'lcp_message(\'Ошибка AJAX!\',\'false\');lcp_loading_default({loading_show:\'false\'});',
    'show_error'=>'false'
  ));
















  // $html.=components('lcp_ajax',array(
  //   'function_name'=>'get_datas_'.$ids,
  //   'begin_html'=>'
  //   lcp_loading_default({loading_show:\'true\'});
  //   ',
  //   'url'=>'/components/builder/html/product-more/ajax-product-more.php',
  //   'data_js'=>'{
  //     action:\'load_datas\',
  //     ids:\''.$_GET['i'].'\'
  //   }',
  //   'lcp_success_true'=>'
  //   data.string[\'id\']=\''.$ids.'\';
  //   view_pl_more(data.string);
  //   ',
  //   'lcp_success_false'=>'lcp_message(data.string,\'false\');lcp_loading_default({loading_show:\'false\'});',
  //   'error'=>'lcp_message(\'Ошибка AJAX!\',\'false\');lcp_loading_default({loading_show:\'false\'});',
  //   'show_error'=>'false'
  // ));






  // 'Выбрать фото';
  $html.='<div id="'.$ids.'" class="lcp_product_more_all" '.((!empty($catalog_id))?'data-id="'.$catalog_id.'"':'').'>';
  $html.=components('lcp_ajax',array(
    'lcp_type_ajax'=>'include',
    'function_name'=>'view_pl_more_'.$ids,
    'begin_html'=>'
    var files=param[\'files\'];
    var ids=param[\'id\'];
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/'.$puth_php.'ajax-product-more.php',
    'data'=>array(
      'action'=>'view_pl_more',
      'files'=>$files_arr,
      'ids'=>$ids
    ),
    'data_js'=>'{
      action:\'view_pl_more\',
      files:files,
      ids:ids
      }',
      'lcp_success_true'=>'
      lcp_loading_default({loading_show:\'false\'});
      $(\'#\'+ids).append(data.string);
      sortable_pl_more(ids);
      ',
      'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});alert(data.string);',
      'error'=>'lcp_loading_default({loading_show:\'false\'});alert(\'Ошибка при ajax запросе!\');',
      'show_error'=>'false'
    ));
    $html.='<script>sortable_pl_more(\''.$ids.'\');</script></div>';

    $popup_content='';
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Ссылка (высокое качество):',
      'html'=>components('lcp_text_default',array(
        'id'=>'src_pl_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите ссылку'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Ссылка (низкое качество):',
      'html'=>components('lcp_text_default',array(
        'id'=>'src_pl_mini_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите ссылку'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Заголовок:',
      'html'=>components('lcp_text_default',array(
        'id'=>'title_pl_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите заголовок'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Альтернативный текст:',
      'html'=>components('lcp_text_default',array(
        'id'=>'alt_pl_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите альтернативный текст'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Длинное описание ссылки:',
      'html'=>components('lcp_text_default',array(
        'id'=>'longdesc_pl_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите длинное описание ссылки'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>' ',
      'html'=>components('lcp_block_one_line',array(
        'tbody'=>array(components('lcp_button_default',array(
          'id'=>'pl_butt_save_'.$ids,
          'value'=>'Применить свойства к фото',
          'onclick'=>'pl_edit_'.$ids.'_close();pl_edit_save(\''.$ids.'\')'
        ))),
        'width_columns'=>array('100%'),
        'text_align_col_tbody'=>array('center')
      ))
    ));
    $html.=components('lcp_popup_default',array(
      'id_popup'=>'pl_edit_'.$ids,
      'title'=>'Свойства изображения',
      'content_html'=>$popup_content,
      'popup_show'=>'pl_edit_'.$ids,
      'popup_close'=>'pl_edit_'.$ids.'_close',
      'width'=>'400px',
      'height'=>'455px'
    ));

  $html.='<script type="text/javascript">';
  $html.='if (typeof first_id_arr===\'undefined\'){';
  $html.='var first_id_arr={};';
  $html.='}';
  $html.='first_id_arr[\''.$ids.'\']={};';
    for ($i=0; $i < count($files_arr['id']); $i++) {
      $html.='first_id_arr[\''.$ids.'\'][\''.$i.'\']=\''.$files_arr['id'][$i].'\';';
    }
  $html.='first_id_arr[\''.$ids.'\'][\'length\']=\''.count($files_arr['id']).'\'';
  $html.='</script>';
  return $html;
}
?>
