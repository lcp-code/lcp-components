<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_html_block_product_more',
  'type' => 'builder',
  'group' => 'html',
  'inner' => array(
    'description' => 'Компонент для отображения дополнительных блоков товаров, таких как "Вам может быть интересно", "Читайте похожие статьи", "Рекомендуемые товары" и пр.',
    'css' => array(
      '0' => '/components/builder/html/html-block-product-more/style.css'
    ),
    'js' => array(
      '0' => '/components/builder/html/html-block-product-more/script.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'title'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор блока (тег id)',
    '1' => 'Название блока',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_html_block_product_more($param=''){
  // $id_components=getcelldb('id','id','components','name=\'lcp_html_block_default2\'');
  $html='';
  $param['class']=$param['class'].' lcp_html_block_default3 ';
  $html=components('lcp_html_block_default2',$param);
  return $html;
}
?>
