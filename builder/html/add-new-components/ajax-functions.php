<?php
function get_html_component($param){
  $id=$param['id'];
  $title=$param['title'];
  $function_name=$param['function_name'];
  $value=$param['value'];
  $always=$param['always'];

  if (empty($id)){
    generate_exception('id не указан!');
  }
  if (empty($title)){
    generate_exception('title не указан!');
  }
  if (empty($function_name)){
    generate_exception('function_name не указан!');
  }
  if (empty($always)){
    generate_exception('always не указан!');
  }

  $html='';
  $html.='<div data-id="'.$id.'" data-always="'.$always.'" data-funct="'.$function_name.'" class="block_add_new_components">';
  $html.=components('lcp_html_block_default2',array(
    'title'=>$title.' <div class="show_components_edit_field_title" onclick="
    $(\'#id_edit_component\').html(\''.$id.'\');
    lcp_checked_default($(\'#check_edit_new_components\'),{checked:\''.$always.'\'});
    show_edit_component();
    ">(Изменить поле)</div>',
    'html'=>components($function_name,array(
      'id'=>'new_components_'.$id,
      'value'=>$value,
    ))
  ));
  $html.='</div>';
  return $html;
}

?>
