<?php
if (($_LCP_AJAX!=="AJAX")||($_LCP_AJAX_TIME!=md5(getdater()))){header("HTTP/1.0 404 Not Found");exit("HTTP/1.0 404 Not Found");}


// подключение дополнительных функций
$root_dir=lcp_root_folder();
include($root_dir.'components/builder/html/add-new-components/ajax-functions.php');







if ($_POST['action']=='add_component_on_id'){
  // получить компонент по id
  $ids=$_POST['ids'];
  $html='';
  $component_arr=getarrayvaluedb(array('id','title','desc','funct','block'),'id,title,description AS desc,function_name AS funct,block','components_page','id=\''.$ids.'\'','','1');
  $html.=get_html_component(array(
    'id'=>$component_arr['id']['0'],
    'title'=>$component_arr['title']['0'],
    'function_name'=>$component_arr['funct']['0'],
    'value'=>'',
    'always'=>'false',
  ));
  $LCP_AJAX_RESULT=$html;
} else




// if ($_POST['action']=='add_component'){
//   // окно для добавления нового компонента
//   $LCP_AJAX_RESULT=add_component();
// } else







if ($_POST['action']=='get_all_components'){
  $status=$_POST['status'];
  $block=$_POST['block'];
  $html='';

  $components_id_arr=array();
  for ($i=0; $i < count($block); $i++) {
    if (isset($block[$i]['id'])){
      $components_id_arr[]=$block[$i]['id'];
    }
  }


  $show_always=getarrayvaluedb(array('id_components_page'),'id_components_page','components_page_show','status=\''.$status.'\'');
  for ($i=0; $i < count($show_always['id_components_page']); $i++) {
    $components_id_arr[]=$show_always['id_components_page'][$i];
  }

  if (count($components_id_arr)>0){
    $components_arr=sqldb('
    SELECT
    components_page.id,
    components_page.title,
    components_page.description,
    components_page.function_name,
    components_page.block,
    components_page_show.id AS components_page_show_id
    FROM components_page
    LEFT JOIN components_page_show ON
    components_page_show.id_components_page=components_page.id AND
    components_page_show.status=\''.$status.'\'
    WHERE
    components_page.id IN (\''.implode('\',\'',$components_id_arr).'\')
    ');
  }
  for ($i=0; $i < count($components_arr['id']); $i++) {
    $block_index=$block['index'][$components_arr['id'][$i]];
    $html.=get_html_component(array(
      'id'=>$components_arr['id'][$i],
      'title'=>$components_arr['title'][$i],
      'function_name'=>$components_arr['function_name'][$i],
      'value'=>$block[$block_index]['value'],
      'always'=>(!empty($components_arr['components_page_show_id'][$i]))?'true':'false',
    ));
  }
  $LCP_AJAX_RESULT=$html;
} else



if ($_POST['action']=='save_add_components'){
  $ids=$_POST['ids'];
  $value=$_POST['value'];
  $title=$_POST['title'];
  $description=$_POST['description'];
  $function_name=$_POST['function_name'];
  $status=$_POST['status'];
  $always=$_POST['always'];
  $action='add';
  if (!empty($ids)){
    $action='edit';
  }
  if (empty($status)){
    $status='0';
  }

  if (empty($title)){
    generate_exception('Введите название компонента!');
  }

  if ($action=='add'){
    $res=addvaluedb('components_page',array(
      'title'=>$title,
      'description'=>$description,
      'function_name'=>$function_name,
      // 'block'=>'',
    ));
    if ($res['error']===false){
      generate_exception('Ошибка при создании компонента!');
    }
    $ids=$res['id'];
  } else
  if ($action=='edit'){
    $res=updatevaluedb('components_page',array(
      'title'=>$title,
      'description'=>$description,
      'function_name'=>$function_name,
      // 'block'=>'',
    ),'id=\''.$ids.'\'');
    if ($res['error']===false){
      generate_exception('Ошибка при сохранении компонента!');
    }
  }

  if ($always=='true'){
    savedb('components_page_show',array(
      'id_components_page'=>$ids,
      'status'=>$status,
    ),'id_components_page=\''.$ids.'\' AND status=\''.$status.'\'');
  } else {
    deletedb('components_page_show','id_components_page=\''.$ids.'\' AND status=\''.$status.'\'');
  }

  $html='';
  $html.=get_html_component(array(
    'id'=>$ids,
    'title'=>$title,
    'function_name'=>$function_name,
    'value'=>$value,
    'always'=>$always,
  ));
  $LCP_AJAX_RESULT=array(
    'action'=>$action,
    'html'=>$html,
  );
} else









{
  generate_exception('Ни одна функция не найдена!');
}
?>
