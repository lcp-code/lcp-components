class LCP_Add_Component {
  // функция для получения всех значений
  getValue(){
    var res={};

    for (var i = 0; i < $('.block_add_new_components').length; i++) {
      var obj=$('.block_add_new_components').eq(i);
      res[i]=this.getComponent(obj);
    }
    return res;
  }

  getComponent(obj){
    var res={};
    res['id']=$(obj).attr('data-id');
    res['always']=$(obj).attr('data-always');
    res['funct']=$(obj).attr('data-funct');

    // текстовое поле
    if (res['funct']=='lcp_text_default'){
      res['value']=this.getValueText(res);
    }
    // телефон
    if (res['funct']=='lcp_phone_default'){
      res['value']=this.getValueText(res);
    }
    // текстовая область
    if (res['funct']=='lcp_memo_default'){
      res['value']=this.getValueTextArea(res);
    }

    // адрес
    if (res['funct']=='lcp_address_default'){
      res['value']=this.getValueAddress(res);
    }
    return res;
  }

  getValueText(component){
    return $('#new_components_'+component['id']).val();
  }
  getValueTextArea(component){
    return $('#new_components_'+component['id']).val();
  }
  getValueAddress(component){
    return getAddress('new_components_'+component['id']);
  }

}
