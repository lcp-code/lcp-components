<?php
$root_dir=lcp_root_folder();
include_once($root_dir.'components/builder/html/add-new-components/popups/components.php');

$show_components_arr=components_arr();
$k=count($show_components_arr['title']);
$show_components_arr['title'][$k]='Созданные ранее';
$show_components_arr['desc'][$k]='Список созданных ранее компонентов, без добавления нового имени и идентификатора';
$show_components_arr['onclick'][$k]='show_components_close();show_install_components();';

$popup_content='';
for ($i=0; $i < count($show_components_arr['title']); $i++) {
  $onclick=(!empty($show_components_arr['onclick'][$i]))?$show_components_arr['onclick'][$i]:'show_components_close();show_new_components();$(\'#funct_show_new_components\').html(\''.$show_components_arr['funct'][$i].'\');';
  $popup_content.='<div class="show_components_items" onclick="'.$onclick.'">';
  $popup_content.='<div class="show_components_title">';
  $popup_content.='<b>'.$show_components_arr['title'][$i].'</b>';
  $popup_content.='</div>';
  $popup_content.='<div class="show_components_desc">';
  $popup_content.=$show_components_arr['desc'][$i];
  $popup_content.='</div>';
  $popup_content.='</div>';
}
$html.=components('lcp_popup_default',array(
  'id_popup' => 'show_components',
  'title' => 'Список доступных компонентов',
  'content_html' => $popup_content,
  'popup_show' => 'show_components',
  'popup_close' => 'show_components_close',
  'height' => '400px',
  'width' => '700px',
  'title_setting' => array(
    'font-size' => '19px',
    'font-weight' => 'bold',
    'text-align' => 'left'
  )
));
?>
