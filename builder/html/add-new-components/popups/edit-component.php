<?php
  $popup_content='';
  $popup_content.='<div id="id_edit_component" style="display:none;"></div>';
  $popup_content.=components('lcp_html_block_default3',array(
    'title'=>'',
    'html'=> components('lcp_checked_default',array(
      'id'=>'check_edit_new_components',
      'title'=>'Показывать всегда',
      'checked'=>'false'
    ))
  ));
  $popup_content.=components('lcp_html_block_default3',array(
    'title'=>' ',
    'html'=>components('lcp_button_default',array(
      'value'=>'Изменить',
      'onclick'=>'
      var ids=$(\'#id_edit_component\').val();
      save_component({
        ids:ids,
        value:$(\'#new_components_\'+ids).val(),
        always:lcp_checked_default($(\'#check_edit_new_components\'),{checked:\'has_checked\'}),
        });
        '
    ))
  ));
  $html.=components('lcp_popup_default',array(
    'id_popup' => 'show_edit_component',
    'title' => 'Изменить компонент',
    'content_html' => $popup_content,
    'popup_show' => 'show_edit_component',
    'popup_close' => 'show_edit_component_close',
    'height' => '400px',
    'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));
?>
