<?php
$root_dir=lcp_root_folder();
include_once($root_dir.'components/builder/html/add-new-components/popups/components.php');

$components_arr=components_arr();
for ($i=0; $i < count($components_arr['title']); $i++) {
  $components_arr['index_title'][$components_arr['funct'][$i]]=$components_arr['title'][$i];
}

$show_components_arr=getarrayvaluedb(array('id','title','desc','funct','block'),'id,title,description AS desc,function_name AS funct,block','components_page');

$popup_content='';
for ($i=0; $i < count($show_components_arr['id']); $i++) {
  $popup_content.='<div class="show_components_items" onclick="show_install_components_close();add_component_on_id({ids:\''.$show_components_arr['id'][$i].'\'})">';
  $popup_content.='<div class="show_components_title">';
  $popup_content.='<b>'.$show_components_arr['title'][$i].'</b>';
  $popup_content.='</div>';
  $popup_content.='<div class="show_components_desc">';
  $popup_content.=$show_components_arr['desc'][$i];
  $popup_content.='</div>';
  $popup_content.='<div class="show_components_type">';
  $popup_content.=$components_arr['index_title'][$show_components_arr['funct'][$i]];
  $popup_content.='</div>';
  $popup_content.='</div>';
}
$html.=components('lcp_popup_default',array(
  'id_popup' => 'show_install_components',
  'title' => 'Установленные ранее компоненты',
  'content_html' => $popup_content,
  'popup_show' => 'show_install_components',
  'popup_close' => 'show_install_components_close',
  'height' => '400px',
  'width' => '700px',
  'title_setting' => array(
    'font-size' => '19px',
    'font-weight' => 'bold',
    'text-align' => 'left'
  )
));
?>
