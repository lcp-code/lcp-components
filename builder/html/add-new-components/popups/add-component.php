<?php
  $popup_content='';
  $popup_content.='<div id="funct_show_new_components" style="display:none;"></div>';
  $popup_content.=components('lcp_html_block_default3',array(
    'title'=>'Название компонента',
    'html'=> components('lcp_text_default',array(
      'id'=>'txt_show_new_components',
      'value'=>''
    ))
  ));
  $popup_content.=components('lcp_html_block_default3',array(
    'title'=>'Описание компонента',
    'html'=> components('lcp_memo_default',array(
      'id'=>'desc_show_new_components',
      'text'=>''
    ))
  ));
  $popup_content.=components('lcp_html_block_default3',array(
    'title'=>'',
    'html'=> components('lcp_checked_default',array(
      'id'=>'check_show_new_components',
      'title'=>'Показывать всегда',
      'checked'=>'false'
    ))
  ));
  $popup_content.=components('lcp_html_block_default3',array(
    'title'=>' ',
    'html'=>components('lcp_button_default',array(
      'value'=>'Добавить',
      'onclick'=>'save_add_components({
        title:$(\'#txt_show_new_components\').val(),
        description:$(\'#desc_show_new_components\').val(),
        function_name:$(\'#funct_show_new_components\').html(),
        status:\''.$this->status.'\',
        always:lcp_checked_default($(\'#check_show_new_components\'),{checked:\'has_checked\'}),
        });'
    ))
  ));
  $html.=components('lcp_popup_default',array(
    'id_popup' => 'show_new_components',
    'title' => 'Добавить компонент',
    'content_html' => $popup_content,
    'popup_show' => 'show_new_components',
    'popup_close' => 'show_new_components_close',
    'height' => '400px',
    'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));
?>
