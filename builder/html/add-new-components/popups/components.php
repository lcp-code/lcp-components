<?php
function components_arr(){
  $show_components_arr=array();
  $k=0;
  $show_components_arr['title'][$k]='Текстовое поле';
  $show_components_arr['desc'][$k]='Обычное текстовое поле для заполнения данных';
  $show_components_arr['funct'][$k]='lcp_text_default';
  $show_components_arr['field_arr'][$k]=array('placeholder','title','label','label_color');
  $show_components_arr['field_title_arr'][$k]=array('Подсказка (исчезает при клике)','Всплывающая подсказка','Наименование текстового поля','Цвет для наименования');
  $k++;
  $show_components_arr['title'][$k]='Текстовая область';
  $show_components_arr['desc'][$k]='Большое текстовое поле с возможностью переноса строк';
  $show_components_arr['funct'][$k]='lcp_memo_default';
  $show_components_arr['field_arr'][$k]=array('placeholder','title');
  $show_components_arr['field_title_arr'][$k]=array('Подсказка (исчезает при клике)','Всплывающая подсказка');
  $k++;
  $show_components_arr['title'][$k]='Телефон';
  $show_components_arr['desc'][$k]='Текстовое поле с маской для ввода номера телефона';
  $show_components_arr['funct'][$k]='lcp_phone_default';
  $show_components_arr['field_arr'][$k]=array('placeholder','title');
  $show_components_arr['field_title_arr'][$k]=array('Подсказка (исчезает при клике)','Всплывающая подсказка');
  $k++;
  $show_components_arr['title'][$k]='Адрес / Местонахождение (Яндекс)';
  $show_components_arr['desc'][$k]='Для отображения адреса и меток на яндекс карте';
  $show_components_arr['funct'][$k]='lcp_address_default';
  $show_components_arr['field_arr'][$k]=array('placeholder','title');
  $show_components_arr['field_title_arr'][$k]=array('Подсказка (исчезает при клике)','Всплывающая подсказка');
  return $show_components_arr;
}
?>
