<?php
  $html.=components('lcp_ajax',array(
    'function_name'=>'add_component_on_id',
    'begin_html'=>'
    var ids=param[\'ids\'];
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/components/builder/html/add-new-components/ajax-add-new-components.php',
    'data_js'=>'{
      action:\'add_component_on_id\',
      ids:ids,
    }',
    'lcp_success_true'=>'
    lcp_loading_default({loading_show:\'false\'});
    $(data.string).appendTo(\'#new_components_html\');
    ',
    'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});',
    'error'=>'lcp_loading_default({loading_show:\'false\'});',
    'show_error'=>'false',
  ));
?>
