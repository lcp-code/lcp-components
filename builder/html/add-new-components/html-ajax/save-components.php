<?php
  $html.=components('lcp_ajax',array(
    'function_name'=>'save_add_components',
    'begin_html'=>'
    var ids=param[\'ids\'];
    var value=param[\'value\'];
    var title=param[\'title\'];
    var description=param[\'description\'];
    var function_name=param[\'function_name\'];
    var status=param[\'status\'];
    var always=param[\'always\'];
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/components/builder/html/add-new-components/ajax-add-new-components.php',
    'data_js'=>'{
      action:\'save_add_components\',
      ids:ids,
      value:value,
      title:title,
      description:description,
      function_name:function_name,
      status:status,
      always:always,
      }',
      'lcp_success_true'=>'
      lcp_loading_default({loading_show:\'false\'});
      if (data.string.action==\'add\'){
        $(data.string.html).appendTo(\'#new_components_html\');
      } else
      if (data.string.action==\'edit\'){
        alert(\'edit\');
      }
      show_new_components_close();
      ',
      'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});',
      'error'=>'lcp_loading_default({loading_show:\'false\'});'
  ));
?>
