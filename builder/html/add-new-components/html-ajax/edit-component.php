<?php
  $html.=components('lcp_ajax',array(
    'function_name'=>'edit_component',
    'begin_html'=>'
    var status=\''.$this->status.'\';
    var block=\''.$this->block.'\';
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/components/builder/html/add-new-components/ajax-add-new-components.php',
    'data_js'=>'{
      action:\'edit_component\',
      status:status,
      block:block,
    }',
    'lcp_success_true'=>'
    lcp_loading_default({loading_show:\'false\'});
    $(\'#new_components_html\').html(data.string);
    ',
    'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});',
    'error'=>'lcp_loading_default({loading_show:\'false\'});',
    'show_error'=>'false',
  ));
?>
