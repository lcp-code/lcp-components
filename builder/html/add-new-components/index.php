<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_add_component',
  'type' => 'builder',
  'group' => 'html',
  'inner' => array(
    'description' => 'Выбор и добавление существующих компонентов. Обычно размещается для страниц, категорий, товаров (не версии, но можно и для них)',
    'css' => array(
      '0' => '/components/builder/html/add-new-components/add-new-components.css'
    ),
    'js' => array(
      '0' => '/components/builder/html/add-new-components/add-new-components.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'status',
    '1' => 'block',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
  ),
  'set_description_php' => array(
    '0' => 'Статус страницы',
    '1' => 'Массив lcp_add_component из поля block таблицы datas',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>



<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_add_component extends lcp_components implements lcp_components_build{
    public $status;
    public $block;
    public $root_dir;

    function __construct($param=array()){
      $this->status=$param['status'];
      $this->block=$param['block'];
      $this->root_dir=lcp_root_folder();
      parent::__construct($param);
    }

    function __init_component(){
      $index_arr=array();
      for ($i=0; $i < count($this->block); $i++) {
        $index_arr[$this->block[$i]['id']]=$i;
      }
      $this->block['index']=$index_arr;
    }

    function __build_component(){
      $html='';
      // $this->class='lcp_add_component '.$this->class;
      // $attr_str=$this->__get_attributes_html();
      // $html.='<a '.$attr_str.'>'.$this->value.'</a>';

      $html.='<div id="new_components_html">';
      $html.=$this->get_html_ajax();
      $html.='</div>';
      $html.=components('lcp_button_white',array(
        'title'=>'Добавить новый компонент',
        'value'=>'<i class="fa fa-plus" aria-hidden="true"></i>&emsp;<b>Добавить компонент</b>',
        'onclick'=>'show_components();',
      ));
      $html.=$this->get_html_popups();
      $html.='<script>';
      $html.='var lcp_add_component = new LCP_Add_Component();';
      $html.='</script>';
      return $html;
    }

    function get_html_ajax(){
      $html='';
      include_once($this->root_dir.'components/builder/html/add-new-components/html-ajax/get-all-components.php');
      include_once($this->root_dir.'components/builder/html/add-new-components/html-ajax/add-component-on-id.php');
      include_once($this->root_dir.'components/builder/html/add-new-components/html-ajax/save-components.php');
      include_once($this->root_dir.'components/builder/html/add-new-components/html-ajax/edit-component.php');
      return $html;
    }

    function get_html_popups(){
      $html='';
      include_once($this->root_dir.'components/builder/html/add-new-components/popups/get-list.php');
      include_once($this->root_dir.'components/builder/html/add-new-components/popups/add-component.php');
      include_once($this->root_dir.'components/builder/html/add-new-components/popups/edit-component.php');
      include_once($this->root_dir.'components/builder/html/add-new-components/popups/get-install-list.php');
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_add_component_init($param=array()){
    $lcp_obj=new lcp_add_component($param);
    return $lcp_obj->html();
  }
}
?>
