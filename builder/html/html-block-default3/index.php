<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_html_block_default3',
  'type' => 'builder',
  'group' => 'html',
  'inner' => array(
    'description' => 'Блок без границ с белым фоном, где располагаются HTML-элементы. Основано на компоненте lcp_html_block_default2',
    'css' => array(
      '0' => '/components/builder/html/html-block-default3/html-block-default3.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'id_title',
    '2' => 'id_html',
    '3' => 'title',
    '4' => 'html'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => 'Название блока',
    '4' => 'Описание'
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Идентификатор (тег id) для элемента title',
    '2' => 'Идентификатор (тег id) для элемента html',
    '3' => 'Название блока',
    '4' => 'Описание. Можно использовать html'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
$css_install_arr[]='/components/builder/html/html-block-default3/html-block-default3.css';
// $js_install_arr[]='/components/builder/html/html-block-default3/html-block-default3.js';
?>




<?php
function lcp_html_block_default3($param=''){
  // $id_components=getcelldb('id','id','components','name=\'lcp_html_block_default2\'');
  $html='';
  $param['class']=$param['class'].' lcp_html_block_default3 ';
  $html=components('lcp_html_block_default2',$param);
  return $html;
}
?>
