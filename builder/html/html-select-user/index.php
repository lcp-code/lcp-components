<?php
// ini_set('error_reporting', E_ALL & ~E_NOTICE);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_html_select_user',
  'type' => 'builder',
  'group' => 'html',
  'inner' => array(
    'description' => 'Выпадающее окно для выбора и добавления нового пользователя',
    'css' => array('/components/builder/html/html-select-user/html-select-user.css'),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'onchange',
  ),
  'set_value_php' => array(
    '0' => 'lcp_html_select_user',
    '1' => '',
    '2' => '',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Значение в виде id из таблицы userlist',
    '2' => 'Событие, возникающее при изменении (выборе или добавлении пользователя)',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(
    '0'=>'get_lcp_html_select_user',
  ),
  'get_description_js' => array(
    '0'=>'Функция возвращает id выбранного пользователя. Например, get_lcp_html_select_user();',
  )
);
}
$css_install_arr[]='/components/builder/html/html-select-user/html-select-user.css';
$js_install_arr[]='/components/builder/html/html-select-user/html-select-user.js';
?>



<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_html_select_user extends lcp_components implements lcp_components_build{
    public $id='lcp_html_select_user';
    public $value='';
    public $onchange='';

    function __construct($param=array()){
      $this->id=isset($param['id'])?$param['id']:$this->id;
      $this->value=isset($param['value'])?$param['value']:$this->value;
      $this->onchange=isset($param['onchange'])?$param['onchange']:$this->onchange;
      parent::__construct($param);
    }

    function __build_component(){
      $html='';
      $html.='<div class="select_client_id" id="select_client_id" data-id="'.$this->value.'">';
      $client_arr=getarrayvaluedb(array('id','name','surname','patronymic'),'id,name,surname,patronymic','userlist','id=\''.$this->value.'\'','','1');
      $client_arr['id']=$client_arr['id']['0'];
      $client_arr['name']=$client_arr['name']['0'];
      $client_arr['surname']=$client_arr['surname']['0'];
      $client_arr['patronymic']=$client_arr['patronymic']['0'];
      $html.=components('lcp_html_block_default2',array(
        'class'=>'block_pdf_drop',
        'title'=>'<b>Клиент</b>',
        'html'=>components('lcp_button_white',array(
        'id'=>'btn_select_client'.$this->id,
        'class'=>(!empty($client_arr['id'])?' btn_select_client':''),
        'value'=>(!empty($client_arr['id'])?$client_arr['name'].' '.$client_arr['surname'].' '.$client_arr['patronymic']: '-- ВЫБРАТЬ КЛИЕНТА --'),
        'onclick'=>'lcp_html_select_user_get_tbl'.$this->id.'();popup_select_client'.$this->id.'();',
        'style'=>'width:100%;box-sizing: border-box;',
        )),
    ));
    $html.='</div>';

    $popup_content='';
    $popup_content.=components('lcp_text_default',array(
        // 'label'=>'Поиск',
        'placeholder'=>'Искать по ФИО / Телефон / Email',
        'onchange'=>'lcp_html_select_user_get_tbl'.$this->id.'({
            \'like\':$(this).val(),
        });',
    ));
    $popup_content.='<div class="tbl_select_client" id="tbl_select_client"></div>';
    $popup_content.='<div style="height:10px;"></div>';
    $popup_content.=components('lcp_button_white',array(
        'value'=>'Добавить пользователя',
        'onclick'=>'popup_add_client'.$this->id.'();',
      ));
    $html.= components('lcp_popup_default',array(
    'id_popup' => 'popup_select_client'.$this->id,
    'title' => 'Все пользователи',
    'content_html' => $popup_content,
    'popup_show' => 'popup_select_client'.$this->id,
    'popup_close' => 'popup_select_client_close'.$this->id,
    'height' => '400px',
    'width' => '700px',
    'title_setting' => array(
    'font-size' => '19px',
    'font-weight' => 'bold',
    'text-align' => 'left'
        )
    ));

      $html.=components('lcp_ajax',array(
        'function_name'=>'lcp_html_select_user_get_tbl'.$this->id,
        'begin_html'=>'
            lcp_loading_default({loading_show:\'true\'});',
        'url'=>'/components/builder/html/html-select-user/ajax-html-select-user.php',
        'data_js'=>'{
          action:\'get_tbl\',
          id_component:\''.$this->id.'\',
          like:param[\'like\'],
          pageid:param[\'pageid\'],
        }',
        'lcp_success_true'=>'
            lcp_loading_default({loading_show:\'false\'});$(\'#tbl_select_client\').html(data.string.html);
        ',
        'lcp_success_false'=>'lcp_message(data.string,\'false\');lcp_loading_default({loading_show:\'false\'});',
        'error'=>'lcp_message(\'Ошибка AJAX!\',\'false\');lcp_loading_default({loading_show:\'false\'});',
        'show_error'=>'false',
      ));
      $html.='<script>';
      $html.='function next_page(param){';
      $html.='lcp_html_select_user_get_tbl'.$this->id.'(param);';
      $html.='}';
      $html.='function select_id_client_'.$this->id.'(param){';
      $html.='$(\'#btn_select_client'.$this->id.'\').html(param[\'fio\']);';
      $html.='$(\'#btn_select_client'.$this->id.'\').addClass(\'btn_select_client\');';
      $html.='$(\'#select_client_id\').attr(\'data-id\',param[\'id\']);';
      $html.=(!empty($this->onchange))?$this->onchange:'';
      $html.='}';
      $html.='</script>';


      // окно для добавления пользователя
      $popup_content='';
      $popup_content.=components('lcp_text_default',array(
        'id'=>'select_client_surname',
        'value'=>$surname,
        'label'=>'Фамилия',
        'label_color'=>'#009e00',
        'class'=>'',
        'autocomplete'=>'off',
      ));
    $popup_content.='<div style="height:10px"></div>';
    $popup_content.=components('lcp_text_default',array(
        'id'=>'select_client_name',
        'value'=>$name,
        'label'=>'Имя*',
        'label_color'=>'#009e00',
        'class'=>'',
        'autocomplete'=>'off',
    ));
    $popup_content.='<div style="height:10px"></div>';
    $popup_content.=components('lcp_text_default',array(
        'id'=>'select_client_patronymic',
        'value'=>$patronymic,
        'label'=>'Отчество',
        'label_color'=>'#009e00',
        'class'=>'',
        'autocomplete'=>'off',
    ));
    $popup_content.='<div style="height:10px"></div>';
    $popup_content.=components('lcp_address_default',array(
        'id'=>'select_client_address',
        'value'=>'',
        'placeholder'=>'Адрес',
        'label'=>'',
        'label_color'=>'#009e00',
        'class'=>'',
    ));
    $popup_content.='<div style="height:10px"></div>';
    $popup_content.=components('lcp_text_default',array(
        'id'=>'select_client_phone1',
        'value'=>$phone_arr['0'],
        'label'=>'Телефон*',
        'label_color'=>'#009e00',
        'class'=>'',
        'autocomplete'=>'off',
    ));
    $popup_content.='<div style="height:10px"></div>';
    $popup_content.=components('lcp_text_default',array(
        'id'=>'select_client_phone2',
        'value'=>$phone_arr['1'],
        'label'=>'Дополнительный телефон',
        'label_color'=>'#009e00',
        'class'=>'',
        'autocomplete'=>'off',
    ));
    $popup_content.='<div style="height:10px"></div>';
    $popup_content.=components('dropdown_white',array(
        'id'=>'select_client_type_person',
        'value'=>'1',
        'label'=>'Тип персоны',
        'label_color'=>'#009e00',
        'class'=>'',
        'title_child_arr'=>array('Физическое лицо','Юридическое лицо','Индивидуальный предприниматель'),
        'value_child_arr'=>array('1','2','3'),
    ));
    $popup_content.='<div style="height:20px"></div>';
    $popup_content.=components('lcp_button_green',array(
        'value'=>'Добавить',
        'onclick'=>'lcp_html_add_user_'.$this->id.'();',
    ));
      $html.= components('lcp_popup_default',array(
      'id_popup' => 'popup_add_client'.$this->id,
      'title' => 'Добавить пользователя',
      'content_html' => $popup_content,
      'popup_show' => 'popup_add_client'.$this->id,
      'popup_close' => 'popup_add_client_close'.$this->id,
      'height' => '800px',
      'width' => '600px',
      'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
          )
      ));

      $html.=components('lcp_ajax',array(
        'function_name'=>'lcp_html_add_user_'.$this->id,
        'begin_html'=>'
            lcp_loading_default({loading_show:\'true\'});',
        'url'=>'/components/builder/html/html-select-user/ajax-html-select-user.php',
        'data_js'=>'{
          action:\'add_user\',
          surname:$(\'#select_client_surname\').val(),
          name:$(\'#select_client_name\').val(),
          patronymic:$(\'#select_client_patronymic\').val(),
          phone1:$(\'#select_client_phone1\').val(),
          phone2:$(\'#select_client_phone2\').val(),
          type_person:get_dropdown_default2_opt_value(\'select_client_type_person\'),
          address:getAddress(\'select_client_address\'),
        }',
        'lcp_success_true'=>'
            lcp_loading_default({loading_show:\'false\'});
            popup_add_client_close'.$this->id.'();
            popup_select_client_close'.$this->id.'();
            select_id_client_'.$this->id.'({
                \'id\':data.string.id,
                \'fio\':data.string.fio,
            });
        ',
        'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});',
        'error'=>'lcp_loading_default({loading_show:\'false\'});',
      ));
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_html_select_user_init($param=array()){
    $lcp_obj=new lcp_html_select_user($param);
    return $lcp_obj->html();
  }
}
?>