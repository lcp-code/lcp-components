<?php
// ini_set('error_reporting', E_ALL & ~E_NOTICE);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
if (($_LCP_AJAX!=="AJAX")||($_LCP_AJAX_TIME!=md5(getdater()))){header("HTTP/1.0 404 Not Found");exit("HTTP/1.0 404 Not Found");}

include_once($root_folder.'panel/modules/md5.php');





if ($_POST['action']=='get_tbl'){
    $id_component=trim($_POST['id_component']);
    $like=trim($_POST['like']);
    $_GET['pageid']=trim($_POST['pageid']);


    $sql='';
    if (!empty($like)){
        $sql.='
         AND (
            name LIKE \''.$like.'%\' OR
            surname LIKE \''.$like.'%\' OR
            patronymic LIKE \''.$like.'%\' OR
            phone LIKE '.(!empty(getphone($like))?'\''.getphone($like).'%\'':'NULL').' OR
            email LIKE \''.$like.'%\'
         )
         ';
    }

  $result_getarrayvaluedbtbl = getarrayvaluedbtbl(array('id','name','surname','patronymic','phone','email'),'id,name,surname,patronymic,phone,email','userlist','active=1'.$sql,'BY surname','10');

    $html='';
    $html.=components('lcp_table_default',array(
        'sql'=>$result_getarrayvaluedbtbl['sql'],
        'sql_show_title_bool'=>'true',
        'column_check_bool'=>'false',
        'class'=>'tbl_main_content_view',
        'thead'=>array('ФИО','Телефон','Email',''),
        'thead_fields'=>array('[add_pole="0"]','phone','email','[add_pole="1"]'),
        'add_pole'=>array(
            '0'=>'[add_pole_column="surname"] [add_pole_column="name"] [add_pole_column="patronymic"]',
            '1'=>components('lcp_button_green',array(
                'value'=>'Выбрать',
                'onclick'=>'
                    popup_select_client_close'.$id_component.'();
                    select_id_client_'.$id_component.'({
                        \'id\':\'[add_pole_column="id"]\',
                        \'fio\':\'[add_pole_column="surname"] [add_pole_column="name"] [add_pole_column="patronymic"]\',
                    });
                ',
            )),
        ),
        'width_columns'=>array('100%','150px','180px','100px'),
        'text_align_col_thead'=>array('left','center','center','right'),
        'text_align_col_tbody'=>array('left','center','center','right'),
        'alt_body'=>'Пользователей не найдено!',
    ));


    $title_block2 = components('lcp_navigation_default1',array('result_getarrayvaluedbtbl'=>$result_getarrayvaluedbtbl,'ajax_function'=>'next_page'));
    $title_top = '';
    $title_top .= components('lcp_block_one_line',array(
      'tbody'=>array($title_block2),
      'width_columns'=>array('100%'),
      'text_align_col_tbody'=>array('right')
    ));
    $html.=components('lcp_html_block_default1',array('title'=>$title_top));

  $LCP_AJAX_RESULT=array(
    'html'=>$html
  );
} else












if ($_POST['action']=='add_user'){
    $surname=trim($_POST['surname']);
    $name=trim($_POST['name']);
    $patronymic=trim($_POST['patronymic']);
    $phone1=trim($_POST['phone1']);
    $phone2=trim($_POST['phone2']);
    $type_person=trim($_POST['type_person']);
    $address=$_POST['address'];

    if (empty($name)){
        generate_exception('Введите Имя!');
    }
    if (empty($phone1)){
        generate_exception('Введите Телефон!');
    }
    if ((empty($address['pos_x']))||($address['pos_x']=='NaN')){
        generate_exception('Адрес введен не корректно на карте! Попробуйте ещё раз!');
    }
    if ((empty($address['pos_y']))||($address['pos_y']=='NaN')){
        generate_exception('Адрес введен не корректно на карте! Попробуйте ещё раз!');
    }
    if (empty($type_person)){
        generate_exception('Укажите тип персоны! Хотя бы физическое лицо..');
    }

    
    $save_arr=array(
        'name'=>$name,
        'surname'=>$surname,
        'patronymic'=>$patronymic,
        'phone'=>$phone1,
        'phone_additional'=>$phone2,
        'address'=>$address['address'],
        'location_x'=>$address['pos_x'],
        'location_y'=>$address['pos_y'],
        'type_person'=>$type_person,
      );

  // формируем дату создания и изменения
  if (empty($ids)){
    $save_arr['dater_create']=getdatetimer();
  }
  $save_arr['dater_edit']=getdatetimer();

  // формируем идентификатор
  $save_arr['id']=(!empty($ids))?$ids:generateId($save_arr,'userlist');
  $save_arr['md5_edit']=generateMd5($save_arr);
  
  $ids=$save_arr['id'];
  $res=savedb('userlist',$save_arr,'id=\''.$ids.'\'');
  if ($res===false){
    generate_exception('Ошибка при сохранении пользователя!');
  }
 $LCP_AJAX_RESULT=array(
    'id'=>$ids,
    'fio'=>$surname.' '.$name.' '.$patronymic,
 );
} else












{
  generate_exception('Ни одна функция не найдена!');
}
?>