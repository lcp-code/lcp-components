function get_html_parent_category_value(param){
  if (typeof param==='undefined'){
    var param='';
  }
  var id_obj=param['id_obj'];
  if (typeof id_obj==='undefined'){
    id_obj='lcp_html_parent_rubrik';
  }
  var id_catalog=$('#'+id_obj).attr('data-id');
  return id_catalog;
}
