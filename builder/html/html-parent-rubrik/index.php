<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_html_parent_rubrik',
  'type' => 'builder',
  'group' => 'html',
  'inner' => array(
    'description' => 'Блок для выбора рубрик',
    'css' => array(
      '0' => '/components/builder/html/html-parent-rubrik/html-parent-rubrik.css'
    ),
    'js' => array(
      '0' => '/components/builder/html/html-parent-rubrik/html-parent-rubrik.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'id_datas',
    '2' => 'id_except'
  ),
  'set_value_php' => array(
    '0' => 'lcp_html_parent_rubrik',
    '1' => '1',
    '2' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'ID текущей записи в таблице datas',
    '2' => 'Исключить ID (только одно значение)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
  ),
  'set_value_js' => array(
  ),
  'set_description_js' => array(
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}
?>




<?php
function lcp_html_parent_rubrik($param=''){
  $ids=$param['id'];
  $id_datas=$param['id_datas'];
  $id_except=$param['id_except'];
  $default_ids='lcp_html_parent_rubrik';
  $default_id_datas='1';
  $default_id_except='';
  $id_datas=(!empty($id_datas))?$id_datas:$default_id_datas;
  $ids=(!empty($ids))?$ids:$default_ids;
  $id_except=(!empty($id_except))?$id_except:$default_id_except;

  $html='';
  $html.='<div id="'.$ids.'" class="lcp_html_parent_rubrik" data-id="'.$id_datas.'">';
  $html.='<div class="lcp_html_parent_rubrik_block">';
  $html.=components('lcp_html_navigation',array(
    'id_datas' => $id_datas,
    'onclick'=>'show_tree_category_'.$ids.'({id_catalog:$(\'#'.$ids.'\').attr(\'data-id\')});popup_tree_category_'.$ids.'();'
  ));
  $html.='</div>';
  $html.='<div class="lcp_html_parent_rubrik_button">';
  $html.=components('lcp_button_default',array(
    'value' => 'Выбрать категорию',
    'onclick'=>'show_tree_category_'.$ids.'({id_catalog:$(\'#'.$ids.'\').attr(\'data-id\')});popup_tree_category_'.$ids.'();',
    'class'=>'lcp_button_category_navigation'
  ));
  $html.='</div>';
  $html.='</div>';









  $popup_content='';
  $popup_content.='<div id="content_tree_category_'.$ids.'"></div>';
  $html.=components('lcp_popup_default',array(
    'id_popup' => 'popup_tree_category_'.$ids,
    'title' => 'Выберите категорию:',
    'content_html' => $popup_content,
    'popup_show' => 'popup_tree_category_'.$ids,
    'popup_close' => 'popup_tree_category_close_'.$ids,
    'height' => '400px',
    'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));




  $html.=components('lcp_ajax',array(
    'function_name'=>'show_tree_category_'.$ids,
    'begin_html'=>'
    var id_catalog=param[\'id_catalog\'];
    if (typeof id_catalog===\'undefined\'){
      var id_catalog=\''.$id_datas.'\';
      }
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/components/builder/html/html-parent-rubrik/ajax-html-parent-rubrik.php',
    'data_js'=>'{
      action:\'show_tree_category\',
      id_catalog:id_catalog,
      id_obj:\''.$ids.'\',
      id_except:\''.$id_except.'\'
    }',
    'lcp_success_true'=>'$(\'#content_tree_category_'.$ids.'\').html(data.string[\'html\']);lcp_loading_default({loading_show:\'false\'});',
    'lcp_success_false'=>'lcp_message(data.string,\'false\');lcp_loading_default({loading_show:\'false\'});',
    'error'=>'lcp_message(\'Ошибка AJAX!\',\'false\');lcp_loading_default({loading_show:\'false\'});',
    'show_error'=>'false'
  ));


  $html.=components('lcp_ajax',array(
    'function_name'=>'get_tree_'.$ids,
    'begin_html'=>'
    var id_catalog=param[\'id_catalog\'];
    if (typeof id_catalog===\'undefined\'){
      var id_catalog=\''.$id_datas.'\';
      }
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/components/builder/html/html-parent-rubrik/ajax-html-parent-rubrik.php',
    'data_js'=>'{
      action:\'get_tree\',
      id_catalog:id_catalog,
      id_obj:\''.$ids.'\'
    }',
    'lcp_success_true'=>'$(\'#'.$ids.'>.lcp_html_parent_rubrik_block\').html(data.string[\'html\']);popup_tree_category_close_'.$ids.'();lcp_loading_default({loading_show:\'false\'});',
    'lcp_success_false'=>'lcp_message(data.string,\'false\');lcp_loading_default({loading_show:\'false\'});',
    'error'=>'lcp_message(\'Ошибка AJAX!\',\'false\');lcp_loading_default({loading_show:\'false\'});',
    'show_error'=>'false'
  ));


  // $html.='<div class="dropdown_default2 '.$class.'" '.$id_dropdown.$name.$onclick.$onmousedown.$onmouseup.$width.'>';
  return $html;
}
?>
