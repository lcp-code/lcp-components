<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_address_default',
  'type' => 'builder',
  'group' => 'Текстовое поле',
  'inner' => array(
    'description' => 'Текстовое поле, используемое в панели администратора по-умолчанию',
    'css' => array(
      // '0' => '/components/system/texts/text-default1/text-default1.css'
    ),
    'js' => array(
      '0' => '/components/builder/texts/lcp_address_default/lcp-address-default.js',
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'placeholder',
    '3' => 'class',
    '4' => 'onclick',
    '5' => 'onchange',
    '6' => 'onmouseover',
    '7' => 'onmouseout',
    '8' => 'onkeydown',
    '9' => 'onkeyup',
    '10' => 'onkeypress',
    '11' => 'onfocus',
    '12' => 'onblur',
    '13' => 'type',
    '14' => 'disabled',
    '15' => 'width',
    '16' => 'title',
    '17' => 'autocomplete',
    '18' => 'label',
    '19' => 'label_color',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '',
    '11' => '',
    '12' => '',
    '13' => 'text',
    '14' => '',
    '15' => '100%',
    '16' => '',
    '17' => '',
    '18' => '',
    '19' => '',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id). Обязательно для заполнения.',
    '1' => 'Текст внутри поля',
    '2' => 'Подсказка, отображаемая внутри поля (тег placeholder)',
    '3' => 'Класс для изменения стилей (тег class)',
    '4' => 'Событие, возникающее при нажатии мышкой на поле (тег onclick)',
    '5' => 'Событие, возникающее при изменении текста в поле (тег onchange)',
    '6' => 'Событие, возникающее при наведении указателя мыши на объект (тег onmouseover)',
    '7' => 'Событие, возникающее, когда указатель мыши уходит с объекта (тег onmouseout)',
    '8' => 'Событие, возникающее при нажатии клавиши (тег onkeydown)',
    '9' => 'Событие, возникающее при отпускании клавиши (тег onkeyup)',
    '10' => 'Событие, возникающее при печатании (тег onkeypress)',
    '11' => 'Событие, возникающее при наведении фокуса на текстовое поле (тег onfocus)',
    '12' => 'Событие, возникающее при исчезании фокуса из текстового поля (тег onblur)',
    '13' => 'Тип текстового поля, например (text, number, password, email, phone и др.)',
    '14' => 'Активность текстового поля (true - заморозить объект; в других случаях объект активен)',
    '15' => 'Ширина текстового поля',
    '16' => 'Всплывающая подсказка',
    '17' => 'Если off - не запоминать значения, и не отображать во всплывающих подсказках',
    '18' => 'Чем-то похоже на placeholder, но больше подходит для наименования текстового поля',
    '19' => 'Цвет Label, при активном фокусе на текстовое поле',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
// $css_install_arr[]='/components/builder/texts/lcp_address_default/lcp-address-default.css';
$js_install_arr[]='/components/builder/texts/lcp_address_default/lcp-address-default.js';
?>




<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_address_default extends lcp_text_default implements lcp_components_build{
    private $address;
    private $pos_x;
    private $pos_y;

    function __init_component(){
      $this->address=$this->value['address'];
      $this->pos_x=$this->value['pos_x'];
      $this->pos_y=$this->value['pos_y'];
      $this->value=$this->address;
      $this->class='lcp_address_default '.$this->class;
      $this->onchange='geocode();'.$this->onchange;
    }

    function __build_after_component($html){
// $html.='<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>';
$html.='<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=adc9b9bd-f606-4f6b-bba2-7e351d8be8a5" type="text/javascript"></script>';

$html.='<div id="map_'.$this->id.'" style="width: 100%; height:300px; margin-top:10px;"></div>';




      $html.="
<script>
      ymaps.ready(init);

      function init() {
          // Подключаем поисковые подсказки к полю ввода.
          var suggestView = new ymaps.SuggestView('".$this->id."'),
              map,
              placemark;

          // // При клике по кнопке запускаем верификацию введёных данных.
          // onchange
          //
          //
          // $('#".$this->id."').bind('change', function (e) {
          //     geocode();
          // });
          initMap();
        }

          function geocode() {
              // Забираем запрос из поля ввода.
              var request = $('#".$this->id."').val();
              // Геокодируем введённые данные.
              var obj;
              ymaps.geocode(request).then(function (res) {
                  obj = res.geoObjects.get(0),
                      error, hint;

              }, function (e) {
                  console.log(e)
              });
              setTimeout(function () {
                console.log(obj);
                showResult(obj);
              }, 1000);
          }
          function showResult(obj) {
              var mapContainer = $('#map_".$this->id."'),
                  bounds = obj.properties.get('boundedBy'),
                  mapState = ymaps.util.bounds.getCenterAndZoom(
                      bounds,
                      [mapContainer.width(), mapContainer.height()]
                  ),
                  address = [obj.getCountry(), obj.getAddressLine()].join(', '),
                  shortAddress = [obj.getThoroughfare(), obj.getPremiseNumber(), obj.getPremise()].join(' ');
              mapState.controls = [];
              createMap(mapState, shortAddress);
              // Выводим сообщение под картой.
              // showMessage(address);
          }

          function createMap(state, caption) {
              // Если карта еще не была создана, то создадим ее и добавим метку с адресом.
              if (typeof map==='undefined') {
                  map = new ymaps.Map('map_".$this->id."', state);
                  placemark = new ymaps.Placemark(
                      map.getCenter(), {
                          iconCaption: caption,
                          balloonContent: caption
                      }, {
                          preset: 'islands#redDotIconWithCaption'
                      });
                  map.geoObjects.add(placemark);
                  // Если карта есть, то выставляем новый центр карты и меняем данные и позицию метки в соответствии с найденным адресом.
              } else {
                  map.setCenter(state.center, state.zoom);
                  if (typeof placemark==='undefined') {
                    placemark = new ymaps.Placemark(
                        map.getCenter(), {
                            iconCaption: caption,
                            balloonContent: caption
                        }, {
                            preset: 'islands#redDotIconWithCaption'
                        });
                    map.geoObjects.add(placemark);
                  }
                  placemark.geometry.setCoordinates(state.center);
                  placemark.properties.set({iconCaption: caption, balloonContent: caption});
              }
          }

          function initMap() {
              // Создание пустой карты
                  map = new ymaps.Map('map_".$this->id."', {
                		center: [".$this->pos_x.",".$this->pos_y."],
                		zoom: 16
                	},{});
                ".(((!empty($this->pos_x))&&(!empty($this->pos_y)))?"
                placemark = new ymaps.Placemark(
                    map.getCenter(), {
                        iconCaption: '".$this->address."',
                        balloonContent: '".$this->address."',
                    }, {
                        preset: 'islands#redDotIconWithCaption'
                    });
                map.geoObjects.add(placemark);
                ":"")."
          }
</script>


";




















  return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_address_default_init($param=array()){
    $lcp_obj=new lcp_address_default($param);
    return $lcp_obj->html();
  }
}
?>
