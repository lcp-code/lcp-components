<?php
header('Content-Type: application/json');
// ini_set('error_reporting', E_ALL & ~E_NOTICE);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
$root_dir = $_SERVER['DOCUMENT_ROOT'];
$root_folder = '../../../../';

include_once($root_folder.'konsfigli.php');
include_once($root_folder.'includes/connect_db.php');

function generate_exception($string){
  exit(json_encode(array('error' => 'false','string' => $string)));
}


$_LCP_AJAX='AJAX';
include_once($root_dir.'/includes/infosite.php');
include_once($root_dir.'/components/index.php');
$site_lcp = info_site();
$user_lcp = info_user();

$data = array();

if (isset($_GET['loadfile'])){
  if (!empty($_FILES)){
    $folder_polz_str=lcp_root_folder();
    $folder_polz=$folder_polz_str.'polz';
    $files_arr=array();

    for ($iui=0; $iui < count($_FILES); $iui++) {
      if (($_FILES[$iui]['error']!==0)&&(!empty($_FILES[$iui]['name']))){
        generate_exception('Размер некоторых фото превышает '.ini_get('upload_max_filesize').'!');
      } else {
        $temp=$_FILES[$iui]['tmp_name'];
        $img_name=$_FILES[$iui]['name'];

        $tipes_fileses = '.'.end(explode(".", $img_name));
        if ($tipes_fileses=='.php'){
          generate_exception('');
        }
        $img_name=str_replace($tipes_fileses,"",$img_name);
        $img_name=lcp_url_translit(array('url'=>$img_name));
        $img_name=$img_name.$tipes_fileses;
        $path_img=(!empty($_GET['upload']))?$_GET['upload']:$folder_polz.'/'.date('Y').'/'.date('m');
        if (file_exists($path_img)===false)
        mkdir($path_img,0777,true);
        move_uploaded_file($temp,$path_img.'/'.$img_name);
        if ($_GET['download'][strlen($_GET['download'])-1]=='/'){
          $_GET['download']=mb_substr($_GET['download'],0,strlen($_GET['download'])-1,'UTF-8');
        }
        $url_img1=$_GET['download'].'/'.$img_name;
        $url_imgarr[$iui]=$url_img1;
      }
    }
  } else {
    generate_exception('net');
  }
  $files_arr=array(
    'files'=>array(
      'file'=>$url_imgarr,
    )
  );
  exit(json_encode(array('error' => 'true','files_arr' => $files_arr)));
}

generate_exception('Функций не найдено!');
?>
