<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_main_load_files',
  'type' => 'builder',
  'group' => 'Файлы',
  'inner' => array(
    'description' => 'Невизуальный JS скрипт, который отвечает за отправку файлов на сервер и выполнение последующего js кода. Основан на компоненте lcp_load_image.',
    'css' => array(
      // '0' => '/components/system/images/load-photo/load-photo.css'
    ),
    'js' => array(
      // '0' => '/components/system/images/load-photo/load-photo.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'before_load_js',
    '1' => 'after_load_js',
    '2' => 'false_load_js',
    '3' => 'error_load_js',
    '4' => 'upload_folder',
    '5' => 'download_folder',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
  ),
  'set_description_php' => array(
    '0' => '<div>Функция JS, которую нужно выполнить перед загрузкой файлов.</div><div>Например:</div><div>startFunc();</div><div>или</div><div>alert("start");</div>',
    '1' => '<div>Функция JS, которую нужно выполнить сразу после завершения.</div><div>Например:</div><div>makeFunc();</div><div>или</div><div>alert("name");</div>',
    '2' => '<div>Функция JS, которую нужно выполнить при неуспешном выполнении AJAX.</div><div>Например:</div><div>makeFunc();</div><div>или</div><div>alert("name");</div>',
    '3' => '<div>Функция JS, которую нужно выполнить при ошибке AJAX.</div><div>Например:</div><div>makeFunc();</div><div>или</div><div>alert("name");</div>',
    '4' => '<div>Относительный путь для загрузки файла. Указывается только каталог.</div><div>Например:</div><div>../../../../polz/'.date(Y).'/'.date(m).'/</div>',
    '5' => '<div>Относительный путь загруженного файла (с возможностью его скачать). Указывается только каталог.</div><div>Например:</div><div>/polz/'.date(Y).'/'.date(m).'/</div>',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает JS-скрипт для работы с компонентом'
  ),
  'set_name_js' => array(
    '0' => 'files',
  ),
  'set_value_js' => array(
    '0' => '',
  ),
  'set_description_js' => array(
    '0' => 'Массив с путями файлов, которые нужно отправить на сервер (Этот массив можно получить следующим образом: document.getElementById(ids).files, где ids - идентификатор поля <pre style="display:inline-block;margin: 0px;">'.htmlspecialchars('<input type="file">',ENT_QUOTES,'UTF-8',false).'</pre>)',
  ),
  'get_js' => array(
    // '0' => 'array'
  ),
  'get_description_js' => array(
    // '0' => 'Если get установлено в значение "get_load_photos", то возвращает путь с фото'
  )
);
}
?>




<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_main_load_files extends lcp_load_image implements lcp_components_build{
    private $ajax_path_php;

    function __init_component(){
      $path_php=$this->__get_php_path();
      $this->ajax_path_php='/'.str_replace('index.php','ajax-load-photo-server.php?loadimg=true',$path_php);
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_main_load_files_init($param=array()){
    $lcp_obj=new lcp_main_load_files($param);
    return $lcp_obj->html();
  }
}
?>
