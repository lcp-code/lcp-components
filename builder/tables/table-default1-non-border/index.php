<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_table_default_non_border',
  'type' => 'builder',
  'group' => 'Таблица',
  'inner' => array(
    'description' => 'Таблица, основанная на компоненте lcp_table_default. Без границ.',
    'css' => array(
      '0' => '/components/builder/tables/table-default1-non-border/table-default1-non-border.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
  ),
  'set_value_php' => array(
  ),
  'set_description_php' => array(
  ),
  'get_php' => array(
  ),
  'get_description_php' => array(
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>





<?php
function lcp_table_default_non_border($param=""){
  $html='';
  $param['class']=$param['class'].' lcp_table_default_non_border ';
  $html=lcp_table_default($param);
  return $html;
}
?>
