<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_table_default_builder',
  'type' => 'builder',
  'group' => 'Таблица',
  'inner' => array(
    'description' => 'Таблица, основанная на компоненте lcp_table_default (дописать, чем отличается)',
    'css' => array(
      '0' => '/components/builder/tables/table-default1-builder/table-default1-builder.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'thead',
    '1' => 'tbody',
    '2' => 'tfoot',
    '3' => 'width_columns',
    '4' => 'text_align_col_thead',
    '5' => 'text_align_col_tbody',
    '6' => 'text_align_col_tfoot',
    '7' => 'text_valign_col_thead',
    '8' => 'text_valign_col_tbody',
    '9' => 'text_valign_col_tfoot',
    '10' => 'column_check_bool',
    '11' => 'sql',
    '12' => 'thead_fields',
    '13' => 'add_pole',
    '14' => 'sql_show_title_bool',
    '15' => 'sortible',
    '16' => 'limit'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => 'left',
    '5' => 'left',
    '6' => 'left',
    '7' => 'middle',
    '8' => 'middle',
    '9' => 'middle',
    '10' => 'false',
    '11' => '',
    '12' => '',
    '13' => '',
    '14' => 'false',
    '15' => 'false',
    '16' => ''
  ),
  'set_description_php' => array(
    '0' => 'Массив колонок с заголовками для тега thead',
    '1' => 'Массив колонок с основным текстом ячеек для тега tbody',
    '2' => 'Массив колонок с текстом ячеек для тега tfoot',
    '3' => 'Массив со значениями ширины колонок',
    '4' => 'Массив с выравниванием текста по горизонтали для тега thead',
    '5' => 'Массив с выравниванием текста по горизонтали для тега tbody',
    '6' => 'Массив с выравниванием текста по горизонтали для тега tfoot',
    '7' => 'Массив с выравниванием текста по вертикали для тега thead',
    '8' => 'Массив с выравниванием текста по вертикали для тега tbody',
    '9' => 'Массив с выравниванием текста по вертикали для тега tfoot',
    '10' => '<div>true - отображать галочки в первой колонке</div><div>false - скрыть галочки в первой колонке</div>',
    '11' => 'SQL-запрос для отображения содержимого ячеек для тега tbody',
    '12' => 'Массив полей для связи заголовка thead с именем столбца в БД MySQL. Для указания нового поля из add_pole, используйте: <b>[add_pole="0"]</b> , где 0 - номер записи массива из add_pole.',
    '13' => 'Массив содержимого нового поля (шаблон для всех строк). Для указания значения id, используйте: [add_pole_id]',
    '14' => '<div>true - отображать заголовки SQL-запроса</div><div>false - не отображать заголовки SQL-запроса</div>',
    '15' => '<div>true - сортировать строки внутри таблицы, перетаскивая их вручную</div><div>false - не сортировать строки в таблице</div>',
    '16' => 'Ограничение на количество отображаемых строк (ограничения в sql-запросах игнорируются)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_table_default_builder($param=""){
  $html='';
  $param['class']=$param['class'].' table_default_builder ';
  $html=components('lcp_table_default',$param);
  return $html;
}
?>
