<?php
// Для использования - вызовите функцию с имененем компонента. Если необходимо - передавайте параметры в скобках. Пример: defalt_button1(array('value'=>'Название кнопки'))
$kesh_html = ($kesh==0) ? '?ver='.date('Y-m-d-H-i-s') : '';
$install='false';
prov_files_include($root_dir.'/components/classes/lcp-components.class.php');

$components_all=array();
$components_all['puth_php'][]='components/system/html/lcp-head/index.php';
$components_all['puth_php'][]='components/system/checkeds/checked-default1/index.php';
$components_all['puth_php'][]='components/system/checkeds/main-checked-default1/index.php';
$components_all['puth_php'][]='components/system/checkeds/toggle-default1/index.php';
$components_all['puth_php'][]='components/system/messages/message-default1/index.php';
$components_all['puth_php'][]='components/system/popups/popup-default1/index.php';
$components_all['puth_php'][]='components/system/ckeditor/lcp-install.php';
$components_all['puth_php'][]='components/system/texts/password-default1/index.php';
$components_all['puth_php'][]='components/system/dropdowns/dropdowns-default1/index.php';
$components_all['puth_php'][]='components/system/dropdowns/dropdowns-default2/index.php';
$components_all['puth_php'][]='components/system/url/transliterated/index.php';
$components_all['puth_php'][]='components/system/html/block-one-line2/index.php';
$components_all['puth_php'][]='components/system/loadings/loading-default1/index.php';
$components_all['puth_php'][]='components/system/buttons/button-default1/index.php';
$components_all['puth_php'][]='components/system/buttons/lcp-button-minus/index.php';
$components_all['puth_php'][]='components/system/buttons/lcp-button-plus/index.php';
$components_all['puth_php'][]='components/system/tabs/tab-default1/index.php';
$components_all['puth_php'][]='components/system/menu/menu-left/index.php';
$components_all['puth_php'][]='components/system/images/load-photo/index.php';
$components_all['puth_php'][]='components/system/fontawesome/index.php';
$components_all['puth_php'][]='components/system/files/load-files/index.php';
$components_all['puth_php'][]='components/system/video/load-youtube/index.php';
$components_all['puth_php'][]='components/system/charts/index.php';
$components_all['puth_php'][]='components/system/jquery/index.php';
$components_all['puth_php'][]='components/system/texts/date-default1/index.php';
$components_all['puth_php'][]='components/system/texts/date-timer-default1/index.php';
$components_all['puth_php'][]='components/system/tegs/tegs-default1/index.php';
$components_all['puth_php'][]='components/system/specifications/specifications-default1/index.php';
$components_all['puth_php'][]='components/system/files/convert-size-files/index.php';
$components_all['puth_php'][]='components/system/files/copy-files/index.php';
$components_all['puth_php'][]='components/system/files/delete-files/index.php';
$components_all['puth_php'][]='components/system/files/zip-files/index.php';
$components_all['puth_php'][]='components/system/excel/PHPExcel/index.php';
$components_all['puth_php'][]='components/system/excel/PHPXLSXWriter/index.php';
$components_all['puth_php'][]='components/system/excel/SimpleXLSX/index.php';
$components_all['puth_php'][]='components/system/excel/XLSXReader/index.php';
$components_all['puth_php'][]='components/system/filter/filter-default1/index.php';
$components_all['puth_php'][]='components/system/phpQuery/index.php';
$components_all['puth_php'][]='components/system/html/html-block-default2/index.php';
$components_all['puth_php'][]='components/system/html/html-block-default1/index.php';
$components_all['puth_php'][]='components/system/navigations/navigation-default2/index.php';
$components_all['puth_php'][]='components/system/navigations/navigation-default1/index.php';
$components_all['puth_php'][]='components/system/html/block-one-line/index.php';
$components_all['puth_php'][]='components/system/html/block-one-line-mobile/index.php';
$components_all['puth_php'][]='components/system/sql/getarrayvaluedbtbl/index.php';
$components_all['puth_php'][]='components/system/url/lcp-url/index.php';
$components_all['puth_php'][]='components/system/tables/table-default1/index.php';
$components_all['puth_php'][]='components/system/texts/memo-default1/index.php';
$components_all['puth_php'][]='components/system/texts/seo/index.php';
$components_all['puth_php'][]='components/system/ajaxs/ajax/index_main_componenters.php';
$components_all['puth_php'][]='components/system/images/load-photo-with-area/index.php';
$components_all['puth_php'][]='components/system/images/load/index.php';
// $components_all['puth_php'][]='components/system/mail/imap/lcp-imap/index.php';
$components_all['puth_php'][]='components/system/html/html-block-drop/index.php';
// $components_all['puth_php'][]='components/system/tables/table-default2/index.php';
$components_all['puth_php'][]='components/system/texts/text-default1/index.php';
$components_all['puth_php'][]='components/system/word/PHPWord/index.php';
$components_all['puth_php'][]='components/system/texts/phone-default1/index.php';
$components_all['puth_php'][]='components/system/texts/phone-default2/index.php';
$components_all['puth_php'][]='components/system/files/load-from-other/install.php';
$components_all['puth_php'][]='components/system/files/send-on-other/index.php';
// $components_all['puth_php'][]='components/system/images/watermark/install.php';
// $components_all['puth_php'][]='components/system/images/resize/install.php';
$components_all['puth_php'][]='components/system/dropdowns/dropdown-multi-check/index.php';
$components_all['puth_php'][]='components/builder/html/html-parent-object/index.php';
$components_all['puth_php'][]='components/builder/html/html-parent-products/index.php';
$components_all['puth_php'][]='components/builder/dropdowns/dropdown-tree/index.php';
$components_all['puth_php'][]='components/builder/dropdowns/dropdown_text/index.php';
$components_all['puth_php'][]='components/builder/tables/table-default1-non-border/index.php';
$components_all['puth_php'][]='components/builder/tables/table-default1-color/index.php';
$components_all['puth_php'][]='components/builder/tables/table-default1-builder/index.php';
$components_all['puth_php'][]='components/builder/html/html-parent-category/index.php';
$components_all['puth_php'][]='components/builder/html/html-parent-article/index.php';
$components_all['puth_php'][]='components/builder/html/html-parent-rubrik/index.php';
$components_all['puth_php'][]='components/builder/html/product-more/index.php';
$components_all['puth_php'][]='components/builder/html/html-block-default3/index.php';
$components_all['puth_php'][]='components/builder/html/product-more-edit/index.php';
$components_all['puth_php'][]='components/builder/html/html-navigation/index.php';
$components_all['puth_php'][]='components/builder/tables/table-default1-non-border2/index.php';
$components_all['puth_php'][]='components/builder/html/html-navigation-recursion/index.php';
$components_all['puth_php'][]='components/builder/tables/table-default1-with-border/index.php';
$components_all['puth_php'][]='components/builder/buttons/lcp-button-red/index.php';
$components_all['puth_php'][]='components/builder/buttons/lcp-button-green/index.php';
// $components_all['puth_php'][]='components/builder/files/load/index.php';
$components_all['puth_php'][]='components/builder/dropdowns/dropdown-white/index.php';
$components_all['puth_php'][]='components/builder/buttons/lcp-button-white/index.php';
$components_all['puth_php'][]='components/builder/texts/lcp_address_default/index.php';
// $components_all['puth_php'][]='components/builder/html/add-new-components/index.php';
// $components_all['puth_php'][]='components/builder/html/html-parser-one/index.php';
$components_all['puth_php'][]='components/builder/dropdowns/dropdown-multi-check-white/index.php';
$components_all['puth_php'][]='components/builder/buttons/lcp-button-green2/index.php';
$components_all['puth_php'][]='components/builder/buttons/lcp-button-red2/index.php';
$components_all['puth_php'][]='components/system/aws/index.php';
$components_all['puth_php'][]='components/builder/html/html-select-user/index.php';

// $components_all=getarrayvaluedb(array('0'=>'puth_php'),'puth_php','components','','BY type DESC');
$components_all_count=count($components_all['puth_php']);
for ($i=0; $i < $components_all_count; $i++) {
  prov_files_include($root_dir.'/'.$components_all['puth_php'][$i]);
  // exit($components_all['puth_php'][$i]);
  // generate_exception($components_all['puth_php'][$i]);
}
$components_all_css=array();
$components_all_css['puth'][]='components/builder/html/html-parent-object/html-parent-object.css';
$components_all_css['puth'][]='components/builder/html/html-parent-products/html-parent-products.css';
$components_all_css['puth'][]='components/system/checkeds/checked-default1/checked-default1.css';
$components_all_css['puth'][]='components/system/checkeds/toggle-default1/toggle-default1.css';
$components_all_css['puth'][]='components/system/messages/message-default1/message-default1.css';
$components_all_css['puth'][]='components/system/popups/popup-default1/popup-default1.css';
$components_all_css['puth'][]='components/builder/dropdowns/dropdown_text/dropdown_text.css';
$components_all_css['puth'][]='components/system/dropdowns/dropdowns-default1/dropdown-default1.css';
$components_all_css['puth'][]='components/system/dropdowns/dropdowns-default2/dropdown-default2.css';
$components_all_css['puth'][]='components/builder/tables/table-default1-non-border/table-default1-non-border.css';
$components_all_css['puth'][]='components/builder/tables/table-default1-color/table-default1-color.css';
$components_all_css['puth'][]='components/builder/tables/table-default1-builder/table-default1-builder.css';
$components_all_css['puth'][]='components/builder/html/html-parent-category/html-parent-category.css';
$components_all_css['puth'][]='components/builder/html/html-parent-article/html-parent-article.css';
$components_all_css['puth'][]='components/builder/html/html-parent-rubrik/html-parent-rubrik.css';
$components_all_css['puth'][]='components/builder/html/product-more/style.css';
$components_all_css['puth'][]='components/builder/html/html-block-default3/html-block-default3.css';
$components_all_css['puth'][]='components/system/html/block-one-line2/block-one-line2.css';
$components_all_css['puth'][]='components/system/loadings/loading-default1/loading-default1.css';
$components_all_css['puth'][]='components/system/buttons/button-default1/button-default1.css';
$components_all_css['puth'][]='components/system/buttons/lcp-button-minus/button-minus.css';
$components_all_css['puth'][]='components/system/buttons/lcp-button-plus/button-plus.css';
$components_all_css['puth'][]='components/system/tabs/tab-default1/tab-default.css';
$components_all_css['puth'][]='components/system/menu/menu-left/menu-left.css';
$components_all_css['puth'][]='components/system/images/load-photo/load-photo.css';
$components_all_css['puth'][]='components/system/fontawesome/css/font-awesome.min.css';
$components_all_css['puth'][]='components/system/files/load-files/load-files.css';
$components_all_css['puth'][]='components/system/video/load-youtube/load-youtube.css';
$components_all_css['puth'][]='components/system/charts/dist/chart.min.css';
$components_all_css['puth'][]='components/builder/html/product-more-edit/style.css';
$components_all_css['puth'][]='components/system/jquery/jquery/jquery.min.css';
$components_all_css['puth'][]='components/system/texts/date-default1/date-default1.css';
$components_all_css['puth'][]='components/system/texts/date-timer-default1/date-timer-default1.css';
$components_all_css['puth'][]='components/system/texts/date-timer-default1/ui-timepicker.css';
$components_all_css['puth'][]='components/system/tegs/tegs-default1/tegs-default.css';
$components_all_css['puth'][]='components/system/specifications/specifications-default1/spec-default.css';
$components_all_css['puth'][]='components/system/filter/filter-default1/filter-default.css';
$components_all_css['puth'][]='components/builder/html/html-navigation/html-navigation.css';
$components_all_css['puth'][]='components/system/html/html-block-default2/html-block-default2.css';
$components_all_css['puth'][]='components/system/html/html-block-default1/html-block-default1.css';
$components_all_css['puth'][]='components/system/navigations/navigation-default2/navigation-default2.css';
$components_all_css['puth'][]='components/system/navigations/navigation-default1/navigation-default1.css';
$components_all_css['puth'][]='components/system/html/block-one-line/block-one-line.css';
$components_all_css['puth'][]='components/system/html/block-one-line-mobile/block-one-line-mobile.css';
$components_all_css['puth'][]='components/builder/tables/table-default1-non-border2/table-default1-non-border2.css';
$components_all_css['puth'][]='components/builder/html/html-navigation-recursion/html-navigation-recursion.css';
$components_all_css['puth'][]='components/builder/tables/table-default1-with-border/table-default1-with-border.css';
$components_all_css['puth'][]='components/system/tables/table-default1/table-default1.css';
$components_all_css['puth'][]='components/system/texts/memo-default1/memo-default.css';
$components_all_css['puth'][]='components/system/texts/seo/seo.css';
$components_all_css['puth'][]='components/builder/buttons/lcp-button-red/lcp-button-red.css';
$components_all_css['puth'][]='components/builder/buttons/lcp-button-green/lcp-button-green.css';
$components_all_css['puth'][]='components/system/images/load-photo-with-area/load-photo-area.css';
$components_all_css['puth'][]='components/system/html/html-block-drop/html-block-drop.css';
$components_all_css['puth'][]='components/system/tables/table-default2/table-default2.css';
$components_all_css['puth'][]='components/system/texts/text-default1/text-default1.css';
$components_all_css['puth'][]='components/builder/dropdowns/dropdown-white/dropdown-white.css';
$components_all_css['puth'][]='components/builder/buttons/lcp-button-white/lcp-button-white.css';
$components_all_css['puth'][]='components/builder/html/add-new-components/add-new-components.css';
$components_all_css['puth'][]='components/system/dropdowns/dropdown-multi-check/dropdown-multi-check.css';
$components_all_css['puth'][]='components/builder/dropdowns/dropdown-multi-check-white/dropdown-multi-check-white.css';
$components_all_css['puth'][]='components/builder/buttons/lcp-button-green2/lcp-button-green2.css';
$components_all_css['puth'][]='components/builder/buttons/lcp-button-red2/lcp-button-red2.css';
$components_all_css['puth'][]='components/system/texts/phone-default2/phone-default2.css';
// $components_all_css=getarrayvaluedb(array('0'=>'puth'),'*','components_css','','BY numm');
$components_all_count=count($components_all_css['puth']);
for ($i=0; $i < $components_all_count; $i++) {
  $css.='<link rel="stylesheet" href="/'.$components_all_css['puth'][$i].$kesh_html.'">';
}
$components_all_js=array();
$components_all_js['puth'][]='components/system/jquery/jquery/jquery1.10.2.min.js';
$components_all_js['puth'][]='components/system/jquery/jquery/jquery1.10.3.min.js';
$components_all_js['puth'][]='components/system/filter/filter-default1/filter-default.js';
$components_all_js['puth'][]='components/system/url/lcp-url/lcp-url.js';
$components_all_js['puth'][]='components/system/texts/seo/seo.js';
$components_all_js['puth'][]='components/system/images/load-photo-with-area/load-photo-area.js';
$components_all_js['puth'][]='components/system/html/html-block-drop/html-block-drop.js';
$components_all_js['puth'][]='components/system/texts/text-default1/text-default1.js';
$components_all_js['puth'][]='components/system/texts/phone-default1/phone-default1.js';
$components_all_js['puth'][]='components/system/texts/phone-default2/phone-default2.js';
$components_all_js['puth'][]='components/builder/texts/lcp_address_default/lcp-address-default.js';
$components_all_js['puth'][]='components/builder/html/add-new-components/add-new-components.js';
$components_all_js['puth'][]='components/system/dropdowns/dropdown-multi-check/dropdown-multi-check.js';
$components_all_js['puth'][]='components/builder/html/html-parent-object/html-parent-category.js';
$components_all_js['puth'][]='components/builder/html/html-parent-products/html-parent-products.js';
$components_all_js['puth'][]='components/system/checkeds/checked-default1/checked-default1.js';
$components_all_js['puth'][]='components/system/checkeds/toggle-default1/toggle-default1.js';
$components_all_js['puth'][]='components/system/messages/message-default1/message-default1.js';
$components_all_js['puth'][]='components/system/ckeditor/full/ckeditor.js';
$components_all_js['puth'][]='components/system/ckeditor/lcp-install.js';
$components_all_js['puth'][]='components/builder/dropdowns/dropdown_text/dropdown_text.js';
$components_all_js['puth'][]='components/system/dropdowns/dropdowns-default1/dropdown-default1.js';
$components_all_js['puth'][]='components/system/dropdowns/dropdowns-default2/dropdown-default2.js';
$components_all_js['puth'][]='components/builder/html/html-parent-category/html-parent-category.js';
$components_all_js['puth'][]='components/builder/html/html-parent-article/html-parent-article.js';
$components_all_js['puth'][]='components/builder/html/html-parent-rubrik/html-parent-rubrik.js';
$components_all_js['puth'][]='components/builder/html/product-more/script.js';
$components_all_js['puth'][]='components/system/loadings/loading-default1/loading-default1.js';
$components_all_js['puth'][]='components/system/tabs/tab-default1/tab-default.js';
$components_all_js['puth'][]='components/system/menu/menu-left/menu-left.js';
$components_all_js['puth'][]='components/system/images/load-photo/load-photo.js';
$components_all_js['puth'][]='components/system/files/load-files/load-files.js';
$components_all_js['puth'][]='components/system/video/load-youtube/load-youtube.js';
$components_all_js['puth'][]='components/system/charts/dist/chart.min.js';
$components_all_js['puth'][]='components/builder/html/product-more-edit/script.js';
$components_all_js['puth'][]='components/system/texts/date-timer-default1/ui-timepicker.js';
$components_all_js['puth'][]='components/system/tegs/tegs-default1/tegs-default.js';
$components_all_js['puth'][]='components/system/specifications/specifications-default1/spec-default.js';
// $components_all_js=getarrayvaluedb(array('0'=>'puth'),'*','components_js','','BY numm');
$components_all_count=count($components_all_js['puth']);
for ($i=0; $i < $components_all_count; $i++) {
  $js.='<script src="/'.$components_all_js['puth'][$i].$kesh_html.'" charset="utf-8"></script>';
}


function components($functiones="",$param_arr="",$param_arr1="",$param_arr2="",$param_arr3="",$param_arr4="",$param_arr5="",$param_arr6="",$param_arr7="",$param_arr8="",$param_arr9="",$param_arr10="",$param_arr11="",$param_arr12="",$param_arr13="",$param_arr14="",$param_arr15=""){
  global $_LCP_AJAX,$LCP_AJAX_RESULT;
  if (function_exists($functiones)===true){
      return $functiones($param_arr,$param_arr1,$param_arr2,$param_arr3,$param_arr4,$param_arr5,$param_arr6,$param_arr7,$param_arr8,$param_arr9,$param_arr10,$param_arr11,$param_arr12,$param_arr13,$param_arr14,$param_arr15);
    } else
    if (function_exists($functiones.'_init')===true){
      return ($functiones.'_init')($param_arr);
    } else {
      // if
      $html = '';
      $html .= '<script type="text/javascript">';
      $html .= 'alert(\'Функция "'.$functiones.'" не найдена!\');';
      $html .= '</script>';
      if (!empty($_LCP_AJAX)){
        if (function_exists('generate_exception')===true){
          generate_exception('Функция "'.$functiones.'" не найдена!');
        }
        return $html;
      } else {
        echo $html;
      }
    }
  }
?>
