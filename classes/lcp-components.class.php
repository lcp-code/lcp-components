<?php
interface lcp_components_build{
  function __build_component(); // создание общего компонента
  function __init_component(); // выполнение кода перед созданием общего компонента (при наследовании можно изменять свойства)
  function __build_after_component($html); // выполнение кода после создания общего компонента (при наследовании можно изменять свойства)
  // function __get_property_default();
  function __get_attributes_html();
  function __get_php_path();
  function html();
}

class lcp_components{
  // private $id_components=NULL;
  public $id;
  public $title;
  public $name;
  public $class;
  public $onclick;
  public $onchange;
  public $onkeydown;
  public $onkeyup;
  public $onkeypress;
  public $onmouseover;
  public $onmouseout;
  public $onmousedown;
  public $onmouseup;
  public $onfocus;
  public $onblur;
  public $maxlength;
  public $disabled;
  public $autocomplete;
  public $placeholder;
  public $width;
  public $data_id;
  public $style;
  public $attr; // для добавления новых атрибутов


  function __construct($param=array()){
    // $this->id_components=getcelldb('id','id','components','name=\''.get_class($this).'\'');
    $this->id=$param['id'];
    $this->title=$param['title'];
    $this->name=$param['name'];
    $this->class=$param['class'];
    $this->onclick=$param['onclick'];
    $this->onchange=$param['onchange'];
    $this->onkeydown=$param['onkeydown'];
    $this->onkeyup=$param['onkeyup'];
    $this->onkeypress=$param['onkeypress'];
    $this->onmouseover=$param['onmouseover'];
    $this->onmouseout=$param['onmouseout'];
    $this->onmousedown=$param['onmousedown'];
    $this->onmouseup=$param['onmouseup'];
    $this->onfocus=$param['onfocus'];
    $this->onblur=$param['onblur'];
    $this->maxlength=$param['maxlength'];
    $this->disabled=$param['disabled'];
    $this->autocomplete=$param['autocomplete'];
    $this->placeholder=$param['placeholder'];
    $this->width=$param['width'];
    $this->data_id=$param['data-id'];
    $this->style=$param['style'];
    $this->attr=$param['attr'];
    // $this->__get_property_default();
  }

  // function __get_property_default(){
  //   $property_arr=get_class_vars(get_class($this));
  //   foreach ($property_arr as $key => $value) {
  //     if (!isset($this->$key)){
  //       $default=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$this->id_components.'\'  AND components_param_name=\''.$key.'\'');
  //       if (!empty($default)){
  //         $this->$key=$default;
  //       }
  //     }
  //   }
  // }

  function __get_php_path(){
    // $php_path=getcelldb('puth_php','puth_php','components','id=\''.$this->id_components.'\'');
    return $php_path;
  }

  function __get_attributes_html(){
    // общие допустимые аттрибуты для добавления в html строку
    $attr_arr=array();
    $attr_arr[]='id';
    $attr_arr[]='title';
    $attr_arr[]='name';
    $attr_arr[]='class';
    $attr_arr[]='onclick';
    $attr_arr[]='onchange';
    $attr_arr[]='onkeydown';
    $attr_arr[]='onkeyup';
    $attr_arr[]='onkeypress';
    $attr_arr[]='onmouseover';
    $attr_arr[]='onmouseout';
    $attr_arr[]='onmousedown';
    $attr_arr[]='onmouseup';
    $attr_arr[]='onfocus';
    $attr_arr[]='onblur';
    $attr_arr[]='maxlength';
    $attr_arr[]='disabled';
    $attr_arr[]='autocomplete';
    $attr_arr[]='placeholder';
    $attr_arr[]='width';
    $attr_arr[]='style';

    $res_arr=array();

    for ($i=0; $i < count($attr_arr); $i++) {
      if (isset($this->{$attr_arr[$i]})){
        $res_arr[]=$attr_arr[$i].'="'.$this->{$attr_arr[$i]}.'"';
      }
    }
    if (isset($this->data_id)){
      $res_arr[]='data-id="'.$this->data_id.'"';
    }
    if (isset($this->attr)){
      foreach ($this->attr as $key => $value) {
        $res_arr[]=$key.'="'.$value.'"';
      }
    }
    $html=implode(' ',$res_arr);
    return $html;
  }

  function __init_component(){
  }

  function __build_after_component($html){
    return $html;
  }

  function html(){
    $html='';
    $this->__init_component();
    $html=$this->__build_component();
    $html=$this->__build_after_component($html);
    return $html;
  }
}




if (function_exists('components')===false){
  function components($functiones="",$param_arr="",$param_arr1="",$param_arr2="",$param_arr3="",$param_arr4="",$param_arr5="",$param_arr6="",$param_arr7="",$param_arr8="",$param_arr9="",$param_arr10="",$param_arr11="",$param_arr12="",$param_arr13="",$param_arr14="",$param_arr15=""){
    global $_LCP_AJAX,$LCP_AJAX_RESULT;
    if (function_exists($functiones)===true){
      return $functiones($param_arr,$param_arr1,$param_arr2,$param_arr3,$param_arr4,$param_arr5,$param_arr6,$param_arr7,$param_arr8,$param_arr9,$param_arr10,$param_arr11,$param_arr12,$param_arr13,$param_arr14,$param_arr15);
    } else
    if (function_exists($functiones.'_init')===true){
      return ($functiones.'_init')($param_arr);
    } else {
      // if
      $html = '';
      $html .= '<script type="text/javascript">';
      $html .= 'alert(\'Функция "'.$functiones.'" не найдена!\');';
      $html .= '</script>';
      if (!empty($_LCP_AJAX)){
        if (function_exists('generate_exception')===true){
          generate_exception('Функция "'.$functiones.'" не найдена!');
        }
        return $html;
      } else {
        echo $html;
      }
    }
  }
}

if (function_exists('components_install')===false){
  function components_install(){
    global $css_install_arr,$js_install_arr,$kesh_html;
    for ($i=0; $i < count($js_install_arr); $i++) {
      echo '<script src="'.$js_install_arr[$i].$kesh_html.'" charset="utf-8"></script>';
    }
    for ($i=0; $i < count($css_install_arr); $i++) {
      echo '<link rel="stylesheet" href="'.$css_install_arr[$i].$kesh_html.'">';
    }
  }
}
?>
