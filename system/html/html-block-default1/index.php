<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_html_block_default1',
  'type' => 'system',
  'group' => 'HTML',
  'inner' => array(
    'description' => 'Блок, где располагаются HTML-элементы, используемые в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/html/html-block-default1/html-block-default1.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'title',
    '1' => 'html',
    '2' => 'class'
  ),
  'set_value_php' => array(
    '0' => '<!-- Название блока -->',
    '1' => '<!-- Описание -->',
    '2' => ''
  ),
  'set_description_php' => array(
    '0' => 'Название блока',
    '1' => 'Описание. Можно использовать html',
    '2' => 'Класс для изменения стилей (тег class)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_html_block_default1($param=''){
  $title_block=$param['title'];
  $html_block=$param['html'];
  $class=$param['class'];
  $default_title='<!-- Название блока -->';
  $default_html='<!-- Описание -->';
  $default_class='';
  $title_block=(!empty($title_block))?$title_block:$default_title;
  $html_block=(!empty($html_block))?$html_block:$default_html;
  $class=(!empty($class))?$class:$default_class;

  $html='';
  $html.='<div class="block_default '.$class.'">';
  $html.=(!empty($title_block))?'<div class="block_default_title">'.$title_block.'</div>':'<div class="block_default_title_top"></div>';
  $html.=(!empty($html_block))?'<div class="block_default_html">'.$html_block.'</div>':'<div class="block_default_html_bottom"></div>';
  $html.='</div>';
  return $html;
}
?>
