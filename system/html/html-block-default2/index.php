<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_html_block_default2',
  'type' => 'system',
  'group' => 'html',
  'inner' => array(
    'description' => 'Блок без границ, где располагаются HTML-элементы, используемые в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/html/html-block-default2/html-block-default2.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'id_title',
    '2' => 'id_html',
    '3' => 'class',
    '4' => 'title',
    '5' => 'html'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '<!-- Название блока -->',
    '5' => '<!-- Описание -->'
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Идентификатор (тег id) для элемента title',
    '2' => 'Идентификатор (тег id) для элемента html',
    '3' => 'Класс для изменения стилей (тег class).',
    '4' => 'Название блока',
    '5' => 'Описание. Можно использовать html'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
$css_install_arr[]='/components/system/html/html-block-default2/html-block-default2.css';
// $js_install_arr[]='/components/system/html/html-block-default2/html-block-default2.js';
?>




<?php
function lcp_html_block_default2($param=''){
  $ids=$param['id'];
  $id_title=$param['id_title'];
  $id_html=$param['id_html'];
  $class=$param['class'];
  $title_block=$param['title'];
  $html_block=$param['html'];
  $default_title='<!-- Название блока -->';
  $default_html='<!-- Описание -->';
  $default_class='';
  $ids=(!empty($ids))?' id="'.$ids.'" ':'';
  $id_title=(!empty($id_title))?' id="'.$id_title.'" ':'';
  $id_html=(!empty($id_html))?' id="'.$id_html.'" ':'';
  $class=(!empty($class))?$class:$default_class;
  $title_block=(!empty($title_block))?$title_block:$default_title;
  $html_block=(!empty($html_block))?$html_block:$default_html;

  $html='';
  $html.='<div class="block_default2 '.$class.'" '.$ids.'>';
  $html.=(!empty($title_block))?'<div class="block_default_title2" '.$id_title.'>'.$title_block.'</div>':'<div class="block_default_title_top2"></div>';
  $html.=(!empty($html_block))?'<div class="block_default_html2" '.$id_html.'>'.$html_block.'</div>':'<div class="block_default_html_bottom2"></div>';
  $html.='</div>';
  return $html;
}
?>
