<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_head',
  'type' => 'system',
  'group' => 'HTML',
  'inner' => array(
    'description' => 'Отображение начального html заголовка (тег head), используемого в панели администратора по-умолчанию.',
    'css' => array(),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'dopheader_start',
    '1' => 'dopheader_end',
    '2' => 'newheader'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => ''
  ),
  'set_description_php' => array(
    '0' => 'Добавить свои строки HTML между <head> и </head> в начале кода (сразу после <head>)',
    '1' => 'Добавить свои строки HTML между <head> и </head> в конце кода (перед </head>)',
    '2' => 'Если указано - заменяет полностью основной head'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_head($param=''){
  $dopheader_start=$param['dopheader_start'];
  $dopheader_end=$param['dopheader_end'];
  $newheader=$param['newheader'];
  // $newheader=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'title\'');
  // $default_html=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'html\'');
  // $title_block=(!empty($title_block))?$title_block:$default_title;
  // $html_block=(!empty($html_block))?$html_block:$default_html;

  $html='';
  if (empty($newheader)){
  $html.='<!DOCTYPE html>'."\n";
  $html.='<html lang="ru">'."\n";
  $html.='<head>'."\n";
  $html.=$dopheader_start."\n";
  $html.='<meta http-equiv="Content-Type" content="text/html; charset=utf-8">'."\n";
  $html.='<title></title>'."\n";
  $html.='<meta name="description" content="">'."\n";
  $html.='<meta name="keywords" content="">'."\n";
  $html.=$dopheader_end."\n";
  $html.='</head>';
} else {
  $html.=$newheader;
}
  return $html;
}
?>
