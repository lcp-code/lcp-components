function block_drop_more(obj){
  var obj_drop=$(obj).parent('.block_drop');
  var ids=$(obj_drop).attr('id');
  ids=typeof(ids)==='undefined'?'0':ids;
  if ($(obj_drop).attr('data-drop')=='true'){
    $(obj_drop).children('.block_drop_html').slideUp('fast','linear');
    $(obj_drop).attr('data-drop','false');
    $(obj_drop).children('.block_drop_title').children('.block_one_line_mobile').children('.block_one_line_mobile_table').children('.block_one_line_mobile_td:nth-child(2)').html(html_img_down_arrow_show[ids]);
  } else {
    $(obj_drop).children('.block_drop_html').slideDown('fast','linear');
    $(obj_drop).attr('data-drop','true');
    $(obj_drop).children('.block_drop_title').children('.block_one_line_mobile').children('.block_one_line_mobile_table').children('.block_one_line_mobile_td:nth-child(2)').html(html_img_down_arrow_hide[ids]);
  }
}

function block_drop_more_show(obj_ids){
  var obj_drop=$('#'+obj_ids);
  if ($(obj_drop).attr('data-drop')=='false'){
    $(obj_drop).attr('data-drop','true');
  }
  $(obj_drop).children('.block_drop_html').slideDown('fast','linear');
  $(obj_drop).attr('data-drop','true');
  $(obj_drop).children('.block_drop_title').children('.block_one_line_mobile').children('.block_one_line_mobile_table').children('.block_one_line_mobile_td:nth-child(2)').html(html_img_down_arrow_hide[obj_ids]);
}

function block_drop_more_hidden(obj_ids){
  var obj_drop=$('#'+obj_ids);
  if ($(obj_drop).attr('data-drop')=='false'){
    $(obj_drop).attr('data-drop','true');
  }
  $(obj_drop).children('.block_drop_html').slideUp('fast','linear');
  $(obj_drop).attr('data-drop','false');
  $(obj_drop).children('.block_drop_title').children('.block_one_line_mobile').children('.block_one_line_mobile_table').children('.block_one_line_mobile_td:nth-child(2)').html(html_img_down_arrow_show[obj_ids]);
}
