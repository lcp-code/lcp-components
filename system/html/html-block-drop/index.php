<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_html_block_drop',
  'type' => 'system',
  'group' => 'html',
  'inner' => array(
    'description' => 'Выпадающий Блок, используемый в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/html/html-block-drop/html-block-drop.css'
    ),
    'js' => array(
      '0' => '/components/system/html/html-block-drop/html-block-drop.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'id_title',
    '2' => 'id_html',
    '3' => 'class',
    '4' => 'title_short',
    '5' => 'title_onclick',
    '6' => 'html',
    '7' => 'html_img_down_arrow_show',
    '8' => 'html_img_down_arrow_hide',
    '9' => 'title_width_arr',
    '10' => 'title_align_arr',
    '11' => 'show_default',
    // '11' => 'visible',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '<i class="fa fa-angle-down" aria-hidden="true"></i>',
    '8' => '<i class="fa fa-angle-up" aria-hidden="true"></i>',
    '9' => '',
    '10' => '',
    '11' => 'true',
    // '11' => 'true',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Идентификатор (тег id) для элемента title',
    '2' => 'Идентификатор (тег id) для элемента html',
    '3' => 'Класс для изменения стилей (тег class).',
    '4' => 'Название блока',
    '5' => 'Событие, возникающее при нажатии левой кнопкой мыши (тег onclick)',
    '6' => 'Описание. Можно использовать html',
    '7' => 'Изображение, html-код или тег из fontawesome для отображения стрелки вниз выпадающего списка (крайняя правая кнопка)',
    '8' => 'Изображение, html-код или тег из fontawesome для отображения стрелки вверх выпадающего списка (крайняя правая кнопка)',
    '9' => 'Массив со значениями ширины колонок в названии блока',
    '10' => 'Массив с выравниванием текста по горизонтали в названии блока',
    '11' => 'Отображать ли html текст в блоке по-умолчанию, где true - отображать, false - скрыть',
    // '11' => 'Видимость компонента',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
$css_install_arr[]='/components/system/html/html-block-drop/html-block-drop.css';
$js_install_arr[]='/components/system/html/html-block-drop/html-block-drop.js';
?>




<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_html_block_drop extends lcp_components implements lcp_components_build{
    public $id_title;
    public $id_html;
    public $title_short;
    public $title_onclick;
    public $html;
    public $html_img_down_arrow_show='<i class="fa fa-angle-down" aria-hidden="true"></i>';
    public $html_img_down_arrow_hide='<i class="fa fa-angle-up" aria-hidden="true"></i>';
    public $title_width_arr;
    public $title_align_arr;
    public $show_default='true';

    function __construct($param=array()){
      $this->id_title=isset($param['id_title'])?$param['id_title']:$this->id_title;
      $this->id_html=isset($param['id_html'])?$param['id_html']:$this->id_html;
      $this->title_short=isset($param['title_short'])?$param['title_short']:$this->title_short;
      $this->title_onclick=isset($param['title_onclick'])?$param['title_onclick']:$this->title_onclick;
      $this->html=isset($param['html'])?$param['html']:$this->html;
      $this->html_img_down_arrow_show=isset($param['html_img_down_arrow_show'])?$param['html_img_down_arrow_show']:$this->html_img_down_arrow_show;
      $this->html_img_down_arrow_hide=isset($param['html_img_down_arrow_hide'])?$param['html_img_down_arrow_hide']:$this->html_img_down_arrow_hide;
      $this->title_width_arr=isset($param['title_width_arr'])?$param['title_width_arr']:$this->title_width_arr;
      $this->title_align_arr=isset($param['title_align_arr'])?$param['title_align_arr']:$this->title_align_arr;
      $this->show_default=isset($param['show_default'])?$param['show_default']:$this->show_default;
      parent::__construct($param);
      $this->title_width_arr=(!empty($this->title_width_arr))?$this->title_width_arr:array("100%","50px");
      $this->title_align_arr=(!empty($this->title_align_arr))?$this->title_align_arr:array("left","right");
    }

    function __build_component(){
      //   $visible_class='';
      //   if ($this->visible=='false'){
      //     $visible_class=' block_drop_visible_false';
      //   }
      $show_default_class='';
      if ($this->show_default=='true'){
        $show_default_class=' block_drop_html_active';
      }
      $html='';
      $this->class='block_drop '.$this->class.$visible_class;
      $attr_str=$this->__get_attributes_html();
      $html.='<div '.$attr_str.' data-drop='.$this->show_default.'>';
      $html.=(!empty($this->title_short))?'<div class="block_drop_title" '.$this->id_title.' onclick="block_drop_more(this);'.$this->title_onclick.'">'.components('lcp_block_one_line_mobile',array(
        'class'=>'block_drop_tt',
        'tbody'=>array($this->title_short,(($this->show_default=='true')?$this->html_img_down_arrow_hide:$this->html_img_down_arrow_show)),
        'width_columns'=>$this->title_width_arr,
        'text_align_col_tbody'=>$this->title_align_arr,
        'padding_arr'=>array('10px','10px'),
      )).'</div>':'<div class="block_drop_title_top"></div>';
      $html.=(!empty($this->html))?'<div class="block_drop_html'.$show_default_class.'" '.$this->id_html.'>'.$this->html.'</div>':'<div class="block_drop_html_bottom"></div>';
      $html.='</div>';

      $ids_obj=(!empty($this->id))?$this->id:'0';

      $html.='<script type="text/javascript">'."\n";
      $html.='var html_img_down_arrow_show={};'."\n";
      $html.='html_img_down_arrow_show['.$ids_obj.'] = \''.$this->html_img_down_arrow_show.'\';'."\n";
      $html.='var html_img_down_arrow_hide={};'."\n";
      $html.='html_img_down_arrow_hide['.$ids_obj.'] = \''.$this->html_img_down_arrow_hide.'\';'."\n";
      $html.='</script>'."\n";

      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_html_block_drop_init($param=array()){
    $lcp_obj=new lcp_html_block_drop($param);
    return $lcp_obj->html();
  }
}
?>




<?php
// function lcp_html_block_drop($param=''){
//   $id_components=getcelldb('id','id','components','name=\'lcp_html_block_drop\'');
//   $ids=$param['id'];
//   $id_title=$param['id_title'];
//   $id_html=$param['id_html'];
//   $class=$param['class'];
//   $title_block=$param['title'];
//   $html_block=$param['html'];
//   $html_img_down_arrow_show=$param['html_img_down_arrow_show'];
//   $html_img_down_arrow_hide=$param['html_img_down_arrow_hide'];
//   $title_width_arr=$param['title_width_arr'];
//   $title_align_arr=$param['title_align_arr'];
//   $show_default=$param['show_default'];
//   $visible=$param['visible'];
//   $default_title=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'title\'');
//   $default_html=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'html\'');
//   $default_class=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'class\'');
//   $default_title_width_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'title_width_arr\'');
//   $default_title_align_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'title_align_arr\'');
//   $default_html_img_down_arrow_show=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'html_img_down_arrow_show\'');
//   $default_html_img_down_arrow_hide=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'html_img_down_arrow_hide\'');
//   $default_show_default=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'show_default\'');
//   $default_visible=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'visible\'');
//   $ids=(!empty($ids))?' id="'.$ids.'" ':'';
//   $id_title=(!empty($id_title))?' id="'.$id_title.'" ':'';
//   $id_html=(!empty($id_html))?' id="'.$id_html.'" ':'';
//   $class=(!empty($class))?$class:$default_class;
//   $title_block=(!empty($title_block))?$title_block:$default_title;
//   $html_block=(!empty($html_block))?$html_block:$default_html;
//   $show_default=(!empty($show_default))?$show_default:$default_show_default;
//   $visible=(!empty($visible))?$visible:$default_visible;
//   if (empty($title_width_arr)){
//     eval('$title_width_arr='.$default_title_width_arr);
//   }
//   if (empty($title_align_arr)){
//     eval('$title_align_arr='.$default_title_align_arr);
//   }
//   $html_img_down_arrow_show=(!empty($html_img_down_arrow_show))?$html_img_down_arrow_show:$default_html_img_down_arrow_show;
//   $html_img_down_arrow_hide=(!empty($html_img_down_arrow_hide))?$html_img_down_arrow_hide:$default_html_img_down_arrow_hide;
//
//   $show_default_class='';
//   if ($show_default=='true'){
//     $show_default_class=' block_drop_html_active';
//   }
//
//   $visible_class='';
//   if ($visible=='false'){
//     $visible_class=' block_drop_visible_false';
//   }
//
//   $html='';
//   $html.='<div class="block_drop '.$class.$visible_class.'" '.$ids.' data-drop='.$show_default.'>';
//   $html.=(!empty($title_block))?'<div class="block_drop_title" '.$id_title.' onclick="block_drop_more(this);">'.components('lcp_block_one_line_mobile',array(
//     'class'=>'block_drop_tt',
//     'tbody'=>array($title_block,(($show_default=='true')?$html_img_down_arrow_hide:$html_img_down_arrow_show)),
//     'width_columns'=>$title_width_arr,
//     'text_align_col_tbody'=>$title_align_arr,
//     'padding_arr'=>array('10px','10px'),
//   )).'</div>':'<div class="block_drop_title_top"></div>';
//   $html.=(!empty($html_block))?'<div class="block_drop_html'.$show_default_class.'" '.$id_html.'>'.$html_block.'</div>':'<div class="block_drop_html_bottom"></div>';
//   $html.='</div>';
//
//   $ids_obj=(!empty($ids))?$ids:'0';
//
//   $html.='<script type="text/javascript">'."\n";
//   $html.='var html_img_down_arrow_show={};'."\n";
//   $html.='html_img_down_arrow_show['.$ids_obj.'] = \''.$html_img_down_arrow_show.'\';'."\n";
//   $html.='var html_img_down_arrow_hide={};'."\n";
//   $html.='html_img_down_arrow_hide['.$ids_obj.'] = \''.$html_img_down_arrow_hide.'\';'."\n";
//   $html.='</script>'."\n";
//
//   return $html;
// }
?>
