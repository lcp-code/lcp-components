<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_block_one_line2',
  'type' => 'system',
  'group' => 'HTML',
  'inner' => array(
    'description' => 'Блок, где располагаются HTML-элементы в одну строку, используемые в панели администратора по-умолчанию. В мобильной версии эти колонки превращаются в строки.',
    'css' => array(
      '0' => '/components/system/html/block-one-line2/block-one-line2.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'tbody',
    '1' => 'width_columns',
    '2' => 'text_align_col_tbody',
    '3' => 'text_valign_col_tbody',
    '4' => 'class'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => 'left',
    '3' => 'middle',
    '4' => ''
  ),
  'set_description_php' => array(
    '0' => 'Массив колонок с основным текстом ячеек',
    '1' => 'Массив со значениями ширины колонок',
    '2' => 'Массив с выравниванием текста по горизонтали',
    '3' => 'Массив с выравниванием текста по вертикали',
    '4' => 'Класс для изменения стилей (тег class)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_block_one_line2($param=''){
  $tbody=$param['tbody'];
  $width_columns=$param['width_columns'];
  $text_align_col_tbody=$param['text_align_col_tbody'];
  $text_valign_col_tbody=$param['text_valign_col_tbody'];
  $class=$param['class'];
  $default_width_columns='';
  $default_text_align_col_tbody='left';
  $default_text_valign_col_tbody='middle';
  $default_class='';
  $class=(!empty($class))?$class:$default_class;

  $html='';
  $html.='<div class="block_one_line2 '.$class.'">';
  $html.='<div class="block_one_line_table">';
  for ($i=0; $i < count($tbody); $i++) {
    $css_line='';
    if (!empty($width_columns[$i])){
      $css_line.='width:'.$width_columns[$i].';min-width:'.$width_columns[$i].';max-width:'.$width_columns[$i].';';
    } else {
      if (!empty($default_width_columns)){
        $css_line.='width:'.$default_width_columns.';min-width:'.$default_width_columns.';max-width:'.$default_width_columns.';';
      }
    }
    if (!empty($text_align_col_tbody[$i])){
      $css_line.='text-align:'.$text_align_col_tbody[$i].';';
    } else {
      if (!empty($default_text_align_col_tbody)){
        $css_line.='text-align:'.$default_text_align_col_tbody.';';
      }
    }
    if (!empty($text_valign_col_tbody[$i])){
      $css_line.='vertical-align:'.$text_valign_col_tbody[$i].';';
    } else {
      if (!empty($default_text_valign_col_tbody)){
        $css_line.='vertical-align:'.$default_text_valign_col_tbody.';';
      }
    }
    $css_line=(empty($css_line))?'':'style="'.$css_line.'"';
    $html.='<div class="block_one_line_td" '.$css_line.'>';
    $html.= $tbody[$i];
    $html.='</div>';

    if (!empty($tbody[$i+1])){
      $html.='<div class="block_one_line_td_empty block_one_line_td_empty_border">';
      $html.='</div>';
      $html.='<div class="block_one_line_td_empty">';
      $html.='</div>';
    }
  }
  $html.='</div>';
  $html.='</div>';
  return $html;
}
?>
