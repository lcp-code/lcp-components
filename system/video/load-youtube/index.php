<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
  $component_property=array(
    'name' => 'lcp_load_youtube',
    'type' => 'system',
    'group' => 'Видео',
    'inner' => array(
      'description' => 'Загрузка youtube-видео, используемая в панели администратора по-умолчанию',
      'css' => array(
        '0' => '/components/system/video/load-youtube/load-youtube.css'
      ),
      'js' => array(
        '0' => '/components/system/video/load-youtube/load-youtube.js'
      )
    ),
    'set_name_php' => array(
      '0' => 'id',
      // '1' => 'max_video',
      '1' => 'files_arr'
    ),
    'set_value_php' => array(
      '0' => 'youtube_field_main',
      // '1' => '0',
      '1' => ''
    ),
    'set_description_php' => array(
      '0' => 'Идентификатор (тег id)',
      // '1' => 'Максимальное количество видеофайлов',
      '1' => 'Массив с видеофайлами в следующем виде: array(
        "id" => array(0,1),
        "title" => array("Название видео №1","Название видео №2"),
        "description" => array("Описание к видео №1","Описание к видео №2"),
        "uploadDate" => array("2016-01-09","2016-01-07"),
        "datePublished" => array("2016-01-09","2016-01-07"),
        "duration" => array("PT33M55S","PT20M33S"),
        "embedUrl" => array("https://www.youtube.com/embed/video1","https://www.youtube.com/embed/video2"),
        "img" => array("https://i.ytimg.com/vi/fghg/photo1","https://i.ytimg.com/vi/fghg/photo2"),
        "numm" => array("0","1"),
        "img_field" => array("youtube_field_main","youtube_field_main"),
        "index" => array("0","1"),
        "length" => "2"
      )'
    ),
    'get_php' => array(
      '0' =>'text'
    ),
    'get_description_php' => array(
      '0' =>'Функция возвращает текст'
    ),
    'set_name_js' => array(
      '0' => 'id',
      '1' => 'get'
    ),
    'set_value_js' => array(
      '0' => 'youtube_field_main',
      '1' => ''
    ),
    'set_description_js' => array(
      '0' => 'Идентификатор (тег id), по которому следует искать элемент',
      '1' => '<div>Получение данных о видео (по заданному id), где:</div><div>get_load_video - получить готовые видео и аттрибуты</div>'
    ),
    'get_js' => array(
      '0' => 'array'
    ),
    'get_description_js' => array(
    '0' => 'Если get установлено в значение "get_load_video", то возвращает массив с аттрибутами видео'
    )
    );
  }
  ?>




  <?php
  function lcp_load_youtube($param=''){
    $id_components=getcelldb('id','id','components','name=\'lcp_load_youtube\'');
    $puth_php=getcelldb('puth_php','puth_php','components','name=\'lcp_load_youtube\'');
    $puth_php=str_replace('index.php','',$puth_php);

    $ids=$param['id'];
    // $max_video=$param['max_video'];
    $files_arr=$param['files_arr'];
    $default_ids=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'id\'');
    // $default_max_files=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'max_files\'');
    $ids=(!empty($ids))?$ids:$default_ids;
    // $max_video=(!empty($max_video))?$max_video:$default_max_video;


    $html='';
    $html.=components('lcp_button_default',array(
    'value' => 'Добавить видео',
    'onclick'=>'popup_tree_youtube_'.$ids.'();',
    ));


    $popup_content='';
    $popup_content.=components('lcp_html_block_default3',array(
    'title'=>'Ссылка на видео',
    'html'=> components('lcp_text_default',array(
    'id' => 'url_youtube_'.$ids,
    'placeholder' => 'Н-р, https://youtu.be/gvFGDgGbfgh',
    ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
    'title'=>' ',
    'html'=> components('lcp_button_default',array(
    'value' => 'Добавить',
    'onclick' => 'if ($(\'#url_youtube_'.$ids.'\').val()==\'\'){lcp_message(\'Укажите ссылку на видео!\',\'false\');return;}add_youtube_'.$ids.'({url:$(\'#url_youtube_'.$ids.'\').val()});popup_tree_youtube_close_'.$ids.'();',
    ))
    ));
    $html.=components('lcp_popup_default',array(
    'id_popup' => 'popup_tree_youtube_'.$ids,
    'title' => 'Добавить видео youtube',
    'content_html' => $popup_content,
    'popup_show' => 'popup_tree_youtube_'.$ids,
    'popup_close' => 'popup_tree_youtube_close_'.$ids,
    'height' => '200px',
    'width' => '400px',
    'title_setting' => array(
    'font-size' => '19px',
    'font-weight' => 'bold',
    'text-align' => 'left'
    )
    ));


    $html.=components('lcp_ajax',array(
    'function_name'=>'add_youtube_'.$ids,
    'begin_html'=>'
    var url=param[\'url\'];
    if (url==\'\'){
      lcp_message(\'Введите URL!\',\'false\');
      return;
    }
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/'.$puth_php.'ajax-load-youtube.php',
    'data_js'=>'{
      action:\'add_youtube\',
      url:url
    }',
    'lcp_success_true'=>'
    data.string[\'id\']=\''.$ids.'\';
    view_youtube_'.$ids.'(data.string);
    popup_tree_youtube_close_'.$ids.'();
    lcp_loading_default({loading_show:\'false\'});
    ',
    'lcp_success_false'=>'lcp_message(data.string,\'false\');lcp_loading_default({loading_show:\'false\'});',
    'error'=>'lcp_message(\'Ошибка AJAX!\',\'false\');lcp_loading_default({loading_show:\'false\'});',
    'show_error'=>'false'
    ));






    // отобразить фото
    $html.='<div id="youtube_content_'.$ids.'">';
      $html.=components('lcp_ajax',array(
      'lcp_type_ajax'=>'include',
      'function_name'=>'view_youtube_'.$ids,
      'begin_html'=>'
      var video=param[\'video\'];
      var ids=param[\'id\'];
      lcp_loading_default({loading_show:\'true\'});
      ',
      'url'=>'/'.$puth_php.'ajax-load-youtube.php',
      'data'=>array(
      'action'=>'view_youtube',
      'video'=>$files_arr,
      'ids'=>$ids
      ),
      'data_js'=>'{
        action:\'view_youtube\',
        video:video,
        ids:ids
      }',
      'lcp_success_true'=>'lcp_loading_default({loading_show:\'false\'});$(\'#youtube_content_\'+ids).append(data.string);sortable_youtube(ids);',
      'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});alert(data.string);',
      'error'=>'lcp_loading_default({loading_show:\'false\'});alert(\'Ошибка при ajax запросе!\');',
      'show_error'=>'false'
      ));
      $html.='<script>sortable_youtube(\''.$ids.'\');</script></div>';
      return $html;
    }
    ?>
