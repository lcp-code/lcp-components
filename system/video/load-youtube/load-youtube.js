function lcp_load_youtube(param){
  var ids=param['id'];
  var get=param['get'];
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='youtube_field_main';
  }
  if (get=='get_load_youtube'){
    return get_load_youtube(ids);
  }
  // if (get=='get_for_save_photos'){
  //   return get_for_save_photos(ids);
  // }
}

function sortable_youtube(ids){
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='youtube_field_main';
  }
  $('#youtube_content_'+ids).sortable();
  $('#youtube_content_'+ids).disableSelection();
}

function del_youtube(obj){
  $(obj).parent('.youtube_field_items').remove();
}

function get_load_youtube(ids){
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='youtube_field_main';
  }
  console.log(ids);
  var obj=$('#youtube_content_'+ids).children('.youtube_field_items');
  var files={};
  files['id']={};
  files['title']={};
  files['description']={};
  files['uploadDate']={};
  files['datePublished']={};
  files['duration']={};
  files['embedUrl']={};
  files['img']={};
  files['numm']={};
  files['img_field']={};
  files['index']={};
  for (var i = 0; i < obj.length; i++) {
    var obj_item=$(obj).eq(i);
    files['id'][i]=$(obj_item).attr('data-id');
    files['title'][i]=$(obj_item).attr('data-title');
    files['description'][i]=$(obj_item).attr('data-description');
    files['uploadDate'][i]=$(obj_item).attr('data-uploadDate');
    files['datePublished'][i]=$(obj_item).attr('data-datePublished');
    files['duration'][i]=$(obj_item).attr('data-duration');
    files['embedUrl'][i]=$(obj_item).attr('data-embedUrl');
    files['img'][i]=$(obj_item).attr('data-img');
    files['numm'][i]=i;
    files['img_field'][i]=ids;
    files['index'][$(obj_item).attr('data-id')]=i;
  }
  files['length']=obj.length;
  return files;
}
