<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_button_default',
  'type' => 'system',
  'group' => 'Кнопки',
  'inner' => array(
    'description' => 'Синяя кнопка, используемая в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/buttons/button-default1/button-default1.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'class',
    '3' => 'href',
    '4' => 'target',
    '5' => 'onclick',
    '6' => 'onmouseover',
    '7' => 'onmouseout',
    '8' => 'width',
    '9' => 'data-id',
    '10' => 'title',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => 'Новая кнопка',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор',
    '1' => 'Название кнопки',
    '2' => 'Класс для изменения стилей',
    '3' => 'Ссылка для кнопки',
    '4' => '<div>Открытие ссылки:</div><div>_self - в текущем окне (по умолч.),</div><div>_blank - в новом окне,</div><div>_parent - во фрейм-родитель,</div><div>_top - в новом окне, вместо фреймов</div>',
    '5' => 'Событие, возникающее при нажатии левой кнопкой мыши',
    '6' => 'Событие, возникающее при наведении указателя мыши на объект',
    '7' => 'Событие, возникающее, когда указатель мыши уходит с объекта',
    '8' => 'Ширина компонента',
    '9' => 'Дополнительный идентификатор',
    '10' => 'Всплывающая подсказка (тег title)',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array(),
);
}
$css_install_arr[]='/components/system/buttons/button-default1/button-default1.css';
// $js_install_arr[]='/components/system/buttons/button-default1/button-default1.js';
?>




<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_button_default extends lcp_components implements lcp_components_build{
    public $value='Новая кнопка';
    public $href;
    public $target;
    public $dataid;

    function __construct($param=array()){
      $this->value=isset($param['value'])?$param['value']:$this->value;
      $this->href=isset($param['href'])?$param['href']:$this->href;
      $this->target=isset($param['target'])?$param['target']:$this->target;
      $this->dataid=isset($param['dataid'])?$param['dataid']:$this->dataid;
      parent::__construct($param);
    }

    function __build_component(){
      $html='';
      $this->class='button_default '.$this->class;
      $attr_str=$this->__get_attributes_html();
      $html.='<a '.$attr_str.'>'.$this->value.'</a>';
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_button_default_init($param=array()){
    $lcp_obj=new lcp_button_default($param);
    return $lcp_obj->html();
  }
}
?>
