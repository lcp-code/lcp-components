<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_button_minus',
  'type' => 'system',
  'group' => 'Кнопки',
  'inner' => array(
    'description' => 'Красная кнопка со знаком - (минус, если не задано другое значение через свойство value), основанная на компоненте lcp_button_default',
    'css' => array(
      '0' => '/components/system/buttons/lcp-button-minus/button-minus.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'href',
    '3' => 'target',
    '4' => 'onclick',
    '5' => 'onmouseover',
    '6' => 'onmouseout'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => 'Новая кнопка',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор',
    '1' => 'Название кнопки',
    '2' => 'Ссылка для кнопки',
    '3' => '<div>Открытие ссылки:</div><div>_self - в текущем окне (по умолч.),</div><div>_blank - в новом окне,</div><div>_parent - во фрейм-родитель,</div><div>_top - в новом окне, вместо фреймов</div>',
    '4' => 'Событие, возникающее при нажатии левой кнопкой мыши',
    '5' => 'Событие, возникающее при наведении указателя мыши на объект',
    '6' => 'Событие, возникающее, когда указатель мыши уходит с объекта'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>


<?php
function lcp_button_minus($param=''){
  $html='';
  $param['class']=$param['class'].' lcp_button_minus ';
  $param['value']=(!empty($param['value']))?$param['value']:'&ndash;';
  $html=lcp_button_default($param);
  return $html;
}
?>
