<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_filter_default',
  'type' => 'system',
  'group' => 'Теги',
  'inner' => array(
    'description' => 'Фильтрация по характеристикам, используемая в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/filter/filter-default1/filter-default.css'
    ),
    'js' => array(
      '0' => '/components/system/filter/filter-default1/filter-default.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'value_arr',
  ),
  'set_value_php' => array(
    '0' => '',
  ),
  'set_description_php' => array(
    '0' => 'Массив с идентификаторами фильтрации'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_filter_default($param=''){
  $id_components=getcelldb('id','id','components','name=\'lcp_filter_default\'');
  $value_arr=$param['value_arr'];

  $html='';
  $html.=components('lcp_button_green',array(
    'value'=>'Добавить',
    'onclick'=>'lcp_loading_default({loading_show:\'true\'});popup_filter();get_table_filter();'
  ));

  if (count($value_arr['filter_value'])>0){
    $html_filter='';
    // $html_filter_sql=(count($value_arr['filter_value'])>1)?'id IN (\''.implode('\',\'',$value_arr['filter_value']).'\')':'id=\''.$value_arr['filter_value'][0].'\'';
    // echo $html_filter_sql;
    $html_filter_arr=getarrayvaluedb(array('id','title_short','childr'),'id,title_short,childr','datas','id IN (\''.implode('\',\'',$value_arr['filter_value']).'\')','','','1');
    $html_filter_group=getarrayvaluedb(array('id','title_short'),'id,title_short,childr','datas','id IN (\''.implode('\',\'',$html_filter_arr['childr']).'\')','','','1');
    for ($i1=0; $i1 < count($html_filter_group['id']); $i1++) {
        $html_filter.='<div class="filter_all_group" id="filter_all_group_'.$html_filter_group['id'][$i1].'" data-id="'.$html_filter_group['id'][$i1].'">
        <div class="group_title">'.$html_filter_group['title_short'][$i1].'</div><div class="filter_all_group_delete" onclick="filter_all_group_delete_click(\''.$html_filter_group['id'][$i1].'\');">x</div>
        <div class="filter_all" id="filter_all_'.$html_filter_group['id'][$i1].'">';
        for ($i2=0; $i2 < count($html_filter_arr['id']); $i2++) {
          if ($html_filter_arr['childr'][$i2]==$html_filter_group['id'][$i1]){
            $html_filter.='<div class="filter_all_item" data-id="'.$html_filter_arr['id'][$i2].'">'.$html_filter_arr['title_short'][$i2].' <div class="filter_all_delete" onclick="filter_all_item_click(\''.$html_filter_arr['id'][$i2].'\');">x</div></div>';
          }
        }
        $html_filter.='</div></div>';
    }
  }
  $html.='<div class="filter_all_block">'.$html_filter.'</div>';


  $popup_content='';

  $popup_content.=components('lcp_ajax',array(
    'lcp_type_ajax'=>'include',
    'function_name'=>'get_table_filter',
    'begin_html'=>'
    var id_catalog=param[\'id_catalog\'];
    if (id_catalog==\'\'){
      id_catalog=\''.$id_catalog.'\';
      }
    ',
    'url'=>'/components/system/filter/filter-default1/ajax-filter.php',
    'data'=>array(
      'action'=>'get_table_filter',
      'value_arr'=>(!empty($value_arr))?implode(',',$value_arr):'',
      'id_catalog'=>$id_catalog
    ),
    'data_js'=>'{
      action:\'get_table_filter\',
      value_arr:get_filter_default_str(),
      id_catalog:id_catalog
      }',
    'lcp_success_true'=>'lcp_loading_default({loading_show:\'false\'});$(\'.get_table_filter\').html(data.string);',
    'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});alert(data.string);',
    'error'=>'lcp_loading_default({loading_show:\'false\'});alert(\'Ошибка при ajax запросе!\');',
    'show_error'=>'false'
  ));




  $html.=components('lcp_popup_default',array(
    'id_popup' => 'popup_filter',
    'title' => 'Выберите фильтрацию из списка',
    'content_html' => $popup_content,
    'popup_show' => 'popup_filter',
    'popup_close' => 'popup_filter_close',
    // 'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));





// Добавление новой фильтрации
  $popup_content='';
  $popup_content.='<div class="get_table_filter_for_save"></div>';
  $popup_content.=components('lcp_ajax',array(
    'function_name'=>'get_table_filter_for_save',
    'begin_html'=>'',
    'url'=>'/components/system/filter/filter-default1/ajax-filter.php',
    // 'data'=>array(
    //   'action'=>'get_table_filter_for_save',
    //   // 'value_arr'=>(!empty($value_arr))?implode(',',$value_arr):''
    // ),
    'data_js'=>'{
      action:\'get_table_filter_for_save\'
      }',
    'lcp_success_true'=>'lcp_loading_default({loading_show:\'false\'});$(\'.get_table_filter_for_save\').html(data.string);',
    'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});alert(data.string);',
    'error'=>'lcp_loading_default({loading_show:\'false\'});alert(\'Ошибка при ajax запросе!\');',
    'show_error'=>'false'
  ));
  $html.=components('lcp_popup_default',array(
    'id_popup' => 'popup_new_filter',
    'title' => 'Добавить новую фильтрацию',
    'content_html' => $popup_content,
    'popup_show' => 'popup_new_filter',
    'popup_close' => 'popup_new_filter_close',
    // 'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));
  return $html;
}
?>
