function filter_all_item_click(filter_id){
  $('.filter_all>[data-id="'+filter_id+'"]').remove();
}

function filter_all_group_delete_click(group_id){
  $('#filter_all_group_'+group_id).remove();
}

function add_item_filter(id_catalog){
  var id_arr=[];
  id_arr['id']=[];
  id_arr['title']=[];
  k=0;
  // получение выбранных id и заголовка
  for (var i = 0; i < $('.filter_tbl_check').length; i++) {
    var obj=$('.filter_tbl_check').eq(i);
    if (lcp_checked_default(obj,{checked:'has_checked'})=='true'){
      id_arr['id'][k]=$(obj).attr('data-id');
      id_arr['title'][k]=$('#filter_title_'+id_arr['id'][k]).html();
      k++;
    }
  }
  // добавление в поле характеристик
  if (typeof($('#filter_all_group_'+id_catalog).html())==='undefined'){ // если нет группы, создать
    $('.filter_all_block').append('<div class="filter_all_group" id="filter_all_group_'+id_catalog+'" data-id="'+id_catalog+'"><div class="group_title">'+$('.get_table_filter .lcp_html_navigation_span:last-child').html()+'</div><div class="filter_all_group_delete" onclick="filter_all_group_delete_click(\''+id_catalog+'\');">x</div><div class="filter_all" id="filter_all_'+id_catalog+'"></div></div>');
  }
  for (var i = 0; i < id_arr['id'].length; i++) { // добавление характеристик
    $('#filter_all_'+id_catalog).append('<div class="filter_all_item" data-id="'+id_arr['id'][i]+'">'+id_arr['title'][i]+' <div class="filter_all_delete" onclick="filter_all_item_click(\''+id_arr['id'][i]+'\');">x</div></div>');
  }
}

function get_filter_default(){
  // получение id групп
  var id_arr={};
  id_arr['filter_group']={};
  id_arr['filter_value']={};

  if ($('.filter_all_group').length>0){
    for (var i = 0; i < $('.filter_all_group').length; i++) {
      var obj=$('.filter_all_group').eq(i);
      id_arr['filter_group'][i]=$(obj).attr('data-id');
    }
  }

  if ($('.filter_all_item').length>0){
    for (var i = 0; i < $('.filter_all_item').length; i++) {
      var obj=$('.filter_all_item').eq(i);
      id_arr['filter_value'][i]=$(obj).attr('data-id');
    }
  }

  return id_arr;
}

function get_filter_default_str(){
  var str='';
  // получение выбранных id
  for (var i = 0; i < $('.filter_all_item').length; i++) {
    var obj=$('.filter_all_item').eq(i);
    str+=str==''?$(obj).attr('data-id'):','+$(obj).attr('data-id');
  }
  return str;
}

function enter_search_filter(e,id_catalog,search){
  if (13==e.keyCode){
    lcp_loading_default({loading_show:'true'});
    get_table_spec({id_catalog:id_catalog,search:search});
    return false;
  }
}

function next_page_filtr(param){
  lcp_loading_default({loading_show:'true'});
  get_table_spec(param);
}
