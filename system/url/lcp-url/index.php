<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_url',
  'type' => 'system',
  'group' => 'URL',
  'inner' => array(
    'description' => 'Работа с адресной строкой',
    'css' => array(),
    'js' => array(
      '0' => '/components/system/url/lcp-url/lcp-url.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'params',
    '1' => 'set_params_name_arr',
    '2' => 'set_params_value_arr',
    '3' => 'remove_params_name_arr'
    // '3' => 'function_name'
  ),
  'set_value_php' => array(
    '0' => 'get',
    '1' => '',
    '2' => '',
    '3' => ''
    // '3' => 'lcp_url'
  ),
  'set_description_php' => array(
    '0' => '<div>get - получение всех параметров</div><div>set - передать параметр (set_params_name_arr, set_params_value_arr)</div><div>remove - удаление параметров (remove_params_name_arr)</div><div>post - переход по сформированной ссылке (form method не используется)</div>',
    '1' => 'Массив названий параметров',
    '2' => 'Массив значений параметров',
    '3' => 'Массив для удаления параметров'
    // '0' => 'Название функции для обращения и задания различных свойств на JS'
  ),
  'get_php' => array(
    '0' => 'array'
  ),
  'get_description_php' => array(
    '0' => 'Функция возвращает массив с параметрами'
  ),
  'set_name_js' => array(
    '0' => 'params',
    '1' => 'set_params_name_arr',
    '2' => 'set_params_value_arr',
    '3' => 'remove_params_name_arr'
    // '3' => 'function_name',
  ),
  'set_value_js' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => ''
    // '3' => ''
  ),
  'set_description_js' => array(
    '0' => '<div>get - получение всех параметров</div>
            <div>set - передача параметров (set_params_arr)</div>
            <div>set_json - передача параметров (set_params_arr) (отличается от set возвращаемыми параметрами)</div>
            <div>remove - удаление параметров (remove_params_arr)</div>
            <div>post - переход по сформированной ссылке (form method не используется)</div>
            <div>get_post - получение всех параметров и переход по сформированной ссылке</div>
            <div>set_post - передача параметров (set_params_arr) и переход по сформированной ссылке</div>
            <div>remove_post - удаление параметров (remove_params_arr) и переход по сформированной ссылке</div>
            <div>remove_set - удаление параметров (remove_params_arr) и передача параметров (set_params_arr)</div>
            <div>remove_set_post - удаление параметров (remove_params_arr), передача параметров (set_params_arr), переход по сформированной ссылке</div>
            <div>set_browser - изменить ссылку в браузере без перезагрузки страницы (set_params_name_arr, set_params_value_arr)</div>',
    '1' => 'Массив названий параметров',
    '2' => 'Массив значений параметров',
    '3' => 'Массив для удаления параметров'
    // '0' => 'Название используемой функции для обращения и задания различных свойств',
  ),
  'get_js' => array(
    '0' => 'array / text'
  ),
  'get_description_js' => array(
    '0' => '<div>Если params:</div>
            <div>get или set - функция возвращает массив с параметрами</div>
            <div>post - ничего не возвращает (автоматический переход по сформированной ссылке)</div>
            <div>set_json - функция возвращает массив с параметрами в формате json для использования в data (AJAX)</div>'
  )
);
}
// $css_install_arr[]='/components/system/url/lcp-url/lcp-url.css';
$js_install_arr[]='/components/system/url/lcp-url/lcp-url.js';
?>




<?php
function lcp_url($param=''){
  $id_components=getcelldb('id','id','components','name=\'lcp_button_default\'');
  $function_name=$param['function_name'];
  $params=$param['params'];
  $set_params_name_arr=$param['set_params_name_arr'];
  $set_params_value_arr=$param['set_params_value_arr'];
  $remove_params_name_arr=$param['remove_params_name_arr'];
  $default_function_name=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'function_name\'');
  $default_params=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'params\'');
  $default_set_params_name_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'set_params_name_arr\'');
  $default_set_params_value_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'set_params_value_arr\'');
  $default_remove_params_name_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'remove_params_name_arr\'');
  $function_name=(empty($function_name))?$default_function_name:$function_name;
  $params=(empty($params))?$default_params:$params;
  $set_params_name_arr=(empty($set_params_name_arr))?$default_set_params_name_arr:$set_params_name_arr;
  $set_params_value_arr=(empty($set_params_value_arr))?$default_set_params_value_arr:$set_params_value_arr;
  $remove_params_name_arr=(empty($remove_params_name_arr))?$default_remove_params_name_arr:$remove_params_name_arr;

  if ($params=='remove'){
    for ($i=0; $i < count($remove_params_name_arr); $i++) {
      unset($_GET[$remove_params_name_arr[$i]]);
    }
    $params='get';
  }

  if ($params=='set'){
    for ($i=0; $i < count($set_params_name_arr); $i++) {
      $_GET[$set_params_name_arr[$i]]=$set_params_value_arr[$i];
    }
    $params='get';
  }

  if (($params=='get') || ($params=='remove') || ($params=='post')){
    $html = array();
    if ((count($_GET))>0){
      $geter=array_keys($_GET)or die (exit(var_dump($_GET)));
      for ($i=0; $i < count($_GET); $i++) {
        $html[$i]['name']=$geter[$i];
        $html[$i]['value']=$_GET[$geter[$i]];
      }
    }
  }

  if ($params=='post'){
    $str_params='';
    for ($i=0; $i < count($html); $i++) {
      if (empty($str_params)){
        $str_params.='?'.$html[$i]['name'].'='.$html[$i]['value'];
      } else {
        $str_params.='&'.$html[$i]['name'].'='.$html[$i]['value'];
      }
    }
    echo '<script type="text/javascript">location.href=window.location.origin+window.location.pathname+\''.$str_params.'\';</script>';
    exit();
  }
  return $html;
}
?>
