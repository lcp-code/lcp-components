var lcp_url_all_params=[];
lcp_url_all_params=lcp_url({params:'get'});

function lcp_url(arr_param){
  var params = arr_param['params'];
  var set_params_name_arr = arr_param['set_params_name_arr'];
  var set_params_value_arr = arr_param['set_params_value_arr'];
  var remove_params_name_arr = arr_param['remove_params_name_arr'];
  var res_count=0;
  var resulter=[];

  if ((params=='get' ) || (params=='set') || (params=='set_json') || (params=='set_browser') || (params=='remove') || (params=='get_post') || (params=='set_post') || (params=='remove_post') || (params=='remove_set') || (params=='remove_set_browser') || (params=='remove_set_post')){
    var res=[];
    var url = window.location.href;
    /*извлекаем строку из URL или объекта window*/
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
    /*если есть строка запроса*/
    if (queryString) {
      /*данные после знака # будут опущены*/
      queryString = queryString.split('#')[0];
      /*разделяем параметры*/
      var arr = queryString.split('&');
      for (var i=0; i<arr.length; i++) {
        /*разделяем параметр на ключ => значение*/
        var a = arr[i].split('=');
        /*обработка данных вида: list[]=thing1&list[]=thing2*/
        var paramNum = undefined;
        var paramName = a[0].replace(/\[\d*\]/, function(v) {
          paramNum = v.slice(1,-1);
          return '';
        });
        /*передача значения параметра ('true' если значение не задано)*/
        var paramValue = typeof(a[1])==='undefined' ? true : a[1];
        /*преобразование регистра*/
        paramName = paramName.toLowerCase();
        paramValue = paramValue.toLowerCase();
        /*если ключ параметра уже задан*/
        if (res[paramName]) {
          /*преобразуем текущее значение в массив*/
          if (typeof res[paramName] === 'string') {
            res[paramName] = [res[paramName]];
            if ((params!='remove')&&(params!='remove_post')&&(params!='remove_set')&&(params!='remove_set_browser')&&(params!='remove_set_post')){
              resulter[res_count]=[];
              resulter[res_count]['name']=paramName;
              resulter[res_count]['value']=res[paramName];
              res_count++;
            } else {
              var bb='false';
              for (var i_r = 0; i_r < remove_params_name_arr.length; i_r++) {
                if (paramName==remove_params_name_arr[i_r]){
                  bb='true';
                  break;
                }
              }
              if (bb=='false'){
                resulter[res_count]=[];
                resulter[res_count]['name']=paramName;
                resulter[res_count]['value']=res[paramName];
                res_count++;
              }
            }
          }
          /*если не задан индекс...*/
          if (typeof paramNum === 'undefined') {
            /*помещаем значение в конец массива*/
            res[paramName].push(paramValue);
            if ((params!='remove')&&(params!='remove_post')&&(params!='remove_set')&&(params!='remove_set_browser')&&(params!='remove_set_post')){
              resulter[res_count]=[];
              resulter[res_count]['name']=paramName;
              resulter[res_count]['value']=paramValue;
              res_count++;
            } else {
              var bb='false';
              for (var i_r = 0; i_r < remove_params_name_arr.length; i_r++) {
                if (paramName==remove_params_name_arr[i_r]){
                  bb='true';
                  break;
                }
              }
              if (bb=='false'){
                resulter[res_count]=[];
                resulter[res_count]['name']=paramName;
                resulter[res_count]['value']=paramValue;
                res_count++;
              }
            }
          }
          /*если индекс задан...*/
          else {
            /*размещаем элемент по заданному индексу*/
            res[paramName][paramNum] = paramValue;
            if ((params!='remove')&&(params!='remove_post')&&(params!='remove_set')&&(params!='remove_set_browser')&&(params!='remove_set_post')){
              resulter[res_count]=[];
              resulter[res_count]['name']=paramName;
              resulter[res_count]['value']=paramValue;
              res_count++;
            } else {
              var bb='false';
              for (var i_r = 0; i_r < remove_params_name_arr.length; i_r++) {
                if (paramName==remove_params_name_arr[i_r]){
                  bb='true';
                  break;
                }
              }
              if (bb=='false'){
                resulter[res_count]=[];
                resulter[res_count]['name']=paramName;
                resulter[res_count]['value']=paramValue;
                res_count++;
              }
            }
          }
        }
        /*если параметр не задан, делаем это вручную*/
        else {
          res[paramName] = paramValue;
          if ((params!='remove')&&(params!='remove_post')&&(params!='remove_set')&&(params!='remove_set_browser')&&(params!='remove_set_post')){
            resulter[res_count]=[];
            resulter[res_count]['name']=paramName;
            resulter[res_count]['value']=paramValue;
            res_count++;
          } else {
            var bb='false';
            for (var i_r = 0; i_r < remove_params_name_arr.length; i_r++) {
              if (paramName==remove_params_name_arr[i_r]){
                bb='true';
                break;
              }
            }
            if (bb=='false'){
              resulter[res_count]=[];
              resulter[res_count]['name']=paramName;
              resulter[res_count]['value']=paramValue;
              res_count++;
            }
          }
        }
      }
    }
  }

  if ((params=='set') || (params=='set_json') || (params=='set_post') || (params=='set_browser') || (params=='remove_set') || (params=='remove_set_browser') || (params=='remove_set_post')){
    for (var i = 0; i < set_params_name_arr.length; i++) {
      var bb='false';
      for (var j = 0; j < res_count; j++) {
        if (resulter[j]['name']==set_params_name_arr[i]){
          bb='true';
          break;
        }
      }
      if (bb=='true'){
        resulter[j]['value']=set_params_value_arr[i];
      } else {
        resulter[res_count]=[];
        resulter[res_count]['name']=set_params_name_arr[i];
        resulter[res_count]['value']=set_params_value_arr[i];
        res_count++;
      }
    }
  }

  if ((params=='get' ) || (params=='set') || (params=='set_json') || (params=='set_browser') || (params=='remove') || (params=='get_post') || (params=='set_post') || (params=='remove_post') || (params=='remove_set') || (params=='remove_set_browser') || (params=='remove_set_post')){
    lcp_url_all_params = resulter;
  }

  if (params=='set_json'){
    var page_param_data={};
      for (var i = 0; i < resulter.length; i++) {
        page_param_data[resulter[i]['name']]=resulter[i]['value'];
      }
    return page_param_data;
  }

  if ((params=='set_browser') || (params=='remove_set_browser')){
    var str_params='';
    for (var i = 0; i < lcp_url_all_params.length; i++) {
      if (str_params==''){
        str_params+='?'+lcp_url_all_params[i]['name']+'='+lcp_url_all_params[i]['value'];
      } else {
        str_params+='&'+lcp_url_all_params[i]['name']+'='+lcp_url_all_params[i]['value'];
      }
    }
    var locations=window.location.origin+window.location.pathname+str_params;
    history.pushState(null, null, locations);
  }


  if ((params=='post') || (params=='get_post') || (params=='set_post') || (params=='remove_post') || (params=='remove_set_post')){
    var str_params='';
    console.log(lcp_url_all_params);
    for (var i = 0; i < lcp_url_all_params.length; i++) {
      if (str_params==''){
        str_params+='?'+lcp_url_all_params[i]['name']+'='+lcp_url_all_params[i]['value'];
      } else {
        str_params+='&'+lcp_url_all_params[i]['name']+'='+lcp_url_all_params[i]['value'];
      }
    }
    location.href=window.location.origin+window.location.pathname+str_params;
    return;
  }

return lcp_url_all_params;
}








/*
функция для переворачивания параметров lcp_url_result в name и value
например:
входной массив:
[0]['name']='name1';
[0]['value']='val1';
[1]['name']='name2';
[1]['value']='val2';
выходной массив:
['name'][0]='name1';
['name'][1]='name2';
['value'][0]='val1';
['value'][1]='val2';
*/
function lcp_url_convert(lcp_url_result){
  res={};
  res['name']=[];
  res['value']=[];
  for (var i = 0; i < lcp_url_result.length; i++) {
    res['name'][i]=lcp_url_result[i]['name'];
    res['value'][i]=lcp_url_result[i]['value'];
  }
  return res;
}






/*
функция для преобразования параметров обычного объекта в lcp_url (name и value)
например:
входной массив:
['name1']='val1';
['name2']='val2';
выходной массив:
['name'][0]='name1';
['name'][1]='name2';
['value'][0]='val1';
['value'][1]='val2';
*/
function object_to_lcp_url_convert(obj){
  res={};
  res['name']=[];
  res['value']=[];
  for (var i = 0; i < Object.keys(obj).length; i++) {
    res.name[i]=Object.keys(obj)[i];
    res.value[i]=obj[res.name[i]];
  }
  return res;
}









/*
функция для преобразования lcp_url в обычный объект (name и value)
например:
входной массив:
['name'][0]='name1';
['name'][1]='name2';
['value'][0]='val1';
['value'][1]='val2';
выходной массив:
['name1']='val1';
['name2']='val2';
*/
function lcp_url_to_object_convert(lcp_url_obj){
  var param={};
  for (var i = 0; i < lcp_url_obj.length; i++) {
    param[lcp_url_obj[i].name]=decodeURI(lcp_url_obj[i].value);
  }
  return param;
}
