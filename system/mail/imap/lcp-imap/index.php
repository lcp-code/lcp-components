<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_imap',
  'type' => 'system',
  'group' => 'Почта',
  'inner' => array(
    'description' => 'Функция для получения почты средствами IMAP',
    'css' => array(),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'server',
    '1' => 'port',
    '2' => 'username',
    '3' => 'password',
  ),
  'set_value_php' => array(
    '0' => 'imap.server.ru',
    '1' => '993',
    '2' => 'username@mail.ru',
    '3' => 'password',
  ),
  'set_description_php' => array(
    '0' => 'IMAP сервер для подключения к почте',
    '1' => 'Порт (безопасный, по умолчанию 993)',
    '2' => 'Почта (логин)',
    '3' => 'Пароль от почты',
  ),
  'get_php' => array(
    '0' =>'header',
    '1' =>'body',
    '2' =>'attachments',
  ),
  'get_description_php' => array(
    '0' => 'Возвращает массив с заголовками письма (тема, фио отправителя, почта отправителя и пр.)',
    '1' => 'Возвращает массив с текстом письма',
    '2' => 'Возвращает массив с файлами, прикрепленными к письму (внимание, имена файлов оригинальные и могут содержать кириллицу. Для сохранения содержимого файла, воспользуйтесь функцией file_put_contents)',
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array(),
);
}
?>




<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_imap extends lcp_components implements lcp_components_build{
    public $server;
    public $port;
    public $username;
    public $password;
    private $imap;

    function __construct($param=array()){
      $this->server=$param['server'];
      $this->port=$param['port'];
      $this->username=$param['username'];
      $this->password=$param['password'];
      parent::__construct($param);
    }

    function __build_component(){
      $res_arr=array();

      $this->imap = imap_open('{'.$this->server.':'.$this->port.'/imap/ssl}INBOX', $this->username, $this->password);
      $mails_id = imap_search($this->imap, 'ALL');

      foreach ($mails_id as $num) {
        $msg_arr=$this->getmsg($this->imap,$num);
        $res_arr['header'][]=$msg_arr['header'];
        $res_arr['body'][]=$msg_arr['body'];
        $res_arr['attachments'][]=$msg_arr['attachments'];
      }

      imap_close($this->imap);
      return $res_arr;
    }

    private function getmsg($mbox,$mid) {
        // input $mbox = IMAP stream, $mid = message id
        // output all the following:
        $htmlmsg = $plainmsg = $charset = $subject = '';
        $attachments = array();

        // HEADER
        $h = imap_header($mbox,$mid);
        $h = json_decode(json_encode($h), true);
        $subject = mb_decode_mimeheader($h['subject']);
        // add code here to get date, from, to, cc, subject...

        // BODY
        $s = imap_fetchstructure($mbox,$mid);
        if (!$s->parts){  // simple
          $param=$this->getpart($mbox,$mid,$s,0);  // pass 0 as part-number
          $htmlmsg=$param['body'];
          $attachments=$param['attachments'];
        } else {  // multipart: cycle through each part
            foreach ($s->parts as $partno0=>$p){
              $param=$this->getpart($mbox,$mid,$p,$partno0+1,$htmlmsg,$attachments);
              $htmlmsg=$param['body'];
              $attachments=$param['attachments'];
            }
        }
      return array(
        'header'=>array(
          'subject'=>$subject
        ),
        'body'=>$htmlmsg,
        'attachments'=>$attachments,
      );
    }



    private function getpart($mbox,$mid,$p,$partno,$htmlmsg='',$attachments=array()) {
        // $partno = '1', '2', '2.1', '2.1.3', etc for multipart, 0 if simple
        // global $htmlmsg,$plainmsg,$charset,$attachments;

        // DECODE DATA
        $data = ($partno)?
            imap_fetchbody($mbox,$mid,$partno):  // multipart
            imap_body($mbox,$mid);  // simple
        // Any part may be encoded, even plain text messages, so check everything.
        if ($p->encoding==4)
            $data = quoted_printable_decode($data);
        elseif ($p->encoding==3)
            $data = base64_decode($data);

        // PARAMETERS
        // get all parameters, like charset, filenames of attachments, etc.
        $params = array();
        if ($p->parameters)
            foreach ($p->parameters as $x)
                $params[strtolower($x->attribute)] = $x->value;
        if ($p->dparameters)
            foreach ($p->dparameters as $x)
                $params[strtolower($x->attribute)] = $x->value;

        // ATTACHMENT
        // Any part with a filename is an attachment,
        // so an attached text file (type 0) is not mistaken as the message.
        if ($params['filename'] || $params['name']) {
            // filename may be given as 'Filename' or 'Name' or both
            $filename = ($params['filename'])? $params['filename'] : $params['name'];
            // filename may be encoded, so see imap_mime_header_decode()
            $filename = mb_decode_mimeheader($filename);
            $attachments['filename'][] = $filename;  // this is a problem if two files have same name
            $attachments['data'][] = $data;
        }

        // TEXT
        if (($p->type==0) && ($data)) {
            // echo $data;
            // Messages may be split in different parts because of inline attachments,
            // so append parts together with blank row.
            if (strtolower($p->subtype)=='plain')
                $plainmsg.= trim($data) ."\n\n";
            else
                $htmlmsg .= $data."<br><br>";
            $charset = $params['charset'];  // assume all parts are same charset
        }

        // EMBEDDED MESSAGE
        // Many bounce notifications embed the original message as type 2,
        // but AOL uses type 1 (multipart), which is not handled here.
        // There are no PHP functions to parse embedded messages,
        // so this just appends the raw source to the main message.
        elseif ($p->type==2 && $data) {
            $plainmsg.= $data."\n\n";
        }

        // SUBPART RECURSION
        if ($p->parts) {
            foreach ($p->parts as $partno0=>$p2){
              $param=$this->getpart($mbox,$mid,$p2,$partno.'.'.($partno0+1),$htmlmsg,$attachments);  // 1.2, 1.2.1, etc.
              $htmlmsg=$param['body'];
              $attachments=$param['attachments'];
            }
        }
        return array(
          'body'=>$htmlmsg,
          'attachments'=>$attachments,
        );
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_imap_init($param=array()){
    $lcp_obj=new lcp_imap($param);
    return $lcp_obj->html();
  }
}
?>
