Пример использования:
<?php
$popup_content='';
echo components('lcp_popup_default',array(
  'id_popup' => 'popup_basket',
  'title' => 'Сайт создан на платформе сервиса LCP!',
  'content_html' => $popup_content,
  'popup_show' => 'popup_basket',
  'popup_close' => 'popup_basket_close',
  'height' => '400px',
  'width' => '700px',
  'title_setting' => array(
    'font-size' => '19px',
    'font-weight' => 'bold',
    'text-align' => 'left'
  )
));
?>
