<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_popup_default',
  'type' => 'system',
  'group' => 'Сообщения/диалоги',
  'inner' => array(
    'description' => 'POP-UP - всплывающее окно, используемое в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/popups/popup-default1/popup-default1.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id_popup',
    '1' => 'title',
    '2' => 'content_html',
    '3' => 'popup_show',
    '4' => 'popup_close',
    '5' => 'title_setting',
    '6' => 'close_setting',
    '7' => 'width',
    '8' => 'height'
  ),
  'set_value_php' => array(
    '0' => 'popup_basket',
    '1' => 'Название окна',
    '2' => 'Описание',
    '3' => 'popup_basket',
    '4' => 'popup_basket_close',
    '5' => '',
    '6' => '',
    '7' => '400',
    '8' => '400'
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Заголовок окна',
    '2' => 'Описание (можно использовать html)',
    '3' => 'Название функции для отображения всплывающего окна (function)',
    '4' => 'Название функции для скрытия всплывающего окна (function)',
    '5' => '<div>Массив css стилей для настройки внешнего вида заголовка.</div><div>Например:</div><div>array("font-size"=>"19px", "font-weight"=>"bold", "text-align"=>"center");</div>',
    '6' => '<div>Массив css стилей для настройки внешнего вида кнопки закрытия окна.</div>',
    '7' => 'Ширина всплывающего окна',
    '8' => 'Высота всплывающего окна'
  ),
  'get_php' => array(),
  'get_description_php' => array(),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
$css_install_arr[]='/components/system/popups/popup-default1/popup-default1.css';
// $js_install_arr[]='/components/system/popups/popup-default1/popup-default1.js';
?>




<?php
function lcp_popup_default($param=''){
  $id_popup=$param['id_popup'];
  $title=$param['title'];
  $content=$param['content_html'];
  $function_popup_show=$param['popup_show'];
  $function_popup_close=$param['popup_close'];
  $title_setting=$param['title_setting'];
  $close_setting=$param['close_setting'];
  $width=$param['width'];
  $height=$param['height'];
  $width=(!empty($width))?$width:'500px';
  $height=(!empty($height))?$height:'490px';
  $title_font_size=$title_setting['font-size'];
  $title_font_weight=$title_setting['font-weight'];
  $title_text_align=$title_setting['text-align'];

  $close_display=$close_setting['display'];
  $close_text_align=$close_setting['text-align'];
  $close_vertical_align=$close_setting['vertical-align'];

  $html = '';
  $html .= '<div class="popup_message" id="'.$id_popup.'">';
  $html .= '<div class="popup_message_zatemn" onclick="'.$function_popup_close.'();"></div>';
  $html .= '<div class="popup_message_content">';
  $html .= '<div class="popup_message_title_table">';
  $html .= '<div class="popup_message_title_table_td">';
  $html .= $title;
  $html .= '</div>';
  $html .= '<div class="popup_message_title_close_table_td" onclick="'.$function_popup_close.'();">';
  $html .= '<span class="fa">&#10006;</span>';
  $html .= '</div>';
  $html .= '</div>';
  $html .= '<div class="popup_message_line"></div>';
  $html .= '<div class="popup_form_padding">';
  $html .= $content;
  $html .= '</div>';
  $html .= '</div>';
  $html .= '</div>';

  $html .= '<style media="screen">';
  $html .= '#'.$id_popup.'>.popup_message_content{';
  $html .= 'max-width: '.$width.';';
  $html .= 'max-height: '.$height.';';
  $html .= '}';
  $html .= '#'.$id_popup.'>.popup_message_content>.popup_message_title_table>.popup_message_title_table_td{';
  $html .= 'font-size: '.$title_font_size.';';
  $html .= 'font-weight: '.$title_font_weight.';';
  $html .= 'text-align: '.$title_text_align.';';
  $html .= '}';
  $html .= '#'.$id_popup.'>.popup_message_content>.popup_message_title_table>.popup_message_title_close_table_td{';
  $html .= 'display: '.$close_display.';';
  $html .= 'text-align: '.$close_text_align.';';
  $html .= 'vertical-align: '.$close_vertical_align.';';
  $html .= '}';
  $html .= '#'.$id_popup.'>.popup_message_content>.popup_form_padding{';
  $html .= 'height: '.(((float)str_replace('px','',$height))-49).'px;';
  $html .= 'overflow-y: auto;';
  $html .= 'overflow-x: hidden;';
  $html .= '}';
  $html .='</style>';

  $html.='<script type="text/javascript">';
  $html.='function '.$function_popup_show.'(){';
  $html.='document.getElementById(\''.$id_popup.'\').style.display=\'block\';';
  $html.='document.body.classList.add(\'body_scroll_popup\');';
  $html.='}';
  $html.='function '.$function_popup_close.'(){';
  $html.='document.getElementById(\''.$id_popup.'\').style.display=\'none\';';
  $html.='document.body.classList.remove(\'body_scroll_popup\');';
  $html.='}';
  $html.='</script>';

  return $html;
}
?>
