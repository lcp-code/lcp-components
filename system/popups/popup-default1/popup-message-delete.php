<?php
function popup_message($param){
  $id_popup=$param['id_popup'];
  $title=$param['title'];
  $content=$param['content_html'];
  $function_popup_show=$param['popup_show'];
  $function_popup_close=$param['popup_close'];
  $title_setting=$param['title_setting'];
  $close_setting=$param['close_setting'];
  $width=$param['width'];
  $height=$param['height'];
  $width=(!empty($width))?$width:'500px';
  $height=(!empty($height))?$height:'490px';
  $title_font_size=$title_setting['font-size'];
  $title_font_weight=$title_setting['font-weight'];
  $title_text_align=$title_setting['text-align'];

  $close_display=$close_setting['display'];
  $close_text_align=$close_setting['text-align'];
  $close_vertical_align=$close_setting['vertical-align'];

  $html = '';
  $html .= '<div class="popup_message" id="'.$id_popup.'">';
  $html .= '<div class="popup_message_zatemn" onclick="'.$function_popup_close.'();"></div>';
  $html .= '<div class="popup_message_content">';
  $html .= '<div class="popup_message_title_table">';
  $html .= '<div class="popup_message_title_table_td">';
  $html .= $title;
  $html .= '</div>';
  $html .= '<div class="popup_message_title_close_table_td" onclick="'.$function_popup_close.'();">';
  $html .= 'X';
  $html .= '</div>';
  $html .= '</div>';
  $html .= '<div class="popup_message_line"></div>';
  $html .= $content;
  $html .= '</div>';
  $html .= '</div>';

  $html .= '<style media="screen">';
  $html .= '#'.$id_popup.'>.popup_message_content{';
  $html .= 'max-width: '.$width.';';
  $html .= 'max-height: '.$height.';';
  $html .= '}';
  $html .= '#'.$id_popup.'>.popup_message_content>.popup_message_title_table>.popup_message_title_table_td{';
  $html .= 'font-size: '.$title_font_size.';';
  $html .= 'font-weight: '.$title_font_weight.';';
  $html .= 'text-align: '.$title_text_align.';';
  $html .= '}';
  $html .= '#'.$id_popup.'>.popup_message_content>.popup_message_title_table>.popup_message_title_close_table_td{';
  $html .= 'display: '.$close_display.';';
  $html .= 'text-align: '.$close_text_align.';';
  $html .= 'vertical-align: '.$close_vertical_align.';';
  $html .= '}';

  $html .='</style>';

  $html.='<script type="text/javascript">';
  $html.='function '.$function_popup_show.'(){';
  $html.='$(\'#'.$id_popup.'\').css(\'display\',\'block\');';
  $html.='}';
  $html.='function '.$function_popup_close.'(){';
  $html.='$(\'#'.$id_popup.'\').css(\'display\',\'none\');';
  $html.='}';
  $html.='</script>';

  return $html;
}
?>
