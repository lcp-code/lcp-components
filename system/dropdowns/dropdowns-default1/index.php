<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_dropdown_default1',
  'type' => 'system',
  'group' => 'Выпадающий список',
  'inner' => array(
    'description' => 'Выпадающий список, используемый в панели администратора по-умолчанию. Создан на основе тега select и option (в браузерах и мобильных устройствах может отображаться по-разному)',
    'css' => array(
      '0' => '/components/system/dropdowns/dropdowns-default1/dropdown-default1.css'
    ),
    'js' => array(
      '0' => '/components/system/dropdowns/dropdowns-default1/dropdown-default1.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'name',
    '2' => 'value',
    '3' => 'class',
    '4' => 'onclick',
    '5' => 'onmousedown',
    '6' => 'onmouseup',
    '7' => 'id_child_arr',
    '8' => 'title_child_arr',
    '9' => 'name_child_arr',
    '10' => 'value_child_arr',
    '11' => 'class_child_arr'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '',
    '11' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Имя выпадающего списка (тег name)',
    '2' => 'Благодаря свойству value, отображается (и выбирается) необходимое значение списка',
    '3' => 'Класс для изменения стилей (тег class)',
    '4' => 'Событие, возникающее при клике левой кнопкой мыши (тег onclick)',
    '5' => 'Событие, возникающее при нажатии левой кнопкой мыши (тег onmousedown)',
    '6' => 'Событие, возникающее при отпускании левой кнопкой мыши (тег onmouseup)',
    '7' => 'Массив идентификаторов дочерних (выпадающих) пунктов списка (тег id)',
    '8' => 'Массив отображаемого текста для дочерних (выпадающих) пунктов списка',
    '9' => 'Массив имен для дочерних (выпадающих) пунктов списка (тег name)',
    '10' => 'Массив значений (предназначенных для выбора), указанных для дочерних (выпадающих) пунктов списка (тег value)',
    '11' => 'Массив классов для изменения стилей, указанных для дочерних (выпадающих) пунктов списка (тег class)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
  ),
  'set_value_js' => array(
  ),
  'set_description_js' => array(
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}
?>




<?php
function lcp_dropdown_default1($param=''){
  $id_dropdown=$param['id'];
  $name=$param['name'];
  $value=$param['value'];
  $class=$param['class'];
  $onlick=$param['onclick'];
  $onmousedown=$param['onmousedown'];
  $onmouseup=$param['onmouseup'];
  $id_child_arr=$param['id_child_arr'];
  $title_child_arr=$param['title_child_arr'];
  $name_child_arr=$param['name_child_arr'];
  $value_child_arr=$param['value_child_arr'];
  $class_child_arr=$param['class_child_arr'];
  $default_id_dropdown='';
  $default_name='';
  $default_value='';
  $default_class='';
  $default_onlick='';
  $default_onmousedown='';
  $default_onmouseup='';
  $default_id_child_arr='';
  $default_title_child_arr='';
  $default_name_child_arr='';
  $default_value_child_arr='';
  $default_class_child_arr='';
  $id_dropdown=(empty($id_dropdown))?$default_id_dropdown:$id_dropdown;
  $name=(empty($name))?$default_name:$name;
  $value=(empty($value))?$default_value:$value;
  $class=(empty($class))?$default_class:$class;
  $onlick=(empty($onlick))?$default_onlick:$onlick;
  $onmousedown=(empty($onmousedown))?$default_onmousedown:$onmousedown;
  $onmouseup=(empty($onmouseup))?$default_onmouseup:$onmouseup;
  $id_child_arr=(empty($id_child_arr))?$default_id_child_arr:$id_child_arr;
  $title_child_arr=(empty($title_child_arr))?$default_title_child_arr:$title_child_arr;
  $name_child_arr=(empty($name_child_arr))?$default_name_child_arr:$name_child_arr;
  $value_child_arr=(empty($value_child_arr))?$default_value_child_arr:$value_child_arr;
  $class_child_arr=(empty($class_child_arr))?$default_class_child_arr:$class_child_arr;
  $id_dropdown=(!empty($id_dropdown))?' id="'.$id_dropdown.'" ':'';
  $name=(!empty($name))?' name="'.$name.'" ':'';
  $onlick=(!empty($onlick))?' onclick="'.$onlick.'" ':'';
  $onmousedown=(!empty($onmousedown))?' onmousedown="'.$onmousedown.'" ':'';
  $onmouseup=(!empty($onmouseup))?' onmouseup="'.$onmouseup.'" ':'';

  $html='';
  $html.='<div class="dropdown_default '.$class.'" '.$id_dropdown.$name.$onlick.$onmousedown.$onmouseup.'>';
  $html.='<select>';
  for ($i=0; $i < count($title_child_arr); $i++) {
    $id_child_arr_opt=(!empty($id_child_arr[$i]))?' id="'.$id_child_arr[$i].'" ':'';
    $name_child_arr_opt=(!empty($name_child_arr[$i]))?' name="'.$name_child_arr[$i].'" ':'';
    $value_child_arr_opt=(!empty($value_child_arr[$i]))?' value="'.$value_child_arr[$i].'" ':'';
    $class_child_arr_opt=(!empty($class_child_arr[$i]))?' class="'.$class_child_arr[$i].'" ':'';
    $html.='<option '.$id_child_arr_opt.$name_child_arr_opt.$value_child_arr_opt.$class_child_arr_opt.'>'.$title_child_arr[$i].'</option>';
  }
  $html.='</select>';
  $html.='</div>';
  // $html='<span class="checked_default_all '.$class.'" onclick="var param=[];param[\'checked\']=\'checked_change\';lcp_checked_default(this,param);'.$onclick.'" '.$id_checked.'><div class="checked_default_div">'.$check_html.'</div>'.$title.'</span>';
  // $html.='<div class="checked_default_div"><input type="checkbox" '.$id_checked.' class="checkbox_default">'.$title.'</div>';
  return $html;
}
?>
