<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_dropdown_default2',
  'type' => 'system',
  'group' => 'Выпадающий список',
  'inner' => array(
    'description' => 'Выпадающий список, используемый в панели администратора по-умолчанию. Создан на основе тега div (в браузерах и мобильных устройствах отображается одинаково)',
    'css' => array(
      '0' => '/components/system/dropdowns/dropdowns-default2/dropdown-default2.css'
    ),
    'js' => array(
      '0' => '/components/system/dropdowns/dropdowns-default2/dropdown-default2.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'name',
    '2' => 'value',
    '3' => 'text',
    '4' => 'class',
    '5' => 'onclick',
    '6' => 'onmousedown',
    '7' => 'onmouseup',
    '8' => 'height_child',
    '9' => 'width',
    '10' => 'html_img_down_arrow',
    '11' => 'id_child_arr',
    '12' => 'title_child_arr',
    '13' => 'name_child_arr',
    '14' => 'value_child_arr',
    '15' => 'class_child_arr',
    '16' => 'onclick_child_arr'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '<i class="fa fa-sort-desc" aria-hidden="true"></i>',
    '11' => '',
    '12' => '',
    '13' => '',
    '14' => '',
    '15' => '',
    '16' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Имя выпадающего списка (тег name)',
    '2' => 'Благодаря свойству value, отображается (и выбирается) необходимое значение списка',
    '3' => 'Текст, отображаемый по-умолчанию при загрузке компонента (если ни один пункт не выбран)',
    '4' => 'Класс для изменения стилей (тег class)',
    '5' => 'Событие, возникающее при клике левой кнопкой мыши (тег onclick)',
    '6' => 'Событие, возникающее при нажатии левой кнопкой мыши (тег onmousedown)',
    '7' => 'Событие, возникающее при отпускании левой кнопкой мыши (тег onmouseup)',
    '8' => 'Максимальная высота выпадающего списка',
    '9' => 'Ширина компонента',
    '10' => 'Изображение, html-код или тег из fontawesome для отображения стрелки вниз выпадающего списка (крайняя правая кнопка)',
    '11' => 'Массив идентификаторов дочерних (выпадающих) пунктов списка (тег id)',
    '12' => 'Массив отображаемого текста для дочерних (выпадающих) пунктов списка',
    '13' => 'Массив имен для дочерних (выпадающих) пунктов списка (тег name)',
    '14' => 'Массив значений (предназначенных для выбора), указанных для дочерних (выпадающих) пунктов списка (тег value)',
    '15' => 'Массив классов для изменения стилей, указанных для дочерних (выпадающих) пунктов списка (тег class)',
    '16' => 'Массив событий, возникающих при клике левой кнопкой мыши, для дочерних (выпадающих) пунктов списка (тег onclick)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
  ),
  'set_value_js' => array(
  ),
  'set_description_js' => array(
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}
$css_install_arr[]='/components/system/dropdowns/dropdowns-default2/dropdown-default2.css';
$js_install_arr[]='/components/system/dropdowns/dropdowns-default2/dropdown-default2.js';
?>




<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_dropdown_default2 extends lcp_components implements lcp_components_build{
    public $value;
    public $text;
    public $height_child;
    public $html_img_down_arrow='<i class="fa fa-sort-desc" aria-hidden="true"></i>';
    public $id_child_arr;
    public $title_child_arr;
    public $name_child_arr;
    public $value_child_arr;
    public $class_child_arr;
    public $onclick_child_arr;


    function __construct($param=array()){
      $this->value=isset($param['value'])?$param['value']:$this->value;
      $this->text=isset($param['text'])?$param['text']:$this->text;
      $this->height_child=isset($param['height_child'])?$param['height_child']:$this->height_child;
      $this->html_img_down_arrow=isset($param['html_img_down_arrow'])?$param['html_img_down_arrow']:$this->html_img_down_arrow;
      $this->id_child_arr=isset($param['id_child_arr'])?$param['id_child_arr']:$this->id_child_arr;
      $this->title_child_arr=isset($param['title_child_arr'])?$param['title_child_arr']:$this->title_child_arr;
      $this->name_child_arr=isset($param['name_child_arr'])?$param['name_child_arr']:$this->name_child_arr;
      $this->value_child_arr=isset($param['value_child_arr'])?$param['value_child_arr']:$this->value_child_arr;
      $this->class_child_arr=isset($param['class_child_arr'])?$param['class_child_arr']:$this->class_child_arr;
      $this->onclick_child_arr=isset($param['onclick_child_arr'])?$param['onclick_child_arr']:$this->onclick_child_arr;
      parent::__construct($param);
    }

    function __build_component(){
      $html='';
      $this->class='dropdown_default2 '.$this->class;
      $this->style='width:'.$this->width.';'.$this->style;
      $attr_str=$this->__get_attributes_html();

      if ((!empty($this->value))||($this->value=='0')){
        for ($i=0; $i < count($this->value_child_arr); $i++) {
          if ($this->value===$this->value_child_arr[$i]){
            $titl=$this->title_child_arr[$i];
            break;
          }
        }
      }
      if ((empty($titl))&&(empty($this->text))){
        $titl=$this->title_child_arr[0];
      } else {
        $titl=(!empty($titl))?$titl:$this->text;
      }
      if (!empty($this->height_child)){
        $height_child_html.='';
        $height_child_html.='overflow-y: auto;';
        $height_child_html.='max-height: '.$this->height_child.';';
        $height_child_style=' style="'.$height_child_html.'" ';
      }

      $html='';
      $html.='<div '.$attr_str.'>';
      $html.='<div class="dropdown_blocks" onclick="dropdown_default2_list_start(this);">';
      $html.='<div class="dropdown_block2">';
      $html.='<div class="dropdown_title2" data-value="'.$this->value.'">';
      $html.=$titl;
      $html.='</div>';
      $html.='<div class="dropdown_down_img">';

      $html.=$this->html_img_down_arrow;
      $html.='</div>';
      $html.='</div>';
      $html.='</div>';


      $html.='<div tabindex="'.mt_rand().'" class="dropdown_default2_list dropdown_default2_list_hide" onblur="dropdown_default2_blur(this);" onkeydown="dropdown_keyboard(event,this);">';
      $html.='<div class="dropdown_default2_list_ul" '.$height_child_style.'>';
      for ($i=0; $i < count($this->title_child_arr); $i++) {
        $id_child_arr_str=(!empty($this->id_child_arr[$i]))?' id="'.$this->id_child_arr[$i].'" ':'';
        $html.='<div class="dropdown_default2_option '.$this->class_child_arr[$i].'" '.$id_child_arr_str.$this->name_child_arr[$i].' onclick="'.$this->onclick_child_arr[$i].'dropdown_default2_opt_click(this);" data-value="'.$this->value_child_arr[$i].'">';
        $html.=$this->title_child_arr[$i];
        $html.='</div>';
      }
      $html.='</div>';
      $html.='</div>';
      $html.='</div>';
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_dropdown_default2_init($param=array()){
    $lcp_obj=new lcp_dropdown_default2($param);
    return $lcp_obj->html();
  }
}
?>





<?php
// function lcp_dropdown_default2($param=''){
//   $id_components=getcelldb('id','id','components','name=\'lcp_dropdown_default2\'');
//
//
//   // $value=$param['value'];
//   // $text=$param['text'];
//   // $height_child=$param['height_child'];
//   // $html_img_down_arrow=$param['html_img_down_arrow'];
//   // $id_child_arr=$param['id_child_arr'];
//   // $title_child_arr=$param['title_child_arr'];
//   // $name_child_arr=$param['name_child_arr'];
//   // $value_child_arr=$param['value_child_arr'];
//   // $class_child_arr=$param['class_child_arr'];
//   // $onclick_child_arr=$param['onclick_child_arr'];
//   //
//   //
//   //
//   // $default_id_dropdown=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'id\'');
//   // $default_name=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'name\'');
//   // $default_class=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'class\'');
//   // $default_onlick=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'onclick\'');
//   // $default_height_child=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'height_child\'');
//   // $default_width=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'width\'');
//   // $default_html_img_down_arrow=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'html_img_down_arrow\'');
//   // $default_onmousedown=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'onmousedown\'');
//   // $default_onmouseup=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'onmouseup\'');
//   // $default_id_child_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'id_child_arr\'');
//   // $default_title_child_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'title_child_arr\'');
//   // $default_name_child_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'name_child_arr\'');
//   // $default_value_child_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'value_child_arr\'');
//   // $default_class_child_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'class_child_arr\'');
//   // $default_onclick_child_arr=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'onclick_child_arr\'');
//   // $id_dropdown=(empty($id_dropdown))?$default_id_dropdown:$id_dropdown;
//   // $name=(empty($name))?$default_name:$name;
//   // $class=(empty($class))?$default_class:$class;
//   // $onclick=(empty($onclick))?$default_onlick:$onclick;
//   // $onmousedown=(empty($onmousedown))?$default_onmousedown:$onmousedown;
//   // $onmouseup=(empty($onmouseup))?$default_onmouseup:$onmouseup;
//   // $height_child=(empty($height_child))?$default_height_child:$height_child;
//   // $width=(empty($width))?$default_width:$width;
//   // $html_img_down_arrow=(empty($html_img_down_arrow))?$default_html_img_down_arrow:$html_img_down_arrow;
//   // $id_child_arr=(empty($id_child_arr))?$default_id_child_arr:$id_child_arr;
//   // $title_child_arr=(empty($title_child_arr))?$default_title_child_arr:$title_child_arr;
//   // $name_child_arr=(empty($name_child_arr))?$default_name_child_arr:$name_child_arr;
//   // $value_child_arr=(empty($value_child_arr))?$default_value_child_arr:$value_child_arr;
//   // $class_child_arr=(empty($class_child_arr))?$default_class_child_arr:$class_child_arr;
//   // $onclick_child_arr=(empty($onclick_child_arr))?$default_onclick_child_arr:$onclick_child_arr;
//   // $id_dropdown=(!empty($id_dropdown))?' id="'.$id_dropdown.'" ':'';
//   // $name=(!empty($name))?' name="'.$name.'" ':'';
//   // $onclick=(!empty($onclick))?' onclick="'.$onclick.'" ':'';
//   // $onmousedown=(!empty($onmousedown))?' onmousedown="'.$onmousedown.'" ':'';
//   // $onmouseup=(!empty($onmouseup))?' onmouseup="'.$onmouseup.'" ':'';
//
//   // $width=(!empty($width))?' style="width:'.$width.'" ':'';
//
//   if ((!empty($value))||($value=='0')){
//     for ($i=0; $i < count($value_child_arr); $i++) {
//       if ($value===$value_child_arr[$i]){
//         $titl=$title_child_arr[$i];
//         break;
//       }
//     }
//   }
//   if ((empty($titl))&&(empty($text))){
//     $titl=$title_child_arr[0];
//   } else {
//     $titl=(!empty($titl))?$titl:$text;
//   }
//   if (!empty($height_child)){
//     $height_child_html.='';
//     $height_child_html.='overflow-y: auto;';
//     $height_child_html.='max-height: '.$height_child.';';
//     $height_child_style=' style="'.$height_child_html.'" ';
//   }
//
//   $html='';
//   $html.='<div class="dropdown_default2 '.$class.'" '.$id_dropdown.$name.$onclick.$onmousedown.$onmouseup.$width.'>';
//   $html.='<div class="dropdown_blocks" onclick="dropdown_default2_list_start(this);">';
//   $html.='<div class="dropdown_block2">';
//   $html.='<div class="dropdown_title2" data-value="'.$value.'">';
//   $html.=$titl;
//   $html.='</div>';
//   $html.='<div class="dropdown_down_img">';
//   $html.=$html_img_down_arrow;
//   $html.='</div>';
//   $html.='</div>';
//   $html.='</div>';
//
//
//   $html.='<div tabindex="'.mt_rand().'" class="dropdown_default2_list dropdown_default2_list_hide" onblur="dropdown_default2_blur(this);" onkeydown="dropdown_keyboard(event,this);">';
//   $html.='<ul class="dropdown_default2_list_ul" '.$height_child_style.'>';
//   for ($i=0; $i < count($title_child_arr); $i++) {
//     $id_child_arr_str=(!empty($id_child_arr[$i]))?' id="'.$id_child_arr[$i].'" ':'';
//     $html.='<li class="dropdown_default2_option '.$class_child_arr[$i].'" '.$id_child_arr_str.$name_child_arr[$i].' onclick="'.$onclick_child_arr[$i].'dropdown_default2_opt_click(this);" data-value="'.$value_child_arr[$i].'">';
//     $html.=$title_child_arr[$i];
//     $html.='</li>';
//   }
//   $html.='</ul>';
//   $html.='</div>';
//   $html.='</div>';
//   return $html;
// }
?>
