<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_dropdown_multi_check',
  'type' => 'system',
  'group' => 'Выпадающий список',
  'inner' => array(
    'description' => '
    <p>Выпадающий список с использованием множественного выбора (через галочки), используемый в панели администратора по-умолчанию. Создан на основе тега div (в браузерах и мобильных устройствах отображается одинаково). Совместно с компонентом "lcp_checked_default".</p>
    <p>Для получения значения через js, используйте функцию: get_dropdown_multi_check_opt_value("id_dropdown");</p>
    <p>Чтобы установить значение через js, используйте функцию: set_dropdown_multi_check_opt_value("id_dropdown","values_arr");</p>
    ',
    'css' => array(
      '0' => '/components/system/dropdowns/dropdown-multi-check/dropdown-multi-check.css'
    ),
    'js' => array(
      '0' => '/components/system/dropdowns/dropdown-multi-check/dropdown-multi-check.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'name',
    '2' => 'value_arr',
    '3' => 'text',
    '4' => 'class',
    '5' => 'onclick',
    '6' => 'onmousedown',
    '7' => 'onmouseup',
    '8' => 'height_child',
    '9' => 'width',
    '10' => 'html_img_down_arrow',
    '11' => 'id_child_arr',
    '12' => 'title_child_arr',
    '13' => 'name_child_arr',
    '14' => 'value_child_arr',
    '15' => 'class_child_arr',
    '16' => 'onclick_child_arr',
    '17' => 'alt_title',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '<i class="fa fa-sort-desc" aria-hidden="true"></i>',
    '11' => '',
    '12' => '',
    '13' => '',
    '14' => '',
    '15' => '',
    '16' => '',
    '17' => '-- Не выбрано --',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Имя выпадающего списка (тег name)',
    '2' => 'Благодаря свойству value_arr, отображается (и выбирается) необходимые значения списка',
    '3' => 'Текст, отображаемый по-умолчанию при загрузке компонента (если ни один пункт не выбран)',
    '4' => 'Класс для изменения стилей (тег class)',
    '5' => 'Событие, возникающее при клике левой кнопкой мыши (тег onclick)',
    '6' => 'Событие, возникающее при нажатии левой кнопкой мыши (тег onmousedown)',
    '7' => 'Событие, возникающее при отпускании левой кнопкой мыши (тег onmouseup)',
    '8' => 'Максимальная высота выпадающего списка',
    '9' => 'Ширина компонента',
    '10' => 'Изображение, html-код или тег из fontawesome для отображения стрелки вниз выпадающего списка (крайняя правая кнопка)',
    '11' => 'Массив идентификаторов дочерних (выпадающих) пунктов списка (тег id)',
    '12' => 'Массив отображаемого текста для дочерних (выпадающих) пунктов списка',
    '13' => 'Массив имен для дочерних (выпадающих) пунктов списка (тег name)',
    '14' => 'Массив значений (предназначенных для выбора), указанных для дочерних (выпадающих) пунктов списка (тег value_arr)',
    '15' => 'Массив классов для изменения стилей, указанных для дочерних (выпадающих) пунктов списка (тег class)',
    '16' => 'Массив событий, возникающих при клике левой кнопкой мыши, для дочерних (выпадающих) пунктов списка (тег onclick)',
    '17' => 'Если значение не указано или ничего не выбрано - отобразить альтернативный текст',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
  ),
  'set_value_js' => array(
  ),
  'set_description_js' => array(
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}
$css_install_arr[]='/components/system/dropdowns/dropdown-multi-check/dropdown-multi-check.css';
$js_install_arr[]='/components/system/dropdowns/dropdown-multi-check/dropdown-multi-check.js';
?>



<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_dropdown_multi_check extends lcp_components implements lcp_components_build{
    public $value_arr;
    public $text;
    public $height_child;
    public $html_img_down_arrow='<i class="fa fa-sort-desc" aria-hidden="true"></i>';
    public $id_child_arr;
    public $title_child_arr;
    public $name_child_arr;
    public $value_child_arr;
    public $class_child_arr;
    public $onclick_child_arr;
    public $alt_title='-- Не выбрано --';

    function __construct($param=array()){
      $this->value_arr=isset($param['value_arr'])?$param['value_arr']:$this->value_arr;
      $this->text=isset($param['text'])?$param['text']:$this->text;
      $this->height_child=isset($param['height_child'])?$param['height_child']:$this->height_child;
      $this->html_img_down_arrow=isset($param['html_img_down_arrow'])?$param['html_img_down_arrow']:$this->html_img_down_arrow;
      $this->id_child_arr=isset($param['id_child_arr'])?$param['id_child_arr']:$this->id_child_arr;
      $this->title_child_arr=isset($param['title_child_arr'])?$param['title_child_arr']:$this->title_child_arr;
      $this->name_child_arr=isset($param['name_child_arr'])?$param['name_child_arr']:$this->name_child_arr;
      $this->value_child_arr=isset($param['value_child_arr'])?$param['value_child_arr']:$this->value_child_arr;
      $this->class_child_arr=isset($param['class_child_arr'])?$param['class_child_arr']:$this->class_child_arr;
      $this->onclick_child_arr=isset($param['onclick_child_arr'])?$param['onclick_child_arr']:$this->onclick_child_arr;
      $this->alt_title=isset($param['alt_title'])?$param['alt_title']:$this->alt_title;
      parent::__construct($param);
    }

    function __build_component(){
      $html='';
      $this->class='dropdown_multi_check '.$this->class;
      $this->style='width:'.$this->width.';'.$this->style;
      $attr_str=$this->__get_attributes_html();
      $phpVersion = substr(phpversion(), 0, 3)*1;

      if (empty($this->value_arr)){
        $this->value_arr=array();
      }
      if (count($this->value_arr)<=0){
        $this->value_arr=array();
      }

      $value_child_arr_index=array();
      for ($i=0; $i < count($this->value_child_arr); $i++) {
        $value_child_arr_index[$this->value_child_arr[$i]]=$i;
      }

      $title_arr=array();
      $val_arr=array();
      $k=0;
      for ($i=0; $i < count($this->value_arr); $i++) {
        $val_index=$value_child_arr_index[$this->value_arr[$i]];
          if (($val_index>=0)&&(isset($val_index))){
            $title_arr[$k]=$this->title_child_arr[$val_index];
            $val_arr[$k]=$this->value_child_arr[$val_index];
            $k++;
          }
      }


      // for ($i=0; $i < count($this->value_child_arr); $i++) {
      //   if ($this->value===$this->value_child_arr[$i]){
      //     $titl=$this->title_child_arr[$i];
      //     break;
      //   }
      // }

      // if ((empty($titl))&&(empty($this->text))){
      //   $titl=$this->title_child_arr[0];
      // } else {
      //   $titl=(!empty($titl))?$titl:$this->text;
      // }
      if (!empty($this->height_child)){
        $height_child_html.='';
        $height_child_html.='overflow-y: auto;';
        $height_child_html.='max-height: '.$this->height_child.';';
        $height_child_style=' style="'.$height_child_html.'" ';
      }

      $val_arr_json=($phpVersion >= 5.4)?json_encode($val_arr, JSON_UNESCAPED_UNICODE):preg_replace('/\\\\u([a-f0-9]{4})/e', "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($val_arr));
      $val_arr_json=htmlspecialchars($val_arr_json,ENT_QUOTES);

      $html='';
      $html.='<div '.$attr_str.'>';
      $html.='<div class="dropdown_blocks" onclick="dropdown_multi_check_list_start(this);">';
      $html.='<div class="dropdown_block2">';
      $html.='<div class="dropdown_title2">';
      if (count($title_arr)>0){
        $html.=implode('; ',$title_arr);
      } else {
        $html.=$this->alt_title;
      }
      $html.='</div>';
      $html.='<div class="dropdown_down_img">';

      $html.=$this->html_img_down_arrow;
      $html.='</div>';
      $html.='</div>';
      $html.='</div>';


      $html.='<div tabindex="'.mt_rand().'" class="dropdown_multi_check_list dropdown_multi_check_list_hide" onblur="dropdown_multi_check_blur(this);" onkeydown="dropdown_keyboard(event,this);">';
      $html.='<div class="dropdown_multi_check_list_ul" '.$height_child_style.'>';
      for ($i=0; $i < count($this->title_child_arr); $i++) {
        $id_child_arr_str=(!empty($this->id_child_arr[$i]))?' id="'.$this->id_child_arr[$i].'" ':'';
        $html.='<div class="dropdown_multi_check_option '.$this->class_child_arr[$i].'" '.$id_child_arr_str.$this->name_child_arr[$i].' onclick="'.$this->onclick_child_arr[$i].'dropdown_multi_check_opt_click(this,\''.$this->alt_title.'\');" data-value="'.$this->value_child_arr[$i].'">';
        $html.='<div class="dropdown_multi_check_option_check">';
        $html.=components('lcp_checked_default',array(
          'class'=>'check_multi_drop',
          'checked'=>in_array($this->value_child_arr[$i],$this->value_arr),
          'onclick'=>'return;',
        ));
        $html.='</div>';
        $html.='<div class="dropdown_multi_check_option_title">';
        $html.=$this->title_child_arr[$i];
        $html.='</div>';
        $html.='</div>';
      }
      $html.='</div>';
      $html.='</div>';
      $html.='</div>';
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_dropdown_multi_check_init($param=array()){
    $lcp_obj=new lcp_dropdown_multi_check($param);
    return $lcp_obj->html();
  }
}
?>
