var blur_remove=0;

function lcp_dropdown_multi_check(param){
  /*Функция для получения выпадающего списка через js*/
  var id_dropdown=param['id'];
  var name=param['name'];
  var value=param['value'];
  var text=param['text'];
  var class_dropdown=param['class'];
  var onclick=param['onclick'];
  var height_child=param['height_child'];
  var onmousedown=param['onmousedown'];
  var onmouseup=param['onmouseup'];
  var html_img_down_arrow=param['html_img_down_arrow'];
  var id_child_arr=param['id_child_arr'];
  var title_child_arr=param['title_child_arr'];
  var name_child_arr=param['name_child_arr'];
  var value_child_arr=param['value_child_arr'];
  var class_child_arr=param['class_child_arr'];
  var onclick_child_arr=param['onclick_child_arr'];
  id_dropdown=(typeof id_dropdown!='undefined')?' id="'+id_dropdown+'" ':'';
  name=(typeof name!='undefined')?' name="'+name+'" ':'';
  onclick=(typeof onclick!='undefined')?' onclick="'+onclick+'" ':'';
  onmousedown=(typeof onmousedown!='undefined')?' onmousedown="'+onmousedown+'" ':'';
  onmouseup=(typeof onmouseup!='undefined')?' onmouseup="'+onmouseup+'" ':'';
  html_img_down_arrow=(typeof html_img_down_arrow!='undefined')?html_img_down_arrow:'<i class="fa fa-sort-desc" aria-hidden="true"></i>';

  var titl='';
  if (value!=''){
    if (typeof value_child_arr!='undefined'){
      console.log(value_child_arr);
      for (i=0; i < value_child_arr.length; i++) {
        if (value==value_child_arr[i]){
          titl=title_child_arr[i];
          break;
        }
      }
    }
  }
  if ((titl=='')&&(text=='')){
    titl=title_child_arr[0];
  } else {
    text=(titl!='')?titl:text;
  }
  if (height_child!=''){
  var height_child_html='';
  height_child_html+='overflow-y: auto;';
  height_child_html+='max-height: '+height_child+';';
  height_child_style=' style="'+height_child_html+'" ';
}




var html='';
html+='<div class="dropdown_multi_check '+class_dropdown+'" '+id_dropdown+name+onclick+onmousedown+onmouseup+'>';
html+='<div class="dropdown_blocks" onclick="dropdown_multi_check_list_start(this);">';
html+='<div class="dropdown_block2">';
html+='<div class="dropdown_title2" data-value="'+value+'">';
html+=text;
html+='</div>';
html+='<div class="dropdown_down_img">';
html+=html_img_down_arrow;
html+='</div>';
html+='</div>';
html+='</div>';


html+='<div tabindex="'+Math.floor(Math.random() * (99999 - 10000) + 10000)+'" class="dropdown_multi_check_list dropdown_multi_check_list_hide" onblur="dropdown_multi_check_blur(this);" onkeydown="dropdown_keyboard(event,this);">';
html+='<ul class="dropdown_multi_check_list_ul" '+height_child_style+'>';
if (typeof title_child_arr!='undefined'){
for (i=0; i < title_child_arr.length; i++) {
  var class_child_arr_str=(typeof class_child_arr!='undefined')?class_child_arr[i]:'';
  var id_child_arr_str=(typeof id_child_arr!='undefined')?id_child_arr[i]:'';
  var name_child_arr_str=(typeof name_child_arr!='undefined')?name_child_arr[i]:'';
  var onclick_child_arr_str=(typeof onclick_child_arr!='undefined')?onclick_child_arr[i]:'';
  var value_child_arr_str=(typeof value_child_arr!='undefined')?value_child_arr[i]:'';
  var title_child_arr_str=(typeof title_child_arr!='undefined')?title_child_arr[i]:'';
  html+='<li class="dropdown_multi_check_option '+class_child_arr_str+'" '+id_child_arr_str+name_child_arr_str+' onclick="dropdown_multi_check_opt_click(this);'+onclick_child_arr_str+'" data-value="'+value_child_arr_str+'">';
  html+=title_child_arr_str;
  html+='</li>';
}
}
html+='</ul>';
html+='</div>';
html+='</div>';
return html;
}


function dropdown_keyboard(e,obj){
  /*функция для управления фокусом в списке через клавиши вверх-вниз и enter*/
  e.preventDefault();
  key=e.charCode || e.keyCode;
  if ((key==38)||(key==40)){
    $(obj).focus();
    var el=$(obj).children('ul').children('li');
    var count_items=$(el).length;
    var count_items_focus=$(obj).children('ul').children('.drop_focus_element');
    count_items_focus=$(count_items_focus).length;
    if (count_items_focus<=0){
      $(el).eq(count_items-1).addClass('drop_focus_element');
    }
    var index = $(obj).children('ul').children('.drop_focus_element').index();
    switch (key) {
      case 40:
      index++;
      break;
      case 38:
      index--;
      break;
    }
    index = index < 0 ? count_items - 1 : index;
    index = index + 1 > count_items ? 0 : index;
    $(el).removeClass('drop_focus_element');
    $(el).eq(index).addClass('drop_focus_element');
    var position = $(obj).children('ul').children('.drop_focus_element').position();
    var scroll_top = $(obj).children('ul').scrollTop();
    $(obj).children('ul').animate({scrollTop: (position.top + scroll_top)}, 50);
    return null;
  }
  if (key==13){
    $(obj).children('ul').children('.drop_focus_element').click();
    return null;
  }
}


/*function get_dropdown_child_arr(param){
функция для получения html кода только списка (обычно не используется)
  var id_dropdown=param['id'];
  var id_child_arr=param['id_child_arr'];
  var title_child_arr=param['title_child_arr'];
  var name_child_arr=param['name_child_arr'];
  var value_child_arr=param['value_child_arr'];
  var class_child_arr=param['class_child_arr'];
  var onclick_child_arr=param['onclick_child_arr'];
  var height_child=param['height_child'];

  if (height_child!=''){
  var height_child_html='';
  height_child_html+='overflow-y: auto;';
  height_child_html+='max-height: '+height_child+';';
  height_child_style=' style="'+height_child_html+'" ';
}

var html='';
html+='<ul class="dropdown_multi_check_list_ul" '+height_child_style+'>';
if (typeof title_child_arr!='undefined'){
for (i=0; i < title_child_arr.length; i++) {
  var class_child_arr_str=(typeof class_child_arr!='undefined')?class_child_arr[i]:'';
  var id_child_arr_str=(typeof id_child_arr!='undefined')?id_child_arr[i]:'';
  var name_child_arr_str=(typeof name_child_arr!='undefined')?name_child_arr[i]:'';
  var onclick_child_arr_str=(typeof onclick_child_arr!='undefined')?onclick_child_arr[i]:'';
  var value_child_arr_str=(typeof value_child_arr!='undefined')?value_child_arr[i]:'';
  var title_child_arr_str=(typeof title_child_arr!='undefined')?title_child_arr[i]:'';
  html+='<li class="dropdown_multi_check_option '+class_child_arr_str+'" '+id_child_arr_str+name_child_arr_str+' onclick="dropdown_opt_click(this);'+onclick_child_arr_str+'" data-value="'+value_child_arr_str+'">';
  html+=title_child_arr_str;
  html+='</li>';
}
}
html+='</ul>';
return html;
}


function set_dropdown_child_arr(param){
функция для отображения нового списка (обычно не используется)
  var id_dropdown=param['id'];
  var html=get_dropdown_child_arr(param);
  $('#'+id_dropdown).children('.dropdown_multi_check_list').html(html);
}


function show_set_dropdown_child_arr(param){
функция для отображения только списка
  var id_dropdown=param['id'];
  set_dropdown_child_arr(param);
  $('#'+id_dropdown).children('.dropdown_multi_check_list').removeClass('dropdown_multi_check_list_hide');
}*/


function dropdown_multi_check_list_start(obj){
  /*Функция для открытия/скрытия списка. Вызывается при нажатии на компонент (обычно не используется)*/
  var obj_html=$(obj).parent('.dropdown_multi_check').children('.dropdown_multi_check_list');
  if ($(obj_html).hasClass('dropdown_multi_check_list_show')===true){
    blur_remove=1;
    $(obj_html).removeClass('dropdown_multi_check_list_show');
    $(obj_html).addClass('dropdown_multi_check_list_hide');
  } else {
    $(obj_html).removeClass('dropdown_multi_check_list_hide');
    $(obj_html).addClass('dropdown_multi_check_list_show');
    $(obj).parent('.dropdown_multi_check').children('.dropdown_multi_check_list').focus();
  }
}

function dropdown_multi_check_list_hide(obj){
  /*функция для скрытия списка, если был потерян фокус (обычно не используется)*/
  if (blur_remove===1){
    blur_remove=0;
    return;
  }
  var obj_html=$(obj);
  $(obj_html).removeClass('dropdown_multi_check_list_show');
  $(obj_html).addClass('dropdown_multi_check_list_hide');
}

function dropdown_multi_check_blur(obj){
  /*функция для скрытия списка, если был потерян фокус, с задержкой (обычно не используется)*/
  setTimeout(function () {
    dropdown_multi_check_list_hide(obj);
  }, 100);
}

function dropdown_multi_check_opt_click(obj,title_default='-- Не выбрано --'){
  /*функция при выборе пункта из списка (обычно не используется)*/
  lcp_checked_default($(obj).children('.dropdown_multi_check_option_check').children('.check_multi_drop').eq(0),{checked:'checked_change'});
  dropdown_multi_check_opt_update($(obj).parent('.dropdown_multi_check_list_ul').parent('.dropdown_multi_check_list').parent('.dropdown_multi_check'),title_default);
}

function dropdown_multi_check_opt_update(obj,title_default='-- Не выбрано --'){
  var val_arr={};
  var title_arr={};
  k=0;
  for (var i = 0; i < $(obj).children('.dropdown_multi_check_list').children('.dropdown_multi_check_list_ul').children('.dropdown_multi_check_option').length; i++) {
    var obj_inner=$(obj).children('.dropdown_multi_check_list').children('.dropdown_multi_check_list_ul').children('.dropdown_multi_check_option').eq(i);
    if (lcp_checked_default($(obj_inner).children('.dropdown_multi_check_option_check').children('.check_multi_drop').eq(0),{checked:'has_checked'})=='true'){
      val_arr[k]=$(obj_inner).attr('data-value');
      title_arr[k]=$(obj_inner).children('.dropdown_multi_check_option_title').html();
      k++;
    }
  }
  var title_str='';
  for (var i = 0; i < Object.keys(title_arr).length; i++) {
    title_str+=(i===0)?title_arr[i]:'; '+title_arr[i];
  }
  if (title_str==''){
    title_str=title_default;
  }
  $(obj).children('.dropdown_blocks').children('.dropdown_block2').children('.dropdown_title2').html(title_str);
}

function set_dropdown_multi_check_opt_value(id_dropdown,values_arr){
  /*Функция для изменения значений выпадающего списка*/
  var obj=$('#'+id_dropdown);
  var values_index_arr=[];
  for (var i = 0; i < values_arr.length; i++) {
    values_index_arr[values_arr[i]]=i;
  }
  for (var i = 0; i < $(obj).children('.dropdown_multi_check_list').children('.dropdown_multi_check_list_ul').children('.dropdown_multi_check_option').length; i++) {
    var obj_inner=$('#'+id_dropdown).children('.dropdown_multi_check_list').children('.dropdown_multi_check_list_ul').children('.dropdown_multi_check_option').eq(i);
    lcp_checked_default($(obj_inner).children('.dropdown_multi_check_option_check').children('.check_multi_drop').eq(0),{checked:'false'});
    if (values_index_arr[$(obj_inner).attr('data-value')]>=0){
      lcp_checked_default($(obj_inner).children('.dropdown_multi_check_option_check').children('.check_multi_drop').eq(0),{checked:'true'});
    }
  }
  dropdown_multi_check_opt_update(obj);
}

function get_dropdown_multi_check_opt_value(id_dropdown){
  /*Функция для получения значения выпадающего списка*/
  var val_arr={};
  k=0;
  for (var i = 0; i < $('#'+id_dropdown).children('.dropdown_multi_check_list').children('.dropdown_multi_check_list_ul').children('.dropdown_multi_check_option').length; i++) {
    var obj=$('#'+id_dropdown).children('.dropdown_multi_check_list').children('.dropdown_multi_check_list_ul').children('.dropdown_multi_check_option').eq(i);
    if (lcp_checked_default($(obj).children('.dropdown_multi_check_option_check').children('.check_multi_drop').eq(0),{checked:'has_checked'})=='true'){
      val_arr[k]=$(obj).attr('data-value');
      k++;
    }
  }
  return val_arr;
}
