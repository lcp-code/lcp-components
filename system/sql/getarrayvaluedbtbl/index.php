<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'getarrayvaluedbtbl',
  'type' => 'system',
  'group' => 'Таблица',
  'inner' => array(
    'description' => '<p>Компонент для таблицы. Получить двумерный массив записей (где 1-ый полученный - колонка записи, 2-ой полученный - строка). Применяется при использовании навигации.</p><p>Пример использования: getarrayvaluedbtbl(array("0"=>"dbCELLarray0","1"=>"dbCELLarray1"), "dbSELECT", "dbFROM", "dbWHERE", "dbORDER", "dbLIMIT", "pageid");</p><p>id в dbCELLarray0 обязателен!</p>',
    'css' => array(),
    'js' => array()
  ),
  'set_name_php' => array(),
  'set_value_php' => array(),
  'set_description_php' => array(),
  'get_php' => array(
    '0' =>'arr',
    '1'=>'count',
    '2'=>'count_all',
    '3'=>'limit',
    '4'=>'sql'
  ),
  'get_description_php' => array(
    '0' =>'Массив со значениями',
    '1'=>'Количество записей на текущей странице',
    '2'=>'Количество записей всего',
    '3'=>'Количество записей, которые должны отображаться на одной странице',
    '4'=>'Полученный SQL-запрос'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
// $css_install_arr[]='/components/system/sql/getarrayvaluedbtbl/index.css';
// $js_install_arr[]='/components/system/sql/getarrayvaluedbtbl/index.js';
?>




<?php
function getarrayvaluedbtbl($dbcellarr="",$dbSELECT="*",$dbFROM="",$dbWHERE="",$dbORDER="",$dbLIMIT,$pageid=""){
  if (empty($_GET['pageid'])){
    $_GET['pageid']='1';
  }
  $start_page=(!empty($pageid))?($pageid-1)*$dbLIMIT:(($_GET['pageid']-1)*$dbLIMIT);
  $ress=getarrayvaluedb($dbcellarr,$dbSELECT,$dbFROM,$dbWHERE,$dbORDER,$dbLIMIT.' OFFSET '.$start_page);
  $sql=$ress['getarrayvaluedb_sql'];
  unset($ress['getarrayvaluedb_sql']);
  return array(
    'arr' => $ress,
    'count' => count($ress['id']),
    'count_all' => getcountdb($dbFROM,$dbWHERE),
    'limit' => $dbLIMIT,
    'sql' => $sql
  );
}

// if ($install!='true'){
// echo getarrayvaluedbtbl();
// }
?>
