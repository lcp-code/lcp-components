<?php
header('Content-Type: application/json');
session_start();
$root_dir = $_SERVER['DOCUMENT_ROOT'];
$root_folder = '../../../../';

include_once($root_folder.'konsfigli.php');
include_once($root_folder.'includes/connect_db.php');

function generate_exception($string){
  exit(json_encode(array('error' => 'false','string' => $string)));
}

if (empty($_POST['lcp_url'])){
  header("HTTP/1.0 404 Not Found");exit();
}


$_LCP_AJAX='AJAX';
include_once($root_folder.'components/index.php');
if (empty($ini)){
  $ini=$_COOKIE['ini'];
}
$_LCP_AJAX_TIME=md5(getdater());

$lcp_url=$_POST['lcp_url'];
$data=$_POST['datas'];
$lcp_url=str_replace('http://','',$lcp_url);
$lcp_url=str_replace('https://','',$lcp_url);
$lcp_url=str_replace($domen_site,'',$lcp_url);
$lcp_url=$root_dir.$lcp_url;
$dater=array_keys($data) or die (exit(var_dump($data)));
for ($i=0; $i < count($dater); $i++) {
  $_POST[$dater[$i]]=$data[$dater[$i]];
}
include_once($lcp_url);
if (!empty($LCP_AJAX_RESULT)){
  $text_inc=$LCP_AJAX_RESULT;
  echo json_encode(array('error' => 'true','string' => $text_inc));
  exit();
}
generate_exception('Ошибка при выполнении AJAX!');
exit();
?>
