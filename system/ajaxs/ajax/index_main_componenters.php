<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_ajax',
  'type' => 'system',
  'group' => 'AJAX',
  'inner' => array(
    'description' => '<div>AJAX-запрос, используемый в панели администратора по-умолчанию.</div><div>Для безопасности использования AJAX-файлов, добавьте следующий код в начале PHP файла:</div><div>if (($_LCP_AJAX!=="AJAX")||($_LCP_AJAX_TIME!=md5(getdater()))){header("HTTP/1.0 404 Not Found");exit();}</div><div>В качестве результата, занесите результаты в переменную: $LCP_AJAX_RESULT</div>',
    'css' => array(),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'lcp_type_ajax',
    '1' => 'function_name',
    '2' => 'begin_html',
    '3' => 'type',
    '4' => 'cache',
    '5' => 'processData',
    '6' => 'contentType',
    '7' => 'url',
    '8' => 'data',
    '9' => 'data_js',
    '10' => 'success',
    '11' => 'error',
    '12' => 'lcp_success_param',
    '13' => 'lcp_success_true',
    '14' => 'lcp_success_false',
    '15' => 'show_error',
    '16' => 'connect_to_url',
  ),
  'set_value_php' => array(
    '0' => 'normal',
    '1' => 'lcp_ajax',
    '2' => '',
    '3' => 'post',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '',
    '11' => '',
    '12' => '',
    '13' => '',
    '14' => '',
    '15' => 'true',
    '16' => 'true',
  ),
  'set_description_php' => array(
    '0' => '<div>Тип запроса:</div><div>normal - обычный ajax</div><div>include - при первичном открытии страницы выполняется ajax-файл</div>',
    '1' => 'Наименование функции для запуска AJAX',
    '2' => 'JS-код перед выполнением AJAX',
    '3' => 'Метод передачи данных (get или post)',
    '4' => 'По умолчанию использовать кэш. Можно указать false',
    '5' => 'По умолчанию конвертировать все данные, массивы и объекты в строку. Можно указать false',
    '6' => 'По умолчанию всегда передается в виде application/x-www-form-urlencoded, что не позволит загружать файлы. Можно указать false',
    '7' => 'Путь к AJAX файлу (.php)',
    '8' => 'Объект PHP передаваемых данных',
    '9' => 'Объект JS передаваемых данных',
    '10' => 'JS, при удачном выполнении AJAX',
    '11' => 'JS, при ошибках в запросе AJAX',
    '12' => 'Параметр, по которому стоит определять, какие дальнейшие действия выполнять при удачном выполнении AJAX (н-р, error). (пока не используется)',
    '13' => 'JS, при верном выполнении AJAX (или зеленое сообщение)',
    '14' => 'JS, при неверном выполнении AJAX (или красное сообщение)',
    '15' => '<div>true - по-умолчанию отображать ошибки при неверном выполнении AJAX (если указан параметр), и при ошибках в запросе AJAX</div><div>false - по-умолчанию не отображать никаких ошибок</div>',
    '16' => '<div>true - по-умолчанию при открытии ajax файла подключать указанный в url файл через include</div><div>false - не отправлять дополнительный параметр url, и не подключать через include требуемый файл. В этом случае запросы будут приходить напрямую на требуемый файл, без посредников</div>',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает js код'
  ),
  'set_name_js' => array(
    // '0' => 'lcp_type_ajax',
    // '1' => 'type',
    // '2' => 'url',
    // '3' => 'data',
    // '4' => 'success',
    // '5' => 'error',
    // '6' => 'lcp_success_param',
    // '7' => 'lcp_success_true',
    // '8' => 'lcp_success_false',
    // '9' => 'show_error'
  ),
  'set_value_js' => array(
    // '0' => 'normal',
    // '1' => 'post',
    // '2' => '',
    // '3' => '',
    // '4' => '',
    // '5' => '',
    // '6' => '',
    // '7' => '',
    // '8' => '',
    // '9' => ''
  ),
  'set_description_js' => array(
    // '0' => '<div>Тип запроса:</div><div>normal - обычный ajax</div><div>include - при первичном открытии страницы выполняется ajax-файл</div>',
    // '1' => 'Метод передачи данных (get или post)',
    // '2' => 'Путь к AJAX файлу (.php)',
    // '3' => 'Объект JS передаваемых данных',
    // '4' => 'JS, при удачном выполнении AJAX',
    // '5' => 'JS, при ошибках в запросе AJAX',
    // '6' => 'Параметр, по которому стоит определять, какие дальнейшие действия выполнять при удачном выполнении AJAX (н-р, error).',
    // '7' => 'JS, при верном выполнении AJAX (или зеленое сообщение)',
    // '8' => 'JS, при неверном выполнении AJAX (или красное сообщение)',
    // '9' => '<div>true - всегда отображать ошибки при неверном выполнении AJAX (если указан параметр), и при ошибках в запросе AJAX</div><div>false - не отображать никаких ошибок</div><div>Если не указано - отображать ошибки при неверном выполнении AJAX (если указан параметр), и при ошибках в запросе AJAX, если не задан обрабатывающий параметр (lcp_success_true, lcp_success_false, error и пр.)</div><div>lcp_success_true - всегда отображать ошибки для параметра lcp_success_true</div><div>lcp_success_false - всегда отображать ошибки для параметра lcp_success_false</div><div>error - всегда отображать ошибки для параметра error</div>'
  ),
  'get_js' => array(
    // '0' =>'text'
  ),
  'get_description_js' => array(
    // '0' =>'Функция возвращает js код'
  )
);
}
// $css_install_arr[]='/components/system/ajaxs/ajax/index_main_componenters.css';
// $js_install_arr[]='/components/system/ajaxs/ajax/index_main_componenters.js';
?>




<?php
function lcp_ajax($param=''){
  global $domen_site,$http;
  $lcp_type_ajax = $param['lcp_type_ajax'];
  $function_name = $param['function_name'];
  $begin_html = $param['begin_html'];
  $_type = $param['type'];
  $cache = $param['cache'];
  $processData = $param['processData'];
  $contentType = $param['contentType'];
  $url = $param['url'];
  $data = $param['data'];
  $data_js = $param['data_js'];
  $success = $param['success'];
  $error = $param['error'];
  $lcp_success_param = $param['lcp_success_param'];
  $lcp_success_true = $param['lcp_success_true'];
  $lcp_success_false = $param['lcp_success_false'];
  $show_error = $param['show_error'];
  $connect_to_url = $param['connect_to_url'];
  $default_lcp_type_ajax='normal';
  $default_function_name='lcp_ajax';
  $default_begin_html='';
  $default_type='post';
  $default_cache='';
  $default_processData='';
  $default_contentType='';
  $default_url='';
  $default_data='';
  $default_data_js='';
  $default_success='';
  $default_error='';
  $default_lcp_success_param='';
  $default_lcp_success_true='';
  $default_lcp_success_false='';
  $default_show_error='true';
  $default_connect_to_url='true';

  $lcp_type_ajax=(empty($lcp_type_ajax))?$default_lcp_type_ajax:$lcp_type_ajax;
  $function_name=(empty($function_name))?$default_function_name:$function_name;
  $begin_html=(empty($begin_html))?$default_begin_html:$begin_html;
  $_type=(empty($_type))?$default_type:$_type;
  $cache=(empty($cache))?$default_cache:$cache;
  $processData=(empty($processData))?$default_processData:$processData;
  $contentType=(empty($contentType))?$default_contentType:$contentType;
  $url=(empty($url))?$default_url:$url;
  $data=(empty($data))?$default_data:$data;
  $data_js=(empty($data_js))?$default_data_js:$data_js;
  $success=(empty($success))?$default_success:$success;
  $error=(empty($error))?$default_error:$error;
  $lcp_success_param=(empty($lcp_success_param))?$default_lcp_success_param:$lcp_success_param;
  $lcp_success_true=(empty($lcp_success_true))?$default_lcp_success_true:$lcp_success_true;
  $lcp_success_false=(empty($lcp_success_false))?$default_lcp_success_false:$lcp_success_false;
  $show_error=(empty($show_error))?$default_show_error:$show_error;
  $connect_to_url=(empty($connect_to_url))?$default_connect_to_url:$connect_to_url;
  // $data_js=str_replace('{','',$data_js);
  // $data_js=str_replace('}','',$data_js);
  $data_js=str_replace(' ','',$data_js);

  $html='';

  if ($lcp_type_ajax=='include'){
    $url_include=$url;
    $url_include=str_replace('http://','',$url_include);
    $url_include=str_replace('https://','',$url_include);
    $url_include=str_replace($domen_site,'',$url_include);
    $url_include=$_SERVER['DOCUMENT_ROOT'].$url_include;
    $_LCP_AJAX='AJAX';
    $_LCP_AJAX_TIME=md5(getdater());
    $dater=array_keys($data) or die (exit(var_dump($data)));
    for ($i=0; $i < count($dater); $i++) {
      $_POST[$dater[$i]]=$data[$dater[$i]];
    }
    $_POST['function_name']=$function_name;
    if (function_exists('generate_exception')===false){
      function generate_exception($param=''){}
    }
    include($url_include);
    $html='';
    $html.= $LCP_AJAX_RESULT;
  }

  // $data_js.=',function_name:\''.$function_name.'\'';

  // if (count($data)>0){
  //   $dater=array_keys($data) or die (exit(var_dump($data)));
  //   for ($i=0; $i < count($dater); $i++) {
  //     // $_POST[$dater[$i]]=$data[$dater[$i]];
  //     $script_data.=','.$dater[$i].':\''.$data[$dater[$i]].'\'';
  //   }
  // }

  if ($show_error!='false'){
    if (function_exists('lcp_message')===true){
      $lcp_success_true_js='lcp_message(data.string,\'true\');';
    } else {
      $lcp_success_true_js='console.log(data.string);';
    }
  }
  if (!empty($lcp_success_true)){
    if (($show_error=='true')||($show_error=='lcp_success_true')){
      $lcp_success_true_js.=$lcp_success_true;
    } else {
      $lcp_success_true_js=$lcp_success_true;
    }
  }

  if ($show_error!='false'){
    if (function_exists('lcp_message')===true){
      $lcp_success_false_js='lcp_message(data.string,\'false\');';
    } else {
      $lcp_success_false_js='console.log(data.string);';
    }
  }
  if (!empty($lcp_success_false)){
    if (($show_error=='true')||($show_error=='lcp_success_false')){
      $lcp_success_false_js.=$lcp_success_false;
    } else {
      $lcp_success_false_js=$lcp_success_false;
    }
  }

  if ($show_error!='false'){
    if (function_exists('lcp_message')===true){
      $lcp_error_js='lcp_message(\'Ошибка AJAX!\',\'false\');';
    } else {
      $lcp_error_js='console.log(\'Ошибка AJAX!\');';
    }
  }
  if (!empty($error)){
    if (($show_error=='true')||($show_error=='error')){
      $lcp_error_js.=$error;
    } else {
      $lcp_error_js=$error;
    }
  }

  if (!empty($cache)){
    $cache='cache:'.$cache.',';
  }
  if (!empty($processData)){
    $processData='processData:'.$processData.',';
  }
  if (!empty($contentType)){
    $contentType='contentType:'.$contentType.',';
  }

  $url_ajax=($connect_to_url=='true')?$http.'://'.$domen_site.'/components/system/ajaxs/ajax/lcp-ajax.php':$url;
  $data_ajax=($connect_to_url=='true')?'
  {
    lcp_url: \''.$url.'\''.((!empty($data_js))?',
    datas: '.$data_js.'':'').'
  }
  ':$data_js;

  $html.='<script type="text/javascript">
  function '.$function_name.'(param){
    if (typeof param===\'undefined\'){
      var param=\'\';
    }
    '.$begin_html.'
    $.ajax({
      url:"'.$url_ajax.'",
      type:"'.$_type.'",
      datatype:"JSON",
      '.$cache.$processData.$contentType.'
      data:'.$data_ajax.',
      success:function(data){
        if (data.error==\'true\'){
          '.$lcp_success_true_js.'
        } else {
          '.$lcp_success_false_js.'
        }
      },
      error:function(data){
        '.$lcp_error_js.'
      }
    });
  }
  </script>';
  return $html;
}
?>
