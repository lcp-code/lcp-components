<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_message',
  'type' => 'system',
  'group' => 'Сообщения/диалоги',
  'inner' => array(
    'description' => 'Плагин для отображения сообщений, используемый в панели администратора по-умолчанию. Инициализируется автоматически',
    'css' => array(
      '0' => '/components/system/messages/message-default1/message-default1.css'
    ),
    'js' => array(
      '0' => '/components/system/messages/message-default1/message-default1.js'
    )
  ),
  'set_name_php' => array(),
  'set_value_php' => array(),
  'set_description_php' => array(),
  'get_php' => array(),
  'get_description_php' => array(),
  'set_name_js' => array(
    '0' => 'string',
    '1' => 'error'
  ),
  'set_value_js' => array(
    '0' => 'Ошибка',
    '1' => 'false'
  ),
  'set_description_js' => array(
    '0' => 'Сообщение об ошибке',
    '1' => '<div>true - отображение зеленого сообщения</div><div>false - отображение красного сообщения</div>'
  ),
  'get_js' => array(),
  'get_description_js' => array()
);
}
$css_install_arr[]='/components/system/messages/message-default1/message-default1.css';
$js_install_arr[]='/components/system/messages/message-default1/message-default1.js';
?>




<?php
function lcp_message($param=array()){
  // $id_components=getcelldb('id','id','components','name=\'lcp_message\'');
  $string_message=$param['string'];
  $error_message=$param['error'];
  $default_string_message='Ошибка';
  $default_error_message='false';
  $string_message=(empty($string_message))?$default_string_message:$string_message;
  $error_message=(empty($error_message))?$default_error_message:$error_message;

  $html='';
  $html.='<div class="message_error_div" id="message_error_div"></div>';
  // $html.='<script>';
  // $html.='lcp_message(\''.$string_message.'\',\''.$error_message.'\')';
  // $html.='</script>';
  return $html;
}

if ($install!='true'){
  if (!empty($_LCP_AJAX)){
    $LCP_AJAX_RESULT .= lcp_message();
  } else {
    $html_install_arr[] = lcp_message();
  }
}
?>
