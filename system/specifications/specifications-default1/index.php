<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_spec_default',
  'type' => 'system',
  'group' => 'Теги',
  'inner' => array(
    'description' => 'Характеристики, используемые в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/specifications/specifications-default1/spec-default.css'
    ),
    'js' => array(
      '0' => '/components/system/specifications/specifications-default1/spec-default.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'value_arr',
    '1' => 'search',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
  ),
  'set_description_php' => array(
    '0' => 'Массив с идентификаторами характеристик',
    '1' => 'Отфильтровать по поиску'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_spec_default($param=''){
  $id_components=getcelldb('id','id','components','name=\'lcp_spec_default\'');
  $value_arr=$param['value_arr'];
  $search=$param['search'];

  $html='';
  $html.=components('lcp_button_green',array(
    'value'=>'Добавить',
    'onclick'=>'lcp_loading_default({loading_show:\'true\'});popup_spec();get_table_spec({search:\''.$search.'\'});'
  ));

  if (count($value_arr['spec_value'])>0){
    $html_spec='';
    // $html_spec_sql=(count($value_arr['spec_value'])>1)?'id IN (\''.implode('\',\'',$value_arr['spec_value']).'\')':'id=\''.$value_arr['spec_value'][0].'\'';
    // echo $html_spec_sql;
    $html_spec_arr=getarrayvaluedb(array('id','title_short','childr'),'id,title_short,childr','datas','id IN (\''.implode('\',\'',$value_arr['spec_value']).'\')','','','1');
    $html_spec_group=getarrayvaluedb(array('id','title_short','childr'),'id,title_short,childr','datas','id IN (\''.implode('\',\'',$html_spec_arr['childr']).'\')','','','1');
    $html_spec_parent=getarrayvaluedb(array('id','title_short'),'id,title_short','datas','id IN (\''.implode('\',\'',$html_spec_group['childr']).'\')','','','1');
    for ($i1=0; $i1 < count($html_spec_group['id']); $i1++) {
        $id_parent='';
        $parent='';
        for ($i_parent=0; $i_parent < count($html_spec_parent['id']); $i_parent++) {
          if ($html_spec_group['childr'][$i1]==$html_spec_parent['id'][$i_parent]){
            $id_parent=$html_spec_parent['id'][$i_parent];
            $parent=$html_spec_parent['title_short'][$i_parent].'&nbsp;-&nbsp;';
          }
        }
        $html_spec.='<div class="spec_all_group" id="spec_all_group_'.$html_spec_group['id'][$i1].'" data-id="'.$html_spec_group['id'][$i1].'" data-parent-id="'.$id_parent.'">
        <div class="group_title"><div class="parent_title">'.$parent.'</div>'.$html_spec_group['title_short'][$i1].'</div><div class="spec_all_group_delete" onclick="spec_all_group_delete_click(\''.$html_spec_group['id'][$i1].'\');">x</div>
        <div class="spec_all" id="spec_all_'.$html_spec_group['id'][$i1].'">';
        for ($i2=0; $i2 < count($html_spec_arr['id']); $i2++) {
          if ($html_spec_arr['childr'][$i2]==$html_spec_group['id'][$i1]){
            $html_spec.='<div class="spec_all_item" data-id="'.$html_spec_arr['id'][$i2].'">'.$html_spec_arr['title_short'][$i2].' <div class="spec_all_delete" onclick="spec_all_item_click(\''.$html_spec_arr['id'][$i2].'\');">x</div></div>';
          }
        }
        $html_spec.='</div></div>';
    }
  }
  $html.='<div class="spec_all_block">'.$html_spec.'</div>';


  $popup_content='';

  $popup_content.=components('lcp_ajax',array(
    'lcp_type_ajax'=>'include',
    'function_name'=>'get_table_spec',
    'begin_html'=>'
    var id_catalog=param[\'id_catalog\'];
    if (id_catalog==\'\'){
      id_catalog=\''.$id_catalog.'\';
    }
    var search=\'\';
    if (typeof(param[\'search\'])===\'string\'){
      search=param[\'search\'];
    }
    var pageid=\'0\';
    if (typeof(param[\'pageid\'])===\'string\'){
      pageid=param[\'pageid\'];
    }
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/components/system/specifications/specifications-default1/ajax-spec.php',
    'data'=>array(
      'action'=>'get_table_spec',
      'value_arr'=>(!empty($value_arr))?implode(',',$value_arr):'',
      'id_catalog'=>$id_catalog,
      'search'=>$search,
      'pageid'=>'0',
    ),
    'data_js'=>'{
      action:\'get_table_spec\',
      value_arr:get_spec_default_str(),
      id_catalog:id_catalog,
      search:search,
      pageid:pageid,
      }',
    'lcp_success_true'=>'lcp_loading_default({loading_show:\'false\'});$(\'.get_table_spec\').html(data.string);',
    'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});alert(data.string);',
    'error'=>'lcp_loading_default({loading_show:\'false\'});alert(\'Ошибка при ajax запросе!\');',
    'show_error'=>'false'
  ));




  $html.=components('lcp_popup_default',array(
    'id_popup' => 'popup_spec',
    'title' => 'Выберите характеристику из списка',
    'content_html' => $popup_content,
    'popup_show' => 'popup_spec',
    'popup_close' => 'popup_spec_close',
    // 'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));





// Добавление новой характеристики
  $popup_content='';
  $popup_content.='<div class="get_table_spec_for_save"></div>';
  $popup_content.=components('lcp_ajax',array(
    'function_name'=>'get_table_spec_for_save',
    'begin_html'=>'',
    'url'=>'/components/system/spec/spec-default1/ajax-spec.php',
    // 'data'=>array(
    //   'action'=>'get_table_spec_for_save',
    //   // 'value_arr'=>(!empty($value_arr))?implode(',',$value_arr):''
    // ),
    'data_js'=>'{
      action:\'get_table_spec_for_save\'
      }',
    'lcp_success_true'=>'lcp_loading_default({loading_show:\'false\'});$(\'.get_table_spec_for_save\').html(data.string);',
    'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});alert(data.string);',
    'error'=>'lcp_loading_default({loading_show:\'false\'});alert(\'Ошибка при ajax запросе!\');',
    'show_error'=>'false'
  ));
  $html.=components('lcp_popup_default',array(
    'id_popup' => 'popup_new_spec',
    'title' => 'Добавить новую характеристику',
    'content_html' => $popup_content,
    'popup_show' => 'popup_new_spec',
    'popup_close' => 'popup_new_spec_close',
    // 'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));
  return $html;
}
?>
