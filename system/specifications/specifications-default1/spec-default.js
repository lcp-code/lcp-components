function spec_all_item_click(spec_id){
  $('.spec_all>[data-id="'+spec_id+'"]').remove();
}

function spec_all_group_delete_click(group_id){
  $('#spec_all_group_'+group_id).remove();
}

function add_item_spec(id_catalog,id_parent,parent_title){
  var id_arr=[];
  id_arr['id']=[];
  id_arr['title']=[];
  k=0;
  // получение выбранных id и заголовка
  for (var i = 0; i < $('.spec_tbl_check').length; i++) {
    var obj=$('.spec_tbl_check').eq(i);
    if (lcp_checked_default(obj,{checked:'has_checked'})=='true'){
      id_arr['id'][k]=$(obj).attr('data-id');
      id_arr['title'][k]=$('#spec_title_'+id_arr['id'][k]).html();
      k++;
    }
  }
  // добавление в поле характеристик
  if (typeof($('#spec_all_group_'+id_catalog).html())==='undefined'){ // если нет группы, создать
    $('.spec_all_block').append('<div class="spec_all_group" id="spec_all_group_'+id_catalog+'" data-id="'+id_catalog+'" data-parent-id="'+id_parent+'"><div class="group_title"><div class="parent_title">'+parent_title+'</div>'+$('.get_table_spec .lcp_html_navigation_span:last-child').html()+'</div><div class="spec_all_group_delete" onclick="spec_all_group_delete_click(\''+id_catalog+'\');">x</div><div class="spec_all" id="spec_all_'+id_catalog+'"></div></div>');
  }
  for (var i = 0; i < id_arr['id'].length; i++) { // добавление характеристик
    $('#spec_all_'+id_catalog).append('<div class="spec_all_item" data-id="'+id_arr['id'][i]+'">'+id_arr['title'][i]+' <div class="spec_all_delete" onclick="spec_all_item_click(\''+id_arr['id'][i]+'\');">x</div></div>');
  }
}

function get_spec_default(){
  // получение id групп
  var id_arr={};
  id_arr['spec_parent']={};
  id_arr['spec_group']={};
  id_arr['spec_value']={};

  if ($('.spec_all_group').length>0){
    for (var i = 0; i < $('.spec_all_group').length; i++) {
      var obj=$('.spec_all_group').eq(i);
      id_arr['spec_parent'][i]=$(obj).attr('data-parent-id');
      id_arr['spec_group'][i]=$(obj).attr('data-id');
    }
  }

  if ($('.spec_all_item').length>0){
    for (var i = 0; i < $('.spec_all_item').length; i++) {
      var obj=$('.spec_all_item').eq(i);
      id_arr['spec_value'][i]=$(obj).attr('data-id');
    }
  }

  return id_arr;
}

function get_spec_default_str(){
  var str='';
  // получение выбранных id
  for (var i = 0; i < $('.spec_all_item').length; i++) {
    var obj=$('.spec_all_item').eq(i);
    str+=str==''?$(obj).attr('data-id'):','+$(obj).attr('data-id');
  }
  return str;
}
