<script src="<?php echo $dir;?>components/lcp/system/kladr/kladr.js?ver=<?php echo mt_rand();?>" charset="utf-8"></script>
<?php
// для начала работы с КЛАДР вызовите lcp_kladr(); КЛАДР работает только с соответствующими функциями, такими, как текстовые поля, lcp_text_kladr. Для использования БД, преобразуйте ее в формат sql и загрузите на сайт. Для каждой таблицы добавьте перед названием префикс kladr_
function lcp_kladr(){
  $kladr_html='';
  $kladr_region_arr=getarrayvaluedb(array('CODE','NAME','SOCR'),'*','kladr_kladr','CODE LIKE \'__000000000__\'');
  for ($i=0; $i < count($kladr_region_arr['NAME']); $i++) {
    $kladr_region_arr['onclick'][$i]='kladr_raion(\''.$kladr_region_arr['CODE'][$i].'\');';
    $kladr_region_arr['NAME_SOCR'][$i]=$kladr_region_arr['NAME'][$i].' | '.$kladr_region_arr['SOCR'][$i];
  }
  $kladr_html .= lcp_html_block_default2(array(
    'id_html'=>'div_kladr_region',
    'html'=>text_dropdown(array(
      'id'=>'tbl_kladr_region',
      'text_id'=>'tbl_kladr_region_text',
      'class'=>'i_block',
      'width'=>'200px',
      'placeholder'=>'Регион',
      'title_child_arr'=>$kladr_region_arr['NAME_SOCR'],
      'value_child_arr'=>$kladr_region_arr['CODE'],
      'onclick_child_arr'=>$kladr_region_arr['onclick'],
      'onkeyup'=>'kladr_region();',
      'autocomplete'=>'off',
      'height_child'=>'200px;'
    ))
  ));
  $kladr_html .= lcp_html_block_default2(array(
    'id_html'=>'div_kladr_raion',
    'html'=>text_dropdown(array(
      'id'=>'tbl_kladr_raion',
      'text_id'=>'tbl_kladr_raion_text',
      'class'=>'i_block',
      'width'=>'200px',
      'placeholder'=>'Район / Районный центр',
      'onkeyup'=>'kladr_raion();',
      'autocomplete'=>'off',
      'height_child'=>'200px;'
    ))
  ));
  $kladr_html .= lcp_html_block_default2(array(
    'id_html'=>'div_kladr_gorod',
    'html'=>text_dropdown(array(
      'id'=>'tbl_kladr_gorod',
      'text_id'=>'tbl_kladr_gorod_text',
      'class'=>'i_block',
      'width'=>'200px',
      'placeholder'=>'Город',
      'onkeyup'=>'kladr_gorod();',
      'autocomplete'=>'off',
      'height_child'=>'200px;'
    ))
  ));
  $kladr_html .= lcp_html_block_default2(array(
    'id_html'=>'div_kladr_street',
    'html'=>text_dropdown(array(
      'id'=>'tbl_kladr_street',
      'text_id'=>'tbl_kladr_street_text',
      'class'=>'i_block',
      'width'=>'200px',
      'placeholder'=>'Улица',
      'onkeyup'=>'kladr_street();',
      'autocomplete'=>'off',
      'height_child'=>'200px;'
    ))
  ));
  $kladr_html .= lcp_html_block_default2(array(
    'id_html'=>'div_kladr_dom',
    'html'=>lcp_text_default(array(
      'id'=>'tbl_kladr_dom',
      'class'=>'i_block',
      'width'=>'200px',
      'placeholder'=>'Дом',
      'autocomplete'=>'off',
      'height_child'=>'200px;'
    ))
  ));
  $kladr_html .= lcp_html_block_default2(array(
    'id_html'=>'div_kladr_korpys',
    'html'=>lcp_text_default(array(
      'id'=>'tbl_kladr_korpys',
      'class'=>'i_block',
      'width'=>'200px',
      'placeholder'=>'Корпус',
      'autocomplete'=>'off',
      'height_child'=>'200px;'
    ))
  ));
  $kladr_html .= lcp_html_block_default2(array(
    'id_html'=>'div_kladr_kvartira',
    'html'=>lcp_text_default(array(
      'id'=>'tbl_kladr_kvartira',
      'class'=>'i_block',
      'width'=>'200px',
      'placeholder'=>'Квартира',
      'autocomplete'=>'off',
      'height_child'=>'200px;'
    ))
  ));
  $kladr_html .= lcp_html_block_default2(array(
    'html'=>lcp_button_default(array(
      'value'=>'Принять текущие значения',
      'onclick'=>'get_address();',
      'height_child'=>'200px;'
    ))
  ));




  $html ='';
  $html .= lcp_popup_default(array(
    'id_popup' => 'popup_basket',
    'title' => 'Классификатор (КЛАДР)',
    'content_html' => $kladr_html,
    'popup_show' => 'kladr_start',
    'popup_close' => 'kladr_start_close',
    'height' => '490px',
    'width' => '500px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));
  return $html;
}
?>
