var reg_code='';
var raion_code='';
var gorod_code='';
var street_code='';
function kladr_region(){
  var like=$('#tbl_kladr_region_text').val();
  $.ajax({
    url:DIR+"components/lcp/system/kladr/ajax_kladr.php",
    type:"post",
    dataType:"JSON",
    data:{
      action:'kladr_region',
      like:like
    },
    success:function(data){
      if (data.result=='false'){
        message.show(data.string);
      } else {
        // Оторажение найденного региона
        show_set_text_dropdown_child_arr({
          id:'tbl_kladr_region',
          text:like,
          title_child_arr: data.kladr_region_arr['NAME_SOCR'],
          value_child_arr: data.kladr_region_arr['CODE'],
          onclick_child_arr:data.kladr_region_arr['onclick'],
          height_child:'200px;'
        });

        var html=text_dropdown({
          id:'tbl_kladr_raion',
          text_id:'tbl_kladr_raion_text',
          class:'i_block',
          width:'200px',
          placeholder:'Район / Районный центр',
          onkeyup:'kladr_raion();',
          autocomplete:'off',
          height_child:'200px;'
        });
        $('#div_kladr_raion').html(html);

        var html=text_dropdown({
          id:'tbl_kladr_gorod',
          text_id:'tbl_kladr_gorod_text',
          class:'i_block',
          width:'200px',
          placeholder:'Город',
          onkeyup:'kladr_gorod();',
          autocomplete:'off',
          height_child:'200px;'
        });
        $('#div_kladr_gorod').html(html);

        var html=text_dropdown({
          id:'tbl_kladr_street',
          text_id:'tbl_kladr_street_text',
          class:'i_block',
          width:'200px',
          placeholder:'Улица',
          onkeyup:'kladr_street();',
          autocomplete:'off',
          height_child:'200px;'
        });
        $('#div_kladr_street').html(html);
      }
    },
    error:function(data){
      message.show('Ошибка ajax');
    }
  });
}


function kladr_raion(){
  reg_code=get_dropdown_opt_value('tbl_kladr_region');
  var like=$('#tbl_kladr_raion_text').val();
  $.ajax({
    url:DIR+"components/lcp/system/kladr/ajax_kladr.php",
    type:"post",
    dataType:"JSON",
    data:{
      action:'kladr_raion',
      reg_code:reg_code,
      like:like
    },
    success:function(data){
      if (data.result=='false'){
        message.show(data.string);
      } else {
        // Отображение найденного района
        show_set_text_dropdown_child_arr({
          id:'tbl_kladr_raion',
          text:like,
          title_child_arr: data.kladr_raion_arr['NAME_SOCR'],
          onclick_child_arr:data.kladr_raion_arr['onclick'],
          value_child_arr:data.kladr_raion_arr['CODE'],
          onkeyup:'kladr_raion();',
          height_child:'200px;'
        });

        var html=text_dropdown({
          id:'tbl_kladr_street',
          text_id:'tbl_kladr_street_text',
          class:'i_block',
          width:'200px',
          placeholder:'Улица',
          title_child_arr: data.kladr_street_arr['NAME_SOCR'],
          onclick_child_arr:data.kladr_street_arr['onclick'],
          value_child_arr:data.kladr_street_arr['CODE'],
          onkeyup:'kladr_street();',
          autocomplete:'off',
          height_child:'200px;'
        });
        $('#div_kladr_street').html(html);
      }
    },
    error:function(data){
      message.show('Ошибка ajax');
    }
  });
}


function kladr_gorod(){
  raion_code=get_dropdown_opt_value('tbl_kladr_raion');
  var like=$('#tbl_kladr_gorod_text').val();
  $.ajax({
    url:DIR+"components/lcp/system/kladr/ajax_kladr.php",
    type:"post",
    dataType:"JSON",
    data:{
      action:'kladr_gorod',
      raion_code:raion_code,
      like:like
    },
    success:function(data){
      if (data.result=='false'){
        message.show(data.string);
      } else {
        // Отображение найденного города
        show_set_text_dropdown_child_arr({
          id:'tbl_kladr_gorod',
          text:like,
          title_child_arr: data.kladr_gorod_arr['NAME_SOCR'],
          onclick_child_arr:data.kladr_gorod_arr['onclick'],
          value_child_arr:data.kladr_gorod_arr['CODE'],
          onkeyup:'kladr_gorod();',
          height_child:'200px;'
        });

        var html=text_dropdown({
          id:'tbl_kladr_street',
          text_id:'tbl_kladr_street_text',
          class:'i_block',
          width:'200px',
          placeholder:'Улица',
          title_child_arr: data.kladr_street_arr['NAME_SOCR'],
          onclick_child_arr:data.kladr_street_arr['onclick'],
          value_child_arr:data.kladr_street_arr['CODE'],
          autocomplete:'off',
          onkeyup:'kladr_street();',
          height_child:'200px;'
        });
        $('#div_kladr_street').html(html);
      }
    },
    error:function(data){
      message.show('Ошибка ajax');
    }
  });
}



function kladr_street(){
  gorod_code=get_dropdown_opt_value('tbl_kladr_gorod');
  raion_code=get_dropdown_opt_value('tbl_kladr_raion');
  if (gorod_code==''){
    var s_code=raion_code;
  } else {
    var s_code=gorod_code;
  }
  var like=$('#tbl_kladr_street_text').val();
  $.ajax({
    url:DIR+"components/lcp/system/kladr/ajax_kladr.php",
    type:"post",
    dataType:"JSON",
    data:{
      action:'kladr_street',
      raion_code:s_code,
      like:like
    },
    success:function(data){
      if (data.result=='false'){
        message.show(data.string);
      } else {
        // Отображение найденной улицы
        show_set_text_dropdown_child_arr({
          id:'tbl_kladr_street',
          text:like,
          title_child_arr: data.kladr_street_arr['NAME_SOCR'],
          onclick_child_arr:data.kladr_street_arr['onclick'],
          value_child_arr:data.kladr_street_arr['CODE'],
          onkeyup:'kladr_street();',
          height_child:'200px;'
        });

      }
    },
    error:function(data){
      message.show('Ошибка ajax');
    }
  });
}

function kladr_street_opt(){
  street_code=get_dropdown_opt_value('tbl_kladr_street');
}


function get_address(){
  lcp_loading_default({loading_show:'true'});
  $.ajax({
    url:DIR+"components/lcp/system/kladr/ajax_kladr.php",
    type:"post",
    dataType:"JSON",
    data:{
      action:'get_address',
      region_code:reg_code,
      raion_code:raion_code,
      gorod_code:gorod_code,
      street_code:street_code,
      dom:$('#tbl_kladr_dom').val(),
      korpys:$('#tbl_kladr_korpys').val(),
      kvartira:$('#tbl_kladr_kvartira').val()
    },
    success:function(data){
      lcp_loading_default({loading_show:'false'});
      if (data.result=='false'){
        message.show(data.string);
      } else {
        $('.text_kladr').val(data.address);
        kladr_start_close();
      }
    },
    error:function(data){
      lcp_loading_default({loading_show:'false'});
      message.show('Ошибка ajax');
    }
  });
}
