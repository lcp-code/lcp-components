<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
  $component_property=array(
    'name' => 'lcp_convert_size_files',
    'type' => 'system',
    'group' => 'Файлы',
    'inner' => array(
      'description' => 'Функция для преобразования байтов в KB, MB, GB, TB и т.д.',
      'css' => array(
      ),
      'js' => array(
      )
    ),
    'set_name_php' => array(
      '0' => 'bytes',
    ),
    'set_value_php' => array(
      '0' => '',
    ),
    'set_description_php' => array(
      '0' => 'Размер файла в байтах',
    ),
    'get_php' => array(
      '0' =>'text'
    ),
    'get_description_php' => array(
      '0' =>'Функция возвращает отформатированный текст'
    ),
  );
}
?>




<?php
function lcp_convert_size_files($param=''){
  $bytes=$param['bytes'];
  $bytes = floatval($bytes);
  $arBytes = array(
    0 => array(
      "UNIT" => "TB",
      "VALUE" => pow(1024, 4)
    ),
    1 => array(
      "UNIT" => "GB",
      "VALUE" => pow(1024, 3)
    ),
    2 => array(
    "UNIT" => "MB",
    "VALUE" => pow(1024, 2)
    ),
    3 => array(
    "UNIT" => "KB",
    "VALUE" => 1024
    ),
    4 => array(
    "UNIT" => "B",
    "VALUE" => 1
    ),
    );

    foreach($arBytes as $arItem)
    {
      if($bytes >= $arItem["VALUE"])
      {
        $result = $bytes / $arItem["VALUE"];
        $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
        break;
      }
    }
    return $result;
  }
  ?>
