<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_send_on_other_files',
  'type' => 'system',
  'group' => 'Файлы',
  'inner' => array(
    'description' => 'Невизуальный php скрипт, который отвечает за отправку файлов на другой сервер.',
    'css' => array(
      // '0' => '/components/system/images/load-photo/load-photo.css'
    ),
    'js' => array(
      // '0' => '/components/system/images/load-photo/load-photo.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'http',
    '1' => 'domen_site',
    '2' => 'folder',
    '3' => 'send_file',
    '4' => 'save_file_name',
    '5' => 'path_component_load_from_other',
    '6' => 'path_component_send_on_other',

  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => 'components/system/files/load-from-other/index.php',
    '6' => 'components/system/files/send-on-other/download.php',
  ),
  'set_description_php' => array(
    '0' => '<div>http или https протокол удаленного сервера</div>',
    '1' => '<div>Домен удаленного сервера (site.ru)</div>',
    '2' => '<div>Относительный путь, куда необходимо загрузить файл на удаленном сервере. Указывается только каталог.</div><div>Например:</div><div>polz/'.date(Y).'/'.date(m).'/</div>',
    '3' => '<div>Полный путь для загрузки файла с текущего сервера на удаленный (с возможностью его скачать). </div><div>Например:</div><div>https://kembri.com/polz/'.date(Y).'/'.date(m).'/tmp/file.txt</div>',
    '4' => '<div>Результативное имя файла (загруженное). Если не указано - берется из send_file.</div><div>Например:</div><div>file.txt</div>',
    '5' => '<div>Путь у удаленого сервера к компоненту lcp_load_from_other_files</div><div>Например:</div><div>components/system/files/load-from-other/index.php</div>',
    '6' => '<div>Путь у текущего сервера к компоненту lcp_load_from_other_files для скачивания файла</div><div>Например:</div><div>components/system/files/send-on-other/download.php</div>',
  ),
  'get_php' => array(
    '0' =>'bool'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает true, если файл был передан успешно и false в случае, если возникли какие-либо ошибки.'
  ),
  'set_name_js' => array(
  ),
  'set_value_js' => array(
  ),
  'set_description_js' => array(
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}











if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_send_on_other_files extends lcp_components implements lcp_components_build{
    public $http;
    public $domen_site;
    public $folder;
    public $send_file;
    public $save_file_name;

    public $path_component_load_from_other;
    public $path_component_send_on_other;
    private $res_send_file;

    function __construct($param=array()){
      $this->http=$param['http'];
      $this->domen_site=$param['domen_site'];
      $this->folder=$param['folder'];
      $this->send_file=$param['send_file'];
      $this->save_file_name=$param['save_file_name'];
      $this->path_component_load_from_other=$param['path_component_load_from_other'];
      $this->path_component_send_on_other=$param['path_component_send_on_other'];
      parent::__construct($param);
    }

    function __build_component(){
      $http=$_SERVER['REQUEST_SCHEME'];
      $domen_site=$_SERVER['SERVER_NAME'];
      if (empty($this->path_component_load_from_other)){
        $this->path_component_load_from_other='components/system/files/load-from-other/index.php';
      }
      if (empty($this->path_component_send_on_other)){
        $this->path_component_send_on_other='components/system/files/send-on-other/download.php';
      }

      $this->res_send_file=$http.'://'.$domen_site.'/'.$this->path_component_send_on_other.'?send_file='.$this->send_file;
      // generate_exception($this->http.'://'.$this->domen_site.'/'.$this->path_component_load_from_other.'?folder='.$this->folder.'&save_file_name='.$this->save_file_name.'&send_file='.$this->res_send_file);

      $html='';
      $html=file_get_contents($this->http.'://'.$this->domen_site.'/'.$this->path_component_load_from_other.'?folder='.$this->folder.'&save_file_name='.$this->save_file_name.'&send_file='.$this->res_send_file);
      if (empty($html)){
        $html=false;
      }
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_send_on_other_files_init($param=array()){
    $lcp_obj=new lcp_send_on_other_files($param);
    return $lcp_obj->html();
  }
}
?>
