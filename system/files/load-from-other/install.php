<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_load_from_other_files',
  'type' => 'system',
  'group' => 'Файлы',
  'inner' => array(
    'description' => 'Невизуальный php скрипт, который отвечает за прием файлов с других серверов.',
    'css' => array(
      // '0' => '/components/system/images/load-photo/load-photo.css'
    ),
    'js' => array(
      // '0' => '/components/system/images/load-photo/load-photo.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'send_file',
    '1' => 'save_file_name',
    '2' => 'folder',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => 'polz/',
  ),
  'set_description_php' => array(
    '0' => '<div>Полный путь для скачивания файла с удаленного сервера. </div><div>Например:</div><div>https://test.ru/polz/'.date(Y).'/'.date(m).'/tmp/file.txt</div>',
    '1' => '<div>Результативное имя файла. Если не указано - берется из send_file</div>',
    '2' => '<div>Относительный путь для сохранения файла. Указывается только каталог.</div><div>Например:</div><div>/polz/'.date(Y).'/'.date(m).'/</div>',
  ),
  'get_php' => array(
    '0' =>'text',
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает JS-скрипт для работы с компонентом',
  ),
  'set_name_js' => array(
    '0' => 'files',
  ),
  'set_value_js' => array(
    '0' => '',
  ),
  'set_description_js' => array(
    '0' => 'Массив с путями файлов, которые нужно отправить на сервер (Этот массив можно получить следующим образом: document.getElementById(ids).files, где ids - идентификатор поля <pre style="display:inline-block;margin: 0px;">'.htmlspecialchars('<input type="file">',ENT_QUOTES,'UTF-8',false).'</pre>)',
  ),
  'get_js' => array(
    // '0' => 'array'
  ),
  'get_description_js' => array(
    // '0' => 'Если get установлено в значение "get_load_photos", то возвращает путь с фото'
  )
);
}
?>




<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_load_from_other_files extends lcp_components implements lcp_components_build{
    public $send_file;
    public $folder;
    public $save_file_name;

    function __construct($param=array()){
      $this->folder=$param['folder'];
      $this->send_file=$param['send_file'];
      $this->save_file_name=$param['save_file_name'];
      parent::__construct($param);
    }

    function __build_component(){
      $root_folder=lcp_root_folder();

      // echo $this->send_file;
      // exit();

      $data=file_get_contents($this->send_file);

      if ($data===false){
        return 'false';
      }
      // echo $this->send_file;
      // exit();
      if (file_exists($root_folder.$this->folder)===false){
        mkdir($root_folder.$this->folder, 0700, true);
      }
      // echo $data;
      // echo $this->folder.basename($this->send_file);
      // exit();
      $this->save_file_name=(!empty($this->save_file_name))?$this->save_file_name:basename($this->send_file);
      // echo $root_folder.$this->folder.$this->save_file_name;
      // exit();
      $res=file_put_contents($root_folder.$this->folder.$this->save_file_name,$data);
      if ($res===false){
        return 'false';
      }
      return 'true';
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_load_from_other_files_init($param=array()){
    $lcp_obj=new lcp_load_from_other_files($param);
    return $lcp_obj->html();
  }
}
?>
