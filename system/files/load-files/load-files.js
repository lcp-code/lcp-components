function lcp_load_files(param){
  var ids=param['id'];
  var get=param['get'];
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='files_field';
  }
  if (get=='get_load_files'){
    return get_load_files(ids);
  }
  if (get=='get_for_save_files'){
    return get_for_save_files(ids);
  }
}

function sortable_files(ids){
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='files_field';
  }
  $('#'+ids).sortable();
  $('#'+ids).disableSelection();
}

function del_files(obj){
  $(obj).parent('.files_field_items').remove();
}

function edit_files(obj){
  var ids=$(obj).parent('.files_field_items').parent('div').attr('id');
  var title=$(obj).parent('.files_field_items').attr('data-title');
  var src=$(obj).parent('.files_field_items').attr('data-src');

  $('#title_files_'+ids).val(title);
  $('#src_files_'+ids).val(src);
  // $('#files_butt_save_'+ids).attr('onclick','files_edit_close();files_edit_save(\''+ids+'\')');
  $('#'+ids).children('.files_field_items[data-edit="1"]').attr('data-edit','0');
  $(obj).parent('.files_field_items').attr('data-edit','1');
}

function files_edit_save(ids){
  var obj=$('#'+ids).children('.files_field_items[data-edit="1"]');
  $(obj).attr('data-title',$('#title_files_'+ids).val());
  $(obj).attr('data-src',$('#src_files_'+ids).val());
  $(obj).children('.files_str').html($('#title_files_'+ids).val());
  $(obj).attr('data-edit','0');
}

function get_load_files(ids){
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='files_field';
  }
  var obj=$('#'+ids).children('.files_field_items');
  var files={};
  files['id']={};
  files['title']={};
  files['src']={};
  files['numm']={};
  files['files_field']={};
  files['index']={};
  for (var i = 0; i < obj.length; i++) {
    var obj_item=$(obj).eq(i);
    files['id'][i]=$(obj_item).attr('data-id');
    files['title'][i]=$(obj_item).attr('data-title');
    files['src'][i]=$(obj_item).attr('data-src');
    files['numm'][i]=i;
    files['files_field'][i]=ids;
    files['index'][$(obj_item).attr('data-id')]=i;
  }
  files['length']=obj.length;
  return files;
}

function get_for_save_files(ids){
  if ((typeof ids==='undefined')||(ids==='')){
    var ids='files_field';
  }
  var files=get_load_files(ids);
  files['deistv']={};
  var files_del={};

  if (typeof first_id_arr[ids]==='undefined'){
    alert('Ошибка! Возможно, id в get_for_save_files введен не верно!');
    return;
  }

  // Нахождение удаленных фото
  var bb='false';
  var k=-1;
  for (var i1 = 0; i1 < first_id_arr[ids]['length']; i1++) {
    bb='false'
    for (var i2 = 0; i2 < files['length']; i2++) {
      if (first_id_arr[ids][i1]==files['id'][i2]){
        bb='true';
        break;
      }
    }
    if (bb=='false'){
      k++;
      files_del[k]=first_id_arr[ids][i1];
      files_del['length']=(k+1);
    }
  }

  // Нахождение добавленных и обновленных фото
  var bb='false';
  var k=-1;
  for (var i1 = 0; i1 < files['length']; i1++) {
    if (files['id'][i1]==''){
      files['deistv'][i1]='INSERT';
    } else {
      bb='false'
      for (var i2 = 0; i2 < first_id_arr[ids]['length']; i2++) {
        if (files['id'][i1]==first_id_arr[ids][i2]){
          bb='true';
          files['deistv'][i1]='UPDATE';
          break;
        }
      }
      if (bb=='false'){
        files['deistv'][i1]='DELETE';
      }
    }
  }

  // Добавление к массиву удаленных фото
  k=files['length'];
  for (var i = 0; i < files_del['length']; i++) {
    files['id'][k]=files_del[i];
    files['deistv'][k]='DELETE';
    k++;
  }

  // добавляем к существующим id аттрибуты
  files['length']=k;
  files['numm']={};
  files['src']={};
  files['title']={};
  files['files_field']={};

  files_get=get_load_files(ids);
  files_get_new=get_load_files(ids);
  for (var i = 0; i < files['length']; i++) {
    var files_get_index=files_get['index'][files['id'][i]];
    files['numm'][i]=files_get['numm'][files_get_index];
    files['src'][i]=files_get['src'][files_get_index];
    files['title'][i]=files_get['title'][files_get_index];
  }

  // добавляем к новым (insert) фото аттрибуты
  k=0;
  for (var i = 0; i < files_get['length']; i++) {
    if (files_get['id'][i]==''){
      files_get_new['numm'][k]=files_get['numm'][i];
      files_get_new['src'][k]=files_get['src'][i];
      files_get_new['title'][k]=files_get['title'][i];
      k++;
    }
  }

  k=0;
  numm=0;
  for (var i = 0; i < files['length']; i++) {
    if (files['deistv'][i]=='INSERT'){
      files['src'][i]=files_get_new['src'][k];
      files['title'][i]=files_get_new['title'][k];
      k++;
    }
    if (files['deistv'][i]!='DELETE'){
      files['numm'][i]=numm;
      numm++;
    }
    files['files_field'][i]=ids;
  }

  return files;
}

function load_files(param){
  var ids=param['id'];
  var max_files=param['max_files'];
  var url_load=param['url_load'];
  if (document.getElementById('files_'+ids).files.length<=0){
    return;
  }
  if (max_files>0){
    if (document.getElementById('files_'+ids).files.length>max_files){
      alert('Можно загрузить не более '+max_files+' файлов!');
      return;
    }
    if ($('#'+ids).children('.files_field_items').length>=max_files){
      alert('Можно загрузить не более '+max_files+' файлов!');
      return;
    }
  }
  lcp_loading_default({loading_show:'true'});
  lcp_message('Идет загрузка файлов... Пожалуйста, дождитесь окончания процесса...');
  var files=document.getElementById('files_'+ids).files;
  var obj = new FormData();
  $.each(files, function(key, value){
    obj.append( key, value );
  });
  document.getElementById('files_'+ids).value='';
  $.ajax({
    url: url_load,
    type: 'POST',
    data: obj,
    cache: false,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function(data){
      lcp_loading_default({loading_show:'false'});
      if(data.error=='false'){
        data.files_arr['id']=ids;
        view_files(data.files_arr);
      } else{
        alert(data.string);
      }
    },
    error: function(){
      lcp_loading_default({loading_show:'false'});
      alert('Ошибка ajax запроса при загрузки файла!');
    }
  });
}

$(function(){
    sortable_files();
  });
