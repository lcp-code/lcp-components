<?php
header('Content-Type: application/json');
$root_dir = $_SERVER['DOCUMENT_ROOT'];
include_once($root_dir.'/konsfigli.php');
$domen_site=domen_site();

function generate_exception($string){
  exit(json_encode(array('error' => 'false','string' => $string)));
}


$_LCP_AJAX='AJAX';
include_once($root_dir.'/includes/valuesdbli.php');
include_once($root_dir.'/includes/infosite.php');
include_once($root_dir.'/components/index.php');
$site_lcp = info_site();
$user_lcp = info_user();

$data = array();

if (isset($_GET['loadfiles'])){
  // $error = false;
  // $files = array();
  //
  // $uploaddir = './uploads/'; // . - текущая папка где находится submit.php
  //
  // // Создадим папку если её нет
  //
  // if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
  //
  // // переместим файлы из временной директории в указанную
  // foreach( $_FILES as $file ){
  //     if( move_uploaded_file( $file['tmp_name'], $uploaddir . basename($file['name']) ) ){
  //         $files[] = realpath( $uploaddir . $file['name'] );
  //     }
  //     else{
  //         $error = true;
  //     }
  // }
  //
  // $data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
  //
  // echo json_encode( $data );

  if (!empty($_FILES)){
    $folder_polz='../';
    $folder_polz_str='';
    $files_arr=array();
    for ($i=0; $i < 15; $i++) {
      if (file_exists($folder_polz_str.'polz')){
        $folder_polz=$folder_polz_str.'polz/files';
        break;
      }
      $folder_polz_str.=$folder_polz;
    }


    for ($iui=0; $iui < count($_FILES); $iui++) {
      if (($_FILES[$iui]['error']!==0)&&(!empty($_FILES[$iui]['name']))){
        echo 'Размер некоторых файлов превышает 8 Мб!';
      } else {
        $temp=$_FILES[$iui]['tmp_name'];
        $files_name=$_FILES[$iui]['name'];
        $files_path_info = pathinfo($files_name);
        $tipes_fileses = $files_path_info['extension'];
        $names_fileses = $files_path_info['filename'];
        $files_name=$files_path_info['filename'];
        $files_name=lcp_url_translit(array('url'=>$files_name));
        $files_name=$files_name.'.'.$tipes_fileses;
        $path_files=$folder_polz.'/'.date('Y').'/'.date('m');
        if (file_exists($path_files)===false)
        mkdir($path_files,0777,true);
        move_uploaded_file($temp,$path_files.'/'.$files_name);
        $url_files1=$http.'://'.$domen_site.'/polz/files/'.date(Y).'/'.date(m).'/'.$files_name;
        $url_filesarr['files'][$iui]=$url_files1;
        $url_filesarr['title'][$iui]=htmlspecialchars($names_fileses.'.'.$tipes_fileses);
      }
    }
  } else {
    generate_exception('net');
  }
  $files_arr=array(
    'files'=>array(
      'files'=>$url_filesarr['files'],
      'title'=>$url_filesarr['title']
    )
  );
  exit(json_encode(array('error' => 'false','files_arr' => $files_arr)));
}

generate_exception('Функций не найдено!');
?>
