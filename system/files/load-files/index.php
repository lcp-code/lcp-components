<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_load_files',
  'type' => 'system',
  'group' => 'Файлы',
  'inner' => array(
    'description' => 'Загрузка файлов, используемая в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/files/load-files/load-files.css'
    ),
    'js' => array(
      '0' => '/components/system/files/load-files/load-files.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'max_files'
  ),
  'set_value_php' => array(
    '0' => 'files_field',
    '1' => '0'
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Максимальное количество файлов'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
    '0' => 'id',
    '1' => 'get'
  ),
  'set_value_js' => array(
    '0' => 'files_field',
    '1' => ''
  ),
  'set_description_js' => array(
    '0' => 'Идентификатор (тег id), по которому следует искать элемент',
    '1' => '<div>Получение данных о файлах (по заданному id), где:</div><div>get_load_files - получить готовые файлы и аттрибуты</div><div>get_for_save_files - получить действия для удаления, обновления и добавления прошлых и новых файлов со всеми аттрибутами (рекомендуется для сохранения в БД)</div>'
  ),
  'get_js' => array(
    '0' => 'array'
  ),
  'get_description_js' => array(
    '0' => 'Если get установлено в значение "get_load_files", то возвращает массив с аттрибутами файла'
  )
);
}
?>




<?php
function lcp_load_files($param=''){
  $id_components=getcelldb('id','id','components','name=\'lcp_load_files\'');
  $puth_php=getcelldb('puth_php','puth_php','components','name=\'lcp_load_files\'');
  $puth_php=str_replace('index.php','',$puth_php);

  $ids=$param['id'];
  $max_files=$param['max_files'];
  $files_arr=$param['files_arr'];
  $default_ids=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'id\'');
  $default_max_files=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'max_files\'');
  $ids=(!empty($ids))?$ids:$default_ids;
  $max_files=(!empty($max_files))?$max_files:$default_max_files;


  $html='';
  $html.='<input type="file" id="files_'.$ids.'" name="files[]" multiple onChange="load_files({max_files:\''.$max_files.'\',id:\''.$ids.'\',url_load:\'/'.$puth_php.'ajax-load-files-server.php?loadfiles=true\'});" class="files_add_files" value="" autocomplete="off">'; // ,image/gif отключен, так как не поддерживает прозрачность
  $html.=components('lcp_button_green',array(
    'value'=>'Выбрать файл',
    'onclick'=>'$(\'#files_'.$ids.'\').click();'
  ));

  // 'Выбрать файл';
  $html.='<div id="'.$ids.'">';
  $html.=components('lcp_ajax',array(
    'lcp_type_ajax'=>'include',
    'function_name'=>'view_files',
    'begin_html'=>'
    var files=param[\'files\'];
    var ids=param[\'id\'];
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/'.$puth_php.'ajax-load-files.php',
    'data'=>array(
      'action'=>'view_files',
      'files'=>$files_arr,
      'ids'=>$ids
    ),
    'data_js'=>'{
      action:\'view_files\',
      files:files,
      ids:ids
      }',
      'lcp_success_true'=>'lcp_loading_default({loading_show:\'false\'});$(\'#\'+ids).append(data.string);sortable_files(ids);',
      'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});alert(data.string);',
      'error'=>'lcp_loading_default({loading_show:\'false\'});alert(\'Ошибка при ajax запросе!\');',
      'show_error'=>'false'
    ));
    $html.='<script>sortable_files(\''.$ids.'\');</script></div>';

    $popup_content='';
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Ссылка (путь к файлу):',
      'html'=>components('lcp_text_default',array(
        'id'=>'src_files_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите ссылку'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Наименование:',
      'html'=>components('lcp_text_default',array(
        'id'=>'title_files_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите название файла'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>' ',
      'html'=>components('lcp_block_one_line',array(
        'tbody'=>array(components('lcp_button_default',array(
          'id'=>'files_butt_save_'.$ids,
          'value'=>'Применить свойства к файлу',
          'onclick'=>'files_edit_'.$ids.'_close();files_edit_save(\''.$ids.'\')'
        ))),
        'width_columns'=>array('100%'),
        'text_align_col_tbody'=>array('center')
      ))
    ));
    $html.=components('lcp_popup_default',array(
      'id_popup'=>'files_edit_'.$ids,
      'title'=>'Свойства файла',
      'content_html'=>$popup_content,
      'popup_show'=>'files_edit_'.$ids,
      'popup_close'=>'files_edit_'.$ids.'_close',
      'width'=>'400px',
      'height'=>'260px'
    ));

  $html.='<script type="text/javascript">';
  $html.='if (typeof first_id_arr===\'undefined\'){';
  $html.='var first_id_arr={};';
  $html.='}';
  $html.='first_id_arr[\''.$ids.'\']={};';
    for ($i=0; $i < count($files_arr['id']); $i++) {
      $html.='first_id_arr[\''.$ids.'\'][\''.$i.'\']=\''.$files_arr['id'][$i].'\';';
    }
  $html.='first_id_arr[\''.$ids.'\'][\'length\']=\''.count($files_arr['id']).'\'';
  $html.='</script>';
  return $html;
}
?>
