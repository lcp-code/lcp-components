<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_zip_files',
  'type' => 'system',
  'group' => 'Файлы',
  'inner' => array(
    'description' => 'Функция для архивирования директории в формат .zip (в т.ч. всех вложенных файлов и папок), используемая в панели администратора по-умолчанию',
    'css' => array(
    ),
    'js' => array(
    )
  ),
  'set_name_php' => array(
    '0' => 'source',
    '1' => 'destination',
    '2' => 'except_arr',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
  ),
  'set_description_php' => array(
    '0' => 'Путь к директории, которую нужно архивировать',
    '1' => 'Полный путь полученного .zip архива',
    '2' => 'Массив Исключенных из архивации файлов и папок (возможно, не работает)',
  ),
  'get_php' => array(
    '0' =>'false'
  ),
  'get_description_php' => array(
    '0' =>'В случае неуспешной архивации, функция возвратит false'
  ),
);
}
?>




<?php
function lcp_zip_files($param=''){
  $source=$param['source'];
  $destination=$param['destination'];
  $except_arr=$param['except_arr'];

  if (!extension_loaded('zip') || !file_exists($source)) {
    return false;
  }

  $zip = new ZipArchive();
  if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
    return false;
  }

  $exception_arr=array('.', '..');
  if (!empty($except_arr)){
    for ($i=0; $i < count($except_arr); $i++) {
      $exception_arr[]=$except_arr[$i];
    }
  }

  $source = str_replace('\\', DIRECTORY_SEPARATOR, realpath($source));
  $source = str_replace('/', DIRECTORY_SEPARATOR, $source);

  if (is_dir($source) === true) {
    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source),
    RecursiveIteratorIterator::SELF_FIRST);

    foreach ($files as $file) {
      $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
      $file = str_replace('/', DIRECTORY_SEPARATOR, $file);

      if ($file == '.' || $file == '..' || empty($file) || $file == DIRECTORY_SEPARATOR) {
        continue;
      }
      // Ignore "." and ".." folders
      if (in_array(substr($file, strrpos($file, DIRECTORY_SEPARATOR) + 1), $exception_arr)) {
        continue;
      }

      $file = realpath($file);
      $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
      $file = str_replace('/', DIRECTORY_SEPARATOR, $file);

      if (is_dir($file) === true) {
        $d = str_replace($source . DIRECTORY_SEPARATOR, '', $file);
        if (empty($d)) {
          continue;
        }
        $zip->addEmptyDir($d);
      } elseif (is_file($file) === true) {
        $zip->addFromString(str_replace($source . DIRECTORY_SEPARATOR, '', $file),
        file_get_contents($file));
      } else {
        // do nothing
      }
    }
  } elseif (is_file($source) === true) {
    $zip->addFromString(basename($source), file_get_contents($source));
  }

  return $zip->close();
}
?>
