<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_delete_files',
  'type' => 'system',
  'group' => 'Файлы',
  'inner' => array(
    'description' => 'Функция для удаления директории (в т.ч. всех вложенных файлов и папок), используемая в панели администратора по-умолчанию',
    'css' => array(
    ),
    'js' => array(
    )
  ),
  'set_name_php' => array(
    '0' => 'path',
  ),
  'set_value_php' => array(
    '0' => '',
  ),
  'set_description_php' => array(
    '0' => 'Путь к директории',
  ),
  'get_php' => array(
    '0' =>'false'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает false, когда завершит удаление'
  ),
);
}
?>




<?php
function lcp_delete_files($param=''){
  $path=$param['path'];
  if (empty($path)){
    return false;
  }
  if (!file_exists($path)){
    return false;
  }
  if (is_dir($path)) {
    foreach(scandir($path) as $p) if (($p!='.') && ($p!='..'))
    components('lcp_delete_files',array('path'=>$path.DIRECTORY_SEPARATOR.$p));
    return rmdir($path);
  }
  if (is_file($path)) return unlink($path);
  return false;
}
?>
