<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_copy_files',
  'type' => 'system',
  'group' => 'Файлы',
  'inner' => array(
    'description' => 'Функция для копирования директории (в т.ч. всех вложенных файлов и папок), используемая в панели администратора по-умолчанию',
    'css' => array(
    ),
    'js' => array(
    )
  ),
  'set_name_php' => array(
    '0' => 'from',
    '1' => 'to',
    '2' => 'rewrite',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => 'true',
  ),
  'set_description_php' => array(
    '0' => 'Директория, которую нужно скопировать',
    '1' => 'Путь, куда нужно скопировать',
    '2' => '<div>true - перезаписывать существующий файл</div><div>false - пропустить существующий файл</div>',
  ),
  'get_php' => array(
    // '0' =>'false'
  ),
  'get_description_php' => array(
    // '0' =>'В случае неуспешной архивации, функция возвратит false'
  ),
);
}
?>




<?php
function lcp_copy_files($param='') {
  $id_components=getcelldb('id','id','components','name=\'lcp_copy_files\'');
  $from=$param['from'];
  $to=$param['to'];
  $rewrite=$param['rewrite'];
  $default_from=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'from\'');
  $default_to=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'to\'');
  $default_rewrite=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'rewrite\'');
  $from=(!empty($from))?$from:$default_from;
  $to=(!empty($to))?$to:$default_to;
  $rewrite=(!empty($rewrite))?$rewrite:$default_rewrite;

  if (is_dir($from)) {
    @mkdir($to);
    $d = dir($from);
    while (false !== ($entry = $d->read())) {
      if ($entry == "." || $entry == "..") continue;
      components('lcp_copy_files',array('from'=>$from.'/'.$entry, 'to'=>$to.'/'.$entry, 'rewrite'=>$rewrite));
    }
    $d->close();
  } else {
    if (!file_exists($to) || $rewrite)
    copy($from, $to);
  }
}
?>
