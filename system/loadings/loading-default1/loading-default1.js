function lcp_loading_default(param){
  if (typeof param==='undefined'){
    var param='';
  }
  var loading_show=param['loading_show'];
  if (loading_show=='true'){
    lcp_loading_default_show();
  } else {
    lcp_loading_default_hidden();
  }
}

function lcp_loading_default_show(){
$('body').addClass('body_scroll_loading');
$('.loading_default').removeClass('loading_hidden');
}

function lcp_loading_default_hidden(){
  $('body').removeClass('body_scroll_loading');
  $('.loading_default').addClass('loading_hidden');
}
