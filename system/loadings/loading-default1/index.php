<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_loading_default',
  'type' => 'system',
  'group' => 'Загрузчик',
  'inner' => array(
    'description' => 'Компонент, предназначенный для ожидания окончания загрузки или выполненных пользователем различных операций. Инициализируется автоматически.',
    'css' => array(
      '0' => '/components/system/loadings/loading-default1/loading-default1.css'
    ),
    'js' => array(
      '0' =>'/components/system/loadings/loading-default1/loading-default1.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'loading_show'
  ),
  'set_value_php' => array(
    '0' => 'false'
  ),
  'set_description_php' => array(
    '0' => '<div>true - отобразить процесс ожидания загрузки.</div><div>false - скрыть процесс ожидания загрузки.</div>',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
    '0' => 'loading_show'
  ),
  'set_value_js' => array(
    '0' => 'false'
  ),
  'set_description_js' => array(
    '0' => '<div>true - отобразить процесс ожидания загрузки.</div><div>false - скрыть процесс ожидания загрузки.</div>',
  ),
  'get_js' => array(),
  'get_description_js' => array()
);
}
$css_install_arr[]='/components/system/loadings/loading-default1/loading-default1.css';
$js_install_arr[]='/components/system/loadings/loading-default1/loading-default1.js';
?>




<?php
function lcp_loading_default($param=''){
  global $loading_default1;
  $loading_show=$param['loading_show'];
  $default_loading_show='false';
  $loading_show=(empty($loading_show))?$default_loading_show:$loading_show;
  $loading_hidden=($loading_show=='true')?'':' loading_hidden ';
  if (!empty($loading_default1)){
    $load_img='';
    $load_img.='<div class="loading_default_block_img">';
    $load_img.='<div class="loading_default_img_back"><div class="loading_default_img_logo_scale"><div class="loading_default_img_logo" style="background-image:url(\''.$loading_default1['logo'].'\');"></div></div></div>';
    $load_img.='<div class="loading_default_block_title_footer_img">Пожалуйста, подождите...</div>';
    $load_img.='</div>';
  } else {
    $load_img='';
    $load_img.='<div class="loading_default_block"><i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>';
    $load_img.='<div class="loading_default_block_title">LCP</div>';
    $load_img.='<div class="loading_default_block_title_footer">Пожалуйста, подождите...</div>';
    $load_img.='</div>';
  }

  $html='';
  $html.='<div class="loading_default '.$loading_hidden.'">';
  $html.='<div class="loading_default_zatem"></div>';
  $html.=$load_img;
  $html.='</div>';
  return $html;
}

if ($install!='true'){
  if (!empty($_LCP_AJAX)){
    $LCP_AJAX_RESULT .= lcp_loading_default(array('loading_show'=>'false'));
    $LCP_AJAX_RESULT .=  '<style>
    .loading_hidden{
      display: none;
    }
    </style>';
  } else {
$html_install_arr[]=lcp_loading_default(array('loading_show'=>'false'));
$html_install_arr[]='<style>
.loading_hidden{
  display: none;
}
</style>';
  }
}
?>
