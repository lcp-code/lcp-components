<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_ckeditor_textedit',
  'type' => 'system',
  'group' => 'Текстовое поле',
  'inner' => array(
    'description' => '
    <div>Текстовый редактор с возможностью изменять стили, шрифты и пр. Основано на компоненте lcp_memo_default. Все PHP параметры указаны там. Настроить плагин ckeditor можно здесь: <a href="'.$http.'://'.$domen_site.'/components/system/ckeditor/full/samples/index.html" target="_blank">'.$http.'://'.$domen_site.'/components/system/ckeditor/full/samples/index.html</a></div>
    ',
    'css' => array(),
    'js' => array(
      '0' => '/components/system/ckeditor/full/ckeditor.js',
    	'1' => '/components/system/ckeditor/lcp-install.js'
    )
  ),
  'outer_name' => array(
  ),
  'outer_value' => array(
  ),
  'set_name_js' => array(
    '0' => 'id',
    '1' => 'zapr',
    '2' => 'value'
  ),
  'set_value_js' => array(
    '0' => '',
    '1' => 'get',
    '2' => ''
  ),
  'set_description_js' => array(
    '0' => 'Идентификатор (тег id) текстового поля, по которому стоит возвратить или передать текст',
    '1' => '<div>Тип запросов к текстовому полю, где:</div>
    <div>get - получить текст</div>
    <div>set - установить текст</div>',
    '2' => 'Устанавливаемое значение, если параметр zapr установлен в set'
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}


function lcp_ckeditor_textedit($param=''){
  $param['name']='lcp_ckedit_'.$param['id'];
  $html='';
  $html.=components('lcp_memo_default',$param);
  $html.='<script type="text/javascript">';
  $html.='CKEDITOR.replace(\'lcp_ckedit_'.$param['id'].'\');';
  $html.='</script>';
  return $html;
}
?>
