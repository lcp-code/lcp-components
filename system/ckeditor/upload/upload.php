<?php
function transliterate($input){
$gost = array(
   "Є"=>"ye","І"=>"i","Ѓ"=>"g","і"=>"i","№"=>"","є"=>"ye","ѓ"=>"g",
   "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Д"=>"d",
   "Е"=>"e","Ё"=>"yo","Ж"=>"zh",
   "З"=>"z","И"=>"i","Й"=>"j","К"=>"k","Л"=>"l",
   "М"=>"m","Н"=>"n","О"=>"o","П"=>"p","Р"=>"r",
   "С"=>"s","Т"=>"t","У"=>"u","Ф"=>"f","Х"=>"x",
   "Ц"=>"c","Ч"=>"ch","Ш"=>"sh","Щ"=>"shh","Ъ"=>"",
   "Ы"=>"y","Ь"=>"","Э"=>"e","Ю"=>"yu","Я"=>"ya",
   "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
   "е"=>"e","ё"=>"yo","ж"=>"zh",
   "з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
   "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
   "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x",
   "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
   "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
   " "=>"-","—"=>"",","=>"","!"=>"","’"=>"","@"=>"",
   "#"=>"","$"=>"","%"=>"","^"=>"","&"=>"-and-","*"=>"",
   "("=>"",")"=>"","+"=>"","="=>"",";"=>"",":"=>"",
   "'"=>"",'"'=>'',"~"=>"","`"=>"","?"=>"","/"=>"",
   "["=>"","]"=>"","{"=>"","}"=>"","|"=>"","."=>"","----"=>"","---"=>"","--"=>"","«"=>"","»"=>""
  );
return mb_strtolower(strtr($input, $gost));
}

function getex($filename) {
return end(explode(".", $filename));
}

function delete_format($filename){
$str_arr=explode(".", $filename);
$new_filename = '';
for ($i=0; $i < count($str_arr)-1; $i++) {
$new_filename .= $str_arr[$i];
}
return $new_filename;
}

if($_FILES['upload'])
{
if (($_FILES['upload'] == "none") OR (empty($_FILES['upload']['name'])) )
{
$message = "Вы не выбрали файл";
}
else if ($_FILES['upload']["size"] == 0 OR $_FILES['upload']["size"] > 2050000)
{
$message = "Размер файла не соответствует нормам";
}
else if (($_FILES['upload']["type"] != "image/jpeg") AND ($_FILES['upload']["type"] != "image/jpeg") AND ($_FILES['upload']["type"] != "image/png"))
{
$message = "Допускается загрузка только картинок JPG и PNG.";
}
else if (!is_uploaded_file($_FILES['upload']["tmp_name"]))
{
$message = "Что-то пошло не так. Попытайтесь загрузить файл ещё раз.";
}
else{
$name =transliterate(delete_format($_FILES['upload']['name'])).'.'.getex($_FILES['upload']['name']);

$path_img="../../../../polz/".date(Y)."/".date(m);
$path_img_fulls="polz/".date(Y)."/".date(m);
$temp=$_FILES['upload']['tmp_name'];
if (file_exists($path_img)==false)
mkdir("$path_img",0777,true);
move_uploaded_file($temp,$path_img.'/'.$name);
$full_path = 'https://'.$_SERVER['SERVER_NAME'].'/'.$path_img_fulls.'/'.$name;
$size=@getimagesize($path_img.'/'.$name);
if($size[0]<10 OR $size[1]<10){
unlink($path_img.'/'.$name);
$message = "Файл не является допустимым изображением";
$full_path="";
}
}
$callback = $_REQUEST['CKEditorFuncNum'];
echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'.$callback.'", "'.$full_path.'", "'.$message.'" );</script>';
}
?>
