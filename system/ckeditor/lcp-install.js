function lcp_ckeditor_textedit(param){
  var ids=param['id'];
  var zapr=param['zapr'];
  var value=param['value'];

  if (typeof zapr==='undefined'){
    zapr='get';
  }

  if (zapr=='set'){
    return CKEDITOR.instances[ids].setData(value);
  } else {
    return CKEDITOR.instances[ids].getData();
  }
}
