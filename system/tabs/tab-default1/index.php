<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_tab_default',
  'type' => 'system',
  'group' => 'Вкладки',
  'inner' => array(
    'description' => 'Вкладки, используемые в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/tabs/tab-default1/tab-default.css'
    ),
    'js' => array(
      '0' => '/components/system/tabs/tab-default1/tab-default.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'tab_field_title_arr',
    '2' => 'tab_field_class_arr',
    '3' => 'tab_field_click_arr',
    '4' => 'tab_field_show_arr',
    '5' => 'tab_field_hidden_arr',
    '6' => 'active',
    '7' => 'class_show_hidden'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Массив наименований вкладок',
    '2' => 'Массив классов для каждой вкладки (class)',
    '3' => 'Массив событий, возникающих при клике левой кнопкой мыши, для вкладок (onclick)',
    '5' => 'Массив классов (array) для каждой вкладки (указывать через array), которые нужно отображать при выбранной вкладке (н-р, array(array("first_tabs"),array("two_tabs"),array("free_tabs")))',
    '4' => 'Массив классов (array) для каждой вкладки (указывать через array), которые нужно скрывать  при выбранной вкладке (н-р, array(array("two_tabs","free_tabs"),array("first_tabs","free_tabs"),array("first_tabs","two_tabs")))',
    '6' => 'Активная вкладка. Определяется по классу, указанному в tab_field_class_arr',
    '7' => 'Класс, предназначенный для отображения или скрытия объектов, панелей и пр. Задается для tab_field_show_arr и tab_field_hidden_arr. Этот класс должен именно скрывать объекты. Если класс не задан - используется style display: block, none'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_tab_default($param=''){
  $ids=$param['id'];
  $tab_field_title_arr=$param['tab_field_title_arr'];
  $tab_field_class_arr=$param['tab_field_class_arr'];
  $tab_field_click_arr=$param['tab_field_click_arr'];
  $tab_field_show_arr=$param['tab_field_show_arr'];
  $tab_field_hidden_arr=$param['tab_field_hidden_arr'];
  $active=$param['active'];
  $class_show_hidden=$param['class_show_hidden'];
  $ids=(!empty($ids))?' id="'.$ids.'" ':'';

  $html='';
  $html.='<div class="tab_default" '.$ids.'>';
  $html.='<div class="tab_default_menu">';
  $html.='<div class="tab_default_ul">';
  for ($i=0; $i < count($tab_field_title_arr); $i++) {
    $obj_arr_show_str=implode(',.',$tab_field_show_arr[$i]);
    $obj_arr_show_str=(!empty($obj_arr_show_str))?'.'.$obj_arr_show_str:'';
    if (empty($class_show_hidden)){
      $obj_arr_show_str=(!empty($obj_arr_show_str))?'$(\''.$obj_arr_show_str.'\').css(\'display\',\'block\');':'';
    } else {
      $obj_arr_show_str=(!empty($obj_arr_show_str))?'$(\''.$obj_arr_show_str.'\').removeClass(\''.$class_show_hidden.'\');':'';
    }
    $obj_arr_hidden_str=implode(',.',$tab_field_hidden_arr[$i]);
    $obj_arr_hidden_str=(!empty($obj_arr_hidden_str))?'.'.$obj_arr_hidden_str:'';
    if (empty($class_show_hidden)){
      $obj_arr_hidden_str=(!empty($obj_arr_hidden_str))?'$(\''.$obj_arr_hidden_str.'\').css(\'display\',\'none\');':'';
    } else {
      $obj_arr_hidden_str=(!empty($obj_arr_hidden_str))?'$(\''.$obj_arr_hidden_str.'\').addClass(\''.$class_show_hidden.'\');':'';
    }

    $html.='<div class="tab_default_li '.$tab_field_class_arr[$i].(($tab_field_class_arr[$i]==$active)?' tab_default_li_active ':'').'" onclick="tab_click_lcp(this);'.$obj_arr_show_str.$obj_arr_hidden_str.$tab_field_click_arr[$i].'">'.$tab_field_title_arr[$i].'</div>'; // tab_default_li_active
  }
  $html.='</div>';
  $html.='</div>';
  $html.='</div>';
  return $html;
}
?>
