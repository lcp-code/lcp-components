<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_toggle_default',
  'type' => 'system',
  'group' => 'Галочки/переключатели',
  'inner' => array(
    'description' => 'Переключатель (тумблер), используемый в панели администратора по-умолчанию. Позволяет включать или отключать различные опции.',
    'css' => array(
      '0' => '/components/system/checkeds/toggle-default1/toggle-default1.css'
    ),
    'js' => array(
      '0' => '/components/system/checkeds/toggle-default1/toggle-default1.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'checked',
    '2' => 'title',
    '3' => 'class',
    '4' => 'onclick',
    '5' => 'data-id'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => 'false',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => '<div>true - поставить галочку</div><div>false - убрать галочку (по умолч.)</div>',
    '2' => 'Текст для переключателя',
    '3' => 'Класс для изменения стилей (тег class). Также может применяться для использования групповых действий (включения-выключения сразу нескольких тумблеров).',
    '4' => 'Событие, возникающее при нажатии левой кнопкой мыши (тег onclick)',
    '5' => 'Значение id из БД'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
    '0' => 'checked'
  ),
  'set_value_js' => array(
    '0' => 'checked_change'
  ),
  'set_description_js' => array(
    '0' => '<div>checked_change - если галочка есть, то исчезнет, и наоборот.</div><div>true - установить галочку.</div><div>false - убрать галочку.</div><div>has_checked - узнать текущее состояние компонента (включена ли сейчас галочка)</div>'
  ),
  'get_js' => array(
    '0' => 'boolean'
  ),
  'get_description_js' => array(
    '0' => 'Возвращает true или false, если передан параметр has_checked. В других случаях ничего не выводит.'
  )
);
}
?>




<?php
function lcp_toggle_default($param=''){
  $id_checked=$param['id'];
  $checked=$param['checked'];
  $title=$param['title'];
  $onclick=$param['onclick'];
  $class=$param['class'];
  $data_id=$param['data-id'];
  $default_id='';
  $default_checked='false';
  $default_title='';
  $default_onclick='';
  $default_class='';
  $default_data_id='';
  $id_checked=(empty($id_checked))?$default_id:$id_checked;
  $checked=(empty($checked))?$default_checked:$checked;
  $onclick=(empty($onclick))?$default_onclick:$onclick;
  $class=(empty($class))?$default_class:$class;
  $data_id=(empty($data_id))?$default_data_id:$data_id;
  $title=(empty($title))?$default_title:$title;
  $title=(!empty($title))?' <div id="toggle_lab_'.$id_checked.'" class="toggle_default_label">'.$title.'</div> ':'';
  $id_checked=(!empty($id_checked))?' id="'.$id_checked.'" ':'';
  $data_id=(!empty($data_id))?' data-id="'.$data_id.'" ':'';
  $check_html=($checked=='true')?'<div class="toggle_default"></div>':'<div class="notoggle_default"></div>';
  $parent_check_html=($checked=='true')?' toggle_default_div_right ':'toggle_default_div_left';

  $html='';
  $html='<span class="toggle_default_all '.$class.'" onclick="var param=[];param[\'checked\']=\'checked_change\';lcp_toggle_default(this,param);'.$onclick.'" '.$id_checked.$data_id.'><div class="toggle_default_div '.$parent_check_html.'">'.$check_html.'</div>'.$title.'</span>';
  // $html.='<div class="checked_default_div"><input type="checkbox" '.$id_checked.' class="checkbox_default">'.$title.'</div>';
  return $html;
}
?>
