function lcp_toggle_default(obj,param){
  var checked=param['checked'];
  if (checked=='checked_change'){
    checked=($(obj).children('.toggle_default_div').hasClass('toggle_default_div_right')===true)?'false':'true';
  }
  if (checked=='true'){
    $(obj).children('.toggle_default_div').removeClass('toggle_default_div_left');
    $(obj).children('.toggle_default_div').addClass('toggle_default_div_right');
    $(obj).children('.toggle_default_div').children('div').removeClass('notoggle_default');
    $(obj).children('.toggle_default_div').children('div').addClass('toggle_default');
  } else
  if (checked=='false'){
    $(obj).children('.toggle_default_div').removeClass('toggle_default_div_right');
    $(obj).children('.toggle_default_div').addClass('toggle_default_div_left');
    $(obj).children('.toggle_default_div').children('div').removeClass('toggle_default');
    $(obj).children('.toggle_default_div').children('div').addClass('notoggle_default');
  } else
  if (checked=='has_checked'){
    return ($(obj).children('.toggle_default_div').hasClass('toggle_default_div_right')===true)?'true':'false';
  }
}
