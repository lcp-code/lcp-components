<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_main_checked_default',
  'type' => 'system',
  'group' => 'Галочки/переключатели',
  'inner' => array(
    'description' => 'Главный чекбокс (или "флажок", или "галочка"), используемый в панели администратора по-умолчанию. Отмечает галкой не только себя, но и другие элементы. Собран из компонента "lcp_checked_default". Тег ID обязателен!',
    'css' => array(
    ),
    'js' => array(
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'checked',
    '2' => 'title',
    '3' => 'class',
    '4' => 'onclick',
    '5' => 'selected_class'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => 'false',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => '<div>true - поставить галочку</div><div>false - убрать галочку (по умолч.)</div>',
    '2' => 'Текст для чекбокса',
    '3' => 'Класс для изменения стилей (тег class). Также может применяться для использования групповых действий (включения-выключения сразу нескольких чекбоксов).',
    '4' => 'Событие, возникающее при нажатии левой кнопкой мыши (тег onclick)',
    '5' => 'Класс, по которому стоит определять, какие галочки включать или выключать (в этом случае тег id обязателен!)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
// $css_install_arr[]='/components/system/checkeds/main-checked-default1/index.css';
// $js_install_arr[]='/components/system/checkeds/main-checked-default1/index.js';
?>




<?php
function lcp_main_checked_default($param=''){
  $id_checked=$param['id'];
  $checked=$param['checked'];
  $title=$param['title'];
  $onclick=$param['onclick'];
  $class=$param['class'];
  $selected_class=$param['selected_class'];
  $default_id='';
  $default_checked='false';
  $default_title='';
  $default_onclick='';
  $default_class='';
  $default_selected_class='';
  $id_checked=(empty($id_checked))?$default_id:$id_checked;
  $checked=(empty($checked))?$default_checked:$checked;
  $onclick=(empty($onclick))?$default_onclick:$onclick;
  $selected_class=(empty($selected_class))?$default_selected_class:$selected_class;
  $class=(empty($class))?$default_class:$class;
  $title=(empty($title))?$default_title:$title;
  // $title=(!empty($title))?' <div id="check_lab_'.$id_checked.'" class="checked_default_label">'.$title.'</div> ':'';
  // $check_html=($checked=='true')?'<i class="fa fa-check checked_default" aria-hidden="true"></i>':'<i class="fa fa-check nochecked_default" aria-hidden="true"></i>';

  $html='';
  $html=lcp_checked_default(array('id'=>$id_checked,'checked'=>$checked,'title'=>$title,'class'=>$class,'onclick'=>$onclick.';var param1=[];var param2=[];param1[\'checked\']=\'has_checked\';param2[\'checked\']=lcp_checked_default($(\'#'.$id_checked.'\'),param1)==\'true\'?\'false\':\'true\';lcp_checked_default($(\'.'.$selected_class.'\'),param2);'));
  // $html.='<div class="checked_default_div"><input type="checkbox" '.$id_checked.' class="checkbox_default">'.$title.'</div>';
  return $html;
}
?>
