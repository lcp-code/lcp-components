function lcp_checked_default(obj,param){
  var checked=param['checked'];
  if (checked=='checked_change'){
    checked=($(obj).children('.checked_default_div').children('.fa').hasClass('checked_default')===true)?'false':'true';
  }
  if (checked=='true'){
    $(obj).children('.checked_default_div').children('.fa').removeClass('nochecked_default');
    $(obj).children('.checked_default_div').children('.fa').addClass('checked_default');
  } else
  if (checked=='false'){
    $(obj).children('.checked_default_div').children('.fa').removeClass('checked_default');
    $(obj).children('.checked_default_div').children('.fa').addClass('nochecked_default');
  } else
  if (checked=='has_checked'){
    return ($(obj).children('.checked_default_div').children('.fa').hasClass('checked_default')===true)?'true':'false';
  }
}
