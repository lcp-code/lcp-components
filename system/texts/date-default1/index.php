<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_date_default',
  'type' => 'system',
  'group' => 'Текстовое поле',
  'inner' => array(
    'description' => 'Текстовое поле с датой на JQuery, используемое в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/texts/date-default1/date-default1.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'placeholder',
    '3' => 'class',
    '4' => 'onchange',
    '5' => 'onmouseover',
    '6' => 'onmouseout',
    '7' => 'onkeypress'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Текст внутри поля',
    '2' => 'Подсказка, отображаемая внутри поля (тег placeholder)',
    '3' => 'Класс для изменения стилей (тег class)',
    '4' => 'Событие, возникающее при изменении текста в поле (тег onchange)',
    '5' => 'Событие, возникающее при наведении указателя мыши на объект (тег onmouseover)',
    '6' => 'Событие, возникающее, когда указатель мыши уходит с объекта (тег onmouseout)',
    '7' => 'Событие, возникающее, при нажатии клавиши на клавиатуре (тег onkeypress)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
$css_install_arr[]='/components/system/texts/date-default1/date-default1.css';
// $js_install_arr[]='/components/system/html/html-block-drop/html-block-drop.js';
?>




<?php
function lcp_date_default($param=''){
  $param['id']=isset($param['id'])?$param['id']:'datepicker';
  $param['class'].=' date_default ';

  $html='';
  $html=components('lcp_text_default',$param);
  $html.='<script type="text/javascript">';
  $html.='$(function(){';
  $html.='$(\'#'.$param['id'].'\').datepicker({
    dateFormat: "dd-mm-yy",
    timeFormat:  "HH:mm:ss",
    monthNames: [\'Январь\', \'Февраль\', \'Март\', \'Апрель\',
        \'Май\', \'Июнь\', \'Июль\', \'Август\', \'Сентябрь\',
        \'Октябрь\', \'Ноябрь\', \'Декабрь\'],
         dayNamesMin: [\'Вс\',\'Пн\',\'Вт\',\'Ср\',\'Чт\',\'Пт\',\'Сб\'],
    firstDay: 1,
     onSelect : function(dateText, inst){
      if (dateText!=\'\'){
         $(\'#'.$param['id'].'\').parent(\'.text_default_block\').children(\'.text_default_label\').addClass(\'text_default_label_focused\');
      }
    }
  });';//minDate: new Date(),
  $html.='});';
  $html.='</script>';
  return $html;
}
?>
