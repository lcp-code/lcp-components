function phone_mask(e,id_obj,code){
  obj=document.getElementById(id_obj);
  var s,s1,s2,i,selstart;
  var keyser=(typeof e.key!=='undefined')?e.key:String.fromCharCode(e.keyCode);
  s=obj.value;
  s1='';
  s2='';
  i=obj.selectionStart;
  if (code=='+7'){
    selstart=4;
  } else {
    selstart=1;
  }
  if ((i>=18)&&(e.keyCode!= 8)){
    e.keyCode=0;
    return false;
  } else
  if (e.keyCode == 13){
    e.keyCode=0;
    return false;
    // передать фокус на другой компонент
  } else
  if (keyser in [0,1,2,3,4,5,6,7,8,9]){
    if (obj.selectionStart<selstart){
      obj.selectionStart=selstart;
      i=selstart;
    }
    while ((s[obj.selectionStart]=='(')||(s[obj.selectionStart]==')')||(s[obj.selectionStart]==' ')||(s[obj.selectionStart]=='+')||(s[obj.selectionStart]=='-')){
      obj.selectionStart=obj.selectionStart+1;
      i=obj.selectionStart;
    }
    if ((s[obj.selectionStart]=='_') || (s[obj.selectionStart] in [0,1,2,3,4,5,6,7,8,9])){
      s1 = s.substring(0,obj.selectionStart);
      s2 = s.substring(obj.selectionStart+1);
      s=s1+keyser+s2;
      obj.value=s;
      obj.setSelectionRange(0,0);
      obj.selectionStart=i+1;
      return false;
    }
  } else
  if (e.keyCode== 8){
    if (obj.selectionStart<(selstart+1)){
      obj.selectionStart=selstart;
      i=selstart;
      e.keyCode=0;
      return false;
    }
    while ((s[obj.selectionStart-1]=='(')||(s[obj.selectionStart-1]==')')||(s[obj.selectionStart-1]==' ')||(s[obj.selectionStart-1]=='+')||(s[obj.selectionStart-1]=='-')||(s[obj.selectionStart-1]=='_')){
      obj.selectionStart=obj.selectionStart-1;
      i=obj.selectionStart;
    }
    s1 = s.substring(0,obj.selectionStart-1);
    s2 = s.substring(obj.selectionStart);
    s=s1+'_'+s2;
    obj.value=s;
    obj.setSelectionRange(0,0);
    obj.selectionStart=i-1;
    return false;
  } else {
    return false;
  }
}

function set_phone_mask(id_obj,code){
  if (document.getElementById(id_obj).value==''){
    if (code=='+7'){
      document.getElementById(id_obj).value='+7 (___) ___-__-__';
      document.getElementById(id_obj).setSelectionRange(0,0);
      document.getElementById(id_obj).selectionStart=4;
    } else {
      document.getElementById(id_obj).value='+_ (___) ___-__-__';
      document.getElementById(id_obj).setSelectionRange(0,0);
      document.getElementById(id_obj).selectionStart=1;
    }
  }
}

function clear_phone_mask(id_obj,code){
  if ((document.getElementById(id_obj).value=='+7 (___) ___-__-__')||(document.getElementById(id_obj).value=='+_ (___) ___-__-__')){
    document.getElementById(id_obj).value='';
  }
}
