<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_phone_default2',
  'type' => 'system',
  'group' => 'Текстовое поле',
  'inner' => array(
    'description' => 'Текстовое поле, предназначенное для ввода мобильных телефонов без маски, используемое в панели администратора по-умолчанию. Основано на компоненте lcp_text_default.',
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'placeholder',
    '3' => 'class',
    '4' => 'onclick',
    '5' => 'onchange',
    '6' => 'onmouseover',
    '7' => 'onmouseout',
    '8' => 'onkeydown',
    '9' => 'onkeyup',
    '10' => 'onkeypress',
    '11' => 'onfocus',
    '12' => 'onblur',
    '13' => 'type',
    '14' => 'disabled',
    '15' => 'width',
    '16' => 'title',
    '17' => 'autocomplete',
    '18' => 'label',
    '19' => 'label_color',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '',
    '11' => '',
    '12' => '',
    '13' => 'text',
    '14' => '',
    '15' => '100%',
    '16' => '',
    '17' => '',
    '18' => '',
    '19' => '',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Текст внутри поля',
    '2' => 'Подсказка, отображаемая внутри поля (тег placeholder)',
    '3' => 'Класс для изменения стилей (тег class)',
    '4' => 'Событие, возникающее при нажатии мышкой на поле (тег onclick)',
    '5' => 'Событие, возникающее при изменении текста в поле (тег onchange)',
    '6' => 'Событие, возникающее при наведении указателя мыши на объект (тег onmouseover)',
    '7' => 'Событие, возникающее, когда указатель мыши уходит с объекта (тег onmouseout)',
    '8' => 'Событие, возникающее при нажатии клавиши (тег onkeydown)',
    '9' => 'Событие, возникающее при отпускании клавиши (тег onkeyup)',
    '10' => 'Событие, возникающее при печатании (тег onkeypress)',
    '11' => 'Событие, возникающее при наведении фокуса на текстовое поле (тег onfocus)',
    '12' => 'Событие, возникающее при исчезании фокуса из текстового поля (тег onblur)',
    '13' => 'Тип текстового поля, например (text, number, password, email, phone и др.)',
    '14' => 'Активность текстового поля (true - заморозить объект; в других случаях объект активен)',
    '15' => 'Ширина текстового поля',
    '16' => 'Всплывающая подсказка',
    '17' => 'Если off - не запоминать значения, и не отображать во всплывающих подсказках',
    '18' => 'Чем-то похоже на placeholder, но больше подходит для наименования текстового поля',
    '19' => 'Цвет Label, при активном фокусе на текстовое поле',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_phone_default2 extends lcp_text_default implements lcp_components_build{

    function __construct($param=array()){
      $param['class']=$param['class'].' lcp_phone_default2 ';
      $param['type']=(!empty($param['type']))?$param['type']:'tel';
      $param['maxlength']=(!empty($param['maxlength']))?$param['maxlength']:'10';
      $param['onkeydown']='return phone_default2_only_number(event);'.$param['onkeydown'];
      $param['onfocus']=$param['onfocus'].'phone_default2_onfocus(\''.$param['id'].'\');';
      $param['onblur']=$param['onblur'].'phone_default2_onblur(\''.$param['id'].'\');';
      parent::__construct($param);
    }

    function __build_after_component($html){
      $html=str_replace('<div class="text_default_block">','<div class="text_default_block"><div class="phone_code">+7</div>',$html);
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_phone_default2_init($param=array()){
    $lcp_obj=new lcp_phone_default2($param);
    return $lcp_obj->html();
  }
}
?>
