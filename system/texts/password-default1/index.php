<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_password_default',
  'type' => 'system',
  'group' => 'Текстовое поле',
  'inner' => array(
    'description' => 'Текстовое поле, предназначенное для ввода паролей, используемое в панели администратора по-умолчанию. Основано на компоненте lcp_text_default.',
    'css' => array(),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'placeholder',
    '3' => 'onchange',
    '4' => 'onmouseover',
    '5' => 'onmouseout'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Текст внутри поля',
    '2' => 'Подсказка, отображаемая внутри поля (тег placeholder)',
    '3' => 'Событие, возникающее при изменении текста в поле (тег onchange)',
    '4' => 'Событие, возникающее при наведении указателя мыши на объект (тег onmouseover)',
    '5' => 'Событие, возникающее, когда указатель мыши уходит с объекта (тег onmouseout)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_password_default($param=''){
  $html = components('lcp_text_default',$param);
  $html = str_replace('type="text"','type="password"',$html);
  return $html;
}
?>
