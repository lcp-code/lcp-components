<!-- <link rel="stylesheet" href="<?php echo $dir;?>components/lcp/system/texts/text-kladr/text-kladr.css?ver=<?php echo mt_rand();?>""> -->
<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_text_kladr',
  'type' => 'system',
  'group' => 'Текстовое поле',
  'inner' => array(
    'description' => 'Текстовое поле классификатора (КЛАДР), используемое в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/texts/text-default1/text-default1.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'placeholder',
    '3' => 'onchange',
    '4' => 'onmouseover',
    '5' => 'onmouseout',
    '6' => 'onkeypress'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => ''
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Текст внутри поля',
    '2' => 'Подсказка, отображаемая внутри поля (тег placeholder)',
    '3' => 'Событие, возникающее при изменении текста в поле (тег onchange)',
    '4' => 'Событие, возникающее при наведении указателя мыши на объект (тег onmouseover)',
    '5' => 'Событие, возникающее, когда указатель мыши уходит с объекта (тег onmouseout)',
    '6' => 'Событие, возникающее, при нажатии клавиши на клавиатуре (тег onkeypress)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_text_kladr($param=''){
  $ids=$param['id'];
  $value=$param['value'];
  $placeholder=$param['placeholder'];
  $onchange=$param['onchange'];
  $onmouseover=$param['onmouseover'];
  $onmouseout=$param['onmouseout'];
  $onkeypress=$param['onkeypress'];
  $kladr_butt_onclick=$param['kladr_butt_onclick'];
  $default='';
  $default_kladr_butt_onclick='kladr_start();';
  $value=(empty($value))?$default:$value;
  $kladr_butt_onclick=(empty($kladr_butt_onclick))?$default_kladr_butt_onclick:$kladr_butt_onclick;
  $ids=(!empty($ids))?' id="'.$ids.'" ':'';
  $placeholder=(!empty($placeholder))?' placeholder="'.$placeholder.'" ':'';
  $onchange=(!empty($onchange))?' onchange="'.$onchange.'" ':'';
  $onmouseover=(!empty($onmouseover))?' onmouseover="'.$onmouseover.'" ':'';
  $onmouseout=(!empty($onmouseout))?' onmouseout="'.$onmouseout.'" ':'';
  $onkeypress=(!empty($onkeypress))?' onkeypress="'.$onkeypress.'" ':'';
  $kladr_butt_onclick=(!empty($kladr_butt_onclick))?' onclick="'.$kladr_butt_onclick.'" ':'';
  $html='';
  $html.='<div class="text_kladr_div">
  <input type="text" class="text_kladr" '.$ids.$placeholder.$onchange.$onmouseover.$onmouseout.$onkeypress.' value="'.$value.'">
  <div class="text_kladr_div_right" title="Открыть классификатор (КЛАДР)" '.$kladr_butt_onclick.'><i class="fa fa-file-text-o" aria-hidden="true"></i></div>
  </div>';
  return $html;
}
?>
