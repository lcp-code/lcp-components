<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_seo_text',
  'type' => 'system',
  'group' => 'Текстовое поле',
  'inner' => array(
    'description' => 'SEO поля, используемые в панели администратора по-умолчанию. Сюда входят title, description и keywords',
    'css' => array(
      '0' => '/components/system/texts/seo/seo.css'
    ),
    'js' => array(
      '0' => '/components/system/texts/seo/seo.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'id_title',
    '2' => 'id_description',
    '3' => 'id_keywords',
    '4' => 'title',
    '5' => 'description',
    '6' => 'keywords',
    '7' => 'show_limit',
    '8' => 'max_title_limit',
    '9' => 'max_description_limit',
  ),
  'set_value_php' => array(
    '0' => 'lcp_seo_text',
    '1' => 'lcp_seo_text_title',
    '2' => 'lcp_seo_text_description',
    '3' => 'lcp_seo_text_keywords',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => 'true',
    '8' => '60',
    '9' => '142',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Идентификатор поля title',
    '2' => 'Идентификатор поля description',
    '3' => 'Идентификатор поля keywords',
    '4' => 'Значение поля title',
    '5' => 'Значение поля description',
    '6' => 'Значение поля keywords',
    '7' => 'Отображать подсказку, сколько осталось символов',
    '8' => 'Максимальное количество символов заголовка',
    '9' => 'Максимальное количество символов описания',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
    '0' => ''
  ),
  'set_value_js' => array(
    '0' => 'id'
  ),
  'set_description_js' => array(
    '0' => '<div>get_seo(id); - получить массив с сео данными, где id - идентификатор общего блока (по-умолчанию lcp_seo_text)</div>'
  ),
  'get_js' => array(
    '0'=>'array'
  ),
  'get_description_js' => array(
    '0'=>'<div>Возвращает массив с данными по title, description и keywords.<br>Например:</div><div>var seo_arr=get_seo();</div><div>var title=seo_arr["title"];</div><div>var description=seo_arr["description"];</div><div>var keywords=seo_arr["keywords"];</div>'
  )
);
}
?>




<?php
function lcp_seo_text($param=''){
  $ids=$param['id'];
  $id_title=$param['id_title'];
  $id_description=$param['id_description'];
  $id_keywords=$param['id_keywords'];
  $title=$param['title'];
  $description=$param['description'];
  $keywords=$param['keywords'];
  $show_limit=$param['show_limit'];
  $max_title_limit=$param['max_title_limit'];
  $max_description_limit=$param['max_description_limit'];
  $default_ids='lcp_seo_text';
  $default_id_title='lcp_seo_text_title';
  $default_id_description='lcp_seo_text_description';
  $default_id_keywords='lcp_seo_text_keywords';
  $default_title='';
  $default_description='';
  $default_keywords='';
  $default_show_limit='true';
  $default_max_title_limit='60';
  $default_max_description_limit='142';
  $ids=(!empty($ids))?$ids:$default_ids;
  $ids=(!empty($ids))?' id="'.$ids.'" ':'';
  $id_title=(!empty($id_title))?$id_title:$default_id_title;
  $id_description=(!empty($id_description))?$id_description:$default_id_description;
  $id_keywords=(!empty($id_keywords))?$id_keywords:$default_id_keywords;
  $title=(!empty($title))?$title:$default_title;
  $description=(!empty($description))?$description:$default_description;
  $keywords=(!empty($keywords))?$keywords:$default_keywords;
  $show_limit=(!empty($show_limit))?$show_limit:$default_show_limit;
  $max_title_limit=(!empty($max_title_limit))?$max_title_limit:$default_max_title_limit;
  $max_description_limit=(!empty($max_description_limit))?$max_description_limit:$default_max_description_limit;
  // $style=(!empty($width))?' style="width:'.$width.'" ':'';

  $count_title=mb_strlen($title,'UTF-8');
  $count_description=mb_strlen($description,'UTF-8');
  $title_error_class='show_limit_error';
  $description_error_class='show_limit_error';
  $title_error=($count_title>$max_title_limit)?$title_error_class:'';
  $description_error=($count_description>$max_description_limit)?$description_error_class:'';


  $html='';
  $html.='<div class="lcp_seo_text"'.$ids.'>';

  // SEO-title
  $html.=components('lcp_html_block_default2',array(
    'title'=>'SEO title',
    'html'=>components('lcp_text_default',array(
      'id'=>$id_title,
      'class'=>$default_id_title,
      'value'=>$title,
      'placeholder'=>'SEO Заголовок',
      'onkeyup'=>(($show_limit=='true')?'lcp_seo_text_change({
        ids:\''.$id_title.'\',
        max_limit:\''.$max_title_limit.'\',
        show_limit_class:\'show_limit_title_err_'.$id_title.'\',
        error_class:\''.$title_error_class.'\',
        count_class:\'lcp_seo_text_title_count_'.$id_title.'\'
        });':''),
    ))
  ));
  if ($show_limit=='true'){
    $html.='<div class="show_limit show_limit_title_err_'.$id_title.' '.$title_error.'"><span class="lcp_seo_text_title_count_'.$id_title.'">'.$count_title.'</span> из '.$max_title_limit.' рекомендуемая длина title</div>';
    $html.='<div class="lcp_seo_text_delimer"></div>';
  }

  // SEO-description
  $html.=components('lcp_html_block_default2',array(
    'title'=>'SEO description',
    'html'=>components('lcp_memo_default',array(
      'id'=>$id_description,
      'class'=>$default_id_description,
      'text'=>$description,
      'placeholder'=>'SEO Описание',
      'onkeyup'=>(($show_limit=='true')?'lcp_seo_text_change({
        ids:\''.$id_description.'\',
        max_limit:\''.$max_description_limit.'\',
        show_limit_class:\'show_limit_description_err_'.$id_description.'\',
        error_class:\''.$description_error_class.'\',
        count_class:\'lcp_seo_text_description_count_'.$id_description.'\'
        });':''),
    ))
  ));
  if ($show_limit=='true'){
    $html.='<div class="show_limit show_limit_description_err_'.$id_description.' '.$description_error.'"><span class="lcp_seo_text_description_count_'.$id_description.'">'.$count_description.'</span> из '.$max_description_limit.' рекомендуемая длина description</div>';
    $html.='<div class="lcp_seo_text_delimer"></div>';
  }

  // SEO-keywords
  $html.=components('lcp_html_block_default2',array(
    'title'=>'SEO keywords',
    'html'=>components('lcp_text_default',array(
      'id'=>$id_keywords,
      'class'=>$default_id_keywords,
      'value'=>$keywords,
      'placeholder'=>'SEO Ключевые слова, через запятую'
    ))
  ));

  $html.='</div>';



  return $html;
}
?>
