function lcp_seo_text_change(param){
  var ids=param['ids'];
  var max_limit=param['max_limit'];
  var show_limit_class=param['show_limit_class'];
  var error_class=param['error_class'];
  var count_class=param['count_class'];

  $('.'+count_class).html($('#'+ids).val().length);
  if ($('#'+ids).val().length<=max_limit){
    $('.'+show_limit_class).removeClass(error_class);
  } else {
    if ($('.'+show_limit_class).hasClass(error_class)===false){
      $('.'+show_limit_class).addClass(error_class);
    }
  }
}

function get_seo(ids='lcp_seo_text'){
  var result_arr={};
  result_arr['title']=$('#'+ids+'>.block_default2>.block_default_html2>.lcp_seo_text_title').val();
  result_arr['description']=$('#'+ids+'>.block_default2>.block_default_html2>.lcp_seo_text_description').val();
  result_arr['keywords']=$('#'+ids+'>.block_default2>.block_default_html2>.lcp_seo_text_keywords').val();
  return result_arr;
}
