<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_text_default',
  'type' => 'system',
  'group' => 'Текстовое поле',
  'inner' => array(
    'description' => 'Текстовое поле, используемое в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/texts/text-default1/text-default1.css'
    ),
    'js' => array(
      '0' => '/components/system/texts/text-default1/text-default1.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'placeholder',
    '3' => 'class',
    '4' => 'onclick',
    '5' => 'onchange',
    '6' => 'onmouseover',
    '7' => 'onmouseout',
    '8' => 'onkeydown',
    '9' => 'onkeyup',
    '10' => 'onkeypress',
    '11' => 'onfocus',
    '12' => 'onblur',
    '13' => 'type',
    '14' => 'disabled',
    '15' => 'width',
    '16' => 'title',
    '17' => 'autocomplete',
    '18' => 'label',
    '19' => 'label_color',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '',
    '11' => '',
    '12' => '',
    '13' => 'text',
    '14' => '',
    '15' => '100%',
    '16' => '',
    '17' => '',
    '18' => '',
    '19' => '',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Текст внутри поля',
    '2' => 'Подсказка, отображаемая внутри поля (тег placeholder)',
    '3' => 'Класс для изменения стилей (тег class)',
    '4' => 'Событие, возникающее при нажатии мышкой на поле (тег onclick)',
    '5' => 'Событие, возникающее при изменении текста в поле (тег onchange)',
    '6' => 'Событие, возникающее при наведении указателя мыши на объект (тег onmouseover)',
    '7' => 'Событие, возникающее, когда указатель мыши уходит с объекта (тег onmouseout)',
    '8' => 'Событие, возникающее при нажатии клавиши (тег onkeydown)',
    '9' => 'Событие, возникающее при отпускании клавиши (тег onkeyup)',
    '10' => 'Событие, возникающее при печатании (тег onkeypress)',
    '11' => 'Событие, возникающее при наведении фокуса на текстовое поле (тег onfocus)',
    '12' => 'Событие, возникающее при исчезании фокуса из текстового поля (тег onblur)',
    '13' => 'Тип текстового поля, например (text, number, password, email, phone и др.)',
    '14' => 'Активность текстового поля (true - заморозить объект; в других случаях объект активен)',
    '15' => 'Ширина текстового поля',
    '16' => 'Всплывающая подсказка',
    '17' => 'Если off - не запоминать значения, и не отображать во всплывающих подсказках',
    '18' => 'Чем-то похоже на placeholder, но больше подходит для наименования текстового поля',
    '19' => 'Цвет Label, при активном фокусе на текстовое поле',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
$css_install_arr[]='/components/system/texts/text-default1/text-default1.css';
$js_install_arr[]= '/components/system/texts/text-default1/text-default1.js';






if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_text_default extends lcp_components implements lcp_components_build{
    public $value;
    public $type='text';
    public $label;
    public $label_color;
    public $width='100%';
    public $autocomplete;

    function __construct($param=array()){
      $this->value=isset($param['value'])?$param['value']:$this->value;
      $this->type=isset($param['type'])?$param['type']:$this->type;
      $this->label=isset($param['label'])?$param['label']:$this->label;
      $this->label_color=isset($param['label_color'])?$param['label_color']:$this->label_color;
      $this->width=isset($param['width'])?$param['width']:$this->width;
      $this->autocomplete=isset($param['autocomplete'])?$param['autocomplete']:$this->autocomplete;
      parent::__construct($param);
    }

    function __build_component(){
      $html='';
      $this->class='text_default '.$this->class;
      $this->style='width:'.$this->width.';'.$this->style;
      $this->autocomplete=isset($this->autocomplete)?' autocomplete="'.$this->autocomplete.'" ':'';
      if (!empty($this->label)){
        $this->id=(!empty($this->id))?$this->id:'text_default1';
        $this->onfocus=(!empty($this->onfocus))?$this->onfocus.'text_default_focused(\''.$this->id.'\',\'text_default_label_'.$this->id.'\',\''.$this->label_color.'\');':'text_default_focused(\''.$this->id.'\',\'text_default_label_'.$this->id.'\',\''.$this->label_color.'\');';
        $this->onblur=(!empty($this->onblur))?$this->onblur.'text_default_blured(\''.$this->id.'\',\'text_default_label_'.$this->id.'\',\''.$this->label_color.'\');':'text_default_blured(\''.$this->id.'\',\'text_default_label_'.$this->id.'\',\''.$this->label_color.'\');';
        $this->class='text_default_padd '.$this->class;
        $this->label_class=((!empty($this->value))||($this->value==='0'))?'text_default_label_focused':'';
        $this->label='<span class="text_default_label text_default_label_'.$this->id.' '.$this->label_class.'" onclick="document.getElementById(\''.$this->id.'\').focus();">'.$this->label.'</span>';
      }
      $attr_str=$this->__get_attributes_html();
      $html.=(!empty($this->label))?'<div class="text_default_block">':'';
      $html.='<input type="'.$this->type.'" '.$this->autocomplete.' '.$attr_str.' value="'.$this->value.'">'.$this->label;
      $html.=(!empty($this->label))?'</div>':'';
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_text_default_init($param=array()){
    $lcp_obj=new lcp_text_default($param);
    return $lcp_obj->html();
  }
}
?>
