<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_memo_default',
  'type' => 'system',
  'group' => 'Текстовое поле',
  'inner' => array(
    'description' => 'Текстовое область (заметка), используемая в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/texts/memo-default1/memo-default.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'value',
    '2' => 'text',
    '3' => 'placeholder',
    '4' => 'width',
    '5' => 'height',
    '6' => 'onchange',
    '7' => 'onmouseover',
    '8' => 'onmouseout',
    '9' => 'onkeydown',
    '10' => 'onkeyup',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '100%',
    '5' => '120px',
    '6' => '',
    '7' => '',
    '8' => '',
    '9' => '',
    '10' => '',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Текст внутри поля',
    '2' => 'Текст внутри поля (устарело)',
    '3' => 'Подсказка, отображаемая внутри поля (тег placeholder)',
    '4' => 'Ширина текстового поля (тег width)',
    '5' => 'Высота текстового поля (тег height)',
    '6' => 'Событие, возникающее при изменении текста в поле (тег onchange)',
    '7' => 'Событие, возникающее при наведении указателя мыши на объект (тег onmouseover)',
    '8' => 'Событие, возникающее, когда указатель мыши уходит с объекта (тег onmouseout)',
    '9' => 'Событие, возникающее, при нажатии клавиши (тег onkeydown)',
    '10' => 'Событие, возникающее, при отпускании клавиши (тег onkeyup)',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
$css_install_arr[]='/components/system/texts/memo-default1/memo-default.css';
// $js_install_arr[]='/components/system/texts/memo-default1/memo-default.js';






if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_memo_default extends lcp_components implements lcp_components_build{
    public $value;
    public $width='100%';
    public $height='120px';
    public $autocomplete;

    function __construct($param=array()){
      $this->value=isset($param['value'])?$param['value']:$this->value;
      $this->height=isset($param['height'])?$param['height']:$this->height;
      $param['autocomplete']=isset($param['autocomplete'])?$param['autocomplete']:$this->autocomplete;
      $param['width']=isset($param['width'])?$param['width']:$this->width;
      parent::__construct($param);
    }
    
    function __build_component(){
      $html='';
      $this->class='memo_default '.$this->class;
      $this->style='width:'.$this->width.';'.$this->style;
      $this->style='height:'.$this->height.';'.$this->style;
      // print($this->style.'<br>');

      $this->autocomplete=isset($this->autocomplete)?' autocomplete="'.$this->autocomplete.'" ':'';
      $attr_str=$this->__get_attributes_html();
      $html='';
      $html.='<textarea '.$this->autocomplete.' '.$attr_str.'>'.$this->value.'</textarea>';
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_memo_default($param=array()){
    $lcp_obj=new lcp_memo_default($param);
    return $lcp_obj->html();
  }
}
?>