<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_navigation_default2',
  'type' => 'system',
  'group' => 'Навигация',
  'inner' => array(
    'description' => 'Навигация или номера страниц, используемая в панели администратора по-умолчанию.',
    'css' => array(
      '0' => '/components/system/navigations/navigation-default2/navigation-default2.css'
    ),
    'js' => array(
    )
  ),
  'set_name_php' => array(
    '0' => 'count',
    '1' => 'value',
    '2' => 'limit',
    '3' => 'result_getarrayvaluedbtbl',
    '4' => 'ajax_function'
  ),
  'set_value_php' => array(
    '0' => '1',
    '1' => '1',
    '2' => '10',
    '3' => '',
    '4' => ''
  ),
  'set_description_php' => array(
    '0' => 'Количество записей (всего на всех страницах)',
    '1' => 'Номер текущей страницы',
    '2' => 'Отображаемое количество записей на одной странице',
    '3' => 'Результат выполнения функции getarrayvaluedbtbl (можно использовать только этот параметр, вместо остальных)',
    '4' => 'JS функция. Заменяет обычный href переход на ajax технологию. В качестве параметра, должен быть массив с ключом "pageid", отвечающий за изменение текущей страницы.'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
  ),
  'set_value_js' => array(
  ),
  'set_description_js' => array(
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}
$css_install_arr[]='/components/system/navigations/navigation-default2/navigation-default2.css';
// $js_install_arr[]='/components/system/navigations/navigation-default2/navigation-default2.js';
?>




<?php
function lcp_navigation_default2($param=''){
  if (empty($_GET['pageid'])){
    $_GET['pageid']='1';
  }
  $count=$param['count'];
  $value=$param['value'];
  $limit=$param['limit'];
  $ajax_function=$param['ajax_function'];
  $result_getarrayvaluedbtbl=$param['result_getarrayvaluedbtbl'];
  $default_count='1';
  $default_value='1';
  $default_limit='10';
  $default_ajax_function='';
  $default_result_getarrayvaluedbtbl='';

  $result_getarrayvaluedbtbl=(empty($result_getarrayvaluedbtbl))?$default_result_getarrayvaluedbtbl:$result_getarrayvaluedbtbl;

  if (!empty($result_getarrayvaluedbtbl)){
    $count=(empty($count))?$result_getarrayvaluedbtbl['count_all']:$count;
    $value=(empty($value))?$_GET['pageid']:$value;
    $limit=(empty($limit))?$result_getarrayvaluedbtbl['limit']:$limit;
    $ajax_function=(empty($ajax_function))?$result_getarrayvaluedbtbl['ajax_function']:$ajax_function;
  }

  $count=(empty($count))?$default_count:$count;
  $value=(empty($value))?$default_value:$value;
  $limit=(empty($limit))?$default_limit:$limit;
  $ajax_function=(empty($ajax_function))?$default_ajax_function:$ajax_function;
  $count_button=6; // максимальное количество отображаемых кнопок
  $count_page=ceil($count/$limit); // количество страниц

  $pagecount=$limit; // Количество отображаемых объявлений на одной странице
  $pageid=$value-1; // Параметры количества объявлений на сайте


  $html='';
  $pagestart=$pageid*$pagecount;
  $kolpagenav=0;
  for ($uio=$pageid+1;(($uio>$pageid-3)&&($uio>0));$uio--){
  $kolpagenav++;
  $pagess[$kolpagenav]=$uio;
  }


  $pagenavcount=$count; // всего отображаемых на странице страниц
  $pagenavcount=ceil($pagenavcount/$pagecount);
  for ($uio=$pageid+2;(($kolpagenav<7)&&($uio<=$pagenavcount));$uio++){
  $kolpagenav++;
  $pagess[$kolpagenav]=$uio;
  }
  sort($pagess);


  $get_param=$_GET;
  $html='';
  if (count($pagess)>1){
    $html .= '<div class="ttotalpage2">';
    if (1!=$pagess[0]){
      $get_param['pageid']='1';
      $locat=(!empty($ajax_function))?' onclick="'.$ajax_function.'({pageid:\'1\'});" ':' href="?'.http_build_query($get_param).'" ';
      $html .= '<a '.$locat.' rel="first" title="Перейти к 1 странице"> &laquo; </a>';
    }
    for ($uio=0;$uio<=count($pagess)-1;$uio++){
      $get_param['pageid']=$pagess[$uio];
      $locat=(!empty($ajax_function))?' onclick="'.$ajax_function.'({pageid:\''.$pagess[$uio].'\'});" ':' href="?'.http_build_query($get_param).'" ';
      $html .= '<a '.$locat.' title="'.$pagess[$uio].' страница"';
      if ($pagess[$uio]==$pageid+1){
        $html .= ' class="ttotalpagesel"';
      }
      $html .= '>'.$pagess[$uio].'</a>';
    }
    if ($pagenavcount!=$pagess[count($pagess)-1]){
      $get_param['pageid']=$pagenavcount;
      $locat=(!empty($ajax_function))?' onclick="'.$ajax_function.'({pageid:\''.$pagenavcount.'\'});" ':' href="?'.http_build_query($get_param).'" ';
      $html .= '<a '.$locat.' rel="last" title="Перейти к '.$pagenavcount.' странице"> &raquo; </a>';
    }
    $html .= '</div>';
  }
  return $html;
}
?>
