<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_table_default2',
  'type' => 'system',
  'group' => 'Таблица',
  'inner' => array(
    'description' => 'Таблица, используемая в панели администратора по-умолчанию. В качетве тегов используются table tr и td.',
    'css' => array(
      '0' => '/components/system/tables/table-default2/table-default2.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'thead',
    '1' => 'class',
    '2' => 'tbody',
    '3' => 'tfoot',
    '4' => 'width_columns',
    '5' => 'text_align_col_thead',
    '6' => 'text_align_col_tbody',
    '7' => 'text_align_col_tfoot',
    '8' => 'text_valign_col_thead',
    '9' => 'text_valign_col_tbody',
    '10' => 'text_valign_col_tfoot',
    '11' => 'column_check_bool',
    '12' => 'sql',
    '13' => 'thead_fields',
    '14' => 'add_pole',
    '15' => 'sql_show_title_bool',
    '16' => 'sortible',
    '17' => 'limit',
    '18' => 'sortible_function_js',
    '19' => 'alt_body'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => 'left',
    '6' => 'left',
    '7' => 'left',
    '8' => 'middle',
    '9' => 'middle',
    '10' => 'middle',
    '11' => 'false',
    '12' => '',
    '13' => '',
    '14' => '',
    '15' => 'false',
    '16' => 'false',
    '17' => '',
    '18' => '',
    '19' => 'Объектов не найдено!'
  ),
  'set_description_php' => array(
    '0' => 'Массив колонок с заголовками для тега thead',
    '1' => 'Класс для изменения стилей (тег class)',
    '2' => 'Массив колонок с основным текстом ячеек для тега tbody',
    '3' => 'Массив колонок с текстом ячеек для тега tfoot',
    '4' => 'Массив со значениями ширины колонок',
    '5' => 'Массив с выравниванием текста по горизонтали для тега thead',
    '6' => 'Массив с выравниванием текста по горизонтали для тега tbody',
    '7' => 'Массив с выравниванием текста по горизонтали для тега tfoot',
    '8' => 'Массив с выравниванием текста по вертикали для тега thead',
    '9' => 'Массив с выравниванием текста по вертикали для тега tbody',
    '10' => 'Массив с выравниванием текста по вертикали для тега tfoot',
    '11' => '<div>true - отображать галочки в первой колонке</div><div>false - скрыть галочки в первой колонке</div>',
    '12' => 'SQL-запрос для отображения содержимого ячеек для тега tbody',
    '13' => 'Массив полей для связи заголовка thead с именем столбца в БД MySQL. Для указания нового поля из add_pole, используйте: <b>[add_pole="0"]</b> , где 0 - номер записи массива из add_pole.',
    '14' => '<div>Массив содержимого нового поля (шаблон для всех строк). Для указания значения поля БД, используйте: [add_pole_column="column_db"]. Указанное поле в select запросе должно быть обязательно. Например, для id: [add_pole_column="id"].</div>
             <div>Значение массива может быть в виде массива (который сливается в одну строку).</div>
             <div>Пример выбора значения через case: array("0"=>array("tbl_funct"=>"case", "value"=>"[add_pole_column="active"]", "get"=>array("1"=>"Активная","2"=>"Скрытая","[else]"=>"В остальных случаях")))</div>
             <div>Пример использования компонента (необходимо доработать): array("0"=>array("tbl_funct"=>"components", "name"=>"name_components_function", "params"=>array(parametrs_arr)))</div>',
    '15' => '<div>true - отображать заголовки SQL-запроса</div><div>false - не отображать заголовки SQL-запроса</div>',
    '16' => '<div>true - сортировать строки внутри таблицы, перетаскивая их вручную</div><div>false - не сортировать строки в таблице</div>',
    '17' => 'Ограничение на количество отображаемых строк (ограничения в sql-запросах игнорируются)',
    '18' => 'Функция для сохранения нового порядка расположения элементов в таблице. Обязательно должно быть включено поле "sortible". В качестве параметра указать "param". Возвращает массив id_arr (param={id_arr:id_arr}). Пример функции: numm_save(param);',
    '19' => 'Если таблица пуста - отобразить альтернативный текст'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_table_default2($param=""){
  $id_components=getcelldb('id','id','components','name=\'lcp_table_default2\'');
  $class=$param['class'];
  $thead=$param['thead'];
  $tbody=$param['tbody'];
  $tfoot=$param['tfoot'];
  $sql=$param['sql'];
  $width_columns=$param['width_columns'];
  $text_align_col_thead=$param['text_align_col_thead'];
  $text_align_col_tbody=$param['text_align_col_tbody'];
  $text_align_col_tfoot=$param['text_align_col_tfoot'];
  $text_valign_col_thead=$param['text_valign_col_thead'];
  $text_valign_col_tbody=$param['text_valign_col_tbody'];
  $text_valign_col_tfoot=$param['text_valign_col_tfoot'];
  $thead_fields=$param['thead_fields'];
  $add_pole=$param['add_pole'];
  $sql_show_title_bool=$param['sql_show_title_bool'];
  $column_check_bool=$param['column_check_bool'];
  $sortible=$param['sortible'];
  $sortible_function_js=$param['sortible_function_js'];
  $alt_body=$param['alt_body'];
  $default_class=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'class\'');
  $default_width_columns=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'width_columnss\'');
  $default_text_align_col_thead=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_align_col_thead\'');
  $default_text_align_col_tbody=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_align_col_tbody\'');
  $default_text_align_col_tfoot=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_align_col_tfoot\'');
  $default_text_valign_col_thead=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_valign_col_thead\'');
  $default_text_valign_col_tbody=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_valign_col_tbody\'');
  $default_text_valign_col_tfoot=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_valign_col_tfoot\'');
  $default_sql_show_title_bool=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'sql_show_title_bool\'');
  $default_column_check_bool=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'column_check_bool\'');
  $default_sortible=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'sortible\'');
  $default_sortible_function_js=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'sortible_function_js\'');
  $default_alt_body=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'alt_body\'');
  // $width_columns=(!empty($width_columns))?$width_columns:array('0' => $default_width_columns);
  // $text_align_columns=(!empty($text_align_columns))?$text_align_columns:array('0' => $default_text_align_columns);
  $class=(!empty($class))?$class:$default_class;
  $sql_show_title_bool=(!empty($sql_show_title_bool))?$sql_show_title_bool:$default_sql_show_title_bool;
  $column_check_bool=(!empty($column_check_bool))?$column_check_bool:$default_column_check_bool;
  $sortible=(!empty($sortible))?$sortible:$default_sortible;
  $sortible_function_js=(!empty($sortible_function_js))?$sortible_function_js:$default_sortible_function_js;
  $alt_body=(!empty($alt_body))?$alt_body:$default_alt_body;

  $html='';
  $html_head='';
  $html_body='';
  $html_foot='';

  if ((empty($html_body))&&(!empty($sql))){
    $res=sqldb($sql);
    // if (empty($res)){
    //   return 'Объектов не найдено!';
    // }
    if (!empty($res)){
      $tbl_field=array_keys($res)or die (exit(var_dump($sql)));
      $tbody=array();
      for ($i1=0; $i1 < count($tbl_field); $i1++) {
        for ($i2=0; $i2 < count($res[$tbl_field[$i1]]); $i2++) {
          $tbody[$i2][$i1]=$res[$tbl_field[$i1]][$i2];
        }
      }
    }
    if ((empty($thead))&&($sql_show_title_bool=='true')){
      $thead=$tbl_field;
    }
    if ((!empty($thead))&&(!empty($thead_fields))){
      $tbl_field_ind=array();
      for ($i=0; $i < count($thead_fields); $i++) {
        $tbl_field_ind[$thead_fields[$i]]=$i;
      }

      unset($tbody);
      $max_row=0;
      for ($i1=0; $i1 < count($tbl_field); $i1++) {
        if ($max_row<count($res[$tbl_field[$i1]])){
          $max_row=count($res[$tbl_field[$i1]]);
        }
      }

      $phpVersion = substr(phpversion(), 0, 3)*1;

      $tbody=array();
      for ($i1=0; $i1 < count($thead_fields); $i1++) {
        if ((strpos($thead_fields[$i1],'[add_pole="'))!==false){
          for ($i2=0; $i2 < $max_row; $i2++) {
            $num_add_col=explode('"',$thead_fields[$i1]);
            $num_add_col=$num_add_col[1];
            $res_add_pole=$add_pole[$num_add_col];
            $serialz='false';
            $res_add_pole=($phpVersion >= 5.4)?json_encode($res_add_pole, JSON_UNESCAPED_UNICODE):preg_replace('/\\\\u([a-f0-9]{4})/e', "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($res_add_pole));
              $kkk=0;
              while ((strpos($res_add_pole,'[add_pole_column=',0))!==false){
                $pos_start=strpos($res_add_pole,'[add_pole_column=',0)+18;
                $pos_end=strpos($res_add_pole,']',$pos_start);
                if ($pos_end===false){
                  break;
                } else {
                  $pos_end--;
                }
                $pos_end=($pos_end===false)?strlen($res_add_pole):$pos_end;
                $res_column=substr($res_add_pole,$pos_start,$pos_end-$pos_start);
                if ($res_column[0]=='"'){
                  $res_column=substr($res_column,1);
                  $res_column=trim($res_column);
                }
                if ($res_column[strlen($res_column)-1]=='\\'){
                  $res_column=substr($res_column,0,strlen($res_column)-1);
                  $res_column=trim($res_column);
                }

                if ($add_pole[$num_add_col][0]['tbl_funct']=='eval_block'){
                  $res[$res_column][$i2]=str_replace('"','<lcpquot>',$res[$res_column][$i2]);
                }

                $res_add_pole=str_replace('[add_pole_column=\''.$res_column.'\']',$res[$res_column][$i2],$res_add_pole);
                $res_add_pole=str_replace('[add_pole_column="'.$res_column.'"]',$res[$res_column][$i2],$res_add_pole);
                $res_add_pole=str_replace('[add_pole_column=\\\''.$res_column.'\\\']',$res[$res_column][$i2],$res_add_pole);
                $res_add_pole=str_replace('[add_pole_column=\"'.$res_column.'\"]',$res[$res_column][$i2],$res_add_pole);
                $kkk++;
              }
              $res_add_pole=json_decode($res_add_pole,true);
              if ($add_pole[$num_add_col][0]['tbl_funct']=='eval_block'){
                $res_add_pole[0]['php']=str_replace('<lcpquot>','"',$res_add_pole[0]['php']);
              }

                if (is_array($res_add_pole)===true){
                  // если массив - выполнить необходимые функции
                  // echo 'vupoln';













                  $result_pole_arr_str='';
                  for ($i3=0; $i3 < count($res_add_pole); $i3++) {
                    if ($res_add_pole[$i3]['tbl_funct']=='case'){

                      $res_pole_arr_str=$res_add_pole[$i3]['value'];
                      if (array_key_exists($res_pole_arr_str,$res_add_pole[$i3]['get'])===false){
                        $result_pole_arr_str_fin=$res_add_pole[$i3]['get']['[else]'];
                      } else {
                        $result_pole_arr_str_fin=$res_add_pole[$i3]['get'][$res_pole_arr_str];
                      }
                    } else
                    if ($res_add_pole[$i3]['tbl_funct']=='eval_block'){
                      $res_pole_arr_str=$res_add_pole[$i3]['php'];
                      $block_arr=json_decode($res['block'][$i2], true);
                      $result_eval='';
                      eval($res_pole_arr_str);
                      $result_pole_arr_str_fin=$result_eval;
                      // echo $result_pole_arr_str_fin;
                      // exit();
                      // if (array_key_exists($res_pole_arr_str,$res_add_pole[$i3]['get'])===false){
                      //   $result_pole_arr_str_fin=$res_add_pole[$i3]['get']['[else]'];
                      // } else {
                      //   $result_pole_arr_str_fin=$res_add_pole[$i3]['get'][$res_pole_arr_str];
                      // }
                    }
                    // if ($res_add_pole[$i3]['tbl_funct']=='components'){
                    // доработать для отображения компонентов
                    //   $res_pole_params=$res_add_pole[$i3]['params'];
                    //
                    //   $res_add_pole_field=array_keys($res_pole_params)or die (exit(var_dump('error in tables show components')));
                    //   $result_pole_arr_str='';
                    //   // for ($i4=0; $i4 < count($res_add_pole_field); $i4++) {
                    //   //   $res_pole_arr_str=$res_pole_params[$res_add_pole_field[$i4]];
                    //   // }
                    //   $result_pole_arr_str.=components($res_add_pole[$i3]['name'],$res_pole_params);
                    //   // print_r($res_pole_params);
                    //   // exit();
                    // }
                    $result_pole_arr_str.=$result_pole_arr_str_fin;
                  }
                  $res_add_pole=$result_pole_arr_str;
                  // echo $res_add_pole;
                  // exit();























                  // echo 'vupoln';
                }

            $tbody[$i2][$i1]=$res_add_pole;
          }
        } else {
        for ($i2=0; $i2 < count($res[$thead_fields[$i1]]); $i2++) {
          $tbody[$i2][$i1]=$res[$thead_fields[$i1]][$i2];
        }
      }
      }
    }
  }

  if (empty($tbody)){
    $sortible='false';
  }

  if ((!empty($thead))&&($sql_show_title_bool=='true')){
    $html_head.='<tr class="table_default2_tr table_default2_thead">';
    if ($column_check_bool=='true'){
      $html_head.='<td class="table_default2_td" style="vertical-align:middle;">';
      $html_head.=lcp_main_checked_default(array('id'=>'table_default2_checked_all','checked'=>'false','selected_class'=>'table_default2_checkeder'));
      $html_head.='</td>';
    }
    for ($i=0; $i < count($thead); $i++) {
      $css_line='';
      if (!empty($width_columns[$i])){
        $css_line.='width:'.$width_columns[$i].';min-width:'.$width_columns[$i].';';
      } else {
        if (!empty($default_width_columns)){
          $css_line.='width:'.$default_width_columns.';min-width:'.$default_width_columns.';';
        }
      }
      if (!empty($text_align_col_thead[$i])){
        $css_line.='text-align:'.$text_align_col_thead[$i].';';
      } else {
        if (!empty($default_text_align_col_thead)){
          $css_line.='text-align:'.$default_text_align_col_thead.';';
        }
      }
      if (!empty($text_valign_col_thead[$i])){
        $css_line.='vertical-align:'.$text_valign_col_thead[$i].';';
      } else {
        if (!empty($default_text_valign_col_thead)){
          $css_line.='vertical-align:'.$default_text_valign_col_thead.';';
        }
      }
      $css_line=(empty($css_line))?'':'style="'.$css_line.'"';
      $html_head.='<td class="table_default2_td" '.$css_line.'>';
      $html_head.=$thead[$i];
      $html_head.='</td>';
    }
    $html_head.='</tr>';
  }
  if (!empty($tbody)){
    for ($i_row=0; $i_row < count($tbody); $i_row++) {
      $html_body.='<tr class="table_default2_tr table_default2_tbody" data-id="'.$res['id'][$i_row].'">';
      if ($column_check_bool=='true'){
        // print_r($tbody);
        // exit();
        $html_body.='<td class="table_default2_td" style="vertical-align:middle;">';
        $html_body.=lcp_checked_default(array('id'=>'table_default2_checkeder_'.$res['id'][$i_row],'data-id'=>$res['id'][$i_row],'class'=>'table_default2_checkeder'));
        $html_body.='</td>';
      }
      for ($i_col=0; $i_col < count($tbody[$i_row]); $i_col++) {
        $css_line='';
        if (!empty($width_columns[$i_col])){
          $css_line.='width:'.$width_columns[$i_col].';min-width:'.$width_columns[$i_col].';';
        } else {
          if (!empty($default_width_columns)){
            $css_line.='width:'.$default_width_columns.';min-width:'.$default_width_columns.';';
          }
        }
        if (!empty($text_align_col_tbody[$i_col])){
          $css_line.='text-align:'.$text_align_col_tbody[$i_col].';';
        } else {
          if (!empty($default_text_align_col_tbody)){
            $css_line.='text-align:'.$default_text_align_col_tbody.';';
          }
        }
        if (!empty($text_valign_col_tbody[$i_col])){
          $css_line.='vertical-align:'.$text_valign_col_tbody[$i_col].';';
        } else {
          if (!empty($default_text_valign_col_tbody)){
            $css_line.='vertical-align:'.$default_text_valign_col_tbody.';';
          }
        }
        $css_line=(empty($css_line))?'':'style="'.$css_line.'"';
        $html_body.='<td class="table_default2_td" '.$css_line.'>';
        $html_body.=$tbody[$i_row][$i_col];
        $html_body.='</td>';
      }
      $html_body.='</tr>';
    }
  } else {
    $html_body.='<div class="object_not">'.$alt_body.'</div>';
  }
  if (!empty($tfoot)){
    $html_foot.='<tr class="table_default2_tr table_default2_tfoot">';
    $html_foot.='<td class="table_default2_td">';
    $html_foot.=$tfoot;
    $html_foot.='</td>';
    $html_foot.='</tr>';
  }


  $html.='<div class="table_default_block2">';
  $html.='<table class="table_default2 '.$class.'" border="0" cellspacing="0" cellpadding="0">';
  $html.='<thead class="table_default2_head_div">';
  $html.=$html_head;
  $html.='</thead>';
  $html.='<tbody class="table_default2_body_div">';
  $html.=$html_body;
  $html.='</tbody>';
  $html.=$html_foot;
  $html.='</table>';
  $html.='</div>';

  if ($sortible=='true'){
    $html.='<script type="text/javascript">';
    $html.='$(function(){';
    $html.='$(\'.table_default2>.table_default2_body_div\').sortable();';
    $html.='$(\'.table_default2>.table_default2_body_div\').disableSelection();';
    $html.='});';
    $html.='$(\'.table_default2>.table_default2_body_div\').on(\'sortstop\', function(event,ui){';
    $html.='var id_arr={};';
    $html.='for (var i = 0; i < $(\'.table_default2_body_div>.table_default2_tr\').length; i++) {';
    $html.='var obj=$(\'.table_default2_body_div>.table_default2_tr\').eq(i);';
    $html.='id_arr[i]=$(obj).attr(\'data-id\');';
    $html.='}';
    $html.='var param={id_arr:id_arr};';
    $html.=$sortible_function_js;
    $html.='});';
    $html.='</script>';
  }

  return $html;
}
?>
