<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_table_default',
  'type' => 'system',
  'group' => 'Таблица',
  'inner' => array(
    'description' => 'Таблица, используемая в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/tables/table-default1/table-default1.css'
    ),
    'js' => array()
  ),
  'set_name_php' => array(
    '0' => 'thead',
    '1' => 'tbody',
    '2' => 'tfoot',
    '3' => 'width_columns',
    '4' => 'text_align_col_thead',
    '5' => 'text_align_col_tbody',
    '6' => 'text_align_col_tfoot',
    '7' => 'text_valign_col_thead',
    '8' => 'text_valign_col_tbody',
    '9' => 'text_valign_col_tfoot',
    '10' => 'column_check_bool',
    '11' => 'sql',
    '12' => 'thead_fields',
    '13' => 'add_pole',
    '14' => 'sql_show_title_bool',
    '15' => 'sortible',
    '16' => 'limit'
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => 'left',
    '5' => 'left',
    '6' => 'left',
    '7' => 'middle',
    '8' => 'middle',
    '9' => 'middle',
    '10' => 'false',
    '11' => '',
    '12' => '',
    '13' => '',
    '14' => 'false',
    '15' => 'false',
    '16' => ''
  ),
  'set_description_php' => array(
    '0' => 'Массив колонок с заголовками для тега thead',
    '1' => 'Массив колонок с основным текстом ячеек для тега tbody',
    '2' => 'Массив колонок с текстом ячеек для тега tfoot',
    '3' => 'Массив со значениями ширины колонок',
    '4' => 'Массив с выравниванием текста по горизонтали для тега thead',
    '5' => 'Массив с выравниванием текста по горизонтали для тега tbody',
    '6' => 'Массив с выравниванием текста по горизонтали для тега tfoot',
    '7' => 'Массив с выравниванием текста по вертикали для тега thead',
    '8' => 'Массив с выравниванием текста по вертикали для тега tbody',
    '9' => 'Массив с выравниванием текста по вертикали для тега tfoot',
    '10' => '<div>true - отображать галочки в первой колонке</div><div>false - скрыть галочки в первой колонке</div>',
    '11' => 'SQL-запрос для отображения содержимого ячеек для тега tbody',
    '12' => 'Массив полей для связи заголовка thead с именем столбца в БД MySQL. Для указания нового поля из add_pole, используйте: <b>[add_pole="0"]</b> , где 0 - номер записи массива из add_pole.',
    '13' => '<div>Массив содержимого нового поля (шаблон для всех строк). Для указания значения поля БД, используйте: [add_pole_column="column_db"]. Указанное поле в select запросе должно быть обязательно. Например, для id: [add_pole_column="id"].</div>
             <div>Значение массива может быть в виде массива (который сливается в одну строку).</div>
             <div>Пример выбора значения через case: array("0"=>array("tbl_funct"=>"case", "value"=>"[add_pole_column="active"]", "get"=>array("1"=>"Активная","2"=>"Скрытая")))</div>
             <div>Пример использования компонента: array("0"=>array("tbl_funct"=>"components", "name"=>"name_components_function", "params"=>array(parametrs_arr)))</div>',
    '14' => '<div>true - отображать заголовки SQL-запроса</div><div>false - не отображать заголовки SQL-запроса</div>',
    '15' => '<div>true - сортировать строки внутри таблицы, перетаскивая их вручную</div><div>false - не сортировать строки в таблице</div>',
    '16' => 'Ограничение на количество отображаемых строк (ограничения в sql-запросах игнорируются)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_table_default($param=""){
  $id_components=getcelldb('id','id','components','name=\'lcp_table_default\'');
  $thead=$param['thead'];
  $tbody=$param['tbody'];
  $tfoot=$param['tfoot'];
  $sql=$param['sql'];
  $width_columns=$param['width_columns'];
  $text_align_col_thead=$param['text_align_col_thead'];
  $text_align_col_tbody=$param['text_align_col_tbody'];
  $text_align_col_tfoot=$param['text_align_col_tfoot'];
  $text_valign_col_thead=$param['text_valign_col_thead'];
  $text_valign_col_tbody=$param['text_valign_col_tbody'];
  $text_valign_col_tfoot=$param['text_valign_col_tfoot'];
  $thead_fields=$param['thead_fields'];
  $add_pole=$param['add_pole'];
  $sql_show_title_bool=$param['sql_show_title_bool'];
  $column_check_bool=$param['column_check_bool'];
  $default_width_columns=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'width_columnss\'');
  $default_text_align_col_thead=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_align_col_thead\'');
  $default_text_align_col_tbody=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_align_col_tbody\'');
  $default_text_align_col_tfoot=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_align_col_tfoot\'');
  $default_text_valign_col_thead=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_valign_col_thead\'');
  $default_text_valign_col_tbody=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_valign_col_tbody\'');
  $default_text_valign_col_tfoot=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'text_valign_col_tfoot\'');
  $default_sql_show_title_bool=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'sql_show_title_bool\'');
  $default_column_check_bool=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\' AND components_param_name=\'column_check_bool\'');
  // $width_columns=(!empty($width_columns))?$width_columns:array('0' => $default_width_columns);
  // $text_align_columns=(!empty($text_align_columns))?$text_align_columns:array('0' => $default_text_align_columns);
  $sql_show_title_bool=(!empty($sql_show_title_bool))?$sql_show_title_bool:$default_sql_show_title_bool;
  $column_check_bool=(!empty($column_check_bool))?$column_check_bool:$default_column_check_bool;

  $html='';
  $html_head='';
  $html_body='';
  $html_foot='';

  if ((empty($html_body))&&(!empty($sql))){
    $res=sqldb($sql);
    if (empty($res)){
      return;
    }
    $tbl_field=array_keys($res)or die (exit(var_dump($sql)));
    $tbody=array();
    for ($i1=0; $i1 < count($tbl_field); $i1++) {
      for ($i2=0; $i2 < count($res[$tbl_field[$i1]]); $i2++) {
        $tbody[$i2][$i1]=$res[$tbl_field[$i1]][$i2];
      }
    }
    if ((empty($thead))&&($sql_show_title_bool=='true')){
      $thead=$tbl_field;
    }
    if ((!empty($thead))&&(!empty($thead_fields))){
      $tbl_field_ind=array();
      for ($i=0; $i < count($thead_fields); $i++) {
        $tbl_field_ind[$thead_fields[$i]]=$i;
      }

      unset($tbody);
      $max_row=0;
      for ($i1=0; $i1 < count($thead_fields); $i1++) {
        if ($max_row<count($res[$thead_fields[$i1]])){
          $max_row=count($res[$thead_fields[$i1]]);
        }
      }


      $tbody=array();
      for ($i1=0; $i1 < count($thead_fields); $i1++) {
        if ((strpos($thead_fields[$i1],'[add_pole="'))!==false){
          for ($i2=0; $i2 < $max_row; $i2++) {
            $num_add_col=explode('"',$thead_fields[$i1]);
            $num_add_col=$num_add_col[1];
            $res_add_pole=$add_pole[$num_add_col];
            if (is_array($res_add_pole)===true){
              // значение замены через массив
              $result_pole_arr_str='';
              for ($i3=0; $i3 < count($res_add_pole); $i3++) {
                if ($res_add_pole[$i3]['tbl_funct']=='case'){
                  $res_pole_arr_str=$res_add_pole[$i3]['value'];
                  $kkk=0;
                  while ((strpos($res_pole_arr_str,'[add_pole_column=',0))!==false){
                    $pos_start=strpos($res_pole_arr_str,'[add_pole_column=',0)+18;
                    $pos_end=strpos($res_pole_arr_str,']',$pos_start);
                    if ($pos_end===false){
                      break;
                    } else {
                      $pos_end--;
                    }
                    $pos_end=($pos_end===false)?strlen($res_pole_arr_str):$pos_end;
                    $res_column=substr($res_pole_arr_str,$pos_start,$pos_end-$pos_start);
                    $res_pole_arr_str=str_replace('[add_pole_column=\''.$res_column.'\']',$res[$res_column][$i2],$res_pole_arr_str);
                    $res_pole_arr_str=str_replace('[add_pole_column="'.$res_column.'"]',$res[$res_column][$i2],$res_pole_arr_str);
                    $kkk++;
                  }
                  $result_pole_arr_str_fin=$res_add_pole[$i3]['get'][$res_pole_arr_str];
                  $kkk=0;
                  while ((strpos($result_pole_arr_str_fin,'[add_pole_column=',0))!==false){
                    $pos_start=strpos($result_pole_arr_str_fin,'[add_pole_column=',0)+18;
                    $pos_end=strpos($result_pole_arr_str_fin,']',$pos_start);
                    if ($pos_end===false){
                      break;
                    } else {
                      $pos_end--;
                    }
                    $pos_end=($pos_end===false)?strlen($result_pole_arr_str_fin):$pos_end;
                    $res_column=substr($result_pole_arr_str_fin,$pos_start,$pos_end-$pos_start);
                    $result_pole_arr_str_fin=str_replace('[add_pole_column=\''.$res_column.'\']',$res[$res_column][$i2],$result_pole_arr_str_fin);
                    $result_pole_arr_str_fin=str_replace('[add_pole_column="'.$res_column.'"]',$res[$res_column][$i2],$result_pole_arr_str_fin);
                    $kkk++;
                  }
                  $result_pole_arr_str.=$result_pole_arr_str_fin;
                }
                if ($res_add_pole[$i3]['tbl_funct']=='components'){
                  $res_pole_params=$res_add_pole[$i3]['params'];

                  $res_add_pole_field=array_keys($res_pole_params)or die (exit(var_dump('error in tables show components')));
                  $result_pole_arr_str='';
                  for ($i4=0; $i4 < count($res_add_pole_field); $i4++) {
                    $res_pole_arr_str=$res_pole_params[$res_add_pole_field[$i4]];
                    if (is_array($res_pole_arr_str)===false){
                      $kkk=0;
                    while ((strpos($res_pole_arr_str,'[add_pole_column=',0))!==false){
                      $pos_start=strpos($res_pole_arr_str,'[add_pole_column=',0)+18;
                      $pos_end=strpos($res_pole_arr_str,']',$pos_start);
                      if ($pos_end===false){
                        break;
                      } else {
                        $pos_end--;
                      }
                      $pos_end=($pos_end===false)?strlen($res_pole_arr_str):$pos_end;
                      $res_column=substr($res_pole_arr_str,$pos_start,$pos_end-$pos_start);
                      $res_pole_arr_str=str_replace('[add_pole_column=\''.$res_column.'\']',$res[$res_column][$i2],$res_pole_arr_str);
                      $res_pole_arr_str=str_replace('[add_pole_column="'.$res_column.'"]',$res[$res_column][$i2],$res_pole_arr_str);
                      $kkk++;
                    }
                    $res_pole_params[$res_add_pole_field[$i4]]=$res_pole_arr_str;
                  } else {
                    $res_add_pole_field4=array_keys($res_pole_params[$res_add_pole_field[$i4]])or die (exit(var_dump('error in tables show components2')));
                    for ($i5=0; $i5 < count($res_add_pole_field4); $i5++) {
                      $res_pole_arr_str=$res_pole_params[$res_add_pole_field[$i4]][$res_add_pole_field4[$i5]];
                        $kkk=0;
                      while ((strpos($res_pole_arr_str,'[add_pole_column=',0))!==false){
                        $pos_start=strpos($res_pole_arr_str,'[add_pole_column=',0)+18;
                        $pos_end=strpos($res_pole_arr_str,']',$pos_start);
                        if ($pos_end===false){
                          break;
                        } else {
                          $pos_end--;
                        }
                        $pos_end=($pos_end===false)?strlen($res_pole_arr_str):$pos_end;
                        $res_column=substr($res_pole_arr_str,$pos_start,$pos_end-$pos_start);
                        $res_pole_arr_str=str_replace('[add_pole_column=\''.$res_column.'\']',$res[$res_column][$i2],$res_pole_arr_str);
                        $res_pole_arr_str=str_replace('[add_pole_column="'.$res_column.'"]',$res[$res_column][$i2],$res_pole_arr_str);
                        $kkk++;
                      }

                      $res_pole_params[$res_add_pole_field[$i4]][$res_add_pole_field4[$i5]]=$res_pole_arr_str;
                    }
                    // print_r($res_pole_params);
                    // exit();
                    // print_r($res_pole_params);
                    // exit();
                  }
                  }
                  $result_pole_arr_str.=components($res_add_pole[$i3]['name'],$res_pole_params);
                  // print_r($res_pole_params);
                  // exit();
                }
              }
              $res_add_pole=$result_pole_arr_str;
            } else {
              // значение замены через строку
              $kkk=0;
              while ((strpos($res_add_pole,'[add_pole_column=',0))!==false){
                $pos_start=strpos($res_add_pole,'[add_pole_column=',0)+18;
                $pos_end=strpos($res_add_pole,']',$pos_start);
                if ($pos_end===false){
                  break;
                } else {
                  $pos_end--;
                }
                $pos_end=($pos_end===false)?strlen($res_add_pole):$pos_end;
                $res_column=substr($res_add_pole,$pos_start,$pos_end-$pos_start);
                $res_add_pole=str_replace('[add_pole_column=\''.$res_column.'\']',$res[$res_column][$i2],$res_add_pole);
                $res_add_pole=str_replace('[add_pole_column="'.$res_column.'"]',$res[$res_column][$i2],$res_add_pole);
                $kkk++;
              }
            }
            $tbody[$i2][$i1]=$res_add_pole;
          }
        } else {
        for ($i2=0; $i2 < count($res[$thead_fields[$i1]]); $i2++) {
          $tbody[$i2][$i1]=$res[$thead_fields[$i1]][$i2];
        }
      }
      }
    }
  }

  if ((!empty($thead))&&($sql_show_title_bool=='true')){
    $html_head.='<div class="table_default_tr table_default_thead">';
    if ($column_check_bool=='true'){
      $html_head.='<div class="table_default_td" style="vertical-align:middle;">';
      $html_head.=lcp_main_checked_default(array('id'=>'table_default_checked_all','checked'=>'false','selected_class'=>'table_default_checkeder'));
      $html_head.='</div>';
    }
    for ($i=0; $i < count($thead); $i++) {
      $css_line='';
      if (!empty($width_columns[$i])){
        $css_line.='width:'.$width_columns[$i].';min-width:'.$width_columns[$i].';';
      } else {
        if (!empty($default_width_columns)){
          $css_line.='width:'.$default_width_columns.';min-width:'.$default_width_columns.';';
        }
      }
      if (!empty($text_align_col_thead[$i])){
        $css_line.='text-align:'.$text_align_col_thead[$i].';';
      } else {
        if (!empty($default_text_align_col_thead)){
          $css_line.='text-align:'.$default_text_align_col_thead.';';
        }
      }
      if (!empty($text_valign_col_thead[$i])){
        $css_line.='vertical-align:'.$text_valign_col_thead[$i].';';
      } else {
        if (!empty($default_text_valign_col_thead)){
          $css_line.='vertical-align:'.$default_text_valign_col_thead.';';
        }
      }
      $css_line=(empty($css_line))?'':'style="'.$css_line.'"';
      $html_head.='<div class="table_default_td" '.$css_line.'>';
      $html_head.=$thead[$i];
      $html_head.='</div>';
    }
    $html_head.='</div>';
  }
  if (!empty($tbody)){
    for ($i_row=0; $i_row < count($tbody); $i_row++) {
      $html_body.='<div class="table_default_tr table_default_tbody">';
      if ($column_check_bool=='true'){
        $html_body.='<div class="table_default_td" style="vertical-align:middle;">';
        $html_body.=lcp_checked_default(array('class'=>'table_default_checkeder'));
        $html_body.='</div>';
      }
      for ($i_col=0; $i_col < count($tbody[$i_row]); $i_col++) {
        $css_line='';
        if (!empty($width_columns[$i_col])){
          $css_line.='width:'.$width_columns[$i_col].';min-width:'.$width_columns[$i_col].';';
        } else {
          if (!empty($default_width_columns)){
            $css_line.='width:'.$default_width_columns.';min-width:'.$default_width_columns.';';
          }
        }
        if (!empty($text_align_col_tbody[$i_col])){
          $css_line.='text-align:'.$text_align_col_tbody[$i_col].';';
        } else {
          if (!empty($default_text_align_col_tbody)){
            $css_line.='text-align:'.$default_text_align_col_tbody.';';
          }
        }
        if (!empty($text_valign_col_tbody[$i_col])){
          $css_line.='vertical-align:'.$text_valign_col_tbody[$i_col].';';
        } else {
          if (!empty($default_text_valign_col_tbody)){
            $css_line.='vertical-align:'.$default_text_valign_col_tbody.';';
          }
        }
        $css_line=(empty($css_line))?'':'style="'.$css_line.'"';
        $html_body.='<div class="table_default_td" '.$css_line.'>';
        $html_body.=$tbody[$i_row][$i_col];
        $html_body.='</div>';
      }
      $html_body.='</div>';
    }
  }
  if (!empty($tfoot)){
    $html_foot.='<div class="table_default_tr table_default_tfoot">';
    $html_foot.='<div class="table_default_td">';
    $html_foot.=$tfoot;
    $html_foot.='</div>';
    $html_foot.='</div>';
  }


  $html.='<div class="table_default">';
  $html.=$html_head;
  $html.=$html_body;
  $html.=$html_foot;
  $html.='</div>';

  return $html;
}
?>
