function tegs_all_item_click(teg_id){
  $('.tegs_all>[data-id="'+teg_id+'"]').remove();
}

function add_item_teg(){
  var id_arr=[];
  id_arr['id']=[];
  id_arr['title']=[];
  k=0;
  // получение выбранных id и заголовка
  for (var i = 0; i < $('.teg_tbl_check').length; i++) {
    var obj=$('.teg_tbl_check').eq(i);
    if (lcp_checked_default(obj,{checked:'has_checked'})=='true'){
      id_arr['id'][k]=$(obj).attr('data-id');
      id_arr['title'][k]=$('#teg_title_'+id_arr['id'][k]).html();
      k++;
    }
  }
  // добавление в поле тегов
  for (var i = 0; i < id_arr['id'].length; i++) {
    $('.tegs_all').append('<div class="tegs_all_item" data-id="'+id_arr['id'][i]+'">'+id_arr['title'][i]+' <div class="tegs_all_delete" onclick="tegs_all_item_click(\''+id_arr['id'][i]+'\');">x</div></div>');
  }
}

function get_tegs_default(){
  var id_arr=[];
  // получение выбранных id
  for (var i = 0; i < $('.tegs_all_item').length; i++) {
    var obj=$('.tegs_all_item').eq(i);
    id_arr[i]=$(obj).attr('data-id');
  }
  return id_arr;
}

function get_tegs_default_str(){
  var str='';
  // получение выбранных id
  for (var i = 0; i < $('.tegs_all_item').length; i++) {
    var obj=$('.tegs_all_item').eq(i);
    str+=str==''?$(obj).attr('data-id'):','+$(obj).attr('data-id');
  }
  return str;
}
