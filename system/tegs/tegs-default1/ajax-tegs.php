<?php
if (($_LCP_AJAX!=="AJAX")||($_LCP_AJAX_TIME!=md5(getdater()))){header("HTTP/1.0 404 Not Found");exit();}





if ($_POST['action']=='get_table_tegs'){
  // получаем все родительские категории и проверяем наличие тегов
  $first_load =$_POST['first_load'];
  $category_id = $_POST['category_id'];
  if (empty($category_id)){
    $category_id='0';
  }
  $block_str=getcelldb('block','block','datas','id=\''.$category_id.'\'','','1');
  $_POST['pageid']=(!empty($_POST['pageid']))?$_POST['pageid']:'1';
  $block_arr=json_decode($block_str, true);
  if (count($block_arr['tegs'])>0){
    $ids_str=implode(',',$block_arr['tegs']);
  }
  while(!empty($category_id)){
    $category_id = getcelldb('childr','childr','datas','id=\''.$category_id.'\'','','1');
    if (!empty($category_id)){
      $block_str=getcelldb('block','block','datas','id=\''.$category_id.'\'','','1');
      $block_arr=json_decode($block_str, true);
      if (count($block_arr['tegs'])>0){
        $ids_str.=(!empty($ids_str))?','.implode(',',$block_arr['tegs']):implode(',',$block_arr['tegs']);
        $category_id='';
      }
    }
  }
  if (!empty($ids_str)){
    $not_id_arr=getarrayvaluedb(array('id'),'id','datas','status=\'12\' AND id not IN ('.$ids_str.')');
    $_POST['value_arr']=(!empty($_POST['value_arr']))?$_POST['value_arr'].','.implode(',',$not_id_arr['id']):implode(',',$not_id_arr['id']);
  }
  $html='';
  $not_id=(!empty($_POST['value_arr']))?' AND (id not IN ('.$_POST['value_arr'].'))':'';
  $html.='<div class="get_table_tegs">';

  if (empty($first_load)){
    $result_getarrayvaluedbtbl=getarrayvaluedbtbl(array('id','title_short'), 'id,title_short', 'datas', 'status=\'12\''.$not_id, 'BY numm', '30', $_POST['pageid']);
  }
  $html.=components('lcp_table_default',array(
    'sql'=>$result_getarrayvaluedbtbl['sql'],
    'sql_show_title_bool'=>'true',
    'thead'=>array(components('lcp_main_checked_default',array(
      'id' => 'tbl_main_check',
      'selected_class' => 'teg_tbl_check'
    )),'Название'),
    'thead_fields'=>array('[add_pole="0"]','[add_pole="1"]'),
    'add_pole'=>array(
      '0'=>components('lcp_checked_default',array(
        'id'=>'teg_check_[add_pole_column="id"]',
        'class' => 'teg_tbl_check',
        'data-id' => '[add_pole_column="id"]'
      )),
      '1'=>'<div style="cursor:pointer;" onclick="document.getElementById(\'teg_check_[add_pole_column="id"]\').click();" id="teg_title_[add_pole_column="id"]">[add_pole_column="title_short"]</div>'
    ),
    'width_columns'=>array('50px','100%')
  ));

  $html.='<div class="get_table_tegs_navigate">';
  $html.=components('lcp_navigation_default1',array(
    'value'=>$_POST['pageid'],
    'result_getarrayvaluedbtbl'=>$result_getarrayvaluedbtbl,
    'ajax_function'=>'get_table_tegs'
  ));
  $html.='</div>';

  $html.='</div>';
  $LCP_AJAX_RESULT=$html;
} else













if ($_POST['action']=='get_table_tegs_for_save'){

  $html='';

  // Название тега
  $html.=components('lcp_html_block_default3',array(
    'title'=>'Название тега:',
    'html'=>components('lcp_text_default',array(
      'id'=>'title_short_teg_new',
      'value'=>'',
      'placeholder'=>'Укажите короткое название тега'
    ))
  ));

  // Ссылка
  $html.=components('lcp_html_block_default3',array(
    'title'=>'Ссылка:',
    'html'=>components('lcp_text_default',array(
      'id'=>'link_teg_new',
      'value'=>'',
      'placeholder'=>'Укажите ссылку'
    ))
  ));

  // Заголовок (H1)
  $html.=components('lcp_html_block_default3',array(
    'title'=>'Заголовок (H1):',
    'html'=>components('lcp_text_default',array(
      'id'=>'title_teg_new',
      'value'=>'',
      'placeholder'=>'Укажите заголовок тега'
    ))
  ));

  // Текст страницы
  $html.=components('lcp_html_block_default3',array(
    'title'=>'Текст страницы:',
    'html'=>components('lcp_ckeditor_textedit',array(
      'id'=>'texts_teg_new',
      'text'=>'',
      'height'=>'700px'
    ))
  ));

  // Основное фото
  $html.=components('lcp_html_block_default3',array(
    'title'=>'Фото тега (желательно в формате .png):',
    'html'=>components('lcp_load_photo',array(
      'id'=>'img_field_main_teg_new',
      'max_files'=>'1',
    ))
  ));


  // Статус
  $html.=components('lcp_html_block_default3',array(
    'title'=>'Статус тега:',
    'html'=>components('dropdown_white',array(
      'id'=>'active_teg_new',
      'value'=>((empty($teg_active))?'1':$teg_active),
      'title_child_arr'=>array('Активная','Скрытая'),
      'value_child_arr'=>array('1','2'),
      'width'=>'100%'
    ))
  ));

  // SEO-title
  $html.=components('lcp_html_block_default3',array(
    'title'=>'SEO title',
    'html'=>components('lcp_text_default',array(
      'id'=>'seo_title_teg_new',
      'value'=>'',
      'placeholder'=>'SEO Заголовок'
    ))
  ));

  // SEO-description
  $html.=components('lcp_html_block_default3',array(
    'title'=>'SEO description',
    'html'=>components('lcp_memo_default',array(
      'id'=>'seo_desc_teg_new',
      'text'=>'',
      'placeholder'=>'SEO Описание'
    ))
  ));

  // SEO-keywords
  $html.=components('lcp_html_block_default3',array(
    'title'=>'SEO keywords',
    'html'=>components('lcp_text_default',array(
      'id'=>'seo_keyw_teg_new',
      'value'=>'',
      'placeholder'=>'SEO Ключевые слова, через запятую'
    ))
  ));

  // Отобразить HTML
  // $popup_content=components('lcp_block_one_line2',array(
  //   'tbody'=>array(
  //     '0'=>$html_left,
  //     '1'=>$html_right
  //   ),
  //   'width_columns'=>array('100%','350px'),
  //   'text_valign_col_tbody'=>array('top','top')
  // ));
  // echo components('lcp_html_block_default1',array(
  //   'title'=>' ',
  //   'html'=>$html
  // ));


  $html.=components('lcp_ajax',array(
    'function_name'=>'save_teg_new',
    'begin_html'=>'lcp_loading_default({loading_show:\'true\'});',
    'url'=>'/lcp-panel/content/tegs/ajax-teg.php',
    'data_js'=>'{
      action:\'create_teg\',
      s:\'12\',
      title_teg:$(\'#title_teg_new\').val(),
      title_short_teg:$(\'#title_short_teg_new\').val(),
      link_teg:$(\'#link_teg_new\').val(),
      texts_teg:lcp_ckeditor_textedit({id:\'texts_teg_new\',zapr:\'get\'}),
      // create_date:$(\'#create_date\').val(),
      // edit_date:$(\'#edit_date\').val(),
      active_teg:get_dropdown_default2_opt_value(\'active_teg_new\'),
      photo_main_arr:lcp_load_photo({id:\'img_field_main_teg_new\',get:\'get_for_save_photos\'}),
      seo_title_teg:$(\'#seo_title_teg_new\').val(),
      seo_desc_teg:$(\'#seo_desc_teg_new\').val(),
      seo_keyw_teg:$(\'#seo_keyw_teg_new\').val(),
      }',
      'lcp_success_true'=>'lcp_loading_default({loading_show:\'false\'});popup_new_tegs_close();popup_tegs();get_table_tegs();',
      'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});',
      'error'=>'lcp_loading_default({loading_show:\'false\'});'
  ));

    $popup_content_button1=components('lcp_button_default',array(
      'value'=>'Добавить тег',
      'onclick'=>'save_teg_new();'
    ));
    $html.=components('lcp_block_one_line',array(
      'tbody'=>array($popup_content_button1),
      'width_columns'=>array('100%'),
      'text_align_col_tbody'=>array('right')
    ));
  $LCP_AJAX_RESULT=$html;
} else












{
  $LCP_AJAX_RESULT='Ни одной функции не найдено!';
}

?>
