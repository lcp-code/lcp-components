<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_tegs_default',
  'type' => 'system',
  'group' => 'Теги',
  'inner' => array(
    'description' => 'Теги, используемые в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/tegs/tegs-default1/tegs-default.css'
    ),
    'js' => array(
      '0' => '/components/system/tegs/tegs-default1/tegs-default.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'value_arr',
  ),
  'set_value_php' => array(
    '0' => '',
  ),
  'set_description_php' => array(
    '0' => 'Массив с идентификаторами тегов'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(),
  'set_value_js' => array(),
  'set_description_js' => array(),
  'get_js' => array(),
  'get_description_js' => array()
);
}
?>




<?php
function lcp_tegs_default($param=''){
  $id_components=getcelldb('id','id','components','name=\'lcp_tegs_default\'');
  $category_id=$param['category_id'];
  $value_arr=$param['value_arr'];

  $html='';
  $html.=components('lcp_button_green',array(
    'value'=>'Добавить тег',
    'onclick'=>'lcp_loading_default({loading_show:\'true\'});popup_tegs();get_table_tegs();'
  ));

  if (count($value_arr)>0){
    $html_tegs_arr=getarrayvaluedb(array('id','title_short'),'id,title_short','datas','id IN ('.implode(',',$value_arr).')','','');
    $html_tegs='';
    for ($i=0; $i < count($html_tegs_arr['id']); $i++) {
      $html_tegs.='<div class="tegs_all_item" data-id="'.$html_tegs_arr['id'][$i].'">'.$html_tegs_arr['title_short'][$i].' <div class="tegs_all_delete" onclick="tegs_all_item_click(\''.$html_tegs_arr['id'][$i].'\');">x</div></div>';
    }
  }
  $html.='<div class="tegs_all">'.$html_tegs.'</div>';


  $popup_content='';

  $popup_content.=components('lcp_ajax',array(
    'lcp_type_ajax'=>'include',
    'function_name'=>'get_table_tegs',
    'begin_html'=>'
      if (param==\'\'){
          var pageid=\'1\';
        } else {
          var pageid=param[\'pageid\'];
        }
    ',
    'url'=>'/components/system/tegs/tegs-default1/ajax-tegs.php',
    'data'=>array(
      'action'=>'get_table_tegs',
      'value_arr'=>(!empty($value_arr))?implode(',',$value_arr):'',
      'category_id'=>$category_id,
      'pageid'=>'1',
      'first_load'=>'yes',
    ),
    'data_js'=>'{
      action:\'get_table_tegs\',
      value_arr:get_tegs_default_str(),
      category_id:\''.$category_id.'\',
      pageid:pageid
      }',
    'lcp_success_true'=>'lcp_loading_default({loading_show:\'false\'});$(\'.get_table_tegs\').html(data.string);',
    'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});alert(data.string);',
    'error'=>'lcp_loading_default({loading_show:\'false\'});alert(\'Ошибка при ajax запросе!\');',
    'show_error'=>'false'
  ));

  $popup_content_button1=components('lcp_button_default',array(
    'value'=>'Новый тег',
    'onclick'=>'lcp_loading_default({loading_show:\'true\'});popup_new_tegs();get_table_tegs_for_save();$(\'.popup_form_padding\').scrollTop(0);'
  ));
  $popup_content_button2=components('lcp_button_green',array(
    'value'=>'Выбрать',
    'onclick'=>'add_item_teg();popup_tegs_close();'
  ));
  $popup_content.=components('lcp_block_one_line',array(
    'tbody'=>array('',$popup_content_button1,$popup_content_button2),
    'width_columns'=>array('100%','100px','100px'),
    'text_align_col_tbody'=>array('left','right','right')
  ));


  $html.=components('lcp_popup_default',array(
    'id_popup' => 'popup_tegs',
    'title' => 'Выберите тег из списка',
    'content_html' => $popup_content,
    'popup_show' => 'popup_tegs',
    'popup_close' => 'popup_tegs_close',
    // 'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));





// Добавление нового тега
  $popup_content='';
  $popup_content.='<div class="get_table_tegs_for_save"></div>';
  $popup_content.=components('lcp_ajax',array(
    'function_name'=>'get_table_tegs_for_save',
    'begin_html'=>'',
    'url'=>'/components/system/tegs/tegs-default1/ajax-tegs.php',
    // 'data'=>array(
    //   'action'=>'get_table_tegs_for_save',
    //   // 'value_arr'=>(!empty($value_arr))?implode(',',$value_arr):''
    // ),
    'data_js'=>'{
      action:\'get_table_tegs_for_save\'
      }',
    'lcp_success_true'=>'lcp_loading_default({loading_show:\'false\'});$(\'.get_table_tegs_for_save\').html(data.string);',
    'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});alert(data.string);',
    'error'=>'lcp_loading_default({loading_show:\'false\'});alert(\'Ошибка при ajax запросе!\');',
    'show_error'=>'false'
  ));
  $html.=components('lcp_popup_default',array(
    'id_popup' => 'popup_new_tegs',
    'title' => 'Добавить новый тег',
    'content_html' => $popup_content,
    'popup_show' => 'popup_new_tegs',
    'popup_close' => 'popup_new_tegs_close',
    // 'width' => '700px',
    'title_setting' => array(
      'font-size' => '19px',
      'font-weight' => 'bold',
      'text-align' => 'left'
    )
  ));
  return $html;
}
?>
