<?php
header('Content-Type: application/json');
// ini_set('error_reporting', E_ALL & ~E_NOTICE);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
$root_dir = $_SERVER['DOCUMENT_ROOT'];
$root_folder = '../../../../';

include_once($root_folder.'konsfigli.php');
include_once($root_folder.'includes/connect_db.php');
// include_once($root_folder.'components/system/aws/index.php');

function generate_exception($string){
  exit(json_encode(array('error' => 'false','string' => $string)));
}


$_LCP_AJAX='AJAX';
include_once($root_dir.'/components/index.php');

lcp_aws();

$aws_get_param=$_GET['aws_param'];
$aws_param=$$aws_get_param; // параметры для подключения к S3
$aws_folder_download=trim($_GET['aws_folder_download']);
$aws_folder_download=!empty($aws_folder_download)?$aws_folder_download:$aws_param['folder_download'];
$aws_folder_download=!empty($aws_folder_download)?$aws_folder_download:'/'; // папка для загрузки фото


$data = array();


  /*---Функция для минимизации фото---*/
  function imageresize($infile,$outfile,$neww,$newh,$quality) {
    $info = getimagesize($infile); //получаем размеры картинки и ее тип
    $size = array($info[0], $info[1]); //закидываем размеры в массив
    //В зависимости от расширения картинки вызываем соответствующую функцию
    if ($info['mime'] == 'image/png') {
      $im = imagecreatefrompng($infile); //создаём новое изображение из файла
    } else if ($info['mime'] == 'image/jpeg') {
      $im = imagecreatefromjpeg($infile);
    } else if ($info['mime'] == 'image/gif') {
      $im = imagecreatefromgif($infile);
    } else {
      return false;
    }
    $k1=$neww/imagesx($im);
    $k2=$newh/imagesy($im);
    $k=$k1>$k2?$k2:$k1;
    $w=intval(imagesx($im)*$k);
    $h=intval(imagesy($im)*$k);
    $im1=imagecreatetruecolor($w,$h);
    imageAlphaBlending($im1, false);
    imageSaveAlpha($im1, true);
    if ($info['mime'] == 'image/png') {
      imagecopyresampled($im1,$im,0,0,0,0,$w,$h,imagesx($im),imagesy($im));
      imagePng($im1,$outfile);
    } else {
      imagecopyresampled($im1,$im,0,0,0,0,$w,$h,imagesx($im),imagesy($im));
      imagejpeg($im1,$outfile,$quality);
      imagedestroy($im);
      imagedestroy($im1);
    }
  }
  /*---Конец функции для минимизации фото---*/

  if (!empty($_FILES)){
    $files_arr=array();
    $folder_polz=$aws_folder_download;




    // Создаем экземпляр S3Client
    $s3 = new Aws\S3\S3Client($aws_param);

    for ($iui=0; $iui < count($_FILES); $iui++) {
        $temp=$_FILES[$iui]['tmp_name'];
        $img_name=$_FILES[$iui]['name'];
        $tipes_fileses = substr($img_name, -4, 4);
        $img_name=str_replace($tipes_fileses,"",$img_name);
        $img_name=lcp_url_translit(array('url'=>$img_name));
        $img_name=$img_name.".jpg";
        $path_img=$folder_polz.'/'.date('Y').'/'.date('m');
        // if (file_exists($path_img)===false)
        // mkdir($path_img,0777,true);
        // move_uploaded_file($temp,$path_img.'/'.$img_name);

        try {
         // Загружаем файл в папку бакета
        $result = $s3->putObject([
          'Bucket' => $aws_param['Bucket'],
          'Key'    => $path_img.'/'.$img_name,
          'SourceFile' => $temp,
          'ACL' => 'public-read',
          'ContentType' => 'image/jpeg',
        ]);
      } catch (Exception $e) {
          generate_exception("Ошибка при загрузке файла на S3: " . $e->getMessage());
      }
        $url_img1=$aws_param['endpoint'].'/'.$path_img.'/'.$img_name;
        $url_img1=str_replace('http://','http://'.$aws_param['Bucket'].'.',$url_img1);
        $url_img1=str_replace('https://','https://'.$aws_param['Bucket'].'.',$url_img1);
        $url_imgarr[$iui]=$url_img1;

        // Создаем миниатюру фото
        // $url_img_mini=$path_img.'/mini'.$img_name;
        // $url_img_mini1=$http.'://'.$domen_site.'/polz/'.date(Y).'/'.date(m).'/mini'.$img_name;
        // imageresize($path_img.'/'.$img_name,$url_img_mini,160,150,75);
        $url_img_miniarr[$iui]=$url_img1;
        //Конец создаем миниатюру фото
      }
  } else {
    generate_exception('Файлы для загрузки не найдены!');
  }
  $files_arr=array(
    'files'=>array(
      'img'=>$url_imgarr,
      'img_mini'=>$url_img_miniarr
    )
  );
  exit(json_encode(array('error' => 'false','files_arr' => $files_arr)));
?>
