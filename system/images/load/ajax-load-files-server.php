<?php
header('Content-Type: application/json');
// ini_set('error_reporting', E_ALL & ~E_NOTICE);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
$root_dir = $_SERVER['DOCUMENT_ROOT'];
$root_folder = '../../../../';

include_once($root_folder.'konsfigli.php');
include_once($root_folder.'includes/connect_db.php');

function generate_exception($string){
  exit(json_encode(array('error' => 'false','string' => $string)));
}


$_LCP_AJAX='AJAX';
include_once($root_dir.'/includes/infosite.php');
include_once($root_dir.'/components/index.php');
$site_lcp = info_site();
$user_lcp = info_user();

$data = array();

if (isset($_GET['loadfile'])){
  // $error = false;
  // $files = array();
  //
  // $uploaddir = './uploads/'; // . - текущая папка где находится submit.php
  //
  // // Создадим папку если её нет
  //
  // if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
  //
  // // переместим файлы из временной директории в указанную
  // foreach( $_FILES as $file ){
  //     if( move_uploaded_file( $file['tmp_name'], $uploaddir . basename($file['name']) ) ){
  //         $files[] = realpath( $uploaddir . $file['name'] );
  //     }
  //     else{
  //         $error = true;
  //     }
  // }
  //
  // $data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
  //
  // echo json_encode( $data );


  /*---Функция для минимизации фото---*/
  function imageresize($infile,$outfile,$neww,$newh,$quality) {
    $info = getimagesize($infile); //получаем размеры картинки и ее тип
    $size = array($info[0], $info[1]); //закидываем размеры в массив
    //В зависимости от расширения картинки вызываем соответствующую функцию
    if ($info['mime'] == 'image/png') {
      $im = imagecreatefrompng($infile); //создаём новое изображение из файла
    } else if ($info['mime'] == 'image/jpeg') {
      $im = imagecreatefromjpeg($infile);
    } else if ($info['mime'] == 'image/gif') {
      $im = imagecreatefromgif($infile);
    } else {
      return false;
    }
    $k1=$neww/imagesx($im);
    $k2=$newh/imagesy($im);
    $k=$k1>$k2?$k2:$k1;
    $w=intval(imagesx($im)*$k);
    $h=intval(imagesy($im)*$k);
    $im1=imagecreatetruecolor($w,$h);
    imageAlphaBlending($im1, false);
    imageSaveAlpha($im1, true);
    if ($info['mime'] == 'image/png') {
      imagecopyresampled($im1,$im,0,0,0,0,$w,$h,imagesx($im),imagesy($im));
      imagePng($im1,$outfile);
    } else {
      imagecopyresampled($im1,$im,0,0,0,0,$w,$h,imagesx($im),imagesy($im));
      imagejpeg($im1,$outfile,$quality);
      imagedestroy($im);
      imagedestroy($im1);
    }
  }
  /*---Конец функции для минимизации фото---*/

  if (!empty($_FILES)){
    $folder_polz_str=lcp_root_folder();
    $folder_polz=$folder_polz_str.'polz';
    $files_arr=array();

    for ($iui=0; $iui < count($_FILES); $iui++) {
      if (($_FILES[$iui]['error']!==0)&&(!empty($_FILES[$iui]['name']))){
        echo 'Размер некоторых фото превышает 8 Мб!';
      } else {
        $temp=$_FILES[$iui]['tmp_name'];
        $img_name=$_FILES[$iui]['name'];
        $tipes_fileses = substr($img_name, -4, 4);
        $img_name=str_replace($tipes_fileses,"",$img_name);
        $img_name=lcp_url_translit(array('url'=>$img_name));
        $img_name=$img_name.".jpg";
        $path_img=$folder_polz.'/'.date('Y').'/'.date('m');
        if (file_exists($path_img)===false)
        mkdir($path_img,0777,true);
        move_uploaded_file($temp,$path_img.'/'.$img_name);
        $url_img1='/polz/'.date(Y).'/'.date(m).'/'.$img_name;
        $url_imgarr[$iui]=$url_img1;

        // Создаем миниатюру фото
        $url_img_mini=$path_img.'/mini'.$img_name;
        $url_img_mini1=$http.'://'.$domen_site.'/polz/'.date(Y).'/'.date(m).'/mini'.$img_name;
        // imageresize($path_img.'/'.$img_name,$url_img_mini,160,150,75);
        $url_img_miniarr[$iui]=$url_img1;
        //Конец создаем миниатюру фото
      }
    }
  } else {
    generate_exception('net');
  }
  $files_arr=array(
    'files'=>array(
      'img'=>$url_imgarr,
      'img_mini'=>$url_img_miniarr
    )
  );
  exit(json_encode(array('error' => 'true','files_arr' => $files_arr)));
}

generate_exception('Функций не найдено!');
?>
