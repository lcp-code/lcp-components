<?php
// ini_set('error_reporting', E_ALL & ~E_NOTICE);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_load_image',
  'type' => 'system',
  'group' => 'Изображения',
  'inner' => array(
    'description' => 'Невизуальный JS скрипт, который отвечает за отправку файлов на сервер и выполнение последующего js кода',
    'css' => array(
      // '0' => '/components/system/images/load-photo/load-photo.css'
    ),
    'js' => array(
      // '0' => '/components/system/images/load-photo/load-photo.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'before_load_js',
    '2' => 'after_load_js',
    '3' => 'false_load_js',
    '4' => 'error_load_js',
    '5' => 'upload_folder',
    '6' => 'download_folder',
    '7' => 'is_aws',
    '8' => 'aws_folder_download',
    '9' => 'aws_param',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
    '3' => '',
    '4' => '',
    '5' => '',
    '6' => '',
    '7' => 'false',
    '8' => '/',
    '9' => 'aws_s3',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => '<div>Функция JS, которую нужно выполнить перед загрузкой файлов.</div><div>Например:</div><div>startFunc();</div><div>или</div><div>alert("start");</div>',
    '2' => '<div>Функция JS, которую нужно выполнить сразу после завершения.</div><div>Например:</div><div>makeFunc();</div><div>или</div><div>alert("name");</div>',
    '3' => '<div>Функция JS, которую нужно выполнить при неуспешном выполнении AJAX.</div><div>Например:</div><div>makeFunc();</div><div>или</div><div>alert("name");</div>',
    '4' => '<div>Функция JS, которую нужно выполнить при ошибке AJAX.</div><div>Например:</div><div>makeFunc();</div><div>или</div><div>alert("name");</div>',
    '5' => '<div>Относительный путь для загрузки файла. Указывается только каталог.</div><div>Например:</div><div>../../../../polz/'.date(Y).'/'.date(m).'/</div>',
    '6' => '<div>Относительный путь загруженного файла (с возможностью его скачать). Указывается только каталог.</div><div>Например:</div><div>/polz/'.date(Y).'/'.date(m).'/</div>',
    '7' => 'Использовать ли AWS S3 хранилище для загрузки файлов (true - включить, false - не использовать)',
    '8' => 'Папка, куда необходимо загрузить фото на S3 хранилище. Если не указано - берется из konsfigli.php. Если и там не указано - то корневая директория (/)',
    '9' => 'Наименование переменной из konsfigli.php (по умолчанию - $aws_s3), содержащей массив с параметрами для подключения к AWS. Пример:
    array(
      "version" => "latest",
      "region"  => "ru-1",
      "endpoint" => "https://s3.site.com",
      "credentials" => array(
         "key" => "key_value",
         "secret" => "secret_value",
      ),
    "Bucket" => "7f1u3431-kembri",
    "folder_download" => "kembri_folder", // папка по-умолчанию, куда необходимо загрузить фото на S3 хранилище (не обязательно). Применяется ко всему сайту
    );
'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает JS-скрипт для работы с компонентом'
  ),
  'set_name_js' => array(
    '0' => 'files',
  ),
  'set_value_js' => array(
    '0' => '',
  ),
  'set_description_js' => array(
    '0' => 'Массив с путями файлов, которые нужно отправить на сервер (Этот массив можно получить следующим образом: document.getElementById(ids).files, где ids - идентификатор поля <pre style="display:inline-block;margin: 0px;">'.htmlspecialchars('<input type="file">',ENT_QUOTES,'UTF-8',false).'</pre>)',
  ),
  'get_js' => array(
    // '0' => 'array'
  ),
  'get_description_js' => array(
    // '0' => 'Если get установлено в значение "get_load_photos", то возвращает путь с фото'
  )
);
}






if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_load_image extends lcp_components implements lcp_components_build{
    public $id='lcp_load_image';
    public $before_load_js;
    public $after_load_js;
    public $false_load_js;
    public $error_load_js;
    public $upload_folder;
    public $download_folder;
    public $is_aws;
    public $aws_folder_download;
    public $aws_param;
    private $ajax_path_php;

    function __construct($param=array()){
      $this->id=isset($param['id'])?$param['id']:$this->id;
      $this->before_load_js=$param['before_load_js'];
      $this->after_load_js=$param['after_load_js'];
      $this->false_load_js=$param['false_load_js'];
      $this->error_load_js=$param['error_load_js'];
      $this->upload_folder=$param['upload_folder'];
      $this->download_folder=$param['download_folder'];
      $this->is_aws=$param['is_aws'];
      $this->aws_folder_download=$param['aws_folder_download'];
      $this->aws_param=$param['aws_param'];
      parent::__construct($param);
    }

    function __build_component(){
      global $http,$domen_site;
      $html='';
      $path_php='components/system/images/load/index.php';
      $path_php=str_replace('index.php','',$path_php);
      
      $this->upload_folder=(!empty($this->upload_folder))?$this->upload_folder:'../../../../polz/'.date('Y').'/'.date('m').'/';
      $this->download_folder=(!empty($this->download_folder))?$this->download_folder:'/polz/'.date('Y').'/'.date('m').'/';
      $this->is_aws=(!empty($this->is_aws))?$this->is_aws:'false';
      $this->aws_param=(!empty($this->aws_param))?$this->aws_param:'aws_s3';
      $this->ajax_path_php=(!empty($this->ajax_path_php))?$this->ajax_path_php:'/ajax-load-files-server.php?loadfile=true&upload='.$this->upload_folder.'&download='.$this->download_folder;
      $this->ajax_path_php=$this->is_aws=='false'?$this->ajax_path_php:'/'.$path_php.'ajax-load-photo-server-aws.php?aws_param='.$this->aws_param.'&aws_folder_download='.$this->aws_folder_download;
      $html.=components('lcp_ajax',array(
        'function_name'=>(!empty($this->id))?$this->id:get_class($this),
        'begin_html'=>'
        var files=param[\'files\'];
        var after_function_js=param[\'after_function_js\'];
        var obj = new FormData();
        $.each(files, function(key, value){
          obj.append( key, value );
        });
        '.((!empty($this->before_load_js))?$this->before_load_js:''),
        'url'=>$this->ajax_path_php,
        'connect_to_url'=>'false',
        'data_js'=>'obj',
        'cache'=>'false',
        'processData'=>'false',
        'contentType'=>'false',
        'lcp_success_true'=>(!empty($this->false_load_js))?$this->false_load_js:'',
        'lcp_success_false'=>(!empty($this->after_load_js))?$this->after_load_js:'',
        'error'=>(!empty($this->error_load_js))?$this->error_load_js:'',
        'show_error'=>'false',
      ));
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_load_image_init($param=array()){
    $lcp_obj=new lcp_load_image($param);
    return $lcp_obj->html();
  }
}
?>
