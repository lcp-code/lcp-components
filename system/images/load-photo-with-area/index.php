<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_load_photo_area',
  'type' => 'system',
  'group' => 'Изображения',
  'inner' => array(
    'description' => 'Перемещаемая область для загрузки изображений, используемая в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/images/load-photo-with-area/load-photo-area.css'
    ),
    'js' => array(
      '0' => '/components/system/images/load-photo-with-area/load-photo-area.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'path',
    '2' => 'max_files',
    '3' => 'after_load_js',
    '4' => 'is_aws',
    '5' => 'aws_folder_download',
    '6' => 'aws_param',
    '7' => 'is_clicked',
  ),
  'set_value_php' => array(
    '0' => 'img_field_area',
    '1' => '',
    '2' => '0',
    '3' => '',
    '4' => 'false',
    '5' => '/',
    '6' => 'aws_s3',
    '7' => 'true',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Путь к загруженному фото (не обязательно)',
    '2' => 'Максимальное количество файлов',
    '3' => '<div>Функция JS, которую нужно выполнить сразу после завершения.</div><div>Например:</div><div>makeFunc();</div><div>или</div><div>alert("name");</div>',
    '4' => 'Использовать ли AWS S3 хранилище для загрузки файлов (true - включить, false - не использовать)',
    '5' => 'Папка, куда необходимо загрузить фото на S3 хранилище. Если не указано - берется из konsfigli.php. Если и там не указано - то корневая директория (/)',
    '6' => 'Наименование переменной из konsfigli.php (по умолчанию - $aws_s3), содержащей массив с параметрами для подключения к AWS. Пример:
    array(
      "version" => "latest",
      "region"  => "ru-1",
      "endpoint" => "https://s3.site.com",
      "credentials" => array(
         "key" => "key_value",
         "secret" => "secret_value",
      ),
    "Bucket" => "7f1u3431-kembri",
    "folder_download" => "kembri_folder", // папка по-умолчанию, куда необходимо загрузить фото на S3 хранилище (не обязательно). Применяется ко всему сайту
    );
',
    '7' => 'true - при нажатии на фото - выбирать новое. false - не кликать на фото',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
    '0' => 'id',
    '1' => 'get'
  ),
  'set_value_js' => array(
    '0' => 'img_field_area',
    '1' => ''
  ),
  'set_description_js' => array(
    '0' => 'Идентификатор (тег id), по которому следует искать элемент',
    '1' => '<div>Получение данных о фото (по заданному id), где:</div><div>get_load_photos - получить путь загруженного фото</div>'
  ),
  'get_js' => array(
    '0' => 'array'
  ),
  'get_description_js' => array(
    '0' => 'Если get установлено в значение "get_load_photos", то возвращает путь с фото'
  )
);
}
$css_install_arr[]='/components/system/images/load-photo-with-area/load-photo-area.css';
$js_install_arr[]='/components/system/images/load-photo-with-area/load-photo-area.js';
?>




<?php
if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_load_photo_area extends lcp_components implements lcp_components_build{
    public $id='img_field_area';
    public $path;
    public $max_files='0';
    public $after_load_js;
    public $is_aws;
    public $aws_folder_download;
    public $aws_param;
    public $is_clicked='true';

    function __construct($param=array()){
      $this->id=isset($param['id'])?$param['id']:$this->id;
      $this->path=isset($param['path'])?$param['path']:$this->path;
      $this->max_files=isset($param['max_files'])?$param['max_files']:$this->max_files;
      $this->after_load_js=isset($param['after_load_js'])?$param['after_load_js']:$this->after_load_js;
      $this->is_aws=$param['is_aws'];
      $this->aws_folder_download=$param['aws_folder_download'];
      $this->aws_param=$param['aws_param'];
      $this->is_clicked=isset($param['is_clicked'])?$param['is_clicked']:$this->is_clicked;
      parent::__construct($param);
    }

    function __build_component(){
      $html='';
      $this->class='lcp_load_photo_area '.$this->class;
      $style_str=(!empty($this->path))?'style="background:url(\''.$this->path.'\');"':'';
      if ($this->is_clicked=='true'){
        $this->onclick.='$(\'#files_'.$this->id.'\').click();';
      }
      $attr_str=$this->__get_attributes_html();
      $html.='<input type="file" id="files_'.$this->id.'" name="files[]" accept="image/jpeg,image/png" multiple onchange="load_img_area({id:\''.$this->id.'\',max_files:\''.$this->max_files.'\',});" value="" autocomplete="off" class="img_add_photo_area">'; // ,image/gif отключен, так как не поддерживает прозрачность
      $html.='<div '.$attr_str.'><div class="lcp_load_photo_area_img" id="lcp_load_photo_area_img_'.$this->id.'" '.$style_str.'></div></div>';
      $html.=components('lcp_load_image',array(
        'after_load_js'=>'
        document.getElementById(\'lcp_load_photo_area_img_'.$this->id.'\').style.background=\'url(\\\'\'+data.files_arr.files.img[0]+\'\\\')\';
        '.$this->after_load_js,
        'is_aws'=>$this->is_aws,
        'aws_folder_download'=>$this->aws_folder_download,
        'aws_param'=>$this->aws_param,
        'false_load_js'=>'lcp_message(\'Не удалось загрузить фото!\');',
        'error_load_js'=>'lcp_message(\'Ошибка AJAX!\');',
      ));
      return $html;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_load_photo_area_init($param=array()){
    $lcp_obj=new lcp_load_photo_area($param);
    return $lcp_obj->html();
  }
}
?>
