function load_img_area(param){
  var ids=param['id'];
  var max_files=param['max_files'];
  if (document.getElementById('files_'+ids).files.length<=0){
    return;
  }
  if (max_files>0){
    if (document.getElementById('files_'+ids).files.length>max_files){
      alert('Можно загрузить не более '+max_files+' файлов!');
      return;
    }
    if ($('#'+ids).children('.img_field_items').length>=max_files){
      alert('Можно загрузить не более '+max_files+' файлов!');
      return;
    }
  }
  lcp_message('Идет загрузка фото... Пожалуйста, дождитесь окончания процесса...','true');
  var files=document.getElementById('files_'+ids).files;
  lcp_load_image({
    files:files,
  });
}
