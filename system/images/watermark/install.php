<?php
// для устаноки требуется установить Imagick и перезагрузить сервер!
//
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_watermark',
  'type' => 'system',
  'group' => 'Изображения',
  'inner' => array(
    'description' => 'Создание и отображение фото с водяным знаком (оригинал не меняет). Н-р, https://'.$domen_site.'/components/system/images/watermark/index.php?photo_orig=polz/12/01/example.jpg&photo_watermark=polz/12/01/watermark.jpg',
    'css' => array(
      // '0' => '/components/system/images/load-photo/load-photo.css'
    ),
    'js' => array(
      // '0' => '/components/system/images/load-photo/load-photo.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'photo_orig',
    '1' => 'photo_watermark',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
  ),
  'set_description_php' => array(
    '0' => 'Путь к фото оригиналу, на которое нужно добавить водяной знак. Указать полный или относительный путь (polz/12/01/example.jpg)',
    '1' => 'Путь к водяному знаку. Указать полный или относительный путь (polz/12/01/watermark.jpg)',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция сразу отображает изображение (ничего не выводит)'
  ),
  'set_name_js' => array(
  ),
  'set_value_js' => array(
  ),
  'set_description_js' => array(
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}






if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_watermark extends lcp_components implements lcp_components_build{
    public $photo_orig;
    public $photo_watermark;

    function __construct($param=array()){
      $this->photo_orig=$param['photo_orig'];
      $this->photo_watermark=$param['photo_watermark'];
      parent::__construct($param);
    }

    function __init_component(){
      $this->photo_orig=getcuts($this->photo_orig);
      if ($this->photo_orig[strlen($this->photo_orig)-1]=='/'){
        $this->photo_orig=substr($this->photo_orig,0,strlen($this->photo_orig)-1);
      }
      $this->photo_watermark=getcuts($this->photo_watermark);
      if ($this->photo_watermark[strlen($this->photo_watermark)-1]=='/'){
        $this->photo_watermark=substr($this->photo_watermark,0,strlen($this->photo_watermark)-1);
      }
    }

    function __build_component(){
      global $http,$domen_site;
      // if (end(explode(".", $this->photo_orig))=='png'){
      //   header("Content-Type: image/png");
      //   echo file_get_contents($this->photo_orig);
      //   exit();
      // }

      $image = new Imagick();
      $image->readimage($this->photo_orig);

      $watermark = new Imagick();
      $watermark->readimage($this->photo_watermark);
      $watermark->evaluateImage(Imagick::EVALUATE_DIVIDE, 2, Imagick::CHANNEL_ALPHA);

      $iWidth = $image->getImageWidth();
      $iHeight = $image->getImageHeight();
      $wWidth = $watermark->getImageWidth();
      $wHeight = $watermark->getImageHeight();



      if ($iWidth < $wWidth) {
        $watermark->scaleImage($iWidth, $iWidth*$iHeight/$wWidth); // get new size
        $wWidth = $watermark->getImageWidth();
        $wHeight = $watermark->getImageHeight();
      } else
      if ($iHeight < $wHeight) {
        $watermark->scaleImage($iWidth*$iHeight/$iHeight, $iHeight); // get new size
        $wWidth = $watermark->getImageWidth();
        $wHeight = $watermark->getImageHeight();
      }

      $left = $iWidth - $wWidth - 10;
      $top = $iHeight - $wHeight - 10;
      // if ($iHeight < $wHeight || $iWidth < $wWidth) {
      //   // resize the watermark
      //   $watermark->scaleImage($iWidth, $iHeight); // get new size
      //   $wWidth = $watermark->getImageWidth();
      //   $wHeight = $watermark->getImageHeight();
      // }



      // $image->compositeImage($watermark, imagick::COMPOSITE_OVER, $left, $top);

      /* Выводим на экран */
      header("Content-Type: image/{$image->getImageFormat()}");
      echo $image;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_watermark_init($param=array()){
    $lcp_obj=new lcp_watermark($param);
    return $lcp_obj->html();
  }
}
?>
