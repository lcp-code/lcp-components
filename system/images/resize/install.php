<?php
// для устаноки требуется установить Imagick и перезагрузить сервер!
//
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_resize',
  'type' => 'system',
  'group' => 'Изображения',
  'inner' => array(
    'description' => 'Отображение фото с другими размерами. Н-р, https://'.$domen_site.'/components/system/images/resize/index.php?photo_orig=polz/12/01/example.jpg&width=100&height=100',
    'css' => array(
      // '0' => '/components/system/images/load-photo/load-photo.css'
    ),
    'js' => array(
      // '0' => '/components/system/images/load-photo/load-photo.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'photo_orig',
    '1' => 'width',
    '2' => 'height',
  ),
  'set_value_php' => array(
    '0' => '',
    '1' => '',
    '2' => '',
  ),
  'set_description_php' => array(
    '0' => 'Путь к фото оригиналу, на которое нужно добавить водяной знак. Указать полный или относительный путь (polz/12/01/example.jpg)',
    '1' => 'Ширина полученной фото',
    '2' => 'Высота полученной фото',
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция сразу отображает изображение (ничего не выводит)'
  ),
  'set_name_js' => array(
  ),
  'set_value_js' => array(
  ),
  'set_description_js' => array(
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}






if ($install!='true'){ // для ООП данное условие использовать обязательно!
  class lcp_resize extends lcp_components implements lcp_components_build{
    public $photo_orig;
    public $width;
    public $height;

    function __construct($param=array()){
      $this->photo_orig=$param['photo_orig'];
      $this->width=$param['width'];
      $this->height=$param['height'];
      parent::__construct($param);
    }

    function __init_component(){
      $this->photo_orig=getcuts($this->photo_orig);
      if ($this->photo_orig[strlen($this->photo_orig)-1]=='/'){
        $this->photo_orig=substr($this->photo_orig,0,strlen($this->photo_orig)-1);
      }
    }

    function __build_component(){
      global $http,$domen_site;
      $image = new Imagick();
      $image->readimage($this->photo_orig);
      $image->setbackgroundcolor('rgb(255, 255, 255)');
      $image->thumbnailImage($this->width,$this->height,true,true);

      /* Выводим на экран */
      header("Content-Type: image/{$image->getImageFormat()}");
      echo $image;
    }
  }









  // добавлять только при использовании ООП.
  // Называться должна строго по шаблону: имя функции + _init
  // Принимать может только один параметр: массив
  function lcp_resize_init($param=array()){
    $lcp_obj=new lcp_resize($param);
    return $lcp_obj->html();
  }
}
?>
