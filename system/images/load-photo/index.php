<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_load_photo',
  'type' => 'system',
  'group' => 'Изображения',
  'inner' => array(
    'description' => 'Загрузка изображений, используемая в панели администратора по-умолчанию',
    'css' => array(
      '0' => '/components/system/images/load-photo/load-photo.css'
    ),
    'js' => array(
      '0' => '/components/system/images/load-photo/load-photo.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'id',
    '1' => 'max_files',
    '2' => 'after_load_js',
    '3' => 'is_aws',
    '4' => 'aws_folder_download',
    '5' => 'aws_param',
  ),
  'set_value_php' => array(
    '0' => 'img_field',
    '1' => '0',
    '2' => '',
    '3' => 'false',
    '4' => '/',
    '5' => 'aws_s3',
  ),
  'set_description_php' => array(
    '0' => 'Идентификатор (тег id)',
    '1' => 'Максимальное количество файлов',
    '2' => '<div>Функция JS, которую нужно выполнить сразу после завершения.</div><div>Например:</div><div>makeFunc();</div><div>или</div><div>alert("name");</div>',
    '3' => 'Использовать ли AWS S3 хранилище для загрузки файлов (true - включить, false - не использовать)',
    '4' => 'Папка, куда необходимо загрузить фото на S3 хранилище. Если не указано - берется из konsfigli.php. Если и там не указано - то корневая директория (/)',
    '5' => 'Наименование переменной из konsfigli.php (по умолчанию - $aws_s3), содержащей массив с параметрами для подключения к AWS. Пример:
    array(
      "version" => "latest",
      "region"  => "ru-1",
      "endpoint" => "https://s3.site.com",
      "credentials" => array(
         "key" => "key_value",
         "secret" => "secret_value",
      ),
    "Bucket" => "7f1u3431-kembri",
    "folder_download" => "kembri_folder", // папка по-умолчанию, куда необходимо загрузить фото на S3 хранилище (не обязательно). Применяется ко всему сайту
    );
'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
    '0' => 'id',
    '1' => 'get'
  ),
  'set_value_js' => array(
    '0' => 'img_field',
    '1' => ''
  ),
  'set_description_js' => array(
    '0' => 'Идентификатор (тег id), по которому следует искать элемент',
    '1' => '<div>Получение данных о фото (по заданному id), где:</div><div>get_load_photos - получить готовые фото и аттрибуты</div><div>get_for_save_photos - получить действия для удаления, обновления и добавления прошлых и новых фото со всеми аттрибутами (рекомендуется для сохранения в БД)</div>'
  ),
  'get_js' => array(
    '0' => 'array'
  ),
  'get_description_js' => array(
    '0' => 'Если get установлено в значение "get_load_photos", то возвращает массив с аттрибутами фото'
  )
);
}
$css_install_arr[]='/components/system/images/load-photo/load-photo.css';
$js_install_arr[]='/components/system/images/load-photo/load-photo.js';
?>




<?php
function lcp_load_photo($param=''){
  $puth_php='components/system/images/load-photo/index.php';
  $puth_php=str_replace('index.php','',$puth_php);
  
  $ids=$param['id'];
  $max_files=$param['max_files'];
  $files_arr=$param['files_arr'];
  $after_load_js=$param['after_load_js'];
  $is_aws=$param['is_aws'];
  $aws_folder_download=$param['aws_folder_download'];
  $aws_param=$param['aws_param'];
  $default_ids='img_field';
  $default_max_files='0';
  $default_after_load_js='';
  $default_is_aws='false';
  $default_aws_param='aws_s3';
  $ids=(!empty($ids))?$ids:$default_ids;
  $max_files=(!empty($max_files))?$max_files:$default_max_files;
  $after_load_js=(!empty($after_load_js))?$after_load_js:$default_after_load_js;
  $is_aws=(!empty($is_aws))?$is_aws:$default_is_aws;
  $aws_param=(!empty($aws_param))?$aws_param:$default_aws_param;
  $url_server_load=$is_aws=='false'?'/'.$puth_php.'ajax-load-photo-server.php?loadimg=true\'':'/'.$puth_php.'ajax-load-photo-server-aws.php?aws_param='.$aws_param.'&aws_folder_download='.$aws_folder_download;

  if ((!empty($files_arr['src']))&&(empty($files_arr['img']))){
    $files_arr['img']=$files_arr['src'];
  }
  if ((!empty($files_arr['src_mini']))&&(empty($files_arr['img_mini']))){
    $files_arr['img']=$files_arr['src'];
  }


  $html='';
  $html.='<input type="file" id="files_'.$ids.'" name="files[]" accept="image/jpeg,image/png" multiple onChange="load_img({max_files:\''.$max_files.'\',id:\''.$ids.'\',url_load:\''.$url_server_load.'\',});" class="img_add_photo" value="" autocomplete="off">'; // ,image/gif отключен, так как не поддерживает прозрачность
  $html.=components('lcp_button_green',array(
    'value'=>'Выбрать фото',
    'onclick'=>'$(\'#files_'.$ids.'\').click();'
  ));

  // 'Выбрать фото';
  $html.='<div id="'.$ids.'">';
  $html.=components('lcp_ajax',array(
    'lcp_type_ajax'=>'include',
    'function_name'=>'view_img',
    'begin_html'=>'
    var files=param[\'files\'];
    var ids=param[\'id\'];
    lcp_loading_default({loading_show:\'true\'});
    ',
    'url'=>'/'.$puth_php.'ajax-load-photo.php',
    'data'=>array(
      'action'=>'view_img',
      'files'=>$files_arr,
      'ids'=>$ids
    ),
    'data_js'=>'{
      action:\'view_img\',
      files:files,
      ids:ids
      }',
      'lcp_success_true'=>'lcp_loading_default({loading_show:\'false\'});$(\'#\'+ids).append(data.string);sortable_img(ids);'.$after_load_js,
      'lcp_success_false'=>'lcp_loading_default({loading_show:\'false\'});alert(data.string);',
      'error'=>'lcp_loading_default({loading_show:\'false\'});alert(\'Ошибка при ajax запросе!\');',
      'show_error'=>'false'
    ));
    $html.='<script>sortable_img(\''.$ids.'\');</script></div>';

    $popup_content='';
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Ссылка (высокое качество):',
      'html'=>components('lcp_text_default',array(
        'id'=>'src_img_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите ссылку'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Ссылка (низкое качество):',
      'html'=>components('lcp_text_default',array(
        'id'=>'src_img_mini_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите ссылку'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Заголовок:',
      'html'=>components('lcp_text_default',array(
        'id'=>'title_img_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите заголовок'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Альтернативный текст:',
      'html'=>components('lcp_text_default',array(
        'id'=>'alt_img_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите альтернативный текст'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>'Длинное описание ссылки:',
      'html'=>components('lcp_text_default',array(
        'id'=>'longdesc_img_'.$ids,
        'value'=>'',
        'placeholder'=>'Укажите длинное описание ссылки'
      ))
    ));
    $popup_content.=components('lcp_html_block_default3',array(
      'title'=>' ',
      'html'=>components('lcp_block_one_line',array(
        'tbody'=>array(components('lcp_button_default',array(
          'id'=>'img_butt_save_'.$ids,
          'value'=>'Применить свойства к фото',
          'onclick'=>'img_edit_'.$ids.'_close();img_edit_save(\''.$ids.'\')'
        ))),
        'width_columns'=>array('100%'),
        'text_align_col_tbody'=>array('center')
      ))
    ));
    $html.=components('lcp_popup_default',array(
      'id_popup'=>'img_edit_'.$ids,
      'title'=>'Свойства изображения',
      'content_html'=>$popup_content,
      'popup_show'=>'img_edit_'.$ids,
      'popup_close'=>'img_edit_'.$ids.'_close',
      'width'=>'400px',
      'height'=>'455px'
    ));

  $html.='<script type="text/javascript">';
  $html.='if (typeof first_id_arr===\'undefined\'){';
  $html.='var first_id_arr={};';
  $html.='}';
  $html.='first_id_arr[\''.$ids.'\']={};';
  if (!empty($files_arr['id'])){
    for ($i=0; $i < count($files_arr['id']); $i++) {
      $html.='first_id_arr[\''.$ids.'\'][\''.$i.'\']=\''.$files_arr['id'][$i].'\';';
    }
    $html.='first_id_arr[\''.$ids.'\'][\'length\']=\''.count($files_arr['id']).'\'';
  } else {
    $html.='first_id_arr[\''.$ids.'\'][\'length\']=\'0\'';
  }
  $html.='</script>';
  return $html;
}
?>
