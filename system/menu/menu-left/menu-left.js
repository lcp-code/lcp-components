function menu_li_more_click(obj){
  if ($(obj).parent('li').children('ul').css('display')=='none'){
    $(obj).parent('li').children('ul').slideDown(300);
    // $(obj).parent('li').children('ul').css('display','block');
    $(obj).parent('li').children('.right_img').css('transform','rotate(90deg)');
  } else {
    $(obj).parent('li').children('ul').slideUp(300);
    // $(obj).parent('li').children('ul').css('display','none');
    $(obj).parent('li').children('.right_img').css('transform','rotate(0deg)');
  }
}
