<?php
// у компонента название функции должно совпадать с названием кнопки
if ($install=='true'){
$component_property=array(
  'name' => 'lcp_menu_left',
  'type' => 'system',
  'group' => 'Меню',
  'inner' => array(
    'description' => 'Вертикальное меню, используемое в панели администратора по-умолчанию. Дочерние элементы выпадают вниз с анимацией. Меню в массиве должно выглядеть следующим образом: $menu_arr[$i1]=array("id"=>$id1,"cat"=>$cat1,"title"=>$title1). Дочернее меню: $menu_arr[$i1]["submenu"][$i2]=array("id"=>$id2,"cat"=>$cat2,"title"=>$title2)',
    'css' => array(
      '0' => '/components/system/menu/menu-left/menu-left.css'
    ),
    'js' => array(
      '0' => '/components/system/menu/menu-left/menu-left.js'
    )
  ),
  'set_name_php' => array(
    '0' => 'menu_arr'
    // '0' => 'id',
    // '1' => 'name',
    // '2' => 'class',
    // '3' => 'active_class',
    // '4' => 'id_child_arr',
    // '5' => 'title_child_arr',
    // '6' => 'name_child_arr',
    // '7' => 'class_child_arr',
    // '8' => 'onclick_child_arr',
    // '9' => 'onmousedown_child_arr',
    // '10' => 'onmouseup_child_arr'
  ),
  'set_value_php' => array(
    '0' => ''
    // '0' => '',
    // '1' => '',
    // '2' => '',
    // '3' => '',
    // '4' => '',
    // '5' => '',
    // '6' => '',
    // '7' => '',
    // '8' => '',
    // '9' => '',
    // '10' => ''
  ),
  'set_description_php' => array(
    '0' => 'Массив меню'
    // '0' => 'Идентификатор (тег id)',
    // '1' => 'Имя меню (тег name)',
    // '2' => 'Класс для изменения стилей (тег class)',
    // '3' => 'Активный класс (тег class) для обозначения выбранного (активного) пункта меню',
    // '4' => 'Массив идентификаторов пунктов меню (тег id)',
    // '5' => 'Массив отображаемого текста для пунктов меню',
    // '6' => 'Массив имен для пунктов меню (тег name)',
    // '7' => 'Массив классов для изменения стилей, указанных для пунктов меню (тег class)',
    // '8' => 'Массив событий, возникающих при клике левой кнопкой мыши на пунктах меню (тег onclick)',
    // '9' => 'Массив событий, возникающих при нажатии левой кнопкой мыши (тег onmousedown)',
    // '10' => 'Массив событий, возникающих при отпускании левой кнопкой мыши (тег onmouseup)'
  ),
  'get_php' => array(
    '0' =>'text'
  ),
  'get_description_php' => array(
    '0' =>'Функция возвращает текст'
  ),
  'set_name_js' => array(
  ),
  'set_value_js' => array(
  ),
  'set_description_js' => array(
  ),
  'get_js' => array(
  ),
  'get_description_js' => array(
  )
);
}
?>




<?php
function lcp_menu_left($param=""){
  //   $id_components=getcelldb('id','id','components','name=\'lcp_menu_left\'');
  //   $ids=$param['id'];
  //   $name=$param['name'];
  //   $class=$param['class'];
  //   $active_class=$param['active_class'];
  //   $id_child_arr=$param['id_child_arr'];
  //   $title_child_arr=$param['title_child_arr'];
  //   $name_child_arr=$param['name_child_arr'];
  //   $class_child_arr=$param['class_child_arr'];
  //   $onclick_child_arr=$param['onclick_child_arr'];
  //   $onmousedown_child_arr=$param['onmousedown_child_arr'];
  //   $onmouseup_child_arr=$param['onmouseup_child_arr'];
  //   $default_$ids=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'id\'');
  //   $default_name=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'name\'');
  //   $default_class=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'class\'');
  //   $default_active_class=getcelldb('components_param_value','components_param_value','components_param','id_components=\''.$id_components.'\'  AND components_param_name=\'active_class\'');
  $menu_left=$param['menu_arr'];

  $html='';
  $html.='<div class="menu_left">';
  $html.='<div class="logo_company"><div class="logo_company_img" style="background-image: url('.$site_lcp['site_logo'].');"></div></div>';
  $html.='<div class="menu_left_div">';
  $html.='<ul class="menu_left_ul">';
  for ($i=0; $i < count($menu_left); $i++) {
    if (!empty($menu_left[$i]['submenu'])){
      $menu_left_li_more='1';
    } else {
      $menu_left_li_more='0';
    }
    $class=$menu_left[$i]['class'];
    $html.='<li class="'.(($menu_left_li_more=='1')?'relative':'').' '.$class.'">';
    $html.='<a '.(($menu_left[$i]['cat']!='[none]')?'href="'.$menu_left[$i]['cat'].'"':'').' '.(($menu_left_li_more=='1')?'class="more_li" onclick="menu_li_more_click(this);"':'').'>'.$menu_left[$i]['title'].'</a>';
    $html.=($menu_left_li_more=='1')?'<div class="right_img"></div>':'';

    if ($menu_left_li_more=='1'){
      $html.='<ul>';
      for ($i2=0; $i2 < count($menu_left[$i]['submenu']); $i2++) {
        $html.='<li>';
        $html.='<a href="'.$menu_left[$i]['submenu'][$i2]['cat'].'">'.$menu_left[$i]['submenu'][$i2]['title'].'</a>';
        $html.='</li>';
      }
      $html.='</ul>';
    }

    $html.='</li>';
  }


  $html.='</ul>';
  $html.='</div>';
  $html.='</div>';
  return $html;
}
?>
